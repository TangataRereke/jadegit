#include <catch2/catch_test_macros.hpp>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/RecursiveFileIterator.h>
#include "SourceFileSystem.h"

using namespace std;
using namespace JadeGit;

namespace Catch {
	template<>
	struct is_range<FileIterator> {
		static const bool value = false;
	};

	template<>
	struct is_range<RecursiveFileIterator> {
		static const bool value = false;
	};
}

TEST_CASE("MemoryFileSystem.BasicFileCreation", "[vfs]") 
{
	MemoryFileSystem fs;

	// Open single file
	File file = fs.open("HelloWorld.txt");

	// Check it doesn't already exist
	CHECK(!file.exists());

	// Write to file
	file.write("Hello\nWorld"sv);

	// Check it now exists and can be read as expected
	CHECK(file.exists());
	CHECK("Hello\nWorld" == file.read());

	// Check it can be re-opened and still read as expected
	file = fs.open("HelloWorld.txt");
	CHECK(file.exists());
	CHECK("Hello\nWorld" == file.read());
}

TEST_CASE("MemoryFileSystem.BasicFolderCreation", "[vfs]") 
{
	MemoryFileSystem fs;

	// Open single file
	File file = fs.open("Folder/HelloWorld.txt");

	// Check it doesn't already exist
	CHECK(!file.exists());

	// Write to file
	file.write("Hello World (again)"sv);

	// Check it now exists and can be read as expected
	CHECK(file.exists());
	CHECK("Hello World (again)" == file.read());

	// Check file can be re-opened via root and still read as expected
	file = fs.open("Folder/HelloWorld.txt");
	CHECK(file.exists());
	CHECK("Hello World (again)" == file.read());

	// Check folder can now be opened
	File folder = fs.open("Folder");
	CHECK(folder.exists());
	CHECK(folder.isDirectory());

	// Check file can be opened via folder
	file = folder.open("HelloWorld.txt");

	// Check it exists and can be read as expected
	CHECK(file.exists());
	CHECK("Hello World (again)" == file.read());
}

TEST_CASE("MemoryFileSystem.BasicIteration", "[vfs]") 
{
	MemoryFileSystem fs;

	// Create single file within folder
	File file = fs.open("Folder/HelloWorld.txt");
	file.write("Hello World (again)"sv);

	// Open root folder
	File folder = fs.open("");

	// Check it exists
	CHECK(folder.exists());

	// Create iterator
	FileIterator iter = folder.begin();

	// Iterator should not be at the end
	CHECK(iter != folder.end());

	// Iterator should point at first entry (the folder)
	CHECK("Folder" == iter->path());

	// Move iterator (which should make it go to end)
	CHECK(++iter == folder.end());
}

TEST_CASE("MemoryFileSystem.Clone", "[vfs]") 
{
	// Setup memory file system, cloning from embedded file system
	MemoryFileSystem fs(SourceFileSystem("resources/SingleFileWithinFolder"));
	
	// Curently, we'll repeat recursive iterator test from above for now
	// May possibly implement a diff class that could be used instead with more complicated example

	// Create iterator
	RecursiveFileIterator iter(fs.open(""));

	// Iterator should not be at the end
	CHECK(iter != end(iter));

	// Iterator should point at first entry (the folder)
	CHECK("Folder" == iter->path());

	// Move iterator (which should move it to file within)
	CHECK("Folder/HelloWorld.txt" == (++iter)->path());

	// Move iterator (which should make it go to end)
	CHECK(++iter == end(iter));
}