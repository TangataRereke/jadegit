#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Attribute.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/Development.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/BaseControlMeta.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>

using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("GUIClass.BasicWrite", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Dereference root schema
	const RootSchema& rootSchema = assembly.GetRootSchema();

	// Load schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", true);
	REQUIRE(schema);

	// Create control class
	GUIClass guiClass(schema, nullptr, "TestControl", static_cast<Class*>(*rootSchema.baseControl));
	guiClass.Created("{72877054-D50E-4E7E-9C87-A35DD69C4A5E}");

	// Create property
	PrimAttribute option(&guiClass, nullptr, "testOption", rootSchema.integer);
	option.Created("{DA1C3BF6-4EC2-465E-A1C0-DC863FB3DF9B}");

	// Setup dev options
	DevControlTypes devTypes(&guiClass, nullptr, "TestControl");

	DevControlProperties devProps(&devTypes, nullptr, "testOption");
	devProps.cntrlType = DevControlProperties::List;
	devProps.optionsList.Add("1 - Hello");
	devProps.optionsList.Add("2 - World");

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("GUIClass.BasicLoad", "[data]") 
{
	SourceFileSystem fs("resources/GUIClass/Basic");
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load control class
	GUIClass* guiClass = Entity::resolve<GUIClass>(schema, "TestControl", false);
	CHECK(guiClass);

	// Check dev options loaded as expected
	DevControlClass* devControlClass = guiClass->controlType;
	CHECK(devControlClass);
	
	DevControlTypes* devControlTypes = dynamic_cast<DevControlTypes*>(devControlClass);
	CHECK(devControlTypes);

	CHECK(1 == devControlTypes->controlProps.size());

	DevControlProperties* devControlProp = devControlTypes->controlProps.back();

	// Check list options
	CHECK("1 - Hello" == devControlProp->optionsList.at(0));
	CHECK("2 - World" == devControlProp->optionsList.at(1));
}