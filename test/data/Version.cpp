#include <catch2/catch_test_macros.hpp>
#include <jadegit/Version.h>

using namespace JadeGit;

TEST_CASE("Version.Creation", "[data]") 
{
	Version version("1.2.3.4");

	CHECK(1 == version.major);
	CHECK(2 == version.minor);
	CHECK(3 == version.revision);
	CHECK(4 == version.build);
}

TEST_CASE("Version.Equal", "[data]") 
{
	Version a("1.2.3.4");
	Version b("1.2.3.4");

	CHECK(b == a);
}

TEST_CASE("Version.LessThan", "[data]") 
{
	Version a(1, 2, 3, 3);
	Version b("1.2.4.2");

	CHECK(b > a);
}

TEST_CASE("Version.GreaterThan", "[data]") 
{
	Version a(2, 0, 1, 2);
	Version b("1.2.4.2");

	CHECK(b < a);
}

TEST_CASE("Version.Empty", "[data]") 
{
	Version a;
	Version b("1.2.4.2");

	CHECK(a.empty());
	CHECK(!b.empty());
}

TEST_CASE("Version.ToString", "[data]")
{
	Version version(20, 1, 3);

	CHECK("20.1.03" == static_cast<std::string>(version));
}

TEST_CASE("Version.ToStringBuild", "[data]")
{
	Version version(1, 4, 3, 2);

	CHECK("1.4.3.2" == static_cast<std::string>(version));
}

TEST_CASE("Version.ToStringEmpty", "[data]")
{
	Version version;

	CHECK("99.0.00" == static_cast<std::string>(version));
}