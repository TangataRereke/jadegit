target_sources(jadegit_test PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/MockBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/Scenarios.cpp
)

catch_discover_tests(jadegit_test
					 TEST_SPEC "[build]"
					 TEST_PREFIX "jadegit.build."
					 WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})