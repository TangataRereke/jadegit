#pragma once
#include <build/Builder.h>
#include <jadegit/vfs/MemoryFileSystem.h>

namespace JadeGit::Build
{
	class MockBuilder : public Builder
	{
	public:	
		MemoryFileSystem fs;

	private:
		int counter = 0;

		std::unique_ptr<std::ostream> AddFile(const std::string& schema, const char* extension, bool latestVersion);
		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) final;
		void addScript(const Script& script) final;
	};
}