#include "MockBuilder.h"
#include <jadegit/vfs/File.h>
#include <format>

using namespace std;

namespace JadeGit::Build
{
	unique_ptr<ostream> MockBuilder::AddFile(const string& schema, const char* extension, bool latestVersion)
	{
		// Derive filename with leading index
		stringstream filename;
		filename << setfill('0') << setw(2) << ++counter << schema << (latestVersion ? "." : ".current.") << extension;

		// Open file & setup output stream
		return fs.open(filename.str()).createOutputStream();
	}

	unique_ptr<ostream> MockBuilder::AddCommandFile(bool latestVersion)
	{
		return AddFile("Commands", "jcf", latestVersion);
	}

	unique_ptr<ostream> MockBuilder::AddSchemaFile(const string& schema, bool latestVersion)
	{
		return AddFile(schema, "scm", latestVersion);
	}

	unique_ptr<ostream> MockBuilder::AddSchemaDataFile(const string& schema, bool latestVersion)
	{
		return AddFile(schema, "ddx", latestVersion);
	}

	ostream& operator<<(ostream& os, const Script& script)
	{
		os << format("schema = {}\n", script.schema);
		os << format("app = {}\n", script.app);

		if (!script.executeSchema.empty())
			os << format("executeSchema = {}\n", script.executeSchema);

		os << format("executeClass = {}\n", script.executeClass);
		os << format("executeMethod = {}\n", script.executeMethod);

		if (!script.executeParam.empty())
			os << format("executeParam = {}\n", script.executeParam);

		if (script.executeTransient)
			os << format("executeTransient = {}\n", script.executeTransient);

		if (script.executeTypeMethod)
			os << format("executeTypeMethod = {}\n", script.executeTypeMethod);

		return os;
	}

	void MockBuilder::addScript(const Script& script)
	{
		// Derive filename with leading index
		stringstream filename;
		filename << setfill('0') << setw(2) << ++counter << "Script";

		// Open file & setup output stream
		auto file = fs.open(filename.str()).createOutputStream();
		*file << script << std::flush;
	}
}