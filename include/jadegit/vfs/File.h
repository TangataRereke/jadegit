#pragma once
#include <filesystem>
#include "AnyFile.h"

namespace JadeGit
{
	class FileSystem;
	class FileIterator;

	class File
	{
	public:
		File();
		File(std::unique_ptr<AnyFile>&& file);
		File(const File& handle);
		File(File&& handle);
		virtual ~File();

		File& operator=(const File& handle);
		File& operator=(File&& handle);

		// Returns associated file system
		const FileSystem* vfs() const;

		// Returns file path relative to the filesystem root directory
		std::filesystem::path path() const;

		std::filesystem::path filename() const;
		
		bool exists() const;
		bool isDirectory() const;

		void signature(std::string& author, std::time_t& modified, size_t lineFrom = 1, size_t lineTo = -1) const;

		FileIterator begin() const;
		FileIterator end() const;

		File open(const std::filesystem::path& relative_path) const;

		void createDirectory();
		void copy(const File& dest) const;
		void move(File& dest);
		void rename(const std::filesystem::path& filename);
		void remove();

		std::string read() const;
		void write(const std::string& content);
		void write(const std::string_view& content);

		std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode = std::ios_base::in) const;
		std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode = std::ios_base::out);

	protected:
		friend AnyFile;
		std::unique_ptr<AnyFile> file;
	};

	std::ostream& operator<<(std::ostream& os, const File& file);
}