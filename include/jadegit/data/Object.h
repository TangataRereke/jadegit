#pragma once
#include "Component.h"
#include "Value.h"
#include <functional>
#include <future>
#include <map>
#include <queue>

// TODO: Remove direct references to tinyxml2
// Need to abstract the reading/writing of a file using FileNode/Element, which'll then allow for an alternative format to XML
namespace tinyxml2
{
	class XMLNode;
	class XMLDocument;
	class XMLElement;
}

namespace JadeGit::Data
{
	class Class;
	class FileElement;
	class Property;
	class RootSchema;
	class State;
	class Type;

	class Object : public Component
	{
	public:
		Object(Object* parent, const Class* dataClass);
		~Object();

		std::pmr::vector<Object*> children;
		const Class* dataClass = nullptr;

		static void dele(Object* object);
		static std::function<Object*()> resolver(const char* klass, const Component* origin, const tinyxml2::XMLElement* source);

		Object* getParentObject() const;
		const RootSchema& GetRootSchema() const;

		Any GetValue(const Property* property, bool instantiate = false) const;
		Any GetValue(const std::string& name, bool instantiate = false) const;
		void resetValue(const Property* property);
		void SetValue(const Property* property, const Any& value);
		void SetValue(const Property* property, const Binary& value);
		void SetValue(const Property* property, bool value);
		void SetValue(const Property* property, Byte value);
		void SetValue(const Property* property, char value);
		void SetValue(const Property* property, double value);
		void SetValue(const Property* property, int value);
		void SetValue(const Property* property, const char* value);
		void SetValue(const Property* property, const std::string& value);
		void SetValue(const Property* property, Object* value);

		template<typename T>
		void SetValue(const std::string& name, T value)
		{
			SetValue(getProperty(name), value);
		}

		// Returns true if object is an extended copy, based on a primary version in an inherited schema/namespace
		virtual bool isCopy() const { return false; }

		// Returns true if object is a child of ancestor supplied
		virtual bool IsDescendent(const Object* ancestor) const;

		// Returns true for implied objects
		bool isImplied() const;

		// Returns true for inferred objects
		bool isInferred() const;

		// Returns true if object is kind of class supplied
		bool isKindOf(const Type* type) const;

		// Returns true for objects currently being loaded
		bool isLoading() const;

		// Returns true if receiver is a major entity
		virtual bool isMajor() const { return false; }

		// Returns true for objects that have been modified
		bool isModified() const;

		// Returns true if children are maintained in a particular order which extracts need to reflect
		// Normally, we preserve the order existing children were loaded to avoid re-ordering extracts
		virtual bool isOrganized() const { return false; }

		// Returns true for shallow objects (haven't been completely loaded)
		bool isShallow() const;

		// Returns true for static objects (predefined metadata)
		bool isStatic() const;

		// Clean-up object if it's been flagged as dirty
		void Clean();

		// Flag object as being dirty, which needs to be cleaned up later if it's not modified in the meantime
		virtual void Dirty(bool deep);

		// Flag object as being loaded
		void loading(bool shallow);
		void loaded(bool shallow);
		void loaded(bool shallow, std::queue<std::future<void>>& tasks);

		// Flag object as being modified, for which changes need to be saved
		void Modified();

		virtual void Write(tinyxml2::XMLNode &parent, tinyxml2::XMLElement* &element, const Object* origin, bool reference) const;

		virtual void LoadHeader(const FileElement& source) {}
		virtual void LoadBody(const FileElement& source) {}

	protected:
		friend State;
		const State* state;

		Object(Assembly& parent, const Class* dataClass, const State* state = nullptr);
		Object(Object* parent, const Class* dataClass, const State* state);

		// Returns bracket to group siblings
		virtual int GetBracket() const { return 0; }

		// Hook for post-load processing
		virtual void loaded(std::queue<std::future<void>>& tasks) {}

		virtual void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const;
		virtual void WriteBody(tinyxml2::XMLElement* element) const;

		template <class TMeta, class TMetaSchema>
		static const Class* GetDataClass(Object* parent, TMeta* const TMetaSchema::* meta, const char* name = nullptr)
		{
			if (!parent)
				return nullptr;

			const TMetaSchema& metaSchema = parent->GetRootSchema();
			return metaSchema.initialized ? *(metaSchema.*meta) : GetDataClass(metaSchema, name);
		}
		static const Class* GetDataClass(const RootSchema& rootSchema, const char* dataClassName);
		static const Class* GetDataClass(Component* parent, const char* dataClassName);

		void SetDataClass(const Class& dataClass);

		// Handles basic conversion of receiver into object supplied
		// NOTE: Handle will be invalid following successful conversion
		Object& Mutate(Object& into);

	private:
		friend Property;
		std::pmr::map<const Property*, AnyValue*> values;

		const Property* getProperty(const std::string& name) const;
		void purgeValues();
	};

	std::ostream& operator<<(std::ostream& os, const Object* object);

	// Use template to determine if type is an object reference
	// NOTE: Assumption is made here where if it's a pointer, it's treated as an object reference.  Using is_base_of relies on complete types otherwise
	template <typename T, typename Enable = void>
	struct is_object_reference : std::false_type {};

	template <typename T>
	struct is_object_reference<T, typename std::enable_if_t<std::is_pointer_v<T>>> : std::true_type {};

	// Use template for casting object references
	// This allows forward declarations of partial specializations to be used to overcome incomplete types
	template<typename To, typename From = Object>
	To* cast_object(From* from)
	{
		return dynamic_cast<To*>(from);
	}

	// Define macros to declare/define object casts
#define DECLARE_OBJECT_CAST(name) template<> name* cast_object(Object* from);
#define DEFINE_OBJECT_CAST(name) template<>	name* cast_object(Object* from) { return dynamic_cast<name*>(from); }

	// Specialize template for casting any object reference values
	template <typename T>
	struct AnyCast<T*>
	{
		static T* Get(const AnyValue* v)
		{
			if (!v)
				return nullptr;

			// Cast to underlying object reference
			auto value = dynamic_cast<const TypeValue<Object*>*>(v);
			if (!value)
				throw jadegit_cast_exception(typeid(*v), typeid(T*));

			// Cast to specific object reference type
			Object* object = *value;
			T* result = cast_object<T>(object);
			
			if (object && !result)
				throw jadegit_cast_exception(typeid(*v), typeid(T*));

			return result;
		}
	};

	// Specialize value template for object references
	template <typename T>
	class Value<T, typename std::enable_if_t<is_object_reference<T>::value && !std::is_const<T>::value>> : public BaseValue<Object*>
	{
	public:
		Value() {}
		Value(T v) : value(v) {}

		AnyValue* Clone() const final
		{
			return new Value(*this);
		}

		operator Object*() const final
		{
			return (Object*)value;
		}

		operator Object&() const
		{
			return *((Object*)value);
		}

		template<typename U = T>
		operator typename std::enable_if_t<!std::is_same_v<U, Object*>, U>() const
		{
			return value;
		}

		T operator->() const noexcept
		{
			return value;
		}

		std::add_lvalue_reference_t<std::remove_pointer_t<T>> operator*() const
		{
			return *value;
		}

		explicit operator bool() const
		{
			return !!value;
		}

		bool empty() const final
		{
			return !value;
		}

		void reset() final
		{
			operator=(nullptr);
		}

		virtual Value& operator=(const T& rhs)
		{
			value = rhs;
			return *this;
		}

		std::string ToString() const final
		{
			throw jadegit_exception("Cannot convert object reference to string");
		}

	private:
		T value = nullptr;

		void Set(const Any& v) override
		{
			operator=(v.Get<T>());
		}

		void Set(const std::string& v) final
		{
			throw jadegit_exception("Cannot set object reference based on string");
		}
	};

	// Specialize value template for const object references
	template <typename T>
	class Value<T, typename std::enable_if_t<is_object_reference<T>::value && std::is_const<T>::value>> : public BaseValue<Object*>
	{
	public:
		Value(T v) : value(v) {}

		AnyValue* Clone() const final
		{
			return new Value(*this);
		}

		operator Object*() const final
		{
			return (Object*)value;
		}

		operator T() const
		{
			return value;
		}

		T operator->() const noexcept
		{
			return value;
		}

		explicit operator bool() const
		{
			return !!value;
		}

		bool empty() const final
		{
			return !value;
		}

		void reset() final
		{
			// No need to reset const value
		}

		std::string ToString() const final
		{
			throw jadegit_exception("Cannot convert object reference to string");
		}

	private:
		T value;

		void Set(const Any& v) final
		{
			throw jadegit_exception("Cannot set const object reference");
		}

		void Set(const std::string& v) final
		{
			throw jadegit_exception("Cannot set object reference based on string");
		}
	};
}