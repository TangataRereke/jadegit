#pragma once

namespace JadeGit::Data
{
	// Ordered alphabetically
	class ActiveXLibrary;
	class Application;
	class Class;
	class CollClass;
	class CompAttribute;
	class Constant;
	class ConstantCategory;
	class Control;
	class CurrencyFormat;
	class Database;
	class DateFormat;
	class DbFile;
	class ExplicitInverseRef;
	class ExternalMethod;
	class Form;
	class Function;
	class GlobalConstant;
	class ImplicitInverseRef;
	class JadeExportedClass;
	class JadeExportedConstant;
	class JadeExportedInterface;
	class JadeExportedMethod;
	class JadeExportedPackage;
	class JadeExportedProperty;
	class JadeExposedClass;
	class JadeExposedFeature;
	class JadeExposedList;
	class JadeHTMLDocument;
	class JadeImportedClass;
	class JadeImportedConstant;
	class JadeImportedInterface;
	class JadeImportedMethod;
	class JadeImportedPackage;
	class JadeImportedProperty;
	class JadeInterface;
	class JadeInterfaceMethod;
	class JadeMethod;
	class JadeWebServicesClass;
	class JadeWebServicesMethod;
	class JadeWebServiceConsumerClass;
	class JadeWebServiceProviderClass;
	class JadeWebServiceSoapHeaderClass;
	class Library;
	class Locale;
	class NumberFormat;
	class PrimAttribute;
	class PrimType;
	class PseudoType;
	class RelationalAttribute;
	class RelationalTable;
	class RelationalView;
	class Schema;
	class TimeFormat;
	class TranslatableString;

	class EntityVisitor
	{
	public:
		virtual ~EntityVisitor() {}

		// Ordered by type alphabetically
		virtual void Visit(ActiveXLibrary* library) = 0;
		virtual void Visit(Application* application) = 0;
		virtual void Visit(Class* cls) = 0;
		virtual void Visit(CollClass* cls) = 0;
		virtual void Visit(CompAttribute* attribute) = 0;
		virtual void Visit(Constant* constant) = 0;
		virtual void Visit(ConstantCategory* category) = 0;
		virtual void Visit(Control* control) = 0;
		virtual void Visit(CurrencyFormat* format) = 0;
		virtual void Visit(Database* database) = 0;
		virtual void Visit(DateFormat* format) = 0;
		virtual void Visit(DbFile* dbFile) = 0;
		virtual void Visit(ExplicitInverseRef* reference) = 0;
		virtual void Visit(ExternalMethod* method) = 0;
		virtual void Visit(Form* form) = 0;
		virtual void Visit(Function* function) = 0;
		virtual void Visit(GlobalConstant* constant) = 0;
		virtual void Visit(ImplicitInverseRef* reference) = 0;
		virtual void Visit(JadeExportedClass* cls) = 0;
		virtual void Visit(JadeExportedConstant* constant) = 0;
		virtual void Visit(JadeExportedInterface* interface) = 0;
		virtual void Visit(JadeExportedMethod* method) = 0;
		virtual void Visit(JadeExportedPackage* package) = 0;
		virtual void Visit(JadeExportedProperty* property) = 0;
		virtual void Visit(JadeExposedClass* cls) = 0;
		virtual void Visit(JadeExposedFeature* feature) = 0;
		virtual void Visit(JadeExposedList* list) = 0;
		virtual void Visit(JadeHTMLDocument* document) = 0;
		virtual void Visit(JadeImportedClass* cls) = 0;
		virtual void Visit(JadeImportedConstant* constant) = 0;
		virtual void Visit(JadeImportedInterface* interface) = 0;
		virtual void Visit(JadeImportedMethod* method) = 0;
		virtual void Visit(JadeImportedPackage* package) = 0;
		virtual void Visit(JadeImportedProperty* property) = 0;
		virtual void Visit(JadeInterface* interface) = 0;
		virtual void Visit(JadeInterfaceMethod* method) = 0;
		virtual void Visit(JadeMethod* method) = 0;
		virtual void Visit(JadeWebServicesClass* cls) = 0;
		virtual void Visit(JadeWebServicesMethod* method) = 0;
		virtual void Visit(JadeWebServiceConsumerClass* cls) = 0;
		virtual void Visit(JadeWebServiceProviderClass* cls) = 0;
		virtual void Visit(JadeWebServiceSoapHeaderClass* cls) = 0;
		virtual void Visit(Library* library) = 0;
		virtual void Visit(Locale* locale) = 0;
		virtual void Visit(NumberFormat* format) = 0;
		virtual void Visit(PrimAttribute* attribute) = 0;
		virtual void Visit(PrimType* primitive) = 0;
		virtual void Visit(PseudoType* pseudoType) = 0;
		virtual void Visit(RelationalAttribute* attribute) = 0;
		virtual void Visit(RelationalTable* table) = 0;
		virtual void Visit(RelationalView* view) = 0;
		virtual void Visit(Schema* schema) = 0;
		virtual void Visit(TimeFormat* format) = 0;
		virtual void Visit(TranslatableString* string) = 0;
	};
}