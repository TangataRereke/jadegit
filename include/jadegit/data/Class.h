#pragma once
#include "Type.h"
#include "Array.h"
#include "Attribute.h"
#include "JadeDynamicPropertyCluster.h"
#include "Reference.h"
#include "RootSchema/ClassMeta.h"
#include <functional>

namespace JadeGit::Data
{
	enum ObjectVolatility : int
	{
		Volatile = 0,
		Frozen = 4,
		Stable = 8
	};

	extern template std::map<ObjectVolatility, const char*> EnumStrings<ObjectVolatility>::data;

	class ActiveXClass;
	class DbClassMap;
	class JadeInterface;
	class JadeInterfaceMapping;

	DECLARE_OBJECT_CAST(ActiveXClass)
	DECLARE_OBJECT_CAST(DbClassMap)
	DECLARE_OBJECT_CAST(JadeInterface)

	class Class : public Type
	{
	public:
		Class(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		ObjectValue<ActiveXClass*, &ClassMeta::activeXClass> activeXClass;
		ObjectValue<Array<DbClassMap*>, &ClassMeta::classMapRefs> classMapRefs;
		EntityDict<JadeDynamicPropertyCluster, &ClassMeta::dynamicPropertyClusters> dynamicPropertyClusters;
		ObjectValue<Array<JadeInterface*>, &ClassMeta::implementedInterfaces> implementedInterfaces;
		Value<ObjectVolatility> instanceVolatility = Volatile;
		EntityDict<Property, &ClassMeta::properties> properties;
		ObjectValue<Array<Class*>, &ClassMeta::subclasses> subclasses;
		ObjectValue<Class*, &ClassMeta::superclass> superclass;
		Value<bool> transient = false;
		Value<bool> webService = false;
		
		void Accept(EntityVisitor &v) override;

		const JadeInterfaceMapping& GetInterfaceMapping(JadeInterface* interface);

		const Class& getOriginal() const override;
		const Class& getRootType() const;
		const Class* getSuperClass() const;

		bool InheritsFrom(const Type* type) const override;

		bool IsSubclass(const Class* superclass) const;

		Property* GetProperty(const std::string& name, bool required = false) const;

	protected:
		Class(JadeImportedPackage* parent, const Class* dataClass, const char* name);

		AnyValue* CreateValue() const final;
		AnyValue* CreateValue(Object& object, const Property& property, bool exclusive) const final;

		void LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, std::queue<std::future<void>>& tasks) const override;
		void WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const override final;
		virtual void WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent, const Object* value) const;
	
	private:
		std::map<const JadeInterface*, JadeInterfaceMapping> interfaceMappings;
	};

	template <>
	void ObjectValue<Class*, &ClassMeta::superclass>::inverseAdd(Object& target) const;

	extern template ObjectValue<ActiveXClass*, &ClassMeta::activeXClass>;
	extern template ObjectValue<Array<DbClassMap*>, &ClassMeta::classMapRefs>;
	extern template ObjectValue<Array<JadeInterface*>, &ClassMeta::implementedInterfaces>;
	extern template Value<ObjectVolatility>;
	extern template EntityDict<Property, &ClassMeta::properties>;
	extern template ObjectValue<Array<Class*>, &ClassMeta::subclasses>;
	extern template ObjectValue<Class*, &ClassMeta::superclass>;

	class JadeInterfaceMapping : public Component
	{
	public:
		JadeInterfaceMapping(Class& type, const JadeInterface& interface);

		Class& type;
		const JadeInterface& interface;

		std::map<std::string, const Method*> methodMappings;
	};

	std::ostream& operator<< (std::ostream& stream, const JadeInterfaceMapping& mapping);

	class ExceptionClass : public Class
	{
	public:
		ExceptionClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);
	};

	class DevControlClass;

	class GUIClass : public Class
	{
	public:
		GUIClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);
	
		DevControlClass* controlType = nullptr;
	};
}