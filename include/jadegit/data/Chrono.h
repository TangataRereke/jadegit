#pragma once
#include "Value.h"
#include <chrono>
#include <optional>

namespace JadeGit::Data
{
	using Date = std::optional<std::chrono::year_month_day>;

	class Time : public std::chrono::hh_mm_ss<std::chrono::milliseconds>
	{
	public:
		using std::chrono::hh_mm_ss<std::chrono::milliseconds>::hh_mm_ss;
	};

	class TimeStamp : public std::optional<std::chrono::sys_time<Time::precision>>
	{
	public:
		using std::optional<std::chrono::sys_time<Time::precision>>::optional;
		TimeStamp(Date date, Time time)
		{
			if (date)
				emplace(std::chrono::sys_days(date.value()) + time.to_duration());
		}
	};

	inline bool operator== (const Time& lhs, const Time& rhs) noexcept
	{
		return lhs.to_duration() == rhs.to_duration();
	}

	inline bool operator== (const TimeStamp& lhs, const TimeStamp& rhs) noexcept
	{
		if (lhs.has_value() && rhs.has_value())
			return lhs.value() == rhs.value();

		return !lhs.has_value() && !rhs.has_value();
	}

	template<class CharT, class Traits>
	std::basic_istream<CharT, Traits>& operator>> (std::basic_istream<CharT, Traits>& stream, Date::value_type& date)
	{
		if (!(stream >> std::chrono::parse("%F", date)))
			throw jadegit_exception("Failed to parse date");

		return stream;
	}

	template<class CharT, class Traits>
	std::basic_istream<CharT, Traits>& operator>> (std::basic_istream<CharT, Traits>& stream, Time& time)
	{
		using namespace std::chrono;
		milliseconds value;
		if (!(stream >> parse("%T", value)))
			throw jadegit_exception("Failed to parse time");

		time = Time(value);
		return stream;
	}

	template<class CharT, class Traits>
	std::basic_istream<CharT, Traits>& operator>> (std::basic_istream<CharT, Traits>& stream, TimeStamp::value_type& ts)
	{
		if (!(stream >> std::chrono::parse("%F %T", ts)))
			throw jadegit_exception("Failed to parse timestamp");

		return stream;
	}

	template<class CharT, class Traits>
	std::basic_ostream<CharT, Traits>& operator<< (std::basic_ostream<CharT, Traits>& stream, const TimeStamp::value_type& ts)
	{
		stream << ts;
		return stream;
	}
}