#pragma once
#include "JadeExportedType.h"
#include "Class.h"
#include "RootSchema/JadeExportedClassMeta.h"

namespace JadeGit::Data
{
	class JadeImportedClass;

	DECLARE_OBJECT_CAST(JadeImportedClass)

	class JadeExportedClass : public JadeExportedType
	{
	public:
		JadeExportedClass(JadeExportedPackage* parent, const Class* dataClass, const char* name);

		ObjectValue<Set<JadeImportedClass*>, &JadeExportedClassMeta::importedClassRefs> importedClassRefs;
		ObjectValue<Class*, &JadeExportedClassMeta::originalClass> originalClass;
		Value<bool> persistentAllowed = false;
		Value<bool> sharedTransientAllowed = false;
		Value<bool> transientAllowed = false;
		Value<bool> transient = false;

		void Accept(EntityVisitor& v) override;

		const Class& getOriginal() const override;

	protected:
		void loaded(std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<Set<JadeImportedClass*>, &JadeExportedClassMeta::importedClassRefs>;
	extern template ObjectValue<Class*, &JadeExportedClassMeta::originalClass>;
}