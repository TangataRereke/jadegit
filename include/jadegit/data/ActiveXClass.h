#pragma once
#include "Object.h"
#include "RootSchema/ActiveXClassMeta.h"

namespace JadeGit::Data
{
	class ActiveXLibrary;

	class ActiveXClass : public Object
	{
	public:
		ActiveXClass(Class* parent, const Class* dataClass);

		ObjectValue<ActiveXLibrary*, &ActiveXClassMeta::activeXLibrary> activeXLibrary;
		ObjectValue<Class* const, &ActiveXClassMeta::baseClass> baseClass;
	};

	extern template ObjectValue<ActiveXLibrary*, &ActiveXClassMeta::activeXLibrary>;
	extern template ObjectValue<Class* const, &ActiveXClassMeta::baseClass>;
}