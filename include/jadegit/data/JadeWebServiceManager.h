#pragma once
#include "Object.h"

namespace JadeGit::Data
{
	class Application;

	class JadeWebServiceManager : public Object
	{
	public:
		JadeWebServiceManager(Application* parent, const Class* dataClass);

		Value<Application* const> application;
	};
}