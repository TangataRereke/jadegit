#pragma once
#include "Routine.h"
#include "Set.h"
#include "RootSchema/MethodMeta.h"

namespace JadeGit::Data
{
	class JadeInterfaceMethod;

	DECLARE_OBJECT_CAST(JadeInterfaceMethod)

	class Method : public Routine
	{
	public:
		Method(Type* parent, const Class* dataClass, const char* name);

		enum class Invocation : char
		{
			Normal = 0,
			Mapping = 1,
			Event = 2,
			Automation = 3
		};

		Value<bool> condition;
		Value<bool> conditionSafe;
		ObjectValue<Method*, &MethodMeta::controlMethod> controlMethod;
		Value<bool> final;
		ObjectValue<Set<JadeInterfaceMethod*>, &MethodMeta::interfaceImplements> interfaceImplements;
		Value<bool> lockReceiver;
		Value<Invocation> invocation = Invocation::Normal;
		Value<bool> partitionMethod;
		Value<bool> subschemaCopyFinal;
		Value<bool> subschemaFinal;
		Value<bool> updating;

		bool isTypeMethod() const;

	protected:
		// Group methods after properties
		int GetBracket() const override { return 3; }
	};
	
	extern template ObjectValue<Method*, &MethodMeta::controlMethod>;
	extern template ObjectValue<Set<Method*>, &MethodMeta::controlMethodRefs>;
	extern template ObjectValue<Set<JadeInterfaceMethod*>, &MethodMeta::interfaceImplements>;
	extern template Value<Method::Invocation>;
	extern template std::map<Method::Invocation, const char*> EnumStrings<Method::Invocation>::data;

	class ExternalMethod : public Method
	{
	public:
		ExternalMethod(Type* parent, const Class* dataClass, const char* name);

		void Accept(EntityVisitor& v) override;
	};

	class JadeMethod : public Method
	{
	public:
		JadeMethod(Type* parent, const Class* dataClass, const char* name);

		void Accept(EntityVisitor& v) override;
	};
}