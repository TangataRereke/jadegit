#pragma once
#include "Type.h"

namespace JadeGit::Data
{
	class PrimType : public Type
	{
	public:
		PrimType(Schema* parent, const Class* dataClass, const char* name);

		void Accept(EntityVisitor &v) override;

		int GetDefaultLength() const override;
		const PrimType& getRootType() const;
		bool isPrimType() const override { return true; }

		PrimType* SetDefaultLength(int length);

	protected:
		AnyValue* CreateValue() const final;
		AnyValue* CreateValue(Object& object, const Property& property, bool exclusive) const final;

		void LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, std::queue<std::future<void>>& tasks) const override;
		void WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const override;

	private:
		mutable int length = 0;
	};
}