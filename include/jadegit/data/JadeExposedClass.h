#pragma once
#include "Constant.h"
#include "EntityDict.h"
#include "JadeExposedFeature.h"
#include "Method.h"
#include "Property.h"
#include "RootSchema/JadeExposedClassMeta.h"

namespace JadeGit::Data
{
	class JadeExposedList;

	class JadeExposedClass : public Entity
	{
	public:
		JadeExposedClass(JadeExposedList* parent, const Class* dataClass, const char* name = nullptr);

		ObjectValue<Array<Constant*>, &JadeExposedClassMeta::consts> consts;
		Value<Byte> defaultStyle;
		EntityDict<JadeExposedFeature, &JadeExposedClassMeta::exposedFeatures> exposedFeatures;
		ObjectValue<JadeExposedList* const, &JadeExposedClassMeta::exposedList> exposedList;
		Value<std::string> exposedName;
		ObjectValue<Array<Method*>, &JadeExposedClassMeta::methods> methods;
		ObjectValue<Array<Property*>, &JadeExposedClassMeta::properties> properties;
		ObjectValue<Class*, &JadeExposedClassMeta::relatedClass> relatedClass;

		void Accept(EntityVisitor& v) override;
	};

	extern template ObjectValue<Array<Constant*>, &JadeExposedClassMeta::consts>;
	extern template EntityDict<JadeExposedFeature, &JadeExposedClassMeta::exposedFeatures>;
	extern template ObjectValue<JadeExposedList* const, &JadeExposedClassMeta::exposedList>;
	extern template ObjectValue<Array<Method*>, &JadeExposedClassMeta::methods>;
	extern template ObjectValue<Array<Property*>, &JadeExposedClassMeta::properties>;
	extern template ObjectValue<Class*, &JadeExposedClassMeta::relatedClass>;
}