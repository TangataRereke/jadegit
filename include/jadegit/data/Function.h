#pragma once
#include "Routine.h"
#include "RootSchema/FunctionMeta.h"

namespace JadeGit::Data
{
	class Schema;

	class Function : public Routine
	{
	public:
		Function(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Schema* const, &FunctionMeta::schema> schema;

		void Accept(EntityVisitor &v) override;

	protected:
		/* Group functions after libraries */
		int GetBracket() const override { return 3; }
	};

	extern template ObjectValue<Schema* const, &FunctionMeta::schema>;
}