#pragma once
#include "JadeExposedClass.h"
#include "RootSchema/JadeExposedListMeta.h"

namespace JadeGit::Data
{
	class Schema;

	class JadeExposedList : public MajorEntity<JadeExposedList, Entity>
	{
	public:
		static const std::filesystem::path subFolder;

		JadeExposedList(Schema* parent, const Class* dataClass, const char* name);

		EntityDict<JadeExposedClass, &JadeExposedListMeta::exposedClasses> exposedClasses;
		Value<int> priorVersion = 0;
		Value<std::string> registryId;
		ObjectValue<Schema* const, &JadeExposedListMeta::schema> schema;
		Value<bool> secureService = false;
		Value<bool> sessionHandling = false;
		Value<bool> useBareFormat = false;
		Value<bool> useEncodedFormat = false;
		Value<bool> useHttpGet = false;
		Value<bool> useHttpPost = false;
		Value<bool> useRPC = false;
		Value<bool> useSOAP12 = false;
		Value<int> version = 0;
		Value<bool> versionControl = false;		

		void Accept(EntityVisitor& v) override;
	};

	extern template EntityDict<JadeExposedClass, &JadeExposedListMeta::exposedClasses>;
	extern template ObjectValue<Schema* const, &JadeExposedListMeta::schema>;
}