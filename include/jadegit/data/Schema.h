#pragma once
#include "ActiveXLibrary.h"
#include "Application.h"
#include "Class.h"
#include "ConstantCategory.h"
#include "Database.h"
#include "Function.h"
#include "GlobalConstant.h"
#include "Library.h"
#include "Locale.h"
#include "LocaleFormat.h"
#include "JadeExportedPackage.h"
#include "JadeExposedList.h"
#include "JadeHTMLDocument.h"
#include "JadeImportedPackage.h"
#include "PrimType.h"
#include "PseudoType.h"
#include "RelationalView.h"
#include "RootSchema/SchemaMeta.h"

namespace JadeGit::Data
{
	class Schema : public MajorEntity<Schema, Entity>
	{
	public:
		static const std::filesystem::path subFolder;

		Schema(Assembly& parent, const Class* dataClass, const char* name);
		~Schema();

		EntityDict<ActiveXLibrary, &SchemaMeta::activeXLibraries> activeXLibraries;
		EntityDict<Application, &SchemaMeta::_applications> applications;
		EntityDict<Class, &SchemaMeta::classes> classes;
		EntityDict<ConstantCategory, &SchemaMeta::constantCategories> constantCategories;
		EntityDict<GlobalConstant, &SchemaMeta::consts> constants;
		EntityDict<Database, &SchemaMeta::databases> databases;
		EntityDict<JadeExportedPackage, &SchemaMeta::exportedPackages> exportedPackages;
		EntityDict<JadeExposedList, &SchemaMeta::_exposedLists> exposedLists;
		EntityDict<Function, &SchemaMeta::functions> functions;
		EntityDict<JadeHTMLDocument, &SchemaMeta::_jadeHTMLDocuments> htmlDocuments;
		EntityDict<JadeImportedPackage, &SchemaMeta::importedPackages> importedPackages;
		EntityDict<JadeInterface, &SchemaMeta::interfaces> interfaces;
		EntityDict<Library, &SchemaMeta::libraries> libraries;
		EntityDict<Locale, &SchemaMeta::locales> locales;
		EntityDict<LocaleFormat, &SchemaMeta::userFormats> localeFormats;
		Value<Locale*> primaryLocale;
		EntityDict<PrimType, &SchemaMeta::primitives> primitives;
		EntityDict<PseudoType, &SchemaMeta::pseudoTypes> pseudoTypes;
		EntityDict<RelationalView, &SchemaMeta::relationalViews> relationalViews;
		ObjectValue<Set<Schema*>, &SchemaMeta::subschemas> subschemas;
		ObjectValue<Schema*, &SchemaMeta::superschema> superschema;
		Value<std::string> text;

		void Accept(EntityVisitor &v) override;

		bool InheritsFrom(const Schema* superschema) const;
		bool IsDescendent(const Object* ancestor) const override;
		bool IsRootSchema() const;

		Database* getDatabase() const;
		Schema* GetSuperSchema() const;
		Type* getType(const std::string& name) const;

		using Entity::Load;
		static Schema* Load(Assembly* parent, const std::string& name, bool shallow = true);

	protected:
		friend MetaSchema;
		Schema(Assembly& parent, const Class* dataClass, const char* name, const RootSchema& rootSchema);
	};

	extern template EntityDict<ActiveXLibrary, &SchemaMeta::activeXLibraries>;
	extern template EntityDict<Application, &SchemaMeta::_applications>;
	extern template EntityDict<Class, &SchemaMeta::classes>;
	extern template EntityDict<ConstantCategory, &SchemaMeta::constantCategories>;
	extern template EntityDict<GlobalConstant, &SchemaMeta::consts>;
	extern template EntityDict<Database, &SchemaMeta::databases>;
	extern template EntityDict<JadeExportedPackage, &SchemaMeta::exportedPackages>;
	extern template EntityDict<JadeExposedList, &SchemaMeta::_exposedLists>;
	extern template EntityDict<Function, &SchemaMeta::functions>;
	extern template EntityDict<JadeHTMLDocument, &SchemaMeta::_jadeHTMLDocuments>;
	extern template EntityDict<JadeImportedPackage, &SchemaMeta::importedPackages>;
	extern template EntityDict<JadeInterface, &SchemaMeta::interfaces>;
	extern template EntityDict<Library, &SchemaMeta::libraries>;
	extern template EntityDict<Locale, &SchemaMeta::locales>;
	extern template EntityDict<LocaleFormat, &SchemaMeta::userFormats>;
	extern template Value<Locale*>;
	extern template EntityDict<PrimType, &SchemaMeta::primitives>;
	extern template EntityDict<PseudoType, &SchemaMeta::pseudoTypes>;
	extern template EntityDict<RelationalView, &SchemaMeta::relationalViews>;
	extern template ObjectValue<Set<Schema*>, &SchemaMeta::subschemas>;
	extern template ObjectValue<Schema*, &SchemaMeta::superschema>;
}