#pragma once
#include "Any.h"
#include "Binary.h"
#include <jadegit/MemoryAllocated.h>
#include <optional>
#include <sstream>
#include <type_traits>

namespace JadeGit::Data
{
	template<typename T>
	concept is_optional = requires(T t)
	{
		typename T::value_type;
		std::is_base_of_v<std::optional<typename T::value_type>, T>;
	};

	// Default value template
	template <typename T, typename Enable>
	class Value : public MemoryAllocated, public BaseValue<T>
	{
		static_assert(!std::is_pointer<T>::value);
		static_assert(!std::is_const<T>::value);

	public:
		Value() : value() {}
		Value(T v) : value(v) {}

		AnyValue* Clone() const final
		{
			return new Value(*this);
		}

		operator T() const final
		{
			return value;
		}

		virtual Value& operator=(const T& rhs)
		{
			value = rhs;
			return *this;
		}

		bool empty() const final
		{
			return false;	// Basic values aren't empty, zero/false are valid
		}

		void reset() final
		{
			operator=(T());
		}

		void Set(const std::string& v) final
		{
			std::istringstream is(v);
			T value;

			if constexpr (is_optional<T>)
			{
				if (!v.empty())
				{
					typename T::value_type actual;
					is >> actual;
					value = actual;
				}
			}
			else
				is >> value;

			operator=(value);
		}

		std::string ToString() const final
		{
			std::ostringstream os;

			if constexpr (is_optional<T>)
			{
				if (value.has_value())
					os << value.value();
			}
			else
				os << value;

			return os.str();
		}

		std::string ToBasicString() const final
		{
			return ToString();
		}

	private:
		T value;

		void Set(const Any& v) final
		{
			operator=(v.Get<T>());
		}
	};

	// ValueType template to retrieve underlying type
	template <typename T>
	struct ValueType;

	template <typename T>
	struct ValueType<Value<T>>
	{
		typedef T type;
	};

	template <typename T>
	struct is_string_or_binary : std::false_type {};

	template <>
	struct is_string_or_binary<std::string> : std::true_type {};

	template <>
	struct is_string_or_binary<Binary> : std::true_type {};

	template <>
	struct is_string_or_binary<std::u8string> : std::true_type {};

	// Specialization for string & binary values, which are used as base types
	template <typename T>
	class Value<T, typename std::enable_if_t<is_string_or_binary<T>::value>> : public MemoryAllocated, public BaseValue<T>, public TypeValue<typename T::value_type>, public T
	{
	public:
		Value() : BaseValue<T>() {}
		Value(const T& value) : T(value) {}
		Value(const char* value) : T(value) {}

		AnyValue* Clone() const final
		{
			return new Value(*this);
		}

		operator T() const final
		{
			return *this;
		}

		operator typename T::value_type() const final
		{
			return empty() ? T::value_type() : T::front();
		}

		virtual Value& operator=(const T& rhs)
		{
			T::operator=(rhs);
			return *this;
		}

		Value& operator=(const char* rhs)
		{
			return operator=(T(rhs));
		}
		
		bool empty() const final
		{
			return T::empty();
		}

		void reset() final
		{
			operator=(T());
		}

		void Set(const std::string& v) final
		{
			if constexpr (std::is_base_of_v<std::u8string, T>)
			{
				T::assign(v.begin(), v.end());
			}
			else
				operator=(static_cast<T>(v));
		}

		std::string ToString() const final
		{
			if constexpr (std::is_base_of_v<std::u8string, T>)
			{
				return std::string(T::begin(), T::end());
			}
			else
				return *this;
		}

	private:
		void Set(const Any& v) final
		{
			operator=(v.Get<T>());
		}
	};

	template<>
	bool Value<char>::empty() const;

	// Partial specializations for converting strings
	template<>
	void Value<bool>::Set(const std::string& v);

	template<>
	void Value<Byte>::Set(const std::string& v);

	template<>
	void Value<char>::Set(const std::string& v);

	template<>
	void Value<int>::Set(const std::string& v);

	template<>
	void Value<double>::Set(const std::string& v);

	// Partial specializations for formatting strings
	template<>
	std::string Value<bool>::ToString() const;

	template<>
	std::string Value<Byte>::ToString() const;

	template<>
	std::string Value<char>::ToString() const;

	template<>
	std::string Value<double>::ToString() const;

	extern template Value<Binary>;
	extern template Value<bool>;
	extern template Value<Byte>;
	extern template Value<char>;
	extern template Value<double>;
	extern template Value<int>;
	extern template Value<std::string>;

	// Partial specializations for casting primitive values
	template <>
	static int AnyCast<int>::Get(const AnyValue* v);
}