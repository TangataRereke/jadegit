#pragma once
#include "ActiveXFeature.h"

namespace JadeGit::Data
{
	class Constant;

	class ActiveXConstant : public ActiveXFeature
	{
	public:
		ActiveXConstant(Constant* parent, const Class* dataClass);
	};
}