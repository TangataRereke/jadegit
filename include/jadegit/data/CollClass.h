#pragma once
#include "Class.h"
#include "Key.h"
#include "RootSchema/CollClassMeta.h"

namespace JadeGit::Data
{
	class CollClass : public Class
	{
	public:
		CollClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		Value<int> blockSize = 0;
		Value<bool> duplicatesAllowed = false;
		Value<int> expectedPopulation = 0;
		Value<int> loadFactor = 0;
		ObjectValue<Type*, &CollClassMeta::memberType> memberType;
		Value<int> memberTypePrecision = 0;
		Value<int> memberTypeScaleFactor = 0;
		Value<int> memberTypeSize = 0;

		std::vector<Key*> keys;

		void Accept(EntityVisitor &v) override;

		Type* GetMemberType() const;

		inline CollClass* SetMemberType(Type* memberType)
		{
			if(this)
				this->memberType = memberType;
			return this;
		}

	protected:
		void LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, std::queue<std::future<void>>& tasks) const override;
		void WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent, const Object* value) const override;
	};

	extern template ObjectValue<Type*, &CollClassMeta::memberType>;
}