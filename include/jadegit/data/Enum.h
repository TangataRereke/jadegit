#pragma once
#include "Value.h"
#include <map>

namespace JadeGit::Data
{
	// Template for enum strings
	// Specialization needs to be defined for each enum using operators below,
	// otherwise there's a compiler error because there's no generic definition.
	template<typename T>
	struct EnumStrings
	{
		static std::map<T, const char*> data;
	};

	template <typename T, typename std::enable_if<std::is_enum<T>::value>::type* = nullptr>
	std::ostream& operator<<(std::ostream& os, const T &value)
	{
		static auto end = std::end(EnumStrings<T>::data);

		auto find = EnumStrings<T>::data.find(value);
		if (find == end)
			throw jadegit_exception("Unknown enumeration value");

		return os << find->second;
	}

	template<typename T, typename std::enable_if<std::is_enum<T>::value>::type* = nullptr>
	std::istream& operator>>(std::istream& is, T &value)
	{
		std::string str;
		is >> str;
		if (is.fail())
			return is;

		for (auto mapping : EnumStrings<T>::data)
		{
			if (str == mapping.second)
			{
				value = mapping.first;
				return is;
			}
		}

		// Handle scenario where underlying value is supplied
		for (auto mapping : EnumStrings<T>::data)
		{
			if (str == Value(static_cast<std::underlying_type_t<T>>(mapping.first)).ToString())
			{
				value = mapping.first;
				return is;
			}
		}

		is.setstate(std::ios::failbit);
		return is;
	}

	// Specialize value template for enumerations, which can be interpreted as underlying type
	template <typename T>
	class Value<T, typename std::enable_if_t<std::is_enum<T>::value>> : public BaseValue<std::underlying_type_t<T>>, public TypeValue<T>
	{
	public:
		Value() {}
		Value(T v) : value(v) {}

		AnyValue* Clone() const final
		{
			return new Value(*this);
		}

		operator T() const final
		{
			return value;
		}

		virtual Value& operator=(const T& rhs)
		{
			static auto end = std::end(EnumStrings<T>::data);

			auto find = EnumStrings<T>::data.find(rhs);
			if (find == end)
				throw jadegit_exception("Invalid enumeration value [" + std::to_string(static_cast<std::underlying_type_t<T>>(rhs)) + "]");

			value = rhs;
			return *this;
		}

		bool empty() const final
		{
			return false;
		}

		void reset() final
		{
			operator=(T());
		}

		void Set(const std::string& v) final
		{
			std::stringstream is(v);
			T value;
			is >> value;
			if (is.fail())
				throw jadegit_exception("Invalid enumeration value [" + v + "]");

			operator=(value);
		}

		std::string ToString() const final
		{
			std::ostringstream os;
			os << value;
			return os.str();
		}

		std::string ToBasicString() const final
		{
			std::ostringstream os;
			os << static_cast<std::underlying_type_t<T>>(value);
			return os.str();
		}

	private:
		T value;

		explicit operator std::underlying_type_t<T>() const final
		{
			return static_cast<std::underlying_type_t<T>>(value);
		}

		void Set(const Any& v) final
		{
			operator=(static_cast<T>(v.Get<std::underlying_type_t<T>>()));
		}
	};
}