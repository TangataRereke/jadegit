#pragma once
#include "NamedObject.h"
#include <filesystem>
#include <ostream>
#include <uuid.h>

namespace JadeGit::Data
{	
	using EntityKey = std::pair<uuids::uuid, uuids::uuid>;

	class EntityVisitor;

	class Entity : public NamedObject<Object>
	{
	public:
		using is_major_entity = std::false_type;

		static Entity* Load(Component* origin, const std::filesystem::path& path, bool shallow = false);

		template <class TEntity>
		static typename std::enable_if_t<std::is_reference<TEntity>::value, TEntity> resolve(const Component& origin, const QualifiedName& name, bool shallow = true, bool inherit = false)
		{
			return *static_cast<std::remove_reference_t<TEntity>*>(resolve(typeid(std::remove_reference_t<TEntity>), origin, name, shallow, inherit, true));
		}

		template <class TEntity>
		static typename std::enable_if_t<!std::is_reference<TEntity>::value, TEntity*> resolve(const Component& origin, const QualifiedName& name, bool shallow = true, bool inherit = false)
		{
			return static_cast<TEntity*>(resolve(typeid(TEntity), origin, name, shallow, inherit, false));
		}

		using NamedObject::NamedObject;
		
		std::string author;
		std::time_t modified = 0;

		// Visits receiver
		virtual void Accept(EntityVisitor &v) = 0;

		// Visits recevier, followed by all it's children recursively
		void AcceptAll(EntityVisitor &v, bool deep = false);

		// Visits all child entities recursively, where just embedded entities will be visited if deep is false
		void AcceptChildren(EntityVisitor &v, bool deep = false);

		// Derives composite key using entity ID's
		EntityKey getKey() const;

		// Returns parent entity
		Entity* getParentEntity() const;

		// Returns parent entity for shorthand qualified name, which is overridden by children when required to omit surplus parent(s)
		virtual const Entity* GetQualifiedParent() const;

		// Returns parent entity for qualified name
		const Entity* GetQualifiedParent(bool shorthand) const;

		// Returns qualified name of entity, in context of supplied entity if supplied (fully qualified otherwise)
		std::string GetQualifiedName(const Entity* context = nullptr, bool shorthand = false) const;
	
		// Flag entity as being new/created, using supplied uuid if provided
		void Created(const char* uuid = nullptr);

		// Flag entity as implied, which we assume exists until explicitly loaded
		Entity* implied();

		// Flag entity as inferred, which doesn't need to be saved unless explicitly modified
		Entity* inferred(bool condition = true);

		// Complete pending load for shallow entity
		Entity* Load();
		const Entity* Load() const;

		// Saves any pending changes
		void Save(bool prelude);

		// Delete entity
		// NOTE: Handle will be invalid following successful deletion
		void Delete();

		// Rename entity
		void Rename(const char* new_name);

		// Write entity to document
		using NamedObject::Write;
		void Write(tinyxml2::XMLDocument &document) const;

		// Recreates entity when it needs to be converted to a different data class
		// Returns replacement entity upon conversion, otherwise original (receiver)
		// NOTE: Handle will be invalid following successful conversion
		Entity* Mutate(const std::string& dataClass);

		// Returns relative path to major entity file
		virtual std::filesystem::path path() const;

	protected:
		friend class InitialState;
		uuids::uuid id;

		// Returns entity ID
		uuids::uuid getId() const;

		// Returns original entity
		virtual const Entity& getOriginal() const;

		// Returns true if receiver is the original entity
		bool isOriginal() const;

		static Entity* resolve(const type_info& type, const Component& origin, const QualifiedName& name, bool shallow, bool inherit, bool expected);

		void Dirty(bool deep) override;

		void inferredFrom(const Entity* major, const Entity* minor);

		void LoadHeader(const FileElement& source) override;
		void LoadBody(const FileElement& source) override;

		void Write(tinyxml2::XMLNode &parent, tinyxml2::XMLElement* &element, const Object* origin, bool reference) const override;
		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const override;
	};

	std::ostream& operator<< (std::ostream& stream, const Entity& entity);

	template<class TComponent, class TBase>
	class MajorEntity : public TBase
	{
	public:
		using is_major_entity = std::true_type;

		std::filesystem::path path() const final
		{
			std::filesystem::path result;

			// Start with parent path as base (if there is one)
			if (const Entity* parent = static_cast<const TComponent*>(this)->getParentEntity())
				result = parent->path();

			// Append sub-folder
			result /= TComponent::subFolder;

			// Append name
			result /= static_cast<const TComponent*>(this)->GetName();

			return result;
		}

	protected:
		using TBase::TBase;

		bool isMajor() const final
		{
			return true;
		}
	};

	template <class Entity>
	inline constexpr bool is_major_entity = Entity::is_major_entity::value;
}