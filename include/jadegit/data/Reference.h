#pragma once
#include "Property.h"
#include "RootSchema/JadeDynamicExplicitInverseRefMeta.h"
#include "RootSchema/JadeDynamicImplicitInverseRefMeta.h"

namespace JadeGit::Data
{
	class JadeDynamicPropertyCluster;
	class JadeMethod;

	DECLARE_OBJECT_CAST(JadeDynamicPropertyCluster)
	DECLARE_OBJECT_CAST(JadeMethod)

	class Reference : public Property
	{
	public:
		Reference(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);
		
		ObjectValue<JadeMethod*, &ReferenceMeta::constraint> constraint;

	protected:
		AnyValue* InstantiateValue(Object& object) const final;
	};
	
	class Inverse;

	class ExplicitInverseRef : public Reference
	{
	public:
		ExplicitInverseRef(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);

		enum ReferenceKind : char
		{
			Peer = 0,
			Parent = 1,
			Child = 2
		};

		enum UpdateMode : char
		{
			Manual = 0,
			Manual_Automatic = 1,
			Automatic = 2,
			Automatic_Deferred = 4,
			Manual_Automatic_Deferred = 5
		};

		Value<bool> inverseNotRequired = false;
		std::map<ExplicitInverseRef*, Inverse*> inverses;
		Value<ReferenceKind> kind = Peer;
		Value<bool> transientToPersistentAllowed = false;
		Value<UpdateMode> updateMode = Manual_Automatic;

		void Accept(EntityVisitor &v) override;

		inline ExplicitInverseRef& setKind(ReferenceKind kind)
		{
			this->kind = kind;
			return *this;
		}

		inline ExplicitInverseRef& setUpdateMode(UpdateMode mode)
		{
			updateMode = mode;
			return *this;
		}

		inline ExplicitInverseRef& automatic()
		{
			return setUpdateMode(Automatic);
		}

		inline ExplicitInverseRef& child()
		{
			return setKind(Child);
		}

		inline ExplicitInverseRef& inverseUnrequired()
		{
			inverseNotRequired = true;
			return *this;
		}

		inline ExplicitInverseRef& manual()
		{
			return setUpdateMode(Manual);
		}

		inline ExplicitInverseRef& parent()
		{
			return setKind(Parent);
		}

	protected:
		bool WriteFilter() const override;
	};

	extern template Value<ExplicitInverseRef::ReferenceKind>;
	extern template Value<ExplicitInverseRef::UpdateMode>;
	extern template std::map<ExplicitInverseRef::ReferenceKind, const char*> EnumStrings<ExplicitInverseRef::ReferenceKind>::data;
	extern template std::map<ExplicitInverseRef::UpdateMode, const char*> EnumStrings<ExplicitInverseRef::UpdateMode>::data;

	class Inverse : public Object
	{
	public:
		Inverse(ExplicitInverseRef* left, ExplicitInverseRef* right);
		~Inverse();

		ExplicitInverseRef* const reference = nullptr;
		Inverse* counterpart = nullptr;

		const Inverse* getPrimary() const;
		ExplicitInverseRef::ReferenceKind getReferenceKind() const;
		ExplicitInverseRef::UpdateMode getReferenceUpdateMode() const;

	protected:
		Inverse(ExplicitInverseRef* parent, Inverse* counterpart, const State* state = nullptr);

		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const override;

	private:
		bool first = false;
	};

	class JadeDynamicExplicitInverseRef : public ExplicitInverseRef
	{
	public:
		JadeDynamicExplicitInverseRef(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeDynamicPropertyCluster*, &JadeDynamicExplicitInverseRefMeta::dynamicPropertyCluster> dynamicPropertyCluster;
	};

	class ImplicitInverseRef : public Reference
	{
	public:
		ImplicitInverseRef(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);

		Value<bool> memberTypeInverse = false;

		void Accept(EntityVisitor &v) override;
	};

	class JadeDynamicImplicitInverseRef : public ImplicitInverseRef
	{
	public:
		JadeDynamicImplicitInverseRef(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeDynamicPropertyCluster*, &JadeDynamicImplicitInverseRefMeta::dynamicPropertyCluster> dynamicPropertyCluster;
	};
}