#pragma once
#include "NamedObject.h"
#include "Enum.h"
#include "Array.h"
#include "Attribute.h"
#include "RootSchema/DevControlPropertiesMeta.h"

namespace JadeGit::Data
{
	class Development : public Object
	{
	public:
		using Object::Object;
	};

	class GUIClass;

	class DevControlClass : public Development
	{
	public:
		DevControlClass(GUIClass* parent, const Class* dataClass);
	};

	class DevControlProperties;

	class DevControlTypes : public NamedObject<DevControlClass>
	{
	public:
		DevControlTypes(GUIClass* parent, const Class* dataClass, const char* name = nullptr);

		std::vector<DevControlProperties*> controlProps;
	};

	class DevControlProperties : public NamedObject<Development>
	{
	public:
		DevControlProperties(DevControlTypes* parent, const Class* dataClass, const char* name = nullptr);

		enum Type : int
		{
			Parent = -102,
			Name = 1,
			String = 2,
			Boolean = 3,
			Integer = 4,
			Character = 6,
			Color = 7,
			Font = 8,
			FontSize = 9,
			Real = 10,
			Picture = 11,
			IntegerSigned = 14,
			RealSigned = 15,
			StringMultiLined = 16,
			Xaml = 17,
			ViaPropertyPage = 99,
			List = 100
		};

		Value<Type> cntrlType = String;
		ObjectValue<Array<std::string>, &DevControlPropertiesMeta::optionsList> optionsList;
		Value<DevControlTypes* const> parent;
	};

	extern template std::map<DevControlProperties::Type, const char*> EnumStrings<DevControlProperties::Type>::data;
}