#pragma once
#include "Collection.h"
#include "Iterator.h"
#include <vector>

namespace JadeGit::Data
{
	template <typename TMember>
	class Array : public TCollection<TMember>, public std::pmr::vector<typename TCollection<TMember>::MemberType>
	{
	public:
		using TCollection<TMember>::TCollection;

		void Add(typename TCollection<TMember>::MemberType value) override
		{
			this->push_back(value);
		}

		void Clear() override
		{
			this->clear();
		}

		std::unique_ptr<Iterator> CreateIterator() const override
		{
			std::unique_ptr<Iterator> iter(new ContainerIterator<std::pmr::vector<typename TCollection<TMember>::MemberType>>(*this));
			return iter;
		}

		bool Empty() const override
		{
			return this->empty();
		}

		bool Includes(const typename TCollection<TMember>::MemberType& value) const override
		{
			return std::find(this->begin(), this->end(), value) != this->end();
		}

		void Remove(typename TCollection<TMember>::MemberType value) override
		{
			this->erase(std::find(this->begin(), this->end(), value));
		}
	};
}