#pragma once
#include <jadegit/vfs/File.h>
#include <ctime>

namespace tinyxml2
{
	class XMLElement;
	class XMLNode;
}

namespace JadeGit::Data
{
	class FileElement
	{
	public:
		FileElement(const File& file, const tinyxml2::XMLElement* source);
		virtual ~FileElement() {}

		const File& file;
		const tinyxml2::XMLElement* const source;

		inline operator const tinyxml2::XMLElement* () const {
			return source;
		}

		void signature(std::string& author, std::time_t& modified) const;

		const char* type() const;
		const char* header(const char* name) const;

	protected:
		int getLastLineNum(const tinyxml2::XMLNode* node) const;
	};
}