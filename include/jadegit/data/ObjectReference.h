#pragma once
#include <jadegit/Exception.h>

namespace JadeGit::Data
{
	class Object;
	class ExplicitInverseRef;

	class inverse_maintenance_exception : public jadegit_exception
	{
	public:
		inverse_maintenance_exception();
	};

	// Global function to determine if inverse maintainance is suppressed (when assemblies are cleaned up)
	bool inverseMaintenanceSuppressed();

	// Default inverse maintenance functions
	void inverseAdd(const ExplicitInverseRef& property, Object& origin, Object& target);
	void inverseRemove(const ExplicitInverseRef& property, Object& origin, Object& target);

	// Base object reference, which defines functions automatic inverse maintenance
	class ObjectReference
	{
	public:
		virtual bool autoAdd(Object& member) = 0;
		virtual bool autoRemove(Object& member) = 0;
	};
}