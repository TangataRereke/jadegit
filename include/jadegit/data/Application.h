#pragma once
#include "Entity.h"
#include "ObjectValue.h"
#include "Set.h"
#include <jadegit/data/RootSchema/ApplicationMeta.h>

namespace JadeGit::Data
{
	class JadeExportedPackage;
	class JadeWebServiceManager;
	class Schema;

	class Application : public MajorEntity<Application, Entity>
	{
	public:
		static const std::filesystem::path subFolder;

		Application(Schema* parent, const Class* dataClass, const char* name);

		Value<char> applicationType;
		ObjectValue<Set<JadeExportedPackage*>, &ApplicationMeta::exportedPackages> exportedPackages;
		JadeWebServiceManager* jadeWebServiceManager = nullptr;
		ObjectValue<Schema* const, &ApplicationMeta::schema> schema;

		void Accept(EntityVisitor &v) override;
	};

	extern template ObjectValue<Set<JadeExportedPackage*>, &ApplicationMeta::exportedPackages>;
	extern template ObjectValue<Schema* const, &ApplicationMeta::schema>;
}