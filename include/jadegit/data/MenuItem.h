#pragma once
#include "Object.h"
#include "ObjectValue.h"
#include "RootSchema/MenuItemMeta.h"

namespace JadeGit::Data
{
	class MenuItemData : public Object
	{
	public:
		using Object::Object;
	};

	class Form;

	class MenuItem : public MenuItemData
	{
	public:
		MenuItem(Form* parent, const Class* dataClass);

		ObjectValue<Form* const, &MenuItemMeta::form> form;
		Value<std::string> name;
	};
}