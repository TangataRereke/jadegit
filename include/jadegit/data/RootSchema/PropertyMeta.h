#pragma once
#include "FeatureMeta.h"

namespace JadeGit::Data
{
	class PropertyMeta : public RootClass<>
	{
	public:
		static const PropertyMeta& get(const Object& object);
		
		PropertyMeta(RootSchema& parent, const FeatureMeta& superclass);
	
		PrimAttribute* const embedded;
		ExplicitInverseRef* const exportedPropertyRefs;
		PrimAttribute* const isHTMLProperty;
		ExplicitInverseRef* const keyPathRefs;
		PrimAttribute* const required;
		PrimAttribute* const webService;
		PrimAttribute* const virtual_;
	};
};
