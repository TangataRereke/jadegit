#pragma once
#include "WindowMeta.h"

namespace JadeGit::Data
{
	class FormMeta : public RootClass<GUIClass>
	{
	public:
		static const FormMeta& get(const Object& object);
		
		FormMeta(RootSchema& parent, const WindowMeta& superclass);
	
		PrimAttribute* const allowDocking;
		PrimAttribute* const backBrush;
		PrimAttribute* const caption;
		PrimAttribute* const clientHeight;
		PrimAttribute* const clientWidth;
		PrimAttribute* const clipControls;
		PrimAttribute* const controlBox;
		ExplicitInverseRef* const controlList;
		PrimAttribute* const icon;
		PrimAttribute* const ignoreSkinAllControls;
		ExplicitInverseRef* const locale;
		PrimAttribute* const maxButton;
		PrimAttribute* const mdiChild;
		PrimAttribute* const mdiFrame;
		ExplicitInverseRef* const menuList;
		PrimAttribute* const minButton;
		PrimAttribute* const minimumHeight;
		PrimAttribute* const minimumWidth;
		PrimAttribute* const scaleForm;
		PrimAttribute* const scrollBars;
		PrimAttribute* const secureForm;
		PrimAttribute* const showMdiCloseAllButPinnedMenu;
		PrimAttribute* const showMdiCloseAllButThisMenu;
		PrimAttribute* const showMdiCloseMenu;
		PrimAttribute* const showMdiDockMenu;
		PrimAttribute* const showMdiFloatMenu;
		PrimAttribute* const showMdiPinMenu;
		PrimAttribute* const webBrowserAutoRefreshInterval;
		PrimAttribute* const webBrowserAutoRefreshURL;
		PrimAttribute* const webBrowserDisableBackButton;
		PrimAttribute* const webEncodingType;
		PrimAttribute* const webFileName;
		PrimAttribute* const windowState;
	};
};
