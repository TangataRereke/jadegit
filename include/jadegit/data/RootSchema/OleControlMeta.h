#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class OleControlMeta : public RootClass<GUIClass>
	{
	public:
		static const OleControlMeta& get(const Object& object);
		
		OleControlMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const activation;
		PrimAttribute* const allowInPlace;
		PrimAttribute* const displayAsIcon;
		ImplicitInverseRef* const oleObject;
		PrimAttribute* const showMenu;
		PrimAttribute* const sizeMode;
	};
};
