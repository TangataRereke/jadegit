#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class JadeExposedClassMeta : public RootClass<>
	{
	public:
		static const JadeExposedClassMeta& get(const Object& object);
		
		JadeExposedClassMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const classAutoAdded;
		ExplicitInverseRef* const consts;
		PrimAttribute* const defaultStyle;
		ExplicitInverseRef* const exposedFeatures;
		ExplicitInverseRef* const exposedList;
		PrimAttribute* const exposedName;
		ExplicitInverseRef* const methods;
		PrimAttribute* const name;
		ExplicitInverseRef* const properties;
		ExplicitInverseRef* const relatedClass;
	};
};
