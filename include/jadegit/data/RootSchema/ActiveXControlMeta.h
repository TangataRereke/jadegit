#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class ActiveXControlMeta : public RootClass<GUIClass>
	{
	public:
		static const ActiveXControlMeta& get(const Object& object);
		
		ActiveXControlMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const usePresentationClient;
	};
};
