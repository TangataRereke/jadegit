#pragma once
#include "PrimAttributeMeta.h"

namespace JadeGit::Data
{
	class ExternalPrimAttributeMeta : public RootClass<>
	{
	public:
		static const ExternalPrimAttributeMeta& get(const Object& object);
		
		ExternalPrimAttributeMeta(RootSchema& parent, const PrimAttributeMeta& superclass);
	};
};
