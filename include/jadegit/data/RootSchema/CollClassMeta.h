#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class CollClassMeta : public RootClass<>
	{
	public:
		static const CollClassMeta& get(const Object& object);
		
		CollClassMeta(RootSchema& parent, const ClassMeta& superclass);
	
		PrimAttribute* const blockSize;
		PrimAttribute* const duplicatesAllowed;
		PrimAttribute* const expectedPopulation;
		PrimAttribute* const loadFactor;
		ExplicitInverseRef* const memberType;
		PrimAttribute* const memberTypePrecision;
		PrimAttribute* const memberTypeSFactor;
		PrimAttribute* const memberTypeSize;
	};
};
