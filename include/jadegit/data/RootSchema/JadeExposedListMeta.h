#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class JadeExposedListMeta : public RootClass<>
	{
	public:
		static const JadeExposedListMeta& get(const Object& object);
		
		JadeExposedListMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const exposedClasses;
		PrimAttribute* const version;
		PrimAttribute* const name;
		PrimAttribute* const priorVersion;
		PrimAttribute* const registryId;
		ExplicitInverseRef* const schema;
		PrimAttribute* const useEncodedFormat;
		PrimAttribute* const useBareFormat;
		PrimAttribute* const secureService;
		PrimAttribute* const sessionHandling;
		PrimAttribute* const useHttpGet;
		PrimAttribute* const useHttpPost;
		PrimAttribute* const useRPC;
		PrimAttribute* const useSOAP12;
		PrimAttribute* const versionControl;
	};
};
