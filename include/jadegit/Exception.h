#pragma once
#include <stdexcept>
#include <iostream>

namespace JadeGit
{
	class jadegit_exception : public std::runtime_error
	{
	public:
		const int code = 0;

		jadegit_exception(const char* message) : std::runtime_error(message) {}
		jadegit_exception(int code, const char* message) : code(code), std::runtime_error(message) {}

		jadegit_exception(const std::string& message) : jadegit_exception(message.c_str()) {}
		jadegit_exception(int code, const std::string& message) : jadegit_exception(code, message.c_str()) {}
	};

	inline std::ostream& operator<< (std::ostream& stream, const jadegit_exception& e)
	{
		stream << e.what();
		if (e.code)
			stream << " (" << e.code << ")";

		return stream;
	}

	/* Originally these were defined as messages in resource file, but have currently simplified this while transitioning to cmake */
	/* Using message resource files in future depends on the need and whether there's a simple cross-platform alternative to the   */
	/* Windows mc.exe, with messages being defined by each library, rather than a big set in a resource dll which everything needs */
	class jadegit_operation_aborted : public jadegit_exception
	{
	public:
		jadegit_operation_aborted();
	};

	class jadegit_unimplemented_feature : public jadegit_exception
	{
	public:
		jadegit_unimplemented_feature();
		jadegit_unimplemented_feature(const std::string& feature);
	};

	class jadegit_unsupported_feature : public jadegit_exception
	{
	public:
		jadegit_unsupported_feature();
		jadegit_unsupported_feature(const std::string& feature);
	};
}