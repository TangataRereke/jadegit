#include "NativeFile.h"
#include "NativeFileIterator.h"
#include <jadegit/vfs/NativeFileSystem.h>
#include <jadegit/Exception.h>
#include <fstream>

namespace fs = std::filesystem;

namespace JadeGit
{
	NativeFile::NativeFile(const NativeFileSystem& fs, const std::filesystem::path& path) : fs(fs), relative_path(path) {}

	NativeFile::~NativeFile() {}

	std::unique_ptr<AnyFile> NativeFile::clone() const
	{
		return std::make_unique<NativeFile>(fs, relative_path);
	}

	const FileSystem* NativeFile::vfs() const
	{
		return &fs;
	}

	fs::path NativeFile::path() const
	{	
		return relative_path;
	}

	bool NativeFile::exists() const
	{
		return fs::exists(fs.base / relative_path);
	}

	bool NativeFile::isDirectory() const
	{
		return fs::is_directory(fs.base / relative_path);
	}

	void NativeFile::signature(std::string& author, std::time_t& modified, size_t lineFrom, size_t lineTo) const
	{
		author = fs.author;

		if (fs.modified)
		{
			modified = fs.modified;
			return;
		}

		// TODO: Use portable solution when file_time_type clock is standardized by C++20
		// https://stackoverflow.com/questions/51273205/how-to-compare-time-t-and-stdfilesystemfile-time-type

		auto filename = fs.base / relative_path;

		#if defined ( _WIN32 )
		{
			struct _stat64 fileInfo;
			if (_wstati64(filename.wstring().c_str(), &fileInfo) != 0)
				throw std::runtime_error("Failed to get last write time.");

			modified = fileInfo.st_mtime;
		}
		#else
		{
			auto fsTime = std::filesystem::last_write_time(filename);
			modified = decltype (fsTime)::clock::to_time_t(fsTime);
		}
		#endif
	}

	std::unique_ptr<AnyFileIterator> NativeFile::begin() const
	{
		return std::make_unique<NativeFileIterator>(clone(), fs::directory_iterator(fs.base / relative_path));
	}

	void NativeFile::createDirectory()
	{
		fs::create_directory(fs.base / relative_path);
	}

	void NativeFile::copy(AnyFile& dest) const
	{
		// Use native method if same filesystem
		if (vfs() == dest.vfs())
			fs::copy(fs.base / relative_path, fs.base / dest.path());

		// Generic implementation
		else
			AnyFile::copy(dest);
	}

	void NativeFile::move(AnyFile& dest)
	{
		// Use native method if same filesystem
		if (vfs() == dest.vfs())
			fs::rename(fs.base / relative_path, fs.base / dest.path());

		// Generic implementation
		else
			AnyFile::move(dest);
	}

	void NativeFile::rename(const fs::path& filename)
	{
		// Copy current filename to compose new when renaming
		fs::path path = relative_path;

		// Rename file
		fs::rename(fs.base / relative_path, fs.base / relative_path.replace_filename(filename));
	}

	void NativeFile::remove()
	{
		fs::remove_all(fs.base / relative_path);
	}

	std::unique_ptr<std::istream> NativeFile::createInputStream(std::ios_base::openmode mode) const
	{
		auto stream = std::make_unique<std::ifstream>(fs.base / relative_path, mode);

		if (stream->fail())
			throw jadegit_exception("Failed to open input file stream [" + path().generic_string() + "]: " + strerror(errno));

		return stream;
	}

	std::unique_ptr<std::ostream> NativeFile::createOutputStream(std::ios_base::openmode mode)
	{
		// Derive entire path
		auto path = fs.base / relative_path;

		// Ensure parent directory exists
		fs::create_directories(path.parent_path());

		// Open output stream
		auto stream = std::make_unique<std::ofstream>(fs.base / relative_path, mode);

		if (stream->fail())
			throw jadegit_exception("Failed to open output file stream [" + relative_path.generic_string() + "]: " + strerror(errno));

		return stream;
	}
}