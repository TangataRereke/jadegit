#include "GitFileSystemSource.h"
#include <jadegit/vfs/File.h>
#include "GitFile.h"
#include "GitFileBlame.h"
#include <assert.h>

using namespace std;
using namespace std::filesystem;

namespace JadeGit
{
	void GitFileSystemSource::add(unique_ptr<GitFileSystemSource> source)
	{
		next ? next->add(move(source)) : next.swap(source);
	}

	unique_ptr<GitFileBlame> GitFileSystemSource::blame(const path& path, const git_oid* oid) const
	{
		if (!next)
			throw jadegit_exception("Failed to identify file source");

		return next->blame(path, oid);
	}

	std::unique_ptr<GitFileBlame> GitFileSystemSource::blame(const unique_ptr<git_commit>& commit, const path& path) const
	{
		git_blame_options options = GIT_BLAME_OPTIONS_INIT;
		options.newest_commit = *git_commit_id(commit.get());

		unique_ptr<git_blame> blame;
		git_throw(git_blame_file(git_ptr(blame), repo, path.generic_string().c_str(), &options));

		return make_unique<GitFileBlame>(move(blame));
	}

	bool GitFileSystemSource::matches(const git_tree* tree, const path& path, const git_oid* id) const
	{
		if (!tree)
			return false;

		// Lookup tree entry
		unique_ptr<git_tree_entry> entry;
		int result = git_tree_entry_bypath(git_ptr(entry), tree, path.generic_string().c_str());

		if (result == GIT_ENOTFOUND)
			return false;
		else
			git_throw(result);

		// Check file id matches tree entry
		return git_oid_equal(id, git_tree_entry_id(entry.get()));
	}

	void GitFileSystemSource::merge(const shared_ptr<git_tree>& src, shared_ptr<git_tree>& dst) const
	{
		// Use src tree directly if no merge required
		if (!dst)
		{
			dst = src;
			return;
		}

		// Perform merge (will fail if there's any unresolved conflicts, which shouldn't happen in this context)
		unique_ptr<git_index> index;
		git_throw(git_merge_trees(git_ptr(index), repo, nullptr, dst.get(), src.get(), nullptr));

		// Write merged index to tree
		// TODO: This tree doesn't need to be kept persistently.  Need to use in-memory repo.
		git_oid id;
		git_throw(git_index_write_tree_to(&id, index.get(), repo));

		// Get merged tree
		git_throw(git_tree_lookup(git_ptr(dst), repo, &id));
	}

	GitFileCommitSource::GitFileCommitSource(unique_ptr<git_commit> commit) : GitFileSystemSource(git_commit_owner(commit.get())), commit(move(commit))
	{
		git_throw(git_commit_tree(git_ptr(tree), this->commit.get()));
	}

	GitFileCommitSource::GitFileCommitSource(unique_ptr<git_reference> reference) : GitFileCommitSource(resolve(move(reference))) {}

	void GitFileCommitSource::merge(std::shared_ptr<git_tree>& dst) const
	{
		GitFileSystemSource::merge(tree, dst);
	}

	unique_ptr<GitFileBlame> GitFileCommitSource::blame(const path& path, const git_oid* id) const
	{
		// Pass onto next source if file id doesn't match tree entry
		if(!matches(tree.get(), path, id))
			return GitFileSystemSource::blame(path, id);

		// Get blame to find out who last modified file
		return GitFileSystemSource::blame(commit, path);
	}

	std::unique_ptr<git_commit> GitFileCommitSource::resolve(const std::unique_ptr<git_reference> reference) const
	{
		// Resolve direct reference
		if (git_reference_type(reference.get()) != git_reference_t::GIT_REFERENCE_DIRECT)
		{
			unique_ptr<git_reference> direct;
			git_throw(git_reference_resolve(git_ptr(direct), reference.get()));
			return resolve(move(direct));
		}

		// Lookup commit
		unique_ptr<git_commit> commit;
		git_throw(git_commit_lookup(git_ptr(commit), git_reference_owner(reference.get()), git_reference_target(reference.get())));
		return commit;
	}

	GitFileTreeSource::GitFileTreeSource(string author, git_oid tree, unique_ptr<git_commit> ours, unique_ptr<git_commit> theirs) : GitFileSystemSource(git_commit_owner(ours.get())),
		author(move(author)),
		ours(move(ours)),
		theirs(move(theirs))
	{
		modified = chrono::system_clock::to_time_t(chrono::system_clock::now());

		git_throw(git_tree_lookup(git_ptr(this->tree), repo, &tree));

		if (ours)
			git_throw(git_commit_tree(git_ptr(this->our_tree), ours.get()));

		if (theirs)
			git_throw(git_commit_tree(git_ptr(this->their_tree), theirs.get()));
	}

	void GitFileTreeSource::merge(std::shared_ptr<git_tree>& dst) const
	{
		GitFileSystemSource::merge(tree, dst);
	}

	unique_ptr<GitFileBlame> GitFileTreeSource::blame(const std::filesystem::path& path, const git_oid* id) const
	{
		// Pass onto next source if file id doesn't match tree entry
		if (!matches(tree.get(), path, id))
			return GitFileSystemSource::blame(path, id);

		// Return blame from relevant parent commit if file matches committed changes
		if (matches(our_tree.get(), path, id))
			return GitFileSystemSource::blame(ours, path);
		if (matches(their_tree.get(), path, id))
			return GitFileSystemSource::blame(theirs, path);

		// Use author/modified timestamp supplied for pending changes
		return make_unique<GitFileBlame>(author, modified);
	}
}