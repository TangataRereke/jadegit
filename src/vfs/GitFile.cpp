#include "GitFile.h"
#include "GitFileBlame.h"
#include "GitFileIterator.h"
#include "GitFileStream.h"
#include "GitFileSystem.h"
#include "GitFileSystemSource.h"
#include <assert.h>

using namespace std;

namespace JadeGit
{
	GitFile::GitFile(const GitFileSystem& fs, const std::filesystem::path& path, const git_oid& id) : fs(fs), relative_path(path), id(id) {}

	GitFile::GitFile(const GitFileSystem& fs, const filesystem::path& path) : GitFile(fs, path, find(fs, path)) {}

	GitFile::~GitFile() {}

	unique_ptr<AnyFile> GitFile::clone() const
	{
		return make_unique<GitFile>(fs, relative_path, id);
	}

	const FileSystem* GitFile::vfs() const
	{
		return &fs;
	}

	filesystem::path GitFile::path() const
	{
		return relative_path;
	}

	bool GitFile::exists() const
	{
		// Return whether oid isn't empty or path is for directory (creation implied when files are created)
		return !git_oid_is_zero(&id) || isDirectory();
	}

	bool GitFile::isDirectory() const
	{
		// Inferred by path not having a file name
		return !relative_path.has_filename();
	}

	void GitFile::signature(std::string& author, std::time_t& modified, size_t lineFrom, size_t lineTo) const
	{
		if(fs.first && !blame)
			blame = fs.first->blame(relative_path, &id);

		if(blame)
			blame->signature(author, modified, lineFrom, lineTo);
	}

	unique_ptr<AnyFileIterator> GitFile::begin() const
	{
		throw jadegit_unimplemented_feature();
	//	return unique_ptr<AnyFileIterator>(new GitFileIterator(vfs, relative_path));
	}

	void GitFile::createDirectory()
	{
		// Directories are implied when files are created
	}

	void GitFile::remove()
	{
		// Can only update via index
		assert(fs.index);

		// Remove directory or file from index
		if(isDirectory())
			git_throw(git_index_remove_directory(fs.index, relative_path.generic_string().c_str(), 0));
		else
			git_throw(git_index_remove_bypath(fs.index, relative_path.generic_string().c_str()));
	}

	unique_ptr<istream> GitFile::createInputStream(ios_base::openmode mode) const
	{
		return make_unique<GitFileStream>(fs, relative_path, mode, id);
	}

	unique_ptr<ostream> GitFile::createOutputStream(ios_base::openmode mode)
	{
		return make_unique<GitFileStream>(fs, relative_path, mode, id);
	}

	git_oid GitFile::find(const GitFileSystem& fs, const std::filesystem::path& relative_path) const
	{
		// Return empty oid for directories, inferred by path not having a file name
		if (!relative_path.has_filename())
			return git_oid();

		// Convert relative path to generic string
		std::string path = relative_path.generic_string();

		// Read from index
		if (fs.index)
		{
			// Lookup current index entry
			const git_index_entry* entry = git_index_get_bypath(fs.index, path.c_str(), 0);

			// If current version wasn't found, check for base version (merge scenario)
			if (!entry)
				entry = git_index_get_bypath(fs.index, path.c_str(), 1);

			// Return entry id if found, otherwise empty oid
			return entry ? entry->id : git_oid();
		}
		// Read from tree
		else if (fs.our_tree)
		{
			// Lookup tree entry
			unique_ptr<git_tree_entry> tree_entry;
			int result = git_tree_entry_bypath(git_ptr(tree_entry), fs.our_tree.get(), path.c_str());

			// Return empty oid if not found
			if (result == GIT_ENOTFOUND)
				return git_oid();
			else
				git_throw(result);

			return *git_tree_entry_id(tree_entry.get());
		}
		// Empty filesystem
		else
		{
			return git_oid();
		}
	}
}