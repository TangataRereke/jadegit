#pragma once
#include "GitFileSystem.h"
#include <sstream>

namespace JadeGit
{
	// TODO: Further work is needed here to inherit from streambuf instead of stringbuf
	// Rather than copying to/from stringbuf, we should be able to use/build a git_buf more directly

	class GitFileBuffer : public std::stringbuf
	{
	public:
		GitFileBuffer(const GitFileSystem& fs, const std::filesystem::path& relative_path, std::ios_base::openmode mode, const git_oid& id) : std::stringbuf(mode), fs(fs), relative_path(relative_path)
		{
			if (mode & std::ios_base::in)
			{
				/* Lookup file blob */
				std::unique_ptr<git_blob> blob;
				git_throw(git_blob_lookup(git_ptr(blob), fs.repo, &id));

				/* Load filters */
				std::unique_ptr<git_filter_list> filters;
				git_throw(git_filter_list_load(git_ptr(filters), fs.repo, blob.get(), relative_path.generic_string().c_str(), git_filter_mode_t::GIT_FILTER_TO_WORKTREE, git_filter_flag_t::GIT_FILTER_DEFAULT));

				/* Load blob into buffer applying filters */
				std::unique_ptr<git_buf> buf = std::make_unique<git_buf>();
				git_throw(git_filter_list_apply_to_blob(buf.get(), filters.get(), blob.get()));

				/* Copy to string buffer */
				str(std::string(buf.get()->ptr, buf.get()->size));
			}
		}

	private:
		const GitFileSystem& fs;
		const std::filesystem::path& relative_path;

		/* Update file when stream is flushed which has to be caused explicitly */
		int sync() override
		{
			/* Convert relative path to generic string */
			std::string path = relative_path.generic_string();

			/* Setup source buffer */
			std::string src = str();

			/* Load filters */
			std::unique_ptr<git_filter_list> filters;
			git_throw(git_filter_list_load(git_ptr(filters), fs.repo, NULL, path.c_str(), git_filter_mode_t::GIT_FILTER_TO_ODB, git_filter_flag_t::GIT_FILTER_DEFAULT));

			/* Apply filters */
			std::unique_ptr<git_buf> buf = std::make_unique<git_buf>();
			git_throw(git_filter_list_apply_to_buffer(buf.get(), filters.get(), src.c_str(), src.size()));

			/* Add index entry */
			git_index_entry entry;
			entry.path = path.c_str();
			entry.mode = GIT_FILEMODE_BLOB;
			entry.flags = GIT_INDEX_ADD_FORCE | GIT_INDEX_ADD_CHECK_PATHSPEC;	/* Required otherwise its ignored because its not in working directory */
			git_throw(git_index_add_from_buffer(fs.index, &entry, buf->ptr, buf->size));

			return 0;
		}
	};

	class GitFileStream : public std::iostream
	{
	public:
		GitFileStream(const GitFileSystem& fs, const std::filesystem::path& path, std::ios_base::openmode mode, const git_oid& id) : std::iostream(&buffer), buffer(fs, path, mode, id) {}

	private:
		GitFileBuffer buffer;
	};
}