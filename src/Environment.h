#pragma once
#include <filesystem>

namespace JadeGit
{
	std::filesystem::path getHomeDirectory();
	std::filesystem::path getLogDirectory();

	std::filesystem::path makeTempDirectory();

	void setLogDirectory(const std::filesystem::path& path);
}