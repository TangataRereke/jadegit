#pragma once
#include "Assembly.h"
#include "DataMapFactory.h"
#include <jadegit/data/RootSchema.h>

namespace JadeGit::Extract
{
	template<typename MetaClass>
	class AnyDataProperty
	{
	public:
		AnyDataProperty(DataMapping::Predicate predicate, const DataTranslator* translator) : predicate(predicate), translator(translator) {}

		DataMapping::Predicate const predicate;
		const DataTranslator* const translator;
		
		virtual Data::Property* property(MetaClass& metaclass) const = 0;
	};

	template <typename MetaClass, typename TProperty = Data::Property>
	class DataProperty : public AnyDataProperty<MetaClass>
	{
	public:
		DataProperty(TProperty* const MetaClass::* member, DataMapping::Predicate predicate = nullptr, const DataTranslator* translator = nullptr) : AnyDataProperty<MetaClass>(predicate, translator), member(member) {}

		Data::Property* property(MetaClass& metaclass) const override
		{
			return member ? metaclass.*member : nullptr;
		}

	private:
		TProperty* const MetaClass::* member;
	};

	template<typename MetaClass>
	class DataMapper : DataMapFactory::Mapper
	{
	public:
		DataMapper(ClassNumber key, MetaClass* const Data::RootSchema::*metaclass, std::initializer_list<std::pair<const JomClassFeatureLevel, const AnyDataProperty<MetaClass>*>> properties = {}) : metaclass(metaclass), properties(properties)
		{
			DataMapFactory::Get().Register(key, this);
		}

		DataMap* Create(Assembly& assembly, DataMap* base) const override
		{
			// Dereference metaclass
			MetaClass* meta = assembly.GetRootSchema().*metaclass;

			// Create mapping
			DataMap* map = new DataMap(base, static_cast<Data::Class*>(*meta));

			// Add explicitly mapped properties
			for (auto prop : properties)
			{
				if (prop.second)
					map->IncludeProperty(prop.first, prop.second->property(*meta), prop.second->predicate, prop.second->translator);
				else
					map->ExcludeProperty(prop.first);
			}

			return map;
		}

	private:
		MetaClass* const Data::RootSchema::*metaclass;
		std::map<JomClassFeatureLevel, const AnyDataProperty<MetaClass>*> properties;
	};
}