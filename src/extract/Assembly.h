#pragma once
#include <jadegit/data/Assembly.h>
#include <jomtypes.h>
#include <map>

namespace JadeGit::Extract
{
	class DataMap;
	class DataMapFactory;

	class Assembly : public Data::Assembly
	{
	public:
		Assembly(const FileSystem& source);
		~Assembly();

	protected:
		void unload() final;

	private:
		friend DataMapFactory;
		std::map<ClassNumber, DataMap*> dataMaps;
	};
}