#include "Assembly.h"
#include <jadegit/Version.h>
#include <jombuild.h>
#include "DataMap.h"

namespace JadeGit::Extract
{
	constexpr Version jadeVersion(JADE_BUILD_VERSION_MAJOR, JADE_BUILD_VERSION_MINOR, JADE_BUILD_VERSION_BUILD);

	Assembly::Assembly(const FileSystem& source) : Data::Assembly(source, jadeVersion)
	{
	}

	Assembly::~Assembly()
	{
		for (auto maps : dataMaps)
			delete maps.second;
	}

	void Assembly::unload()
	{
		// Unload data maps for non-static schemas which are about to be unloaded
		auto it = dataMaps.begin();
		while (it != dataMaps.end())
		{
			if (!it->second->cls->isStatic())
			{
				delete it->second;
				it = dataMaps.erase(it);
			}
			else
				it++;
		}

		Data::Assembly::unload();
	}
}