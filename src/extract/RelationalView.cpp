#pragma once
#include "Entity.h"
#include "Schema.h"
#include "EntityRegistration.h"
#include "DataMapper.h"
#include <jadegit/data/RootSchema/RelationalAttributeMeta.h>
#include <jadegit/data/RootSchema/RelationalTableClassMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMethMeta.h>
#include <jadegit/data/RootSchema/RelationalTableRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalTableXRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalViewMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<RelationalAttributeMeta> relationalAttributeMapper(DSKRELATIONALATTRIBUTE, &RootSchema::relationalAttribute, {
		{PRP_RelationalAttribute_attrName, new DataProperty(&RelationalAttributeMeta::attrName)},
		{PRP_RelationalAttribute_decimals, new DataProperty(&RelationalAttributeMeta::decimals)},
		{PRP_RelationalAttribute_feature, new DataProperty(&RelationalAttributeMeta::feature)},
		{PRP_RelationalAttribute_length, new DataProperty(&RelationalAttributeMeta::length)},
		{PRP_RelationalAttribute_name, new DataProperty(&RelationalAttributeMeta::name)},
		{PRP_RelationalAttribute_referencedClassName, new DataProperty(&RelationalAttributeMeta::referencedClassName)},
		{PRP_RelationalAttribute_rpsColType, new DataProperty(&RelationalAttributeMeta::rpsColType)},
		{PRP_RelationalAttribute_rpsExcluded, new DataProperty(&RelationalAttributeMeta::rpsExcluded)},
		{PRP_RelationalAttribute_rpsKeyKind, new DataProperty(&RelationalAttributeMeta::rpsKeyKind)},
		{PRP_RelationalAttribute_rpsMapKind, new DataProperty(&RelationalAttributeMeta::rpsMapKind)},
		{PRP_RelationalAttribute_sqlType, new DataProperty(&RelationalAttributeMeta::sqlType)}
		});

	static DataMapper<RelationalTableMeta> relationalTableMapper(DSKRELATIONALTABLE, &RootSchema::relationalTable, {
		{PRP_RelationalTable__sourceParcel, nullptr},
		{PRP_RelationalTable_attributes, new DataProperty<RelationalTableMeta>(nullptr)},
		{PRP_RelationalTable_classes, new DataProperty(&RelationalTableMeta::classes)},
		{PRP_RelationalTable_firstClassName, new DataProperty(&RelationalTableMeta::firstClassName)},
		{PRP_RelationalTable_rpsExcluded, new DataProperty(&RelationalTableMeta::rpsExcluded)},
		{PRP_RelationalTable_rpsExtra, new DataProperty(&RelationalTableMeta::rpsExtra)},
		{PRP_RelationalTable_rpsIncludeInCallback, new DataProperty(&RelationalTableMeta::rpsIncludeInCallback)},
		{PRP_RelationalTable_rpsShowMethods, new DataProperty(&RelationalTableMeta::rpsShowMethods)},
		{PRP_RelationalTable_rpsShowObjectFeatures, new DataProperty(&RelationalTableMeta::rpsShowObjectFeatures)},
		{PRP_RelationalTable_rpsShowVirtualProperties, new DataProperty(&RelationalTableMeta::rpsShowVirtualProperties)},
		{PRP_RelationalTable_rpsTableKind, new DataProperty(&RelationalTableMeta::rpsTableKind)}
		});

	static DataMapper<RelationalTableClassMeta> relationalTableClassMapper(DSKRELATIONALTABLECLASS, &RootSchema::relationalTableClass, {
		{PRP_RelationalTableClass_bCallIFAllInstances, new DataProperty(&RelationalTableClassMeta::bCallIFAllInstances)},
		{PRP_RelationalTableClass_rootCollectionName, new DataProperty(&RelationalTableClassMeta::rootCollectionName)}
		});

	static DataMapper<RelationalTableCollectionMeta> relationalTableCollectionMapper(DSKRELATIONALTABLECOLLECTION, &RootSchema::relationalTableCollection, {
		{PRP_RelationalTableCollection_collectionName, new DataProperty(&RelationalTableCollectionMeta::collectionName)},
		{PRP_RelationalTableCollection_collectionType, new DataProperty(&RelationalTableCollectionMeta::collectionType)}
		});

	static DataMapper<RelationalTableCollectionMethMeta> relationalTableCollectionMethMapper(DSKRELATIONALTABLECOLLECTIONMETH, &RootSchema::relationalTableCollectionMeth, {
		{PRP_RelationalTableCollectionMeth_collectionType, new DataProperty(&RelationalTableCollectionMethMeta::collectionType)},
		{PRP_RelationalTableCollectionMeth_methodName, new DataProperty(&RelationalTableCollectionMethMeta::methodName)},
		{PRP_RelationalTableCollectionMeth_secondClassName, new DataProperty(&RelationalTableCollectionMethMeta::secondClassName)}
		});

	static DataMapper<RelationalTableRelationshipMeta> relationalTableRelationshipMapper(DSKRELATIONALTABLERELATIONSHIP, &RootSchema::relationalTableRelationship, {
		{PRP_RelationalTableRelationship_firstPropertyName, new DataProperty(&RelationalTableRelationshipMeta::firstPropertyName)},
		{PRP_RelationalTableRelationship_secondClassName, new DataProperty(&RelationalTableRelationshipMeta::secondClassName)}
		});

	static DataMapper<RelationalTableXRelationshipMeta> relationalTableXRelationshipMapper(DSKRELATIONALTABLEXRELATIONSHIP, &RootSchema::relationalTableXRelationship, {
		{PRP_RelationalTableXRelationship_firstPropertyName, new DataProperty(&RelationalTableXRelationshipMeta::firstPropertyName)},
		{PRP_RelationalTableXRelationship_secondClassName, new DataProperty(&RelationalTableXRelationshipMeta::secondClassName)}
		});

	static DataMapper<RelationalViewMeta> relationalViewMapper(DSKRELATIONALVIEW, &RootSchema::relationalView, {
		{PRP_RelationalView_creator, nullptr},
		{PRP_RelationalView_includedObjectFeatures, new DataProperty(&RelationalViewMeta::includedObjectFeatures)},
		{PRP_RelationalView_tables, new DataProperty<RelationalViewMeta>(nullptr)},
		{PRP_RelationalView_timeCreated, nullptr},
		});

	class RelationalView : public Entity
	{
	public:
		using Entity::Entity;

		void Extract(Assembly& assembly, bool deep) const override
		{
			Entity::Extract(assembly, true);
		}

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_RelationalView_name);
		}

	protected:
	
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema_relationalViews, PRP_Schema_rpsDatabases);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_RelationalView_schema);
		}
	};
	static EntityRegistration<RelationalView> relationalView(DSKRELATIONALVIEW);

	class RelationalTable : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_RelationalTable_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<RelationalView>(ancestor, path, PRP_RelationalView_tables);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_RelationalTable_rView);
		}
	};
	static EntityRegistration<RelationalTable> relationalTable(DSKRELATIONALTABLE);

	class RelationalAttribute : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_RelationalAttribute_name);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<RelationalTable>(ancestor, path, PRP_RelationalTable_attributes);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_RelationalAttribute_table);
		}
	};
	static EntityRegistration<RelationalAttribute> relationalAttribute(DSKRELATIONALATTRIBUTE);
}