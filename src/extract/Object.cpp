#include "Object.h"
#include "ObjectFactory.h"
#include "Assembly.h"
#include <jade/Iterator.h>
#include <jade/Transaction.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/ObjectFactory.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include <Log.h>
#include "DataMapper.h"

using namespace Jade;

namespace JadeGit::Extract
{
	static DataMapper<Data::ObjectMeta> mapper(DSKOBJECT, &Data::RootSchema::object, {
		{PRP_Object__persistentImpRefs, nullptr},
		{PRP_Object__transientImpRefs, nullptr}
		});

	std::string Object::GetTypeName() const
	{
		DskClass klass;
		jade_throw(getClass(klass, __LINE__));
		Character name[101];
		jade_throw(klass.getName(name, __LINE__));

		return narrow(name);
	}

	// Resolves object in assembly, instantiating if required
	Data::Object* Object::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Unless re-implemented, we just instantiate a new object with the view that any previous versions will be discarded 
		return Data::ObjectFactory::Get().Create(GetTypeName(), parent);
	}

	// Copy object to parent, where it'll normally be a child, except where it's self above
	void Object::Copy(Assembly& assembly, Data::Object* parent, const std::string& trail, bool deep, bool self) const
	{
		LOG_DEBUG("Extracting " << GetDisplay());

		Data::Object* target = self ? parent : resolve(assembly, parent, false);

		// Flag object and children as being invalid/dirty (to be cleaned up later if not modified in the meantime)
		target->Dirty(deep);

		// Flag object as modified immediately for originals
		bool original = isOriginal();
		if (original)
			target->Modified();

		// Retrieve class
		DskClass klass;
		jade_throw(getClass(klass, __LINE__));

		// Resolve data mappings
		// TODO: Refactor data map factory to use stack instead of recursion so reference can be returned instead
		DataMap* map = DataMapFactory::Get().Create(klass, assembly);
		assert(map);

		// Copy object details and sub-objects
		map->Copy(assembly, *this, target, trail, deep, original);

		// Clean up any remaining dirty objects
		target->Clean();
	}

	int Object::getProperty(const JomClassFeatureLevel& feature, String* value) const
	{
		DskParamString param;
		jade_throw(getProperty(feature, &param));
		return paramGetString(param, *value);
	}

	void Object::getProperty(const JomClassFeatureLevel& feature, std::set<DskObjectId>& oids) const
	{
		DskObjectId value = NullDskObjectId;
		jade_throw(getProperty(feature, &value, __LINE__));
		
		// Ignore empty references
		if (value.isNull())
			return;

		// Add shared reference directly
		if (!value.isExclusive())
			oids.insert(value);

		// Copy contents of exclusive collection
		else
		{
			DskCollection coll(value);
			DskObject member;
			Iterator<DskObject> iter(coll);
			while (iter.next(member))
				oids.insert(member.oid);
		}		
	}

	Integer64 Object::getUpdateTranID() const
	{
		DskParam pReturn;
		jade_throw(paramSetInteger64(pReturn));
		jade_throw(sendMsg(TEXT("getUpdateTranID"), nullptr, &pReturn));

		Integer64 result = 0;
		jade_throw(paramGetInteger64(pReturn, result));
		return result;
	}

	bool Object::isKindOf(ClassNumber number) const
	{
		bool result = false;
		jade_throw(DskObject::isKindOf(number, &result));
		return result;
	}

	bool Object::isNew() const
	{
		return latestEdition() == 1 && getUpdateTranID() == Transaction::GetTransactionId();
	}

	Edition Object::latestEdition() const
	{
		Edition edition = 0;
		jade_throw(jomGetLatestEdition(nullptr, &oid, &edition, __LINE__));
		return edition;
	}
}