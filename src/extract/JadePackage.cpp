#pragma once
#include "SchemaEntity.h"
#include "Schema.h"
#include "Type.h"
#include "Feature.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/EntityFactory.h>
#include <jadegit/data/RootSchema/JadeExportedPackageMeta.h>
#include <jadegit/data/RootSchema/JadeExportedClassMeta.h>
#include <jadegit/data/RootSchema/JadeExportedInterfaceMeta.h>
#include <jadegit/data/RootSchema/JadeExportedConstantMeta.h>
#include <jadegit/data/RootSchema/JadeExportedMethodMeta.h>
#include <jadegit/data/RootSchema/JadeExportedPropertyMeta.h>
#include <jadegit/data/RootSchema/JadeImportedPackageMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<JadeExportedPackageMeta> jadeExportedPackageMapper(DSKJADEEXPORTEDPACKAGE, &RootSchema::jadeExportedPackage, {
		{PRP_JadeExportedPackage_importedPackageRefs, nullptr}
		});

	static DataMapper<JadeExportedClassMeta> jadeExportedClassMapper(DSKJADEEXPORTEDCLASS, &RootSchema::jadeExportedClass, {
		{PRP_JadeExportedClass_importedClassRefs, nullptr},
		{PRP_JadeExportedClass_persistentAllowed, new DataProperty(&JadeExportedClassMeta::persistentAllowed)},
		{PRP_JadeExportedClass_sharedTransientAllowed, new DataProperty(&JadeExportedClassMeta::sharedTransientAllowed)},
		{PRP_JadeExportedClass_transient, new DataProperty(&JadeExportedClassMeta::transient)},
		{PRP_JadeExportedClass_transientAllowed, new DataProperty(&JadeExportedClassMeta::transientAllowed)}
		});

	static DataMapper<JadeExportedInterfaceMeta> jadeExportedInterfaceMapper(DSKJADEEXPORTEDINTERFACE, &RootSchema::jadeExportedInterface, {
		{PRP_JadeExportedInterface_importedInterfaceRefs, nullptr}
		});

	static DataMapper<JadeExportedConstantMeta> jadeExportedConstantMapper(DSKJADEEXPORTEDCONSTANT, &RootSchema::jadeExportedConstant, {
		{PRP_JadeExportedConstant_importedConstantRefs, nullptr}
		});

	static DataMapper<JadeExportedMethodMeta> jadeExportedMethodMapper(DSKJADEEXPORTEDMETHOD, &RootSchema::jadeExportedMethod, {
		{PRP_JadeExportedMethod_importedMethodRefs, nullptr}
		});

	static DataMapper<JadeExportedPropertyMeta> jadeExportedPropertyMapper(DSKJADEEXPORTEDPROPERTY, &RootSchema::jadeExportedProperty, {
		{PRP_JadeExportedProperty_importedPropertyRefs, nullptr}
		});

	// NOTE: Interfaces are excluded below as imported types are generally inferred from exported types.
	// Imported classes are treated like subschema copies, which are saved to file when local features are added in the importing schema.
	static DataMapper<JadeImportedPackageMeta> jadeImportedPackageMapper(DSKJADEIMPORTEDPACKAGE, &RootSchema::jadeImportedPackage, {
		{PRP_JadeImportedPackage__classesByNumber, nullptr},
		{PRP_JadeImportedPackage_classes, new DataProperty<JadeImportedPackageMeta>(nullptr)},
		{PRP_JadeImportedPackage_exportedPackage, new DataProperty(&JadeImportedPackageMeta::exportedPackage)},
		{PRP_JadeImportedPackage_exportedPackageName, nullptr},
		{PRP_JadeImportedPackage_exportedSchemaName, nullptr},
		{PRP_JadeImportedPackage_incomplete, nullptr},
		{PRP_JadeImportedPackage_interfaces, nullptr}
		});

	static DataMapper<JadeImportedClassMeta> jadeImportedClassMapper(DSKJADEIMPORTEDCLASS, &RootSchema::jadeImportedClass, {
		{PRP_JadeImportedClass_importedConstants, nullptr},
		{PRP_JadeImportedClass_importedMethods, nullptr},
		{PRP_JadeImportedClass_importedProperties, nullptr}
		});

	class JadePackage : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema_exportedPackages, PRP_Schema_importedPackages);
		}
	};
	static EntityRegistration<JadePackage> package(DSKJADEPACKAGE);

	class JadeExportedPackage : public JadePackage
	{
	public:
		using JadePackage::JadePackage;

	protected:
		void dependents(std::set<DskObjectId>& dependents) const
		{
			JadePackage::dependents(dependents);

			getProperty(PRP_JadeExportedPackage_importedPackageRefs, dependents);
		}

		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema_exportedPackages);
		}
	};
	static EntityRegistration<JadeExportedPackage> exportedPackage(DSKJADEEXPORTEDPACKAGE);

	class JadeExportedEntity : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;
	};

	class JadeExportedType : public JadeExportedEntity
	{
	public:
		using JadeExportedEntity::JadeExportedEntity;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExportedPackage>(ancestor, path, PRP_JadeExportedPackage_classes, PRP_JadeExportedPackage_interfaces);
		}
	};

	class JadeExportedClass : public JadeExportedType
	{
	public:
		using JadeExportedType::JadeExportedType;
		using JadeExportedType::resolve;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExportedPackage>(ancestor, path, PRP_JadeExportedPackage_classes);
		}

		Data::JadeExportedClass* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final
		{
			// Resolve class (creating if required)
			Data::Entity* entity = JadeExportedType::resolve(assembly, parent, shallow);
			assert(dynamic_cast<Data::JadeExportedClass*>(entity));

			auto type = static_cast<Data::JadeExportedClass*>(entity);

			// Setup original class if required
			if (!type->originalClass)
			{
				DskObjectId originalClass;
				jade_throw(getProperty(PRP_JadeExportedClass_originalClass, &originalClass));
				if (!originalClass.isNull())
				{
					auto entity = EntityFactory::Get().Create(originalClass);
					type->originalClass = static_cast<Data::Class*>(entity->resolve(assembly));
				}
			}

			return type;
		}
	};
	static EntityRegistration<JadeExportedClass> exportedClass(DSKJADEEXPORTEDCLASS);

	class JadeExportedInterface : public JadeExportedType
	{
	public:
		using JadeExportedType::JadeExportedType;
		using JadeExportedType::resolve;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExportedPackage>(ancestor, path, PRP_JadeExportedPackage_interfaces);
		}

		Data::JadeExportedInterface* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow = true) const final
		{
			// Resolve interface (creating if required)
			Data::Entity* entity = JadeExportedType::resolve(assembly, parent, shallow);
			assert(dynamic_cast<Data::JadeExportedInterface*>(entity));

			auto type = static_cast<Data::JadeExportedInterface*>(entity);

			// Setup original interface if required
			if (!type->originalInterface)
			{
				DskObjectId originalInterface;
				jade_throw(getProperty(PRP_JadeExportedInterface_originalInterface, &originalInterface));
				if (!originalInterface.isNull())
				{
					auto entity = EntityFactory::Get().Create(originalInterface);
					type->originalInterface = static_cast<Data::JadeInterface*>(entity->resolve(assembly));
				}
			}

			return type;
		}
	};
	static EntityRegistration<JadeExportedInterface> exportedInterface(DSKJADEEXPORTEDINTERFACE);

	class JadeExportedFeature : public JadeExportedEntity
	{
	public:
		using JadeExportedEntity::JadeExportedEntity;
	};

	class JadeExportedConstant : public JadeExportedFeature
	{
	public:
		using JadeExportedFeature::JadeExportedFeature;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExportedType>(ancestor, path, PRP_JadeExportedType_exportedConstants);
		}
	};
	static EntityRegistration<JadeExportedConstant> exportedConstant(DSKJADEEXPORTEDCONSTANT);

	class JadeExportedMethod : public JadeExportedFeature
	{
	public:
		using JadeExportedFeature::JadeExportedFeature;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExportedType>(ancestor, path, PRP_JadeExportedType_exportedMethods);
		}
	};
	static EntityRegistration<JadeExportedMethod> exportedMethod(DSKJADEEXPORTEDMETHOD);

	class JadeExportedProperty : public JadeExportedFeature
	{
	public:
		using JadeExportedFeature::JadeExportedFeature;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<JadeExportedType>(ancestor, path, PRP_JadeExportedType_exportedProperties);
		}
	};
	static EntityRegistration<JadeExportedProperty> exportedProperty(DSKJADEEXPORTEDPROPERTY);

	class JadeImportedPackage : public JadePackage
	{
	public:
		using JadePackage::JadePackage;

	protected:
		void children(std::set<DskObjectId>& children) const
		{
			JadePackage::children(children);

			getProperty(PRP_JadeImportedPackage_classes, children);
			getProperty(PRP_JadeImportedPackage_interfaces, children);
		}

		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Schema>(ancestor, path, PRP_Schema_importedPackages);
		}

		Data::Entity* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final
		{
			auto name = getName();
			if (auto entity = Data::EntityFactory::Get().Resolve(GetBasicTypeName(), parent, QualifiedName(name), false, shallow, false))
				return entity->Mutate(GetTypeName());

			// Resolve exported package
			JadeExportedPackage exportedPackage_src;
			jade_throw(getProperty(PRP_JadeImportedPackage_exportedPackage, exportedPackage_src));

			auto exportedPackage = static_cast<Data::JadeExportedPackage*>(exportedPackage_src.resolve(assembly, false));

			// Instantiate new entity
			return new Data::JadeImportedPackage(static_cast<Data::Schema*>(parent), nullptr, name.c_str(), exportedPackage);
		}
	};
	static EntityRegistration<JadeImportedPackage> importedPackage(DSKJADEIMPORTEDPACKAGE);

	class JadeImportedClass : public Class
	{
	public:
		using Class::Class;
		using Class::resolve;

	protected:
		Data::JadeImportedClass* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final
		{
			// Resolve class (creating if required)
			Data::Entity* entity = Class::resolve(assembly, parent, shallow);
			assert(dynamic_cast<Data::JadeImportedClass*>(entity));

			auto type = static_cast<Data::JadeImportedClass*>(entity);

			// Setup exported class if required
			if (!type->superschemaType && !type->exportedClass)
			{
				JadeExportedClass exportedClass;
				jade_throw(getProperty(PRP_JadeImportedClass_exportedClass, exportedClass));
				if (!exportedClass.isNull())
				{
					type->exportedClass = static_cast<Data::JadeExportedClass*>(exportedClass.resolve(assembly));
					type->inferred();
				}
			}

			return type;
		}
	};
	static EntityRegistration<JadeImportedClass> importedClass(DSKJADEIMPORTEDCLASS);

	class JadeImportedInterface : public JadeInterface
	{
	public:
		using JadeInterface::JadeInterface;
		using JadeInterface::resolve;

	protected:
		Data::JadeImportedInterface* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final
		{
			// Resolve interface (creating if required)
			Data::Entity* entity = JadeInterface::resolve(assembly, parent, shallow);
			assert(dynamic_cast<Data::JadeImportedInterface*>(entity));

			auto type = static_cast<Data::JadeImportedInterface*>(entity);

			// Setup exported interface if required
			if (!type->superschemaType && !type->exportedInterface)
			{
				JadeExportedInterface exportedInterface;
				jade_throw(getProperty(PRP_JadeImportedInterface_exportedInterface, exportedInterface));
				if (!exportedInterface.isNull())
				{
					type->exportedInterface = static_cast<Data::JadeExportedInterface*>(exportedInterface.resolve(assembly));
					type->inferred();
				}
			}

			return type;
		}
	};
	static EntityRegistration<JadeImportedInterface> importedInterface(DSKJADEIMPORTEDINTERFACE);
}