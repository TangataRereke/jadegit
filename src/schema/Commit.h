#pragma once
#include "Repository.h"

namespace JadeGit::Schema
{
	class Tree;

	class Commit : public GitObject<git_commit>
	{
	public:
		static Commit lookup(const Repository& repo, const git_oid& oid);
		static Commit lookup(const Repository& repo, std::unique_ptr<git_commit> ptr);
	
		using GitObject::GitObject;

		void parents(DskObjectArray& array) const;
		Repository repo() const;
		bool reset(bool hard, IProgress* progress) const;
		Tree tree() const;

	protected:
		Commit(const Repository& repo, std::unique_ptr<git_commit> ptr);

		static Commit get(const Repository& repo, const git_oid& oid);
	};
}