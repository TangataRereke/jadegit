#pragma once
#include "Repository.h"

namespace JadeGit::Schema
{
	class Reference : public GitObject<git_reference>
	{
	public:
		static Reference lookup(const Repository& repo, const std::string& name);
		static Reference make(const Repository& repo, const std::unique_ptr<git_reference> ptr);

		using GitObject::GitObject;

		std::string path() const;
		Repository repo() const;
		std::string shorthand() const;

	protected:
		Reference(ClassNumber classNo, const Repository& repo, std::unique_ptr<git_reference> ptr);

	private:
		Reference(const Repository& repo, std::unique_ptr<git_reference> ptr);
	};
}