#include "Exception.h"
#include "ObjectRegistration.h"
#include <jade/ExceptionHandler.h>
#include <Log.h>
#include <sstream>

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitException> registration(TEXT("GitException"));

	GitException::GitException(const char* message) : Object(registration)
	{
		LOG_ERROR(message);

		createTransientObject();
		setProperty(TEXT("errorCode"), 64000);						// TODO: Attempt to parse message for error code
		setProperty(TEXT("errorText"), std::string(message));
	}

	GitException::GitException(std::runtime_error& ex, ExceptionCause cause) : GitException(ex.what())
	{
		jomRaiseException(nullptr, &oid, cause, __LINE__);
	}

	int JOMAPI jadegit_exception_handler(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return Jade::ExceptionHandler::handler(pBuffer, pParams, pReturn);
	}
}