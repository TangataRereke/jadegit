#include "Repository.h"
#include "Schemas.h"
#include <Environment.h>

using namespace std;

namespace JadeGit::Schema::Install
{
	unique_ptr<git_repository> fetch()
	{
		// Use temp folder
		auto path = makeTempDirectory() / "schemas";

		unique_ptr<git_repository> repo;

		// Try to open existing
		int result = git_repository_open_bare(git_ptr(repo), path.string().c_str());
		if (result == GIT_ENOTFOUND)
		{
			// Clone repository
			git_clone_options opts = GIT_CLONE_OPTIONS_INIT;
			opts.bare = 1;
			opts.fetch_opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
			git_throw(git_clone(git_ptr(repo), schemas_repo, path.string().c_str(), &opts));
			return repo;
		}
		git_throw(result);

		// Lookup remote
		unique_ptr<git_remote> remote;
		git_throw(git_remote_lookup(git_ptr(remote), repo.get(), "origin"));

		// Fetch updates
		git_fetch_options opts = GIT_FETCH_OPTIONS_INIT;
		opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
		git_throw(git_remote_fetch(remote.get(), nullptr, &opts, nullptr));
		
		return repo;
	}
}