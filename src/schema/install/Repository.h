#pragma once
#include <jadegit/git2.h>

namespace JadeGit::Schema::Install
{
	std::unique_ptr<git_repository> fetch();
}