#pragma once
#include <string>

namespace JadeGit::Schema::Install
{
	class GitGlobal
	{
	public:
		static std::string getRevision();
		static void setRevision(std::string commit);
	};
}