#include "Install.h"
#include "Deployment.h"
#include "Global.h"
#include "Repository.h"
#include "Schemas.h"
#include <deploy/Director.h>
#include <mutex>

using namespace std;
using namespace JadeGit::Deploy;
using namespace JadeGit::Schema::Install;

namespace JadeGit::Schema
{
	bool installed = false;
	mutex installMutex;

	void install()
	{
		// Quick install check
		if (installed)
			return;

		// Lock mutex before carrying out install
		unique_lock<mutex> lock(installMutex);

		// Double check another thread hasn't just performed install
		if (installed)
			return;

		// Get version currently installed
		string current = GitGlobal::getRevision();

		// Suppress install if schema is already up-to-date
		if (current == schemas_commit)
			return;

		// Fetch repository
		auto repo = fetch();

		// Build deployment
		Deployment deployment;
		Deploy::Director director(*repo, deployment, nullptr);
		director.build(current, schemas_commit);

		// Install
		deployment.execute();

		// Update version installed
		GitGlobal::setRevision(schemas_commit);

		// Set installed flag
		installed = true;
	}
}