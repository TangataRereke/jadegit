#include "Deployment.h"
#include <jade/Loader.h>
#include <jadegit/vfs/File.h>
#include <jadegit/Exception.h>
#include <assert.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema::Install
{
	class DeploymentCommand
	{
	public:
		virtual void execute(Loader& loader) = 0;
	};

	class LoadFile : public DeploymentCommand
	{
	public:
		LoadFile(filesystem::path path, bool latest) : path(path), latest(latest) {}

	protected:
		filesystem::path path;
		bool latest = false;
	};

	class LoadCommandFile : public LoadFile
	{
	public:
		using LoadFile::LoadFile;

	private:
		void execute(Loader& loader) final
		{
			loader.loadCommandFile(path, latest);
		}
	};

	class LoadSchemaFile : public LoadFile
	{
	public:
		using LoadFile::LoadFile;

	private:
		void execute(Loader& loader) final
		{
			loader.loadSchemaFile(path, filesystem::path(), latest);
		}
	};

	class LoadSchemaDataFile : public LoadFile
	{
	public:
		using LoadFile::LoadFile;

	private:
		void execute(Loader& loader) final
		{
			loader.loadSchemaFile(filesystem::path(), path, latest);
		}
	};

	class ReorgCommand : public DeploymentCommand
	{
	private:
		void execute(Loader& loader) final
		{
			loader.reorg(TEXT("JadeGitSchema"));
		}
	};

	Deployment::Deployment() : fs("install") 
	{
		fs.purge();
	}

	Deployment::~Deployment() {}

	void Deployment::execute()
	{
		Loader loader;

		for (auto& command : commands)
			command->execute(loader);
	}

	void Deployment::startStage(string currentCommit, string latestCommit)
	{
		assert(!stage);
		stage = ++stages;
	}

	void Deployment::finishStage()
	{
		assert(stage);

		if (needsReorg)
			Reorg();

		stage = 0;

		// TODO: Update schema version at the end of each stage
	}

	void Deployment::changeLoadStyle(bool latestVersion)
	{
		// Reset flag indicating re-org has just been added
		reorging = false;

		// Set flag indicating reorg is needed
		if (latestVersion)
			needsReorg = true;

		// Perform re-org before loading into current schema version
		else if (needsReorg)
			Reorg();
	}

	unique_ptr<ostream> Deployment::AddCommandFile(bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		auto file = makeFile("Commands", "jcf");
		commands.push_back(make_unique<LoadCommandFile>(fs.path() / file.path(), latestVersion));
		return file.createOutputStream();
	}

	unique_ptr<ostream> Deployment::AddSchemaFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		auto file = makeFile(schema, "scm");
		commands.push_back(make_unique<LoadSchemaFile>(fs.path() / file.path(), latestVersion));
		return file.createOutputStream();
	}

	unique_ptr<ostream> Deployment::AddSchemaDataFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		auto file = makeFile(schema, "ddx");
		commands.push_back(make_unique<LoadSchemaDataFile>(fs.path() / file.path(), latestVersion));
		return file.createOutputStream();
	}

	void Deployment::addScript(const Build::Script& script)
	{
		throw jadegit_unimplemented_feature("Deploying scripts during install");
	}

	File Deployment::makeFile(const string& schema, const char* extension)
	{
		assert(stage);

		// Increment file counter
		files++;

		// Derive filename
		stringstream filename;
		filename << "stage-" << setfill('0') << setw(2) << stage << "/" << setfill('0') << setw(2) << files << "-" << schema << "." << extension;

		// Open file
		File file = fs.open(filename.str());

		// Check file doesn't already exist
		if (file.exists())
			throw jadegit_exception("File already exists (" + filename.str() + ")");

		return file;
	}

	void Deployment::Reorg()
	{
		// Suppress duplicate re-orgs
		if (reorging)
			return;

		commands.push_back(make_unique<ReorgCommand>());

		// Set flag indicating we've just added re-org
		reorging = true;

		// Reset flag indicating reorg is needed
		needsReorg = false;
	}
}