#pragma once
#include <deploy/DeploymentBuilder.h>
#include <vfs/TempFileSystem.h>
#include <vector>

namespace JadeGit::Schema::Install
{
	class DeploymentCommand;

	class Deployment : public Deploy::DeploymentBuilder
	{
	public:
		Deployment();
		~Deployment();

		void execute();

	private:
		TempFileSystem fs;
		std::vector<std::unique_ptr<DeploymentCommand>> commands;
		
		int stage = 0;
		int stages = 0;
		int files = 0;
		bool latestVersion = false;
		bool needsReorg = false;
		bool reorging = false;

		void startStage(std::string currentCommit, std::string latestCommit) final;
		void finishStage() final;

		void changeLoadStyle(bool latestVersion);
		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) final;
		void addScript(const Build::Script& script) final;
		File makeFile(const std::string& schema, const char* extension);
		void Reorg() final;
	};
}