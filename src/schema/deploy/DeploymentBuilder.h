#pragma once
#include "Deployment.h"
#include <build/Builder.h>
#include <schema/data/Repository.h>

namespace JadeGit::Schema
{
	class DeploymentBuilder : public Build::Builder
	{
	public:
		DeploymentBuilder(const IGitRepositoryData &data);

		const std::shared_ptr<GitDeployment> deployment;

	protected:
		DeploymentBuilder(std::shared_ptr<GitDeployment> deployment, const IGitRepositoryData& data);

		RepositoryData jade_repo;

		void RegisterSchema(const std::string& name, bool registering);
		void RegisterSchema(const std::string& schema) override { RegisterSchema(schema, true); }
		void DeregisterSchema(const std::string& schema) override { RegisterSchema(schema, false); }

		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) override;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) override;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) override;

		void addScript(const Build::Script& script) final;

		void Reorg() override;
		void Flush(bool final) override;

	private:
		bool needsReorg = false;
		bool reorging = false;
		DskObjectId schemaFile = NullDskObjectId;
	};
}