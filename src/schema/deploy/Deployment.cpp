#include "Deployment.h"
#include "DeploymentCommand.h"
#include <jadegit/Progress.h>
#include <jade/Iterator.h>
#include <jade/Loader.h>
#include <jade/Transaction.h>
#include <schema/data/Repository.h>
#include <schema/data/Schema.h>
#include <schema/data/User.h>
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>
#include <schema/Task.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitDeployment> registration(TEXT("GitDeployment"));

	GitDeployment::GitDeployment() : GitDeploymentCommand(registration) {}

	bool GitDeployment::abort(IProgress* progress)
	{
		// Ensure we aren't trying to abort a completed deployment
		if (isCompleted())
			throw jadegit_exception("Deployment has already been completed");

		if (progress && !progress->start(1, "Aborting"))
			return false;

		// Start transaction
		Transaction transaction;

		// Setup loader
		Loader loader;

		// Abort
		bool done = false;
		if (!abort(transaction, loader, done, progress))
			return false;

		// Clean-up deployment
		deleteObject();

		// Success (kind of)
		transaction.commit();
		return (!progress || progress->finish());
	}

	bool GitDeployment::abort(Transaction& transaction, Loader& loader, bool& done, IProgress* progress) const
	{
		// Suppress attempts to abort sub-deployments which have already been completed in their own right
		// TODO: Need to commit & restart transactions when sub-deployments have completed in their own right
		// This doesn't yet apply to the current builds, but will apply to those which affect multiple repositories
		// where a sub-module/repository deployment is needed in support of another repository deployment. 
		if (isCompleted())
		{
			done = true;
			return (!progress || progress->skip());
		}

		// Start execution (first attempt only)
		if (!isAborting())
			abortEnter();

		Collection<> children(*this, TEXT("children"));
		if (progress && !progress->start(children.size()))
			return false;

		// Process children in reverse
		DskObject child;
		Iterator<DskObject> iter(children);
		while (iter.back(child))
		{
			if (done)
			{
				if (progress && !progress->skip())
					return false;
			}
			else
			{
				if (!GitObjectFactory::get().create<GitDeploymentCommand>(child.oid)->abort(transaction, loader, done, progress))
					return false;
			}
		}

		// Finish execution
		abortExit();
		assert(isAborted() && !isAborting());

		return (!progress || progress->finish());
	}

	bool GitDeployment::execute(IProgress* progress)
	{
		if (progress && !progress->start(1, "Deploying"))
			return false;
		
		// Lock deployment ?

		// Start first transaction (committed in stages)
		Transaction transaction;

		// Setup loader
		Loader loader;
		
		// Execute
		if (!execute(transaction, loader, progress))
			return false;

		// Clean-up deployment
		deleteObject();

		// Success
		transaction.commit();
		return (!progress || progress->finish());
	}

	bool GitDeployment::execute(Transaction& transaction, Loader& loader, IProgress* progress) const
	{
		// Ensure we aren't trying to restart an aborted deployment
		if (isAborted())
			throw jadegit_exception("Deployment has already been aborted");

		// Start execution (first attempt only)
		if (!isExecuting())
			executeEnter();

		Collection<> children(*this, TEXT("children"));
		if (progress && !progress->start(children.size()))
			return false;

		// Process children
		DskObject child;
		Iterator<DskObject> iter(children);
		while (iter.next(child))
		{
			if (!GitObjectFactory::get().create<GitDeploymentCommand>(child.oid)->execute(transaction, loader, progress))
				return false;
		}

		// Finish execution
		executeExit();
		assert(isCompleted() && !isExecuting());

		return (!progress || progress->finish());
	}

	void GitDeployment::executeExit() const
	{
		GitDeploymentCommand::executeExit();
		
		// Process schema deregistrations
		GitSchema schema;
		Iterator<GitSchema> schemas(*this, TEXT("schemas"));
		while (schemas.next(schema))
		{
			// Get intended repository
			RepositoryData repo;
			schema.getProperty(TEXT("repo"), repo);

			// Delete schemas if original is found (which means we're deregistering)
			GitSchema original;
			if (repo.GetSchema(schema.GetName().c_str(), original))
			{
				original.deleteObject();
				schema.deleteObject();
			}
		}

		// Process schema registrations
		jade_throw(schemas.reset());
		while (schemas.next(schema))
		{
			// Reset deployment reference (which makes it active)
			jade_throw(schema.setProperty(TEXT("deployment"), NullDskObjectId));
		}

		// Reset unstable repository state
		RepositoryData repo;
		getProperty(TEXT("repo"), repo);
		if (!repo.isNull())
			repo.SetState(repo.GetState() & ~RepositoryData::Unstable);
	}

	void GitDeployment::GetRepository(RepositoryData& repo) const
	{
		getProperty(TEXT("repo"), repo);
		if (repo.isNull())
			GitDeploymentCommand::GetRepository(repo);
	}

	void GitDeployment::GetUser(User& user) const
	{
		getProperty(TEXT("user"), user);
		if (user.isNull())
			GitDeploymentCommand::GetUser(user);
	}

	int JOMAPI jadegit_deployment_abort(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		DskObjectId oid(pBuffer->oid);
		return Task::make(pReturn, [oid](IProgress* progress) { return GitObjectFactory::get().create<GitDeployment>(oid)->abort(progress); });
	}

	int JOMAPI jadegit_deployment_retry(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		DskObjectId oid(pBuffer->oid);
		return Task::make(pReturn, [oid](IProgress* progress) { return GitObjectFactory::get().create<GitDeployment>(oid)->execute(progress); });
	}
}