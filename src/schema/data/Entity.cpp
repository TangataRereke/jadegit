#include "Entity.h"
#include "ChangeOperation.h"
#include "ChangeSet.h"
#include "EntitySet.h"
#include "Repository.h"
#include "Schema.h"
#include "User.h"
#include <extract/EntityFactory.h>
#include <jade/AppContext.h>
#include <jade/Iterator.h>
#include <jade/Transaction.h>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/EntityFactory.h>
#include <schema/ObjectRegistration.h>
#include <schema/Session.h>
#include <schema/Workers.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Extract;

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitEntity> registration(TEXT("GitEntity"));

	GitEntity::GitEntity() : Object(registration) {}

	GitEntity::GitEntity(const GitEntity& parent, ClassNumber kind, const string& name, const DskObjectId* prior) : GitEntity()
	{
		jade_throw(createObject());
		jade_throw(setProperty(TEXT("kind"), kind));
		setProperty(TEXT("name"), name);
		jade_throw(setProperty(TEXT("parent"), parent));
		GitChangeSet(*this).instantiate();
		GitEntitySet(*this).instantiate();

		if(prior)
			jade_throw(setProperty(TEXT("prior"), prior));
	}

	GitEntity::GitEntity(const GitEntity& parent, ClassNumber kind, const string& name) : GitEntity(parent, kind, name, nullptr)
	{
		// Link to prior version if parent is versioned
		if (auto prior = parent.prior())
			jade_throw(setProperty(TEXT("prior"), *GitEntity::resolver(*prior, kind, ref(name))));
	}

	ClassNumber GitEntity::GetKind() const
	{
		ClassNumber result;
		getProperty(TEXT("kind"), &result);
		return result;
	}

	string GitEntity::GetKindName() const
	{
		// Lookup class name based on number
		// TODO: Change to use mappings defined by extract factory registrations?

		DskSchema rootSchema(&RootSchemaOid);
		DskClass klass;
		jade_throw(rootSchema.getClassByNumber(GetKind(), klass));

		Character buffer[101];
		jade_throw(klass.getName(buffer));
		return narrow(buffer);
	}

	string GitEntity::GetName() const
	{
		Character buffer[101];
		getProperty(TEXT("name"), buffer);
		return narrow(buffer);
	}

	unique_ptr<QualifiedName> GitEntity::GetQualifiedName() const
	{
		unique_ptr<QualifiedName> result = make_unique<QualifiedName>(GetName());
		assert(!result->parent);

		GitEntity parent;
		getProperty(TEXT("parent"), parent);
		if (!parent.isNull())
			result->parent = parent.GetQualifiedName();

		return result;
	}

	unique_ptr<Extract::Entity> GitEntity::GetSource(bool required) const
	{
		unique_ptr<QualifiedName> path = GetQualifiedName();
		return Extract::EntityFactory::Get().resolve(GetKind(), *path.get(), required);
	}

	Data::Entity* GitEntity::GetTarget(Data::Assembly &assembly) const
	{
		unique_ptr<QualifiedName> path = GetQualifiedName();
		return Data::EntityFactory::Get().Resolve<Data::Entity>(GetKindName(), &assembly, path.get(), false, false, false);
	}

	GitChange GitEntity::getChange() const
	{
		// Get all changes for the branch
		vector<GitChange> changes;
		getChanges(changes);

		// Get most recent change which has valid operation
		for (auto it = changes.rbegin(); it != changes.rend(); ++it)
		{
			if (!it->isVoid())
				return *it;
		}

		return GitChange();
	}

	void GitEntity::getChanges(vector<GitChange>& changes) const
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
			return Backend::workers.push(bind(&GitEntity::getChanges, this, ref(changes))).get();

		GitChange change;
		Iterator<GitChange> iter(*this, TEXT("changes"));
		while (iter.next(change))
			changes.push_back(change);
	}

	void GitEntity::getChildChanges(const WorktreeData& worktree, std::vector<GitChange> &changes) const
	{
		/* Redirect to backend thread if required */
		if (AppContext::IsSystem())
			return Backend::workers.push(std::bind(&GitEntity::getChildChanges, this, std::ref(worktree), std::ref(changes))).get();

		GitEntity child;
		Iterator<GitEntity> children(*this, TEXT("children"));
		while (children.next(child))
		{
			GitChange change;
			Iterator<GitChange> iter(child, TEXT("changes"));
			while (iter.next(change))
			{
				if (change.isWorktree(worktree))
					changes.push_back(change);
			}

			child.getChildChanges(worktree, changes);
		}
	}

	unique_ptr<GitEntity> GitEntity::GetChild(ClassNumber kind, const string& name) const
	{
		DskMemberKeyDictionary children;
		getProperty(TEXT("children"), children);

		DskParam pKind;
		paramSetInteger(pKind, kind);

		DskParamCString pName(name);
		
		ShortDskParam keys;
		paramSetParamList(keys, &pKind, &pName);

		DskParam pResult;
		paramSetOid(pResult);
		jade_throw(children.sendMsg(MTH_Dictionary_getAtKey, (DskParam*)&keys, &pResult));

		DskObjectId oid;
		paramGetOid(pResult, oid);

		if (oid.isNull())
			return nullptr;

		return make_unique<GitEntity>(oid);
	}

	RepositoryData GitEntity::repo() const
	{
		auto p = parent();
		return p ? p->repo() : RepositoryData();
	}

	unique_ptr<GitEntity> GitEntity::parent() const
	{
		DskObjectId parent;
		getProperty(TEXT("parent"), parent);

		if (parent.isNull())
			return nullptr;

		return GitObjectFactory::get().create<GitEntity>(parent);
	}

	unique_ptr<GitEntity> GitEntity::makeNext() const
	{
		// Return next version if it's already been instantiated
		if (auto next = this->next())
			return next;

		// Make next version for parent
		auto parent = this->parent()->makeNext();

		// Instantiate next version
		return make_unique<GitEntity>(*parent, GetKind(), GetName(), &oid);
	}

	unique_ptr<GitEntity> GitEntity::resolve(const Extract::Entity& source, bool dryrun)
	{
		return resolve(source, source.getName(), dryrun);
	}

	unique_ptr<GitEntity> GitEntity::resolve(const Extract::Entity& source, const string& name, bool dryrun)
	{
		ClassNumber kind = source.GetKind();
		if (kind == DSKSCHEMA)
			return GitSchema::resolve(source, name);

		// Resolve parent
		unique_ptr<GitEntity> parent = resolve(*source.GetParent(), dryrun);
		if (!parent)
			return nullptr;

		// Ignore changes covered by parent creation (deep extract)
		auto change = parent->getChange();
		if (change.isAdd() && !change.isStaged())
			return nullptr;

		// Resolve child
		if (!dryrun && AppContext::IsSystem())
			return Backend::workers.push(bind(&GitEntity::resolver, ref(*parent), kind, ref(name), dryrun)).get();
		else
			return GitEntity::resolver(*parent, kind, name, dryrun);
	}

	unique_ptr<GitEntity> GitEntity::resolver(const GitEntity& parent, ClassNumber kind, const string& name, bool dryrun)
	{
		// Return existing
		unique_ptr<GitEntity> child = parent.GetChild(kind, name);
		if (child)
			return child;

		// Suppress creation during dry-run
		if (dryrun)
			return nullptr;

		// Create child
		Transaction transaction;
		child = make_unique<GitEntity>(parent, kind, name);
		transaction.commit();

		return child;
	}

	void GitEntity::transition(const GitEntity& parent)
	{
		// Get next version to transition to
		auto next = this->next();

		// Move this to replacement parent if next version doesn't exist
		if (!next)
		{
			jade_throw(setProperty(TEXT("parent"), parent));
			return;
		}

		transition(parent, *next);
	}

	void GitEntity::transition(const GitEntity& parent, const GitEntity& next)
	{
		// Sanity check
		assert(parent.isNull() || next.isParent(parent));

		// Transition changes
		GitChange change;
		Iterator<GitChange> changes(*this, TEXT("changes"));
		while (changes.next(change))
		{
			if (change.isLatest(*this))
				change.setLatest(next);

			if (change.isPrevious(*this))
				change.setPrevious(next);
		}

		// Transition children
		GitEntity child;
		Iterator<GitEntity> children(*this, TEXT("children"));
		while (children.next(child))
		{
			child.transition(next);
		}

		// Break version link & delete self
		jade_throw(next.setProperty(TEXT("prior"), NullDskObjectId));
		jade_throw(deleteObject());
	}

	void GitEntity::updateAssociates(const Extract::Entity& source)
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
			return Backend::workers.push(bind(&GitEntity::updateAssociates, this, ref(source))).get();

		set<DskObjectId> sourceAssociates;
		source.getAssociates(sourceAssociates);

		Transaction transaction;

		GitEntitySet associates(*this);
		associates.clear();

		bool versioned = isVersioned();

		for (auto& sourceAssociate : sourceAssociates)
		{
			auto associate = GitEntity::resolve(*EntityFactory::Get().Create(sourceAssociate));
			associates.add(!versioned || associate->isVersioned() ? associate.get() : associate->makeNext().get());
		}

		transaction.commit();
	}

	void GitEntity::cloneChildren(const GitEntity& counterpart, map<DskObjectId, DskObjectId> &directory) const
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
			return Backend::workers.push(bind(&GitEntity::cloneChildren, this, ref(counterpart), ref(directory))).get();

		Transaction transaction;

		// Iterate children
		GitEntity child;
		Iterator<GitEntity> children(*this, TEXT("children"));
		while (children.next(child))
		{
			// Clone immediate child
			unique_ptr<GitEntity> clone = resolver(counterpart, child.GetKind(), child.GetName());

			// Add to directory
			directory[child.oid] = clone->oid;

			// Clone grandchildren
			child.cloneChildren(*clone, directory);
		}

		transaction.commit();
	}

	void GitEntity::cloneChanges(const WorktreeData& worktree, std::vector<GitChange> &source, std::map<DskObjectId, DskObjectId> &entities, std::map<DskObjectId, DskObjectId> &changes) const
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
			return Backend::workers.push(std::bind(&GitEntity::cloneChanges, this, std::ref(worktree), std::ref(source), std::ref(entities), std::ref(changes))).get();

		Transaction transaction;

		// Clone changes
		for (GitChange src : source)
		{
			GitEntity latest;
			GitEntity previous;
			src.getLatest(latest);
			src.getPrevious(previous);

			if (!latest.isNull())
			{
				auto iter = entities.find(latest.oid);
				if (iter != entities.end())
					latest.oid = iter->second;
			}

			if (!previous.isNull())
			{
				auto iter = entities.find(previous.oid);
				if (iter != entities.end())
					previous.oid = iter->second;
			}

			GitChange dst(worktree, &previous, &latest);
			changes[src.oid] = dst.oid;
		}

		// Clone relationships
		for (GitChange src : source)
		{
			GitChange dst(&changes[src.oid]);

			// Copy predecessor references, converting to replacements created above
			GitChange predecessor;
			Iterator<GitChange> predecessors(src, TEXT("predecessors"));
			while(predecessors.next(predecessor))
			{
				auto it = changes.find(predecessor.oid);
				if (it != changes.end())
					predecessor.oid = it->second;

				dst.successorOf(predecessor);
			}

			// Add replacement predecessors to successors, which weren't replaced above
			GitChange successor;
			Iterator<GitChange> successors(src, TEXT("successors"));
			while(successors.next(successor))
			{
				auto it = changes.find(successor.oid);
				if (it == changes.end())
					dst.predecessorOf(successor);
			}
		}

		transaction.commit();
	}
}
