#pragma once
#include "Entity.h"

namespace JadeGit::Schema
{
	class RepositoryData;

	class GitSchema : public GitEntity
	{
	public:
		// Creates schema without setting repository which implies it's been excluded
		static void exclude(const std::string& name);

		// Forget schema previously excluded
		static void forget(const std::string& name);

		// Determine if schema has been registered (excluded or associated with repository)
		static bool isRegistered(const std::string& name);

		// Resolves schema
		static std::unique_ptr<GitSchema> resolve(const Extract::Entity& source, const std::string& name);

		// Handles schema transition when selected, if required
		static void selected(const std::string& name);

		// Retrieve schema status
		enum class Status
		{
			Unknown,
			Excluded,
			Included,
			Blocked
		};
		static Status status(const std::string& name);

		using GitEntity::GitEntity;
		GitSchema();
		GitSchema(const std::string& name);
		GitSchema(const std::string& name, const RepositoryData& repo);
		GitSchema(const std::string& name, const RepositoryData& repo, const GitSchema& prior);

		// Retrieves associated repository
		RepositoryData repo() const override;

	private:
		static std::unique_ptr<GitSchema> resolver(const std::string& name, ObjectVersionState versioned, const DskObjectId& current, const DskObjectId& latest);
	
		using GitEntity::transition;
		static bool transition(std::unique_ptr<GitSchema>& schema, const DskSchema& rootSchema);
		static bool transition(std::unique_ptr<GitSchema>& schema, const RepositoryData& repo, const std::string& name, ObjectVersionState versioned, const DskObjectId& current, const DskObjectId& latest);
	};
}