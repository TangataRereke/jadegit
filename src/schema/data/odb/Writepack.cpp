#include "Writepack.h"
#include <jade/Transaction.h>
#include <jadegit/git2.h>
#include <Environment.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema::Backend
{
	template <typename T>
	static int wrapper(git_odb_writepack* writepack, T func)
	{
		try
		{
			return func(*static_cast<jadegit_odb_writepack*>(writepack));
		}
		catch (std::runtime_error& e)
		{
			git_error_set_str(GIT_ERROR_ODB, e.what());
		}

		return GIT_ERROR;
	}

	int jadegit_odb_backend__writepack_append(git_odb_writepack* writepack, const void* data, size_t size, git_indexer_progress* stats)
	{
		return git_indexer_append(static_cast<jadegit_odb_writepack*>(writepack)->indexer.get(), data, size, stats);
	}

	int jadegit_odb_backend__writepack_commit_cb(const git_oid* oid, void* payload)
	{
		auto writepack = static_cast<jadegit_odb_writepack*>(payload);

		if (git_odb_exists(writepack->odb, oid))
			return GIT_OK;

		int error = 0;
		unique_ptr<git_odb_object> obj;
		if ((error = git_odb_read(git_ptr(obj), writepack->pack_odb.get(), oid)) < 0)
			return error;

		return writepack->backend->write(writepack->backend, oid, git_odb_object_data(obj.get()), git_odb_object_size(obj.get()), git_odb_object_type(obj.get()));
	}

	int jadegit_odb_backend__writepack_commit(git_odb_writepack* writepack, git_indexer_progress* stats)
	{
		return wrapper(writepack, [&](jadegit_odb_writepack& writepack)
			{
				int error = 0;
				if ((error = git_indexer_commit(writepack.indexer.get(), stats)) < 0)
					return error;

				if ((error = git_odb_new(git_ptr(writepack.pack_odb))) < 0)
					return error;

				git_odb_backend* pack_backend = nullptr;
				if ((error = (git_odb_backend_one_pack(&pack_backend, (makeTempDirectory() / (string("pack-") + git_indexer_name(writepack.indexer.get()) + ".idx")).generic_string().c_str()))) < 0)
					return error;

				if ((error = git_odb_add_backend(writepack.pack_odb.get(), pack_backend, 0)) < 0)
					return error;

				Transaction transaction;

				if ((error = git_odb_foreach(writepack.pack_odb.get(), jadegit_odb_backend__writepack_commit_cb, &writepack)) < 0)
					return error;

				transaction.commit();

				return static_cast<int>(GIT_OK);
			});
	}

	void jadegit_odb_backend__writepack_free(git_odb_writepack* writepack)
	{
		delete static_cast<jadegit_odb_writepack*>(writepack);
	}

	jadegit_odb_writepack::jadegit_odb_writepack(git_odb_backend* backend, git_odb* odb, unique_ptr<git_indexer> indexer) : odb(odb), indexer(move(indexer))
	{
		git_odb_writepack::backend = backend;
		git_odb_writepack::append = jadegit_odb_backend__writepack_append;
		git_odb_writepack::commit = jadegit_odb_backend__writepack_commit;
		git_odb_writepack::free = jadegit_odb_backend__writepack_free;
	}

	int jadegit_odb_writepack::make(git_odb_writepack** pack, git_odb_backend* backend, git_odb* odb, git_indexer_progress_cb progress_cb, void* progress_payload)
	{
		git_indexer_options opts = GIT_INDEXER_OPTIONS_INIT;
		opts.progress_cb = progress_cb;
		opts.progress_cb_payload = progress_payload;

		unique_ptr<git_indexer> indexer;
		int result = 0;
		if ((result = git_indexer_new(git_ptr(indexer), makeTempDirectory().generic_string().c_str(), 0, odb, &opts)) < 0)
			return result;

		*pack = new jadegit_odb_writepack(backend, odb, move(indexer));
		return GIT_OK;
	}
}