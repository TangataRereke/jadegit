#pragma once
#include <schema/Object.h>
#include <memory>

namespace JadeGit::Schema
{
	class GitSchema;

	class Root : public Object
	{
	public:
		static const Root& get();

		bool deploying() const;
		std::unique_ptr<GitSchema> getSchema(const std::string& name) const;

	private:
		Root();
	};
}