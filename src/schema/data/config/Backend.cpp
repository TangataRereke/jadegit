#include "Backend.h"
#include "Entry.h"
#include "Iterator.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema::Backend
{
	template <typename T>
	static int wrapper(git_config_backend* backend, T func)
	{
		try
		{
			return func(*static_cast<jadegit_config_backend*>(backend));
		}
		catch (std::runtime_error& e)
		{
			git_error_set_str(GIT_ERROR_CONFIG, e.what());
		}

		return GIT_ERROR;
	}

	int jadegit_config_backend__del(git_config_backend* backend, const char* key)
	{
		return wrapper(backend, [&](jadegit_config_backend& backend)
			{
				backend.remove(key);
				return GIT_OK;
			});
	}

	int jadegit_config_backend__del_multivar(git_config_backend* backend, const char* key, const char* regexp)
	{
		return wrapper(backend, [&](jadegit_config_backend& backend)
			{
				backend.remove_multivar(key, regexp);
				return GIT_OK;
			});
	}

	void jadegit_config_backend__free(git_config_backend* backend)
	{
		delete static_cast<jadegit_config_backend*>(backend);
	}

	int jadegit_config_backend__get(git_config_backend* backend, const char* key, git_config_entry** out)
	{
		return wrapper(backend, [&](jadegit_config_backend& backend)
			{
				return (*out = backend.get(key)) ? GIT_OK : GIT_ENOTFOUND;
			});
	}

	int jadegit_config_backend__iterator(git_config_iterator** iter, git_config_backend* backend)
	{
		return wrapper(backend, [&](jadegit_config_backend& backend)
			{
				*iter = new jadegit_config_iterator(backend);
				return GIT_OK;
			});
	}

	int jadegit_config_backend__open(struct git_config_backend*, git_config_level_t level, const git_repository* repo)
	{
		return GIT_OK;
	}

	int jadegit_config_backend__set(git_config_backend* backend, const char* key, const char* value)
	{
		return wrapper(backend, [&](jadegit_config_backend& backend)
			{
				backend.set(key, value);
				return GIT_OK;
			});
	}

	int jadegit_config_backend__set_multivar(git_config_backend* backend, const char* key, const char* regexp, const char* value)
	{
		return wrapper(backend, [&](jadegit_config_backend& backend)
			{
				backend.set_multivar(key, regexp, value);
				return GIT_OK;
			});
	}

	int jadegit_config_backend__snapshot(struct git_config_backend** backend, struct git_config_backend* original)
	{
		return wrapper(original, [&](jadegit_config_backend& original)
			{
				*backend = new jadegit_config_backend(original);
				(*backend)->readonly = true;
				return GIT_OK;
			});
	}

	jadegit_config_backend::jadegit_config_backend(const Object& parent)
	{
		git_throw(git_config_init_backend(this, GIT_CONFIG_BACKEND_VERSION));

		git_config_backend::del = &jadegit_config_backend__del;
		git_config_backend::del_multivar = &jadegit_config_backend__del_multivar;
		git_config_backend::free = &jadegit_config_backend__free;
		git_config_backend::get = &jadegit_config_backend__get;
		git_config_backend::iterator = &jadegit_config_backend__iterator;
		git_config_backend::open = &jadegit_config_backend__open;
		git_config_backend::set = &jadegit_config_backend__set;
		git_config_backend::set_multivar = &jadegit_config_backend__set_multivar;
		git_config_backend::snapshot = &jadegit_config_backend__snapshot;

		// Dereference config data
		data = parent.getProperty<ConfigDataDict>(TEXT("config"));

		// Populate entries
		refresh();
	}

	void jadegit_config_backend::add(git_config* config, git_config_level_t level, const git_repository* repo, const DskObjectId& context)
	{
		if (context.isNull())
			return;

		// Initialise config backend
		auto backend = make_unique<Backend::jadegit_config_backend>(&context);

		// Add config backend
		git_throw(git_config_add_backend(config, backend.get(), level, repo, false));

		// Release backend now added
		backend.release();
	}

	git_config_entry* jadegit_config_backend::get(const char* key) const
	{
		return entries ? entries.get()->get(key) : nullptr;
	}

	void jadegit_config_backend::refresh()
	{
		// Repopulate config entries (prior copy will be preserved until all shared references are discarded)
		entries = make_shared<jadegit_config_entries>(*this);
	}

	void jadegit_config_backend::remove(const char* key)
	{
		data.remove(key);
		refresh();
	}

	void jadegit_config_backend::remove_multivar(const char* key, const char* regexp)
	{
		data.remove_multivar(key, regexp);
		refresh();
	}

	void jadegit_config_backend::set(const char* key, const char* value)
	{
		data.set(key, value);
		refresh();
	}

	void jadegit_config_backend::set_multivar(const char* key, const char* regexp, const char* value)
	{
		data.set_multivar(key, regexp, value);
		refresh();
	}
}