#pragma once
#include <schema/Object.h>
#include <functional>
#include <set>

namespace JadeGit::Extract
{
	class Assembly;
	class Entity;
}

namespace JadeGit::Schema
{
	class GitChangeSet;
	class GitEntity;
	class WorktreeData;

	class ChangeOperation;

	class GitChange : public Object
	{
	public:
		enum class Operation : Byte
		{
			Void = 0,
			Add = 1,
			Delete = 2,
			Rename = 3,
			Update = 4
		};

		static GitChange add(const GitEntity& entity);
		static GitChange update(const GitEntity& entity);
		static GitChange rename(const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual);
		static GitChange dele(const GitEntity& entity, const Extract::Entity* actual);

		using Object::Object;
		GitChange();
		GitChange(const WorktreeData& worktree, const GitEntity* previous, const GitEntity* latest, std::set<DskObjectId>& predecessors);
		GitChange(const WorktreeData& worktree, const GitEntity* previous, const GitEntity* latest, const GitChange* predecessor = nullptr);

		bool isWorktree(const WorktreeData& worktree) const;
		bool isPrevious(const GitEntity& entity) const;
		bool isLatest(const GitEntity& entity) const;
		
		bool isStaged() const;
		bool isVersioned() const;

		bool isOperation(Operation operation) const;
		inline bool isAdd() const { return isOperation(Operation::Add); }
		inline bool isVoid() const { return isOperation(Operation::Void); }

		void getPrevious(GitEntity &entity) const;
		GitEntity getPrevious() const;
		void getLatest(GitEntity &entity) const;
		GitEntity getLatest() const;

		const ChangeOperation& getOperation() const;
		const GitChange& setOperation(Operation operation) const;
		const GitChange& setOperation(const GitChange& source) const;
		inline void setVoid() const { setOperation(Operation::Void); }

		void setPrevious(const GitEntity& entity);
		void setLatest(const GitEntity& entity);

		void addPredecessor(const GitChange &predecessor) const;
		void addPredecessors(std::set<DskObjectId>& predecessors);

		const GitChange& peerOf(const GitChange& peer) const;
		const GitChange& predecessorOf(const GitChange& successor) const;
		const GitChange& successorOf(const GitChange& predecessor) const;

		void Reset();

	private:
		static GitChange change(const GitEntity& entity, std::function<GitChange(const ChangeOperation&, const WorktreeData&, const GitChange&)> action);
		static void prelude(const WorktreeData& worktree);
		static void preludeBackground(const WorktreeData& worktree);

		friend GitChangeSet;
		void compact();
		void stagePreload(Extract::Assembly& assembly, std::set<DskObjectId>& loaded);
		void stage(Extract::Assembly& assembly, GitChangeSet& extract);
		void stageExtract(Extract::Assembly& assembly);

		friend ChangeOperation;
		void SetStaged(bool staged);
		void Merge(GitChange& rhs);

		void create(const WorktreeData& worktree, const GitEntity* previous, const GitEntity* latest, std::set<DskObjectId>& predecessors);
	};
}