#include "Root.h"
#include "Schema.h"
#include <jade/AppContext.h>
#include <schema/ObjectRegistration.h>
#include <schema/Workers.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Root> registration(TEXT("Root"));

	Root::Root() : Object(registration)
	{
		jade_throw(classFirstOid(&oid));
		assert(!oid.isNull());
	}

	const Root& Root::get()
	{
		static Root root;
		return root;
	}

	bool Root::deploying() const
	{
		DskObjectId deployment;
		getProperty(TEXT("deployment"), deployment);
		return !deployment.isNull();
	}

	unique_ptr<GitSchema> Root::getSchema(const string& name) const
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
			return Backend::workers.push(bind(&Root::getSchema, this, ref(name))).get();

		DskMemberKeyDictionary schemas;
		getProperty(TEXT("schemas"), schemas);

		auto schema = make_unique<GitSchema>();
		jade_throw(schemas.getAtKey(widen(name).c_str(), *schema));

		if (schema->isNull())
			return nullptr;

		return schema;
	}
}