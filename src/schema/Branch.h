#pragma once
#include "Reference.h"

namespace JadeGit
{
	class IProgress;
}

namespace JadeGit::Schema
{
	class LocalBranch;
	class Remote;

	class Branch : public Reference
	{
	public:
		static Branch lookup(const Repository& repo, const std::string& name, git_branch_t type);
		static Branch make(const Repository& repo, std::unique_ptr<git_reference> ptr);
		static Branch make(const Repository& repo, std::unique_ptr<git_reference> ptr, git_branch_t type);

		using Reference::Reference;

		LocalBranch branch(std::string name, bool track_upstream) const;
	};

	class LocalBranch : public Branch
	{
	public:
		using Branch::Branch;
		LocalBranch(const Repository& repo, std::unique_ptr<git_reference> ptr);

		bool push(const Remote& remote, IProgress* progress) const;
		void set_upstream(const std::string& refname) const;
		std::string upstream_merge() const;
		std::string upstream_name() const;
		std::string upstream_remote() const;
	};

	class RemoteBranch : public Branch
	{
	public:
		using Branch::Branch;
		RemoteBranch(const Repository& repo, std::unique_ptr<git_reference> ptr);

		std::string remote_name() const;
	};
}