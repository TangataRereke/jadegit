#include "Commit.h"
#include "Exception.h"
#include "Signature.h"
#include "Task.h"
#include "Tree.h"
#include "ObjectRegistration.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Commit> registration(TEXT("Commit"));

	Commit Commit::get(const Repository& repo, const git_oid& oid)
	{
		char id[GIT_OID_HEXSZ + 1] = "";
		Commit commit;
		jade_throw(repo.getProperty<DskMemberKeyDictionary>(TEXT("commits")).getAtKey(widen(git_oid_tostr(id, GIT_OID_HEXSZ + 1, &oid)).c_str(), commit));
		return commit;
	}

	Commit Commit::lookup(const Repository& repo, const git_oid& oid)
	{
		// Return existing
		auto commit = get(repo, oid);
		if (!commit.isNull())
			return commit;

		// Lookup commit
		unique_ptr<git_commit> ptr;
		git_throw(git_commit_lookup(git_ptr(ptr), repo, &oid));

		// Return new
		return Commit(repo, move(ptr));
	}

	Commit Commit::lookup(const Repository& repo, std::unique_ptr<git_commit> ptr)
	{
		// Return existing
		auto commit = get(repo, *git_commit_id(ptr.get()));
		if (!commit.isNull())
			return commit;

		// Return new
		return Commit(repo, move(ptr));
	}

	Commit::Commit(const Repository& repo, unique_ptr<git_commit> ptr) : GitObject(registration, move(ptr))
	{
		setProperty(TEXT("id"), git_commit_id(static_cast<git_commit*>(*this)));
		jade_throw(setProperty(TEXT("parent"), repo));

		Signature author(*this, TEXT("author"), git_commit_author(*this));
		Signature committer(*this, TEXT("committer"), git_commit_committer(*this));
	}

	void Commit::parents(DskObjectArray& array) const
	{
		Repository repo = this->repo();
		const git_commit* commit = *this;
		auto count = git_commit_parentcount(commit);

		for (unsigned int i = 0; i < count; i++)
		{
			unique_ptr<git_commit> parent;
			git_throw(git_commit_parent(git_ptr(parent), commit, i));

			Commit commit = Commit::lookup(repo, move(parent));
			jade_throw(array.add(&commit));
		}
	}

	Repository Commit::repo() const
	{
		return getProperty<Repository>(TEXT("parent"));
	}

	bool Commit::reset(bool hard, IProgress* progress) const
	{
		return repo().reset(*this, hard, progress);
	}

	Tree Commit::tree() const
	{
		return Tree::lookup(repo(), *git_commit_tree_id(*this));
	}

	int JOMAPI jadegit_commit_body(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Commit>(pBuffer, pParams, git_commit_body);
	}

	int JOMAPI jadegit_commit_branch(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				throw jadegit_unimplemented_feature();
				return J_OK;
			});
	}

	int JOMAPI jadegit_commit_lookup(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pId = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pId));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string id;
				JADE_RETURN(paramGetString(*pId, id));

				git_oid oid = { 0 };
				git_throw(git_oid_fromstrn(&oid, id.c_str(), id.length()));

				auto commit = Commit::lookup(repo, oid);
				return paramSetOid(pReturn, commit.oid);
			});
	}

	int JOMAPI jadegit_commit_merge(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				throw jadegit_unimplemented_feature();
				return J_OK;
			});
	}

	int JOMAPI jadegit_commit_message(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{ 
		return jadegit_proxy_mapping_string<Commit>(pBuffer, pParams, git_commit_message);
	}

	int JOMAPI jadegit_commit_parents(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		DskParam* pSet = nullptr;
		DskParam* pValue = nullptr;
		JADE_RETURN(paramGetParameter(*pParams, 1, pSet));
		JADE_RETURN(paramGetParameter(*pParams, 2, pValue));

		bool set = false;
		JADE_RETURN(paramGetBoolean(*pSet, set));

		if (set)
			return INVALID_PARAMETER_VALUE;

		DskObjectArray array;
		JADE_RETURN(paramGetOid(*pValue, array.oid));

		return GitException::wrapper([&]()
			{
				// Populate parent array if empty
				bool empty = false;
				jade_throw(array.isEmpty(&empty));
				if (empty)
					Commit(pBuffer).parents(array);

				return J_OK;
			});
	}

	int JOMAPI jadegit_commit_reset(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		bool hard = false;
		JADE_RETURN(paramGetBoolean(*pParams, hard));

		Commit commit(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress) { return commit.reset(hard, progress); });
	}

	int JOMAPI jadegit_commit_summary(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Commit>(pBuffer, pParams, git_commit_summary);
	}

	int JOMAPI jadegit_commit_tree(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				auto tree = Commit(pBuffer).tree();
				return paramSetOid(pReturn, tree.oid);
			});
	}
}