#include "CommandRegistry.h"

namespace JadeGit::Console
{
	CommandRegistry& CommandRegistry::get()
	{
		static CommandRegistry f;
		return f;
	}

	CommandRegistry::Registration::Registration()
	{
		get().registry.push_back(this);
	}

	void CommandRegistry::setup(CLI::App& app, Session& context) const
	{
		for (auto& registration : registry)
			registration->setup(app, context);
	}

	CommandRegistry::CommandRegistry() {}
}