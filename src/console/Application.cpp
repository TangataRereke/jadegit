#include "Application.h"
#include "CommandRegistry.h"
#include "Session.h"
#include <jadegit/Exception.h>
#include <jadegit/build.h>

using namespace std;

namespace JadeGit::Console
{
	string jadegit_version()
	{
		ostringstream v;
		v << JadeGit::build_version << " (" << JadeGit::build_timestamp << ")";
		return v.str();
	}

	class Application : public CLI::App
	{
	public:
		Application() : CLI::App("", "jadegit")
		{
			// add help flag
			set_help_flag("-h,--help", "Display help");

			// add version flag
			set_version_flag("-v, --version", &jadegit_version)->description("Display version");

#if defined(JADEGIT_JADE)
			// setup options for opening database
			add_option("-p, --path", this->dbPath, "JADE database path")->required();
			add_option("-i, --ini", this->iniPath, "JADE initialization file")->required();
			add_flag("-m, --multiUser", this->multiUser, "JADE multi-user mode");
#endif

			// setup subcommands
			CommandRegistry::get().setup(*this, session);
		}

		int run(int argc, char* argv[])
		{
			bool shell = false;
			int result = 0;
			do
			{
				result = 0;

				try
				{
					if (shell)
					{
						cout << "jadegit> ";
						string commandline;
						getline(cin, commandline);
						session.started = chrono::high_resolution_clock::now();
						clear();
						parse(commandline);
					}
					else
					{
						session.started = chrono::high_resolution_clock::now();
						parse(argc, argv);

						// Switch into shell mode for repeated commands when no subcommands provided upfront
						if (get_subcommands().empty())
						{
							shell = true;

							// Setup command to exit shell
							auto exit = add_subcommand("exit", "Exit shell");
							exit->callback([&]() { shell = false; });

							// Replace help option with subcommand
							bool help_all = false;
							add_subcommand("help", "Display help")->callback([&]()
								{
									clear();
									cout << help("", help_all ? CLI::AppFormatMode::All : CLI::AppFormatMode::Normal);
								})->add_flag("-a,--all", help_all);

							// Replace version option with subcommand
							add_subcommand("version", "Display version")->callback([&]()
								{
									throw(CLI::CallForVersion(jadegit_version(), 0));
								});

							// Reset name to omit from help produced within shell
							name();

							// Remove all base command options (help, version & connection options)
							for (auto& opt : get_options())
								remove_option(opt);
						}
					}
				}
				catch (const CLI::Error& e)
				{
					result = exit(e);
				}
				catch (const JadeGit::jadegit_exception& e)
				{
					cout << endl;
					cout << "ERROR: " << e << endl;
					result = e.code ? e.code : 1;
				}
				catch (const std::runtime_error& e)
				{
					cout << endl;
					cout << e.what() << endl;
					result = 1;
				}
			} while (shell);

			return result;
		}

	private:
		Session session;

#if defined(JADEGIT_JADE)
		void pre_callback() final
		{
			// open database
			if (!session.db)
				session.db = std::make_unique<Jade::DbContext>(dbPath.c_str(), iniPath.c_str(), multiUser);

			// initialize application
#if defined(JADEGIT_SCHEMA)
			if (!session.app)
			{
				try
				{
					session.app = std::make_unique<Jade::AppContext>(&session.db->node, TEXT("JadeGitSchema"), TEXT("JadeGitConsole"));
				}
				catch (Jade::Exception& e)
				{
					if (e.code != INVALID_SCHEMA)	// Invalid schema error is expected (may not have been installed)
						throw e;
				}
			}
#endif
			if (!session.app)
				session.app = std::make_unique<Jade::AppContext>(&session.db->node, TEXT("RootSchema"), TEXT("RootSchemaApp"));
		}

		std::string dbPath;
		std::string iniPath;
		bool multiUser = false;
#endif
	};

	int run(int argc, char* argv[])
	{
		return Application().run(argc, argv);
	}
}