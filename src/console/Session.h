#pragma once
#if defined(JADEGIT_JADE)
#include <jade/AppContext.h>
#include <jade/DbContext.h>
#include <jade/Exception.h>
#endif

#if defined(JADEGIT_GIT2)
#include <jadegit/git2.h>
#endif

namespace JadeGit::Console
{
	class Session
	{
	public:
#if defined(JADEGIT_JADE)
		std::unique_ptr<Jade::DbContext> db;
		std::unique_ptr<Jade::AppContext> app;
#endif

#if defined(JADEGIT_GIT2)
		std::unique_ptr<git_repository> repo;
		std::unique_ptr<git_reference> branch;
		std::unique_ptr<git_commit> commit;
		std::unique_ptr<git_index> index;
#endif

		std::chrono::steady_clock::time_point started;
	};
}