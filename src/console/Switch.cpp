#include "RepositoryCommand.h"
#include "CommandRegistration.h"
#include "Session.h"

using namespace std;

namespace JadeGit::Console
{
	class Switch : public RepositoryCommand
	{
	public:
		Switch(CLI::App& cmd, Session& session) : RepositoryCommand(cmd, session)
		{
			cmd.add_option("branch-name", this->branch, "Local branch to switch to")->required();
		}

		void execute() final
		{
			// open repository if required
			RepositoryCommand::execute();

			// lookup branch
			unique_ptr<git_reference> branch;
			git_throw(git_branch_lookup(git_ptr(branch), session.repo.get(), this->branch.c_str(), git_branch_t::GIT_BRANCH_LOCAL));

			open(move(branch));
		}

	protected:
		std::string branch;
	};
	static CommandRegistration<Switch> registration("switch", "Switch branch");
}