#include "Progress.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace std::chrono;

namespace JadeGit::Console
{
	Progress::Progress(const Command& command) : command(command) {}

	Progress::~Progress()
	{
		if (!uncaught_exceptions())
		{
			reset();
			command.print("Done");
		}
		else
			cerr << endl;
	}

	void Progress::message(const string& message)
	{
		reset();

		command.print(message.c_str());	// TODO: Handle embedded \n characters?

		display();
	}

	void Progress::update(double progress, const char* caption)
	{
		if (!caption && static_cast<unsigned int>(progress * 100) == this->progress)
			return;

		this->progress = static_cast<unsigned int>(progress * 100);

		if (caption)
		{
			reset();
			command.print_time();
			cout << caption << endl;
		}
		
		display();
	}

	void Progress::display()
	{
		reset();

		stringstream bar;
		bar << "[" << string((progress / 4), '=') << string(25 - (progress / 4), ' ') << "]" << setw(4) << setfill(' ') << progress << "%";

		cerr << bar.str();
		length = bar.str().length();
	}

	void Progress::reset()
	{
		if (!length)
			return;

		cerr << "\r" << string(length, ' ') << "\r";
		length = 0;
	}
}