#include "Command.h"
#include "CommandRegistration.h"
#include "Progress.h"
#include "Session.h"
#include <schema/data/Repository.h>

using namespace std;

namespace JadeGit::Console
{
	class Clone : public Command
	{
	public:
		Clone(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
			cmd.add_option("remote", this->remote, "Remote repository to clone from")->required();
			cmd.add_option("name", this->name, "New repository name to use (derived from origin otherwise)");
			cmd.add_option("--access-token", this->access_token, "Access token to be used for authorization");
			cmd.add_option("--current-commit", this->current_commit, "Current version already loaded (commit hash)");
		}

		void execute() final
		{
			// Derive name from origin if not supplied
			if (name.empty())
				name = filesystem::path(remote).stem().string();

			// Convert current commit hash supplied
			git_oid commit_id = { 0 };
			if (!current_commit.empty())
				git_throw(git_oid_fromstrp(&commit_id, current_commit.c_str()));

			Progress progress(*this);

			// Clone repository
			Schema::RepositoryData repo;
			if (!repo.clone(remote, name, access_token, git_oid_is_zero(&commit_id) ? nullptr : &commit_id, &progress))
				throw jadegit_operation_aborted();
			
			// Load repository if required
			if (!repo.isActive())
				repo.load(&progress);
		}

	private:
		string remote;
		string name;
		string access_token;
		string current_commit;
	};
	static CommandRegistration<Clone> registration("clone", "Clone remote repository");
}