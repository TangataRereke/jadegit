#include "RepositoryCommand.h"
#include "CommandRegistration.h"
#include "Session.h"

using namespace std;

namespace JadeGit::Console
{
	class OpenRepository : public RepositoryCommand
	{
	public:
		OpenRepository(CLI::App& cmd, Session& session) : RepositoryCommand(cmd, session)
		{
			cmd.add_option("path", this->path, "Path to repository")->required();
		}

		void execute() final
		{
			open(path);
		}

	protected:
		std::string path;
	};
	static CommandRegistration<OpenRepository> open("open", "Open existing repository");
}