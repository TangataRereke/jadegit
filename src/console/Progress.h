#pragma once
#include "Command.h"
#include <jadegit/Progress.h>
#include <chrono>

namespace JadeGit::Console
{
	class Progress : public JadeGit::IProgress
	{
	public:
		Progress(const Command& command);
		~Progress();

	protected:
		void message(const std::string& message) override;
		bool wasCancelled() override { return false; }
		void update(double progress, const char* caption) override;

	private:
		const Command& command;
		unsigned int progress = 0;
		size_t length = 0;

		void display();
		void reset();
	};
}