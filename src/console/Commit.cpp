#include "RepositoryCommand.h"
#include "CommandRegistration.h"
#include "Session.h"

using namespace std;

namespace JadeGit::Console
{
	class Commit : public RepositoryCommand
	{
	public:
		Commit(CLI::App& cmd, Session& session) : RepositoryCommand(cmd, session)
		{
			cmd.add_option("-m, --message", this->message, "Commit message")->required();
		}

		void execute() final
		{
			if (!session.index)
				throw jadegit_exception("Index uninitialized");
			
			// write index to tree
			git_oid tree_id;
			git_throw(git_index_write_tree(&tree_id, session.index.get()));

			// lookup tree
			unique_ptr<git_tree> tree;
			git_throw(git_tree_lookup(git_ptr(tree), session.repo.get(), &tree_id));

			// get signature
			unique_ptr<git_signature> author;
			git_throw(git_signature_default(git_ptr(author), session.repo.get()));


			git_oid commit_id = { 0 };

			// assume repo is unborn if no there's branch or commit
			if (!session.branch && !session.commit)
			{
				// create initial commit with no parent
				git_throw(git_commit_create(&commit_id, session.repo.get(), "HEAD", author.get(), author.get(), nullptr, message.c_str(), tree.get(), 0, nullptr));
				
				// lookup new branch
				git_throw(git_repository_head(git_ptr(session.branch), session.repo.get()));
			}
			else
			{
				// create commit with one parent
				const git_commit* parents[] = { session.commit.get() };
				git_throw(git_commit_create(&commit_id, session.repo.get(), git_reference_name(session.branch.get()), author.get(), author.get(), nullptr, message.c_str(), tree.get(), 1, parents));
			}

			// lookup new commit
			git_throw(git_commit_lookup(git_ptr(session.commit), session.repo.get(), &commit_id));
		}

	protected:
		std::string message;
	};
	static CommandRegistration<Commit> registration("commit", "Commit changes");
}