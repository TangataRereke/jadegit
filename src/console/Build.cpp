#include "RepositoryCommand.h"
#include "CommandRegistration.h"
#include "Progress.h"
#include "Session.h"
#include <deploy/Director.h>
#include <deploy/MultiExtractBuilder.h>
#include <deploy/XMLDeploymentBuilder.h>
#include <jadegit/vfs/NativeFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Deploy;

namespace JadeGit::Console
{
	class Build : public RepositoryCommand
	{
	public:
		Build(CLI::App& cmd, Session& session) : RepositoryCommand(cmd, session)
		{
			cmd.add_option("revision", this->revision, "Revision string representing range of commits for which to build deployment")->required();

			cmd.footer("Builds deployment using range of commits specified by revision string.\n"
				"See http://git-scm.com/docs/git-rev-parse.html#_specifying_revisions for information on the syntax accepted.\n");

			// Need one subcommand to setup deployment format(s)
			cmd.require_subcommand(1);

			// Setup subcommand to build standard extract
			{
				auto sub = cmd.add_subcommand("mul", "Build standard multiple schema file extract (*.mul)");
				sub->add_option("destination", this->destination, "Build output directory")->required();
				sub->add_option("-n,--name", this->name, "Optional name")->capture_default_str();
				sub->callback([this]() { mul(); });
			}

			// Setup subcommand to build generic xml deployment
			{
				auto sub = cmd.add_subcommand("xml", "Build generic deployment with instructions in XML format");
				sub->add_option("destination", this->destination, "Build output directory")->required();
				sub->callback([this]() { xml(); });
			}

			// Setup subcommand to build jari
			{

			}
		}

		void execute() final
		{
			// open repository if required
			RepositoryCommand::execute();

			// setup deployment director
			Progress progress(*this);
			Deploy::Director director(*session.repo, *this->builder, &progress);

			// build deployment
			director.build(this->revision);
			this->builder.reset();
			this->fs.reset();
		}

	protected:
		// Setup multiple schema file extract
		void mul()
		{
			this->fs = make_unique<NativeFileSystem>(destination, true);
			this->builder = make_unique<MultiExtractBuilder>(*fs, name);
		}

		void xml()
		{
			this->fs = make_unique<NativeFileSystem>(destination, true);
			this->builder = make_unique<XMLDeploymentBuilder>(*fs);
		}

		string revision;

		string destination;
		string name = "All Files";

		unique_ptr<FileSystem> fs;
		unique_ptr<DeploymentBuilder> builder;
	};
	static CommandRegistration<Build> registration("build", "Build deployment");	
}