#pragma once
#include "ClassDelta.h"
#include <jadegit/data/CollClass.h>

namespace JadeGit::Build
{
	template<class TComponent = CollClass>
	class CollClassDelta : public ClassDelta<TComponent>
	{
	public:
		CollClassDelta(TaskGraph &graph) : ClassDelta<TComponent>(graph) {}

		using ClassDelta<TComponent>::previous;
		using ClassDelta<TComponent>::latest;

	protected:
		using ClassDelta<TComponent>::graph;
		using ClassDelta<TComponent>::GetDeclaration;

		bool AnalyzeEnter() override
		{
			/* Basic analysis */
			if (!ClassDelta<TComponent>::AnalyzeEnter())
				return false;

			/* Declaration is dependent on latest member type being declared */
			if(const ITypeDelta* latestMemberType = graph.Analyze<ITypeDelta>(latest->memberType))
				graph.AddEdge(latestMemberType->GetDeclaration(), GetDeclaration());

			return true;
		}

		// TODO: Investigate crash which occurs when renaming member class further
		// It would seem the type header declaration is always needed to avoid the crash, or it is
		// perhaps needed in conjunction with the member keys being changed (which this could be
		// changed to check, as well as other collection details).
		bool RequiresDeclaration() const override
		{
			return !latest->isSubschemaCopy();
		}
	};
}