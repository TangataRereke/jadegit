#pragma once
#include "SchemaComponentDelta.h"

namespace JadeGit::Build
{
	class ITypeDelta : public ISchemaComponentDelta
	{
	public:
		using ISchemaComponentDelta::ISchemaComponentDelta;

		virtual Task* GetDeclaration() const { return GetDefinition(); }
	};

	template<class TEntity, class TInterface = ITypeDelta>
	class TypeDelta : public SchemaComponentDelta<TEntity, TInterface>
	{
	public:
		using SchemaComponentDelta<TEntity, TInterface>::SchemaComponentDelta;

	protected:
		using SchemaComponentDelta<TEntity, TInterface>::graph;
		using SchemaComponentDelta<TEntity, TInterface>::previous;
		using SchemaComponentDelta<TEntity, TInterface>::latest;
		using SchemaComponentDelta<TEntity, TInterface>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!SchemaComponentDelta<TEntity, TInterface>::AnalyzeEnter())
			{
				// Check type isn't being deleted with schema (which'll implictly delete all dependencies)
				if (!this->implicitDeletion)
				{
					// Handle deleting or updating dependencies before type is explicitly deleted
					handleRemovingDependenciesBeforeDeletion(&TEntity::collClassRefs);
					handleRemovingDependenciesBeforeDeletion(&TEntity::propertyRefs);
				}

				return false;
			}

			return true;
		}

		Task* HandleDefinition(bool complete) override
		{
			// Not applicable to inferred subschema copies, except when previous wasn't inferred
			if (latest->isInferred() && latest->isSubschemaCopy() && (!previous || previous->isInferred()))
				return nullptr;
			
			return SchemaComponentDelta<TEntity, TInterface>::HandleDefinition(complete);
		}

	private:

		// Handle deleting or updating previous dependencies before deletion
		template<class Refs>
		void handleRemovingDependenciesBeforeDeletion(Refs TEntity::* ptr)
		{
			Task* deletion = GetDeletion();

			for (auto& dependency : previous->*ptr)
			{
				if (const IDelta* delta = graph.Analyze<IDelta>(dependency))
				{
					graph.AddEdge(delta->GetDeletion(), deletion);
					graph.AddEdge(delta->GetDefinition(), deletion);
				}
			}
		}
	};
}