#pragma once
#include "SchemaDelta.h"
#include <jadegit/data/Locale.h>

namespace JadeGit::Build
{
	class CreateLocaleTask : public CommandTask
	{
	public:
		std::string qualifiedName;
		std::string baseLocale;

		CreateLocaleTask(TaskGraph& graph, std::string qualifiedName, std::string baseLocale = std::string()) : CommandTask(graph), 
			qualifiedName(std::move(qualifiedName)),
			baseLocale(std::move(baseLocale))
		{}

		void Execute(CommandStrategy& strategy) const override
		{
			strategy.CreateLocale(qualifiedName.c_str(), baseLocale.empty() ? nullptr : baseLocale.c_str());
		}

		void Print(std::ostream& output) const override
		{
			output << "Create Locale " << qualifiedName << " " << baseLocale;
		}
	};

	class LocaleDelta : public SchemaComponentDelta<Locale>
	{
	public:
		LocaleDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "Locale") {}
		
	protected:
		Task* GetCreation() const override
		{
			return creation ? creation : SchemaComponentDelta::GetCreation();
		}

		Task* GetDeletion() const override
		{
			return deletion ? deletion : SchemaComponentDelta::GetDeletion();
		}

		bool AnalyzeEnter() override
		{
			// Analyze previous base
			const LocaleDelta* previousBaseLocale = nullptr;
			if (previous)
				previousBaseLocale = graph.Analyze<LocaleDelta>(previous->cloneOf);
			
			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				// Handle deletion before deleting previous base
				if (previousBaseLocale)
					graph.AddEdge(GetDeletion(), previousBaseLocale->GetDeletion());

				return false;
			}

			// Analyze latest base
			const LocaleDelta* latestBaseLocale = nullptr;
			latestBaseLocale = graph.Analyze<LocaleDelta>(latest->cloneOf);

			// Determine if schema is being loaded for the first time
			auto schema = graph.Find<SchemaDelta>(latest->schema);
			if (!schema || schema->previous)
			{
				// Determine if locale needs to be created
				if (!previous || previous->baseLocaleName() != latest->baseLocaleName())
				{
					// Handle creation
					creation = new CreateLocaleTask(graph, QualifiedName(), latest->baseLocaleName());

					// Definition dependent on creation
					graph.AddEdge(creation, GetDefinition());

					// Delete previous
					if (previous)
					{
						// Handle deletion
						deletion = HandleDeletion();

						// Handle deletion before recreating
						graph.AddEdge(deletion, creation);

						// Handle deletion before deleting previous base
						if (previousBaseLocale)
							graph.AddEdge(deletion, previousBaseLocale->GetDeletion());
					}
				}
			}

			// Definition dependent on base being created
			if (latestBaseLocale)
				graph.AddEdge(latestBaseLocale->GetCreation(), GetCreation());

			return true;
		}

	private:
		Task* creation = nullptr;
		Task* deletion = nullptr;
	};
}