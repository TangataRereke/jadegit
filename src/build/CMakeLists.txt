target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/CommandDeleteTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/CommandMoveClassTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/CommandRenameTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/Delta.cpp
	${CMAKE_CURRENT_LIST_DIR}/Director.cpp
	${CMAKE_CURRENT_LIST_DIR}/FileSource.cpp
	${CMAKE_CURRENT_LIST_DIR}/ReorgTask.cpp
	${CMAKE_CURRENT_LIST_DIR}/Task.cpp
	${CMAKE_CURRENT_LIST_DIR}/TaskGraph.cpp
	${CMAKE_CURRENT_LIST_DIR}/TaskWaypoint.cpp
)

include(${CMAKE_CURRENT_LIST_DIR}/classic/CMakeLists.txt)