#pragma once
#include "Delta.h"
#include <jadegit/data/JadeExportedPackage.h>

namespace JadeGit::Build
{
	class JadeExportedPackageDelta : public SchemaComponentDelta<JadeExportedPackage>
	{
	public:
		JadeExportedPackageDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "Package") {}

	protected:
		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			// Analyze latest application
			const IDelta* latestApplication = graph.Analyze<IDelta>(latest->application);

			// Package definition is dependent on latest application
			if (latestApplication)
				graph.AddEdge(latestApplication->GetCreation(), GetDefinition());

			return true;
		}
	};

	template<class TComponent>
	class JadeExportedEntityDelta : public Delta<TComponent>
	{
	public:
		JadeExportedEntityDelta(TaskGraph &graph) : Delta<TComponent>(graph) {}

		using Delta<TComponent>::previous;
		using Delta<TComponent>::latest;

	protected:
		using Delta<TComponent>::graph;
		using Delta<TComponent>::GetDefinition;
		using Delta<TComponent>::GetDeletion;

		Task* HandleDeletion() override
		{
			if (Task* deletion = Delta<TComponent>::HandleDeletion())
				return deletion;

			/* Exported entities are cannot be deleted explicitly, rather implicitly by parent definition omitting them */
			const IDelta* parent = graph.Analyze<IDelta>(previous->getParentObject());
			if (!parent)
				throw jadegit_exception("Exported entity missing previous parent delta");

			return parent->GetDefinition();
		}

		bool AnalyzeEnter() override
		{
			/* Returns false when deleted */
			if (!Delta<TComponent>::AnalyzeEnter())
				return false;

			/* Exported entity definition is dependent on original entity being created/renamed */
			if (const IDelta* original = graph.Analyze<IDelta>(&latest->getOriginal()))
				graph.AddEdge(original->GetCreation(), GetDefinition());

			/* Exported packages are dependent on all child exported entities being extracted as a complete definition */
			/* TODO: We may eventually need to support scenarios where a partial package extract is needed to support  */
			/* deleting exported entities before new entities can be added, that also need to be exported              */
			const IDelta* parent = graph.Analyze<IDelta>(latest->getParentEntity());
			if (!parent)
				throw jadegit_exception("Exported entity missing latest parent delta");

			// TODO: Need to reconsider parent/child dependency
			// Making the parent dependent on child may only be relevant in context of complete definitions
			// Whereas in general, children are dependent on parents being established first, hence the following
			// needed to be removed as it was causing a cycle in the graph 
		//	graph.AddEdge(GetDefinition(), parent->GetDefinition());
			
			return true;
		}

		void AnalyzeExit() override
		{
			// Original entity cannot be deleted until exported entity is
			if (Task* deletion = GetDeletion())
			{
				if (const IDelta* original = graph.Analyze<IDelta>(&previous->getOriginal()))
					graph.AddEdge(deletion, original->GetDeletion());
			}

			Delta<TComponent>::AnalyzeExit();
		}
	};
}