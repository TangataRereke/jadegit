#pragma once
#include "ClassDelta.h"
#include "JadeInterfaceDelta.h"
#include "EmptyTask.h"
#include <jadegit/data/JadeImportedPackage.h>

using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class JadeImportedPackageDelta : public SchemaComponentDelta<JadeImportedPackage>
	{
	public:
		JadeImportedPackageDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "Package") {}

	protected:
		bool AnalyzeEnter() override
		{
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			// Dependent on exported package
			if (const IDelta* exported = graph.Analyze<IDelta>(latest->exportedPackage))
			{
				graph.AddEdge(exported->GetDefinition(), GetDefinition());

				// Ideally, schema creation is dependent on exported package definition, provided it won't create a circular dependency
				if (const IDelta* parent = graph.Find<IDelta>(latest->schema))
					graph.tryAddEdge(exported->GetDefinition(), parent->GetCreation());
			}

			return true;
		}

		void AnalyzeExit() override
		{
			// Exported package cannot be deleted until imported package is
			if (Task* deletion = GetDeletion())
			{
				if (const IDelta* exported = graph.Analyze<IDelta>(previous->exportedPackage))
					graph.AddEdge(deletion, exported->GetDeletion());
			}

			SchemaComponentDelta::AnalyzeExit();
		}
	};

	template<class TEntity, template<class> class TBase> 
	class JadeImportedEntityDelta : public TBase<TEntity>
	{
	public:
		using TBase<TEntity>::TBase;

		using TBase<TEntity>::previous;
		using TBase<TEntity>::latest;

	protected:
		using TBase<TEntity>::graph;
		using TBase<TEntity>::GetDefinition;
		using TBase<TEntity>::GetDeletion;

		bool AnalyzeEnter() override
		{
			if (!TBase<TEntity>::AnalyzeEnter())
				return false;

			// Dependent on exported entity
			if (const IDelta* exported = graph.Analyze<IDelta>(latest->GetExportedEntity()))
				graph.AddEdge(exported->GetDefinition(), GetDefinition());

			return true;
		}

		void AnalyzeExit() override
		{
			// Exported entity cannot be deleted until imported entity is
			if (Task* deletion = GetDeletion())
			{
				if (const IDelta* exported = graph.Analyze<IDelta>(previous->GetExportedEntity()))
					graph.AddEdge(deletion, exported->GetDeletion());
			}

			TBase<TEntity>::AnalyzeExit();
		}
	};

	class JadeImportedClassDelta : public JadeImportedEntityDelta<JadeImportedClass, ClassDelta>
	{
	public:
		using JadeImportedEntityDelta::JadeImportedEntityDelta;

	protected:
		bool RequiresDeclaration() const override
		{
			return true;
		}
	};

	using JadeImportedInterfaceDelta = JadeImportedEntityDelta<JadeImportedInterface, JadeInterfaceDelta>;

	template<class TFeature>
	class JadeImportedFeatureDelta : public JadeImportedEntityDelta<TFeature, Delta>
	{
	public:
		using JadeImportedEntityDelta<TFeature, Delta>::JadeImportedEntityDelta;

	protected:
		Task* HandleDefinition(bool complete) override
		{
			// Use empty task for imported features as they're not explicitly defined
			return new EmptyTask(this->graph);
		}
	};
}