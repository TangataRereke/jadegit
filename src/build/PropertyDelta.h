#pragma once
#include "FeatureDelta.h"
#include "TypeDelta.h"

namespace JadeGit::Build
{
	class IPropertyDelta : public IFeatureDelta
	{
	public:
		using IFeatureDelta::IFeatureDelta;

		// No property specific commands (as yet)
	};

	template<class TComponent, class TInterface = IPropertyDelta>
	class PropertyDelta : public FeatureDelta<TComponent, TInterface>
	{
	public:
		PropertyDelta(TaskGraph &graph) : FeatureDelta<TComponent, TInterface>(graph, "Property") {}

		using FeatureDelta<TComponent, TInterface>::previous;
		using FeatureDelta<TComponent, TInterface>::latest;

	protected:
		using FeatureDelta<TComponent, TInterface>::graph;
		using FeatureDelta<TComponent, TInterface>::GetDefinition;
		using FeatureDelta<TComponent, TInterface>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!FeatureDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				// Make property deletion a prerequisite to previous type subclasses being moved
				graph.AddEdge(GetDeletion(), TaskWaypoint::make(graph, static_cast<Data::Type*>(previous->type), TaskWaypoint::MoveSubclass));

				return false;
			}

			// Analyze latest type
			const ITypeDelta* latestType = graph.Analyze<ITypeDelta>(latest->type);

			// Property definition is dependent on type being created/renamed
			if (latestType)
				graph.AddEdge(latestType->GetCreation(), GetDefinition());

			// Make property type change a prerequisite to previous type subclasses being moved
			if (previous && !this->match<Data::Type>(latestType, previous->type, latest->type))
				graph.AddEdge(GetDefinition(), TaskWaypoint::make(graph, static_cast<Data::Type*>(previous->type), TaskWaypoint::MoveSubclass));

			return true;
		}
	};
}