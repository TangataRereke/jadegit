#include "MethodDefinition.h"
#include "TypeDefinition.h"

namespace JadeGit::Build::Classic
{
	MethodDefinition::MethodDefinition(SchemaDefinition* schema, TypeDefinition* type, const Data::Method* method, bool webService) : RoutineDefinition(schema, method), source(method), webService(webService)
	{
		// Add event method mapping
		if (method->controlMethod)
			type->eventMethodMappings[method->GetName()] = method->controlMethod;
	}

	void MethodDefinition::WriteAttributes(std::ostream& output)
	{
		WriteAttribute(output, "updating", source->updating);

		RoutineDefinition::WriteAttributes(output);
		
		WriteAttribute(output, "mapping", source->invocation == Data::Method::Invocation::Mapping);
		WriteAttribute(output, "lockReceiver", source->lockReceiver);
		WriteAttribute(output, "partitionMethod", source->partitionMethod);
		WriteAttribute(output, "subschemaFinal", source->subschemaFinal);
		WriteAttribute(output, "subschemaCopyFinal", source->subschemaCopyFinal);
		WriteAttribute(output, "final", source->final);
		WriteAttribute(output, "condition", source->condition);
		WriteAttribute(output, "conditionSafe", source->conditionSafe);
		WriteAttribute(output, "typeMethod", source->isTypeMethod());
		WriteAttribute(output, "webService", webService);
		WriteAttribute(output, "wsdlName", source->wsdlName);
	}
}