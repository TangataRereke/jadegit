#include "DataNode.h"
#include <jadegit/data/CollClass.h>
#include <jadegit/data/PropertyIterator.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/data/RootSchema/CollectionMeta.h>
#include <jadegit/data/RootSchema/SchemaEntityMeta.h>
#include <jadegit/data/MetaSchema/MetaType.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	DataNode::DataNode(const Data::Object* source, const char* type) : source(source), type(type ? type : source->dataClass->name.c_str()) {}

	DataNode::DataNode(DataNodes& nodes, const Data::Object* source, const char* type) : DataNode(source, type)
	{
		nodes.push_back(this);
	}

	void DataNodes::Print(XMLPrinter& printer) const
	{
		for (auto node : *this)
			node->Print(printer);
	}

	void DataNode::Print(XMLPrinter& printer) const
	{
		printer.OpenElement(type);

		PrintAttributes(printer);
		PrintData(printer);

		printer.CloseElement();
	}

	void DataNode::PrintData(XMLPrinter& printer) const
	{
		PrintData(printer, source);
	}

	void DataNode::PrintData(XMLPrinter& printer, const Data::Object* source) const
	{
		const RootSchema& rootSchema = source->GetRootSchema();
		PropertyIterator iter(source->dataClass);

		for (auto& prop : iter)
		{
			if (!PrintPropertyFilter(rootSchema, &prop))
				continue;

			auto value = source->GetValue(&prop);

			if (auto collClass = dynamic_cast<const CollClass*>(static_cast<const Type*>(prop.type)))
			{
				const Collection* coll = value.Get<Collection*>();
				if (coll && !coll->Empty())
				{
					printer.OpenElement(prop.GetAlias());
					PrintCollection(printer, *collClass, *coll);
					printer.CloseElement();
				}
			}
			else
			{
				printer.OpenElement(prop.GetAlias());
				PrintValue(printer, *static_cast<const Type*>(prop.type), value);
				printer.CloseElement();
			}
		}
	}

	bool DataNode::PrintPropertyFilter(const Data::RootSchema& rootSchema, const Data::Property* property) const
	{
		if (const ExplicitInverseRef* ref = dynamic_cast<const ExplicitInverseRef*>(property))
		{
			// Generally need to filter out automatic references
			if (ref->updateMode == ExplicitInverseRef::Automatic)
				return false;
			
			// Generally need to filter out parent references
			if (ref->kind == ExplicitInverseRef::Parent && ref->updateMode == ExplicitInverseRef::Manual)
				return false;
		}
		else if (const ImplicitInverseRef* ref = dynamic_cast<const ImplicitInverseRef*>(property))
		{
			// Filter out exclusive collections
			if (ref->memberTypeInverse)
				return false;
		}

		// Include property
		return true;
	}

	void DataNode::PrintCollection(XMLPrinter& printer, const Data::CollClass& type, const Data::Collection& coll) const
	{
		auto memberType = type.GetMemberType();
		Any entry;
		auto iter = coll.CreateIterator();
		if (memberType->isPrimType())
		{
			while (iter->Next(entry))
			{
				printer.OpenElement(memberType->name.c_str());
				PrintValue(printer, entry);
				printer.CloseElement();
			}
		}
		else
		{
			while (iter->Next(entry))
			{
				auto object = entry.Get<Object*>();
				printer.OpenElement(object->dataClass->name.c_str());
				PrintValue(printer, object);
				printer.CloseElement();
			}
		}
	}

	void DataNode::PrintValue(XMLPrinter& printer, const Data::Type& type, const Data::Any& value) const
	{
		if (value.empty())
			return;

		if (type.isPrimType())
			PrintValue(printer, value);
		else
			PrintValue(printer, value.Get<Object*>());	
	}

	void DataNode::PrintValue(XMLPrinter& printer, const Data::Any& value) const
	{
		printer.PushText(value.ToBasicString().c_str());
	}

	void DataNode::PrintValue(XMLPrinter& printer, const Data::Object* value) const
	{
		if (auto named = dynamic_cast<const INamedObject*>(value))
			printer.PushAttribute("name", named->GetName().c_str());
		else
			throw jadegit_exception("Unhandled value");
	}
}