#pragma once
#include "RoutineDefinition.h"
#include <jadegit/data/Method.h>

namespace JadeGit::Build::Classic
{
	class TypeDefinition;

	class MethodDefinition : public RoutineDefinition
	{
	public:
		using parent_node = TypeDefinition;

		MethodDefinition(SchemaDefinition* schema, TypeDefinition* type, const Data::Method* method, bool webService = false);

	protected:
		void WriteAttributes(std::ostream& output) override;

	private:
		const Data::Method* source;
		bool webService = false;
	};
}