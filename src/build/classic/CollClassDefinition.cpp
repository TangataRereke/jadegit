#include "CollClassDefinition.h"
#include "SchemaDefinition.h"
#include "MembershipDefinition.h"

namespace JadeGit::Build::Classic
{
	static NodeRegistration<CollClassDefinition, Data::CollClass> collClass;

	CollClassDefinition::CollClassDefinition(SchemaDefinition* schema, const Data::CollClass* klass) : ClassDefinition(schema, klass)
	{
		if(klass->memberType)
			NodeFactory<MembershipDefinition>::Get().Create(std::type_index(typeid(*klass)), schema, klass, false);

		for (auto key : klass->keys)
			NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*key)), schema, key, false);
	}
}