#include "ConstantDefinition.h"
#include "ConstantCategoryDefinition.h"
#include <jadegit/data/GlobalConstant.h>

namespace JadeGit::Build::Classic
{
	class GlobalConstantDefinition : public ConstantDefinition
	{
	public:
		using parent_node = ConstantCategoryDefinition;

		GlobalConstantDefinition(ConstantCategoryDefinition* category, const Data::GlobalConstant* constant) : ConstantDefinition(constant)
		{
			category->constantDefinitions.push_back(this);
		}
	};
	static NodeRegistration<GlobalConstantDefinition, Data::GlobalConstant> registrar;
}