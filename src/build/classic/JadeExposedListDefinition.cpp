#include "SchemaDefinition.h"
#include "SchemaDefinitionNode.h"
#include <jadegit/data/Class.h>
#include <jadegit/data/JadeExposedList.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class JadeExposedListDefinition : public SchemaDefinitionNode<JadeExposedList>
	{
	public:
		using parent_node = SchemaDefinition;

		DefinitionNodes exposedClassDefinitions;

		JadeExposedListDefinition(SchemaDefinition* schema, const JadeExposedList* list) : SchemaDefinitionNode(list),
			exposedClassDefinitions(nullptr)
		{
			schema->exposedListDefinitions.push_back(this);
		}

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			SchemaDefinitionNode::WriteAttributes(output);

			WriteAttribute(output, "version", source->version, true);
			WriteAttribute(output, "priorVersion", source->priorVersion, true);
			WriteAttribute(output, "registryId", source->registryId);
			WriteAttribute(output, "useEncodedFormat", source->useEncodedFormat);
			WriteAttribute(output, "useHttpGet", source->useHttpGet);
			WriteAttribute(output, "useHttpPost", source->useHttpPost);
			WriteAttribute(output, "useRPC", source->useRPC);
			WriteAttribute(output, "useSOAP12", source->useSOAP12);
			WriteAttribute(output, "useSecureService", source->secureService);
			WriteAttribute(output, "useSessionHandling", source->sessionHandling);
			WriteAttribute(output, "useVersionControl", source->versionControl);
			WriteAttribute(output, "useBareFormat", source->useBareFormat);
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			WriteAttributes(output);

			// Modified timestamp written before opening bracket (oddly inconsistent with how it's handled elsewhere)
			output << "\n";
			WriteModifiedTimeStamp(output);
			output << indent << "(\n";

			exposedClassDefinitions.Write(output, indent);
		}
	};

	class JadeExposedClassDefinition : public SchemaDefinitionNode<JadeExposedClass>
	{
	public:
		using parent_node = JadeExposedListDefinition;

		DefinitionNodes exposedFeatureDefinitions;

		JadeExposedClassDefinition(SchemaDefinition* schema, JadeExposedListDefinition* list, const JadeExposedClass* cls) : SchemaDefinitionNode(cls),
			exposedFeatureDefinitions("_exposedJavaFeatures", true)
		{
			list->exposedClassDefinitions.push_back(this);
		}

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			SchemaDefinitionNode::WriteAttributes(output);

			WriteAttribute(output, "javaName", source->exposedName);
			WriteAttribute(output, "defaultStyle", source->defaultStyle);
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			SchemaDefinitionNode::WriteBody(output, indent);

			exposedFeatureDefinitions.Write(output, indent);

			if (!source->properties.empty())
			{
				output << indent << "_exposedPropertyDefinitions\n";
				for (auto& prop : source->properties)
					output << indent << "\t" << prop->name << ";\n";
			}
		}
	};

	class JadeExposedFeatureDefinition : public SchemaDefinitionNode<JadeExposedFeature, false>
	{
	public:
		using parent_node = JadeExposedClassDefinition;

		JadeExposedFeatureDefinition(SchemaDefinition* schema, JadeExposedClassDefinition* cls, const JadeExposedFeature* feature) : SchemaDefinitionNode(feature)
		{
			cls->exposedFeatureDefinitions.push_back(this);
		}

		void WriteAttributes(std::ostream& output) override
		{
			SchemaDefinitionNode::WriteAttributes(output);

			WriteAttribute(output, "javaName", source->exposedName);
			WriteAttribute(output, "javaType", source->exposedType);
		}
	};

	static NodeRegistration<JadeExposedListDefinition, JadeExposedList> list;
	static NodeRegistration<JadeExposedClassDefinition, JadeExposedClass> cls;
	static NodeRegistration<JadeExposedFeatureDefinition, JadeExposedFeature> feature;
}