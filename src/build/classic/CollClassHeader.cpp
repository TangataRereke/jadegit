#include "TypeHeader.h"
#include "SchemaDefinition.h"
#include <jadegit/data/CollClass.h>

namespace JadeGit::Build::Classic
{
	class CollClassHeader : public TypeHeader
	{
	public:
		CollClassHeader(SchemaDefinition* schema, const Data::CollClass* klass) : TypeHeader(schema, klass) {}

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			auto source = static_cast<const Data::CollClass*>(this->source);

			WriteAttribute(output, "duplicatesAllowed", source->duplicatesAllowed);
			WriteAttribute(output, "blockSize", source->blockSize);
			WriteAttribute(output, "expectedPopulation", source->expectedPopulation);
			WriteAttribute(output, "loadFactor", source->loadFactor);

			TypeHeader::WriteAttributes(output);
		}
	};
	static NodeRegistration<CollClassHeader, Data::CollClass> collClass;
}