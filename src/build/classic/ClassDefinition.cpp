#include "ClassDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/HTMLClass.h>
#include <jadegit/data/Database.h>
#include <jadegit/data/Development.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<ClassDefinition, Class> klass;
	static NodeRegistration<ClassDefinition, HTMLClass> htmlClass(true);
	static NodeRegistration<ClassDefinition, ExceptionClass> exceptionClass(true);

	ClassDefinition::ClassDefinition(SchemaDefinition* schema, const Class* klass) : TypeDefinition(schema, klass), 
		attributeDefinitions("attributeDefinitions"),
		referenceDefinitions("referenceDefinitions"),
		interfaceMappingDefinitions("implementInterfaces")
	{
		for(auto classMap : klass->classMapRefs)
			NodeFactory<DefinitionNode>::Get().Create(std::type_index(typeid(*classMap)), schema, classMap, false);
	}

	void ClassDefinition::WriteTypeBody(std::ostream& output, const std::string& indent) const
	{
		constantDefinitions.Write(output, indent);
		attributeDefinitions.Write(output, indent);
		referenceDefinitions.Write(output, indent);
		jadeMethodDefinitions.Write(output, indent);
		webServicesMethodDefinitions.Write(output, indent);
		externalMethodDefinitions.Write(output, indent);
		WriteEventMethodMappings(output, indent);
		interfaceMappingDefinitions.Write(output, indent);
	}

	class GUIClassDefinition : public ClassDefinition
	{
	public:
		GUIClassDefinition(SchemaDefinition* schema, const GUIClass* klass) : ClassDefinition(schema, klass)
		{
			if(klass->controlType)
				NodeFactory<DataNode>::Get().Create(std::type_index(typeid(*klass->controlType)), schema, klass->controlType, false);
		}
	};

	static NodeRegistration<GUIClassDefinition, GUIClass> guiClass(false);
}