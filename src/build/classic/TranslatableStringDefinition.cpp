#pragma once
#include "ConstantDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/TranslatableString.h>
#include <jadegit/data/Locale.h>

namespace JadeGit::Build::Classic
{
	class TranslatableStringLocaleDefinition : public SchemaDefinitionNode<Data::Locale>
	{
	public:
		using base_node = TranslatableStringLocaleDefinition;
		using parent_node = SchemaDefinition;

		TranslatableStringLocaleDefinition(SchemaDefinition* schema, const Data::Locale* locale) : SchemaDefinitionNode(locale), translatableStrings(nullptr)
		{
			schema->translatableStringDefinitions.push_back(this);
		}

		DefinitionNodes translatableStrings;

	protected:
		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			SchemaDefinitionNode::WriteBody(output, indent);

			translatableStrings.Write(output, indent);
		}
	};
	static NodeRegistration<TranslatableStringLocaleDefinition, Data::Locale> translatableStringLocale;

	class TranslatableStringDefinition : public ConstantDefinition
	{
	public:
		using parent_node = TranslatableStringLocaleDefinition;

		TranslatableStringDefinition(SchemaDefinition* schema, TranslatableStringLocaleDefinition* locale, const Data::TranslatableString* string) : ConstantDefinition(string)
		{
			locale->translatableStrings.push_back(this);
		}

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			output << indent << source->source;
		}
	};
	static NodeRegistration<TranslatableStringDefinition, Data::TranslatableString> translatableString;
}