#pragma once
#include <jadegit/data/ObjectFactory.h>
#include <Factory.h>
#include <typeindex>
#include <type_traits>
#include <vector>
#include <map>
#include <assert.h>

namespace JadeGit::Build::Classic
{
	class Node : virtual public MemoryAllocated
	{
	public:
		std::map<std::pair<const Data::Component*, std::type_index>, Node*> children;

		bool complete = false;

		virtual ~Node();
	};

	class SchemaDefinition;
	class DefinitionNode;

	template<class TBase = Node, typename... Args>
	class NodeFactory : public Factory<TBase, std::type_index, SchemaDefinition*, const Data::Component*, bool, Args...> {};

	template <class TDerived, class TComponent, typename... Args>
	class NodeRegistration : NodeFactory<typename TDerived::base_node, Args...>::Registration
	{
	public:
		using TBase = TDerived::base_node;
		using TParent = TDerived::parent_node;
		using TParentBase = TParent::base_node;

		TBase* Create(SchemaDefinition* schema, const Data::Component* origin, bool complete, Args... args) const override
		{
			// Check origin is expected component
			const TComponent* component = dynamic_cast<const TComponent*>(origin);
			if (!component)
			{
				// Resolve using object factory and recycle back to node factory to subclass registration
				if constexpr (std::is_base_of_v<Data::Object, TComponent>)
				{
					component = Data::ObjectFactory::Get().Resolve<TComponent>(origin);
					assert(component);
					return NodeFactory<TBase>::Get().Create(std::type_index(typeid(*component)), schema, component, complete);
				}
				assert(component);
			}
			
			// Find/create parent
			TParentBase* parent = NodeFactory<TParentBase>::Get().Create(std::type_index(typeid(TParent)), schema, component, false);
			if (!parent)
				return nullptr;

			assert(dynamic_cast<TParent*>(parent));

			// Resolve child (creating if required)
			return Resolve(schema, static_cast<TParent*>(parent), component, complete, args...);
		}

		NodeRegistration(bool subclass = false)
		{
			NodeFactory<TBase, Args...>::Get().Register(std::type_index(typeid(TComponent)), this);

			if(!subclass)
				NodeFactory<TBase, Args...>::Get().Register(std::type_index(typeid(TDerived)), this);
		}

	protected:
		virtual TDerived* Resolve(SchemaDefinition* schema, TParent* parent, const TComponent* component, bool complete, Args... args) const
		{
			std::pair<const Data::Component*, std::type_index> key(component, std::type_index(typeid(TBase)));
			TDerived* child = static_cast<TDerived*>(parent->children[key]);
			if (!child)
			{
				if constexpr (std::is_constructible_v<TDerived, TParent*, const TComponent*, Args...>)
					child = new TDerived(parent, component, args...);
				else
					child = new TDerived(schema, parent, component, args...);
			}
			parent->children[key] = child;

			if (complete)
				child->complete = true;

			return child;
		}
	};
}