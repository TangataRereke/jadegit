#pragma once
#include "FeatureDefinition.h"
#include <jadegit/data/Routine.h>

namespace JadeGit::Build::Classic
{
	class RoutineDefinition : public FeatureDefinition<Data::Routine>
	{
	public:
		RoutineDefinition(SchemaDefinition* schema, const Data::Routine* routine);

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteAttributes(std::ostream& output) override;
	};
}