#include "MethodDefinition.h"
#include "TypeDefinition.h"

namespace JadeGit::Build::Classic
{
	class JadeMethodDefinition : public MethodDefinition
	{
	public:
		JadeMethodDefinition(SchemaDefinition* schema, TypeDefinition* type, const Data::JadeMethod* method) : MethodDefinition(schema, type, method)
		{
			type->jadeMethodDefinitions.push_back(this);
		}
	};
	static NodeRegistration<JadeMethodDefinition, Data::JadeMethod> registrar;
}