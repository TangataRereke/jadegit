#include "ConstantDefinition.h"
#include "TypeDefinition.h"

namespace JadeGit::Build::Classic
{
	static NodeRegistration<ConstantDefinition, Data::Constant> registrar;

	ConstantDefinition::ConstantDefinition(TypeDefinition* type, const Data::Constant* constant) : ConstantDefinition(constant)
	{
		type->constantDefinitions.push_back(this);
	}

	void ConstantDefinition::WriteEnter(std::ostream& output, const std::string& indent)
	{
		FeatureDefinition::WriteEnter(output, indent);
		output << ": " << source->GetType()->GetName() << " = " << source->source;
	}
}