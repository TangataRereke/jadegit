#include "RoutineDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Function.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class FunctionDefinition : public RoutineDefinition
	{
	public:
		using parent_node = SchemaDefinition;

		FunctionDefinition(SchemaDefinition* schema, const Function* function) : RoutineDefinition(schema, function), source(function)
		{
			schema->externalFunctionDefinitions.push_back(this);
		}

	private:
		const Function* source;
	};

	static NodeRegistration<FunctionDefinition, Function> registrar;
}