#include "TypeSource.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Class.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/JadeInterface.h>
#include <jadegit/data/JadeWebServicesClass.h>
#include <jadegit/data/PrimType.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<TypeSource, Type> type;
	static NodeRegistration<TypeSource, Class> klass(true);
	static NodeRegistration<TypeSource, CollClass> collClass(true);
	static NodeRegistration<TypeSource, ExceptionClass> exceptionClass(true);
	static NodeRegistration<TypeSource, GUIClass> guiClass(true);
	static NodeRegistration<TypeSource, JadeWebServicesClass> jadeWebServicesClass(true);
	static NodeRegistration<TypeSource, JadeWebServiceConsumerClass> jadeWebServiceConsumerClass(true);
	static NodeRegistration<TypeSource, JadeWebServiceProviderClass> jadeWebServiceProviderClass(true);
	static NodeRegistration<TypeSource, JadeWebServiceSoapHeaderClass> jadeWebServiceSoapHeaderClass(true);
	static NodeRegistration<TypeSource, JadeInterface> jadeInterface(true);
	static NodeRegistration<TypeSource, PrimType> primType(true);

	TypeSource::TypeSource(SchemaDefinition* schema, const Type* type) : SchemaDefinitionNode(type),
		jadeMethodSources("jadeMethodSources"),
		webServicesMethodSources("webServicesMethodSources"),
		externalMethodSources("externalMethodSources")
	{
		schema->typeSources.push_back(this);
	}

	void TypeSource::WriteEnter(std::ostream& output, const std::string& indent)
	{
		output << indent << source->GetLocalName();
	}

	void TypeSource::WriteBody(std::ostream& output, const std::string& indent)
	{
		SchemaDefinitionNode::WriteBody(output, indent);

		jadeMethodSources.Write(output, indent);
		webServicesMethodSources.Write(output, indent);
		externalMethodSources.Write(output, indent);
	}
}