#include "TypeHeader.h"
#include "SchemaDefinition.h"
#include <jadegit/data/HTMLClass.h>
#include <jadegit/data/JadeWebServicesClass.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	static NodeRegistration<TypeHeader, Class> klass;
	static NodeRegistration<TypeHeader, ExceptionClass> exceptionClass;
	static NodeRegistration<TypeHeader, GUIClass> guiClass;
	static NodeRegistration<TypeHeader, HTMLClass> htmlClass;
	static NodeRegistration<TypeHeader, JadeWebServicesClass> jadeWebServicesClass;
	static NodeRegistration<TypeHeader, JadeWebServiceConsumerClass> jadeWebServiceConsumerClass;
	static NodeRegistration<TypeHeader, JadeWebServiceProviderClass> jadeWebServiceProviderClass;
	static NodeRegistration<TypeHeader, JadeWebServiceSoapHeaderClass> jadeWebServiceSoapHeaderClass;

	TypeHeader::TypeHeader(SchemaDefinition* schema, const Class* klass) : TypeDeclaration(klass)
	{
		schema->typeHeaders.push_back(this);

		transient = klass->transient;
	}

	void TypeHeader::WriteEnter(std::ostream& output, const std::string& indent)
	{
		TypeDeclaration::WriteEnter(output, indent);

		output << " subclassOf " << static_cast<const Class*>(this->source)->getSuperClass()->GetName();
	}

	void TypeHeader::WriteAttributes(std::ostream& output)
	{
		TypeDeclaration::WriteAttributes(output);

		const Class* source = static_cast<const Class*>(this->source);

		if (source->instanceVolatility)
			WriteAttribute(output, "objectVolatility", source->instanceVolatility == Frozen ? "frozen" : "stable");		

		for (auto& cluster : source->dynamicPropertyClusters)
			WriteAttribute(output, "dynamicPropertyCluster", cluster.second->name.c_str());

		WriteAttribute(output, "webService", source->webService);

		if(!dynamic_cast<const JadeWebServicesClass*>(source))
			WriteAttribute(output, "wsdlName", source->wsdlName);
	}
}