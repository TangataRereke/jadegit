#include "SchemaEntityDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/LocaleFormat.h>
#include <jadegit/data/Class.h>

namespace JadeGit::Build::Classic
{
	template <class TLocaleFormat>
	class LocaleFormatDefinition : public SchemaEntityDefinition<TLocaleFormat>
	{
	public:
		using parent_node = SchemaDefinition;

		LocaleFormatDefinition(SchemaDefinition* schema, const TLocaleFormat* format) : SchemaEntityDefinition<TLocaleFormat>(format)
		{
			schema->localeFormatDefinitions.push_back(this);
		}

	protected:
		using SchemaEntityDefinition<TLocaleFormat>::source;

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			output << indent << source->name << ": " << source->dataClass->name << "(";
		}

		void WriteExit(std::ostream& output, const std::string& indent) override
		{
			output << ");\n";
			this->WriteModifiedTimeStamp(output);
		}
	};

	class DateFormatDefinition : public LocaleFormatDefinition<Data::DateFormat>
	{
	public:
		using LocaleFormatDefinition<Data::DateFormat>::LocaleFormatDefinition;

	protected:
		using LocaleFormatDefinition<Data::DateFormat>::source;

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			if (source->shortFormatOrder)
			{
				output << "true, " << source->dayHasLeadingZeros.ToString() << ", " << source->monthHasLeadingZeros.ToString() << ", \"" << source->separator << "\", "
					<< source->showFullCentury.ToString() << ", " << source->shortFormatOrder;
			}
			else
			{
				std::string sep1 = (source->shortDayNames.size() > 0 ? source->shortDayNames.at(0) : "");
				std::string sep2 = (source->shortDayNames.size() > 1 ? source->shortDayNames.at(1) : "");
				std::string sep3 = (source->shortDayNames.size() > 2 ? source->shortDayNames.at(2) : "");

				output << "false, " << source->dayHasLeadingZeros.ToString() << ", " << source->firstDayOfWeek << ", " << source->firstWeekOfYear << ", " << source->longFormatOrder
					<< ", \"" << sep1 << "\", \"" << sep2 << "\", \"" << sep3 << "\", " << source->showFullCentury.ToString();
			}
		}
	};

	template <class TNumberFormat>
	class NumberFormatDefinition : public LocaleFormatDefinition<TNumberFormat>
	{
	public:
		using LocaleFormatDefinition<TNumberFormat>::LocaleFormatDefinition;

	protected:
		using LocaleFormatDefinition<TNumberFormat>::source;

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			output << source->decimalPlaces << ", \"" << source->decimalSeperator << "\", \"" << source->thousandSeparator << "\", "
				<< source->negativeFormat << ", " << source->showLeadingZeros.ToString() << ", \"" << source->groupings << "\"";
		}
	};

	class CurrencyFormatDefinition : public NumberFormatDefinition<Data::CurrencyFormat>
	{
	public:
		using NumberFormatDefinition<Data::CurrencyFormat>::NumberFormatDefinition;

	protected:
		using NumberFormatDefinition<Data::CurrencyFormat>::source;

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			NumberFormatDefinition<Data::CurrencyFormat>::WriteBody(output, indent);

			output << ", " << source->positiveFormat << ", \"" << source->symbol << "\"";
		}
	};

	class TimeFormatDefinition : public LocaleFormatDefinition<Data::TimeFormat>
	{
	public:
		using LocaleFormatDefinition<Data::TimeFormat>::LocaleFormatDefinition;

	protected:
		using LocaleFormatDefinition<Data::TimeFormat>::source;

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			output << "\"" << source->amText << "\", " << source->ampmIsSuffix.ToString() << ", " << source->is12HourFormat.ToString() << ", \"" << source->pmText
				<< "\", \"" << source->separator << "\", " << source->showLeadingZeros.ToString() << ", " << source->showSeconds.ToString();
		}
	};

	static NodeRegistration<DateFormatDefinition, Data::DateFormat> dateFormat;
	static NodeRegistration<NumberFormatDefinition<Data::NumberFormat>, Data::NumberFormat> numberFormat;
	static NodeRegistration<CurrencyFormatDefinition, Data::CurrencyFormat> currencyFormat;
	static NodeRegistration<TimeFormatDefinition, Data::TimeFormat> timeFormat;
}