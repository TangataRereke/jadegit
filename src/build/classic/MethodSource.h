#pragma once
#include "RoutineSource.h"
#include <jadegit/data/Method.h>

namespace JadeGit::Build::Classic
{
	class TypeSource;

	class MethodSource : public RoutineSource
	{
	public:
		using parent_node = TypeSource;

		MethodSource(SchemaDefinition* schema, TypeSource* type, const Data::Method* method) : RoutineSource(schema, method) {}
	};
}