#pragma once
#include "DefinitionNode.h"
#include <jadegit/data/Reference.h>

namespace JadeGit::Build::Classic
{
	class InverseDefinition : public DefinitionNode
	{
	public:
		using parent_node = SchemaDefinition;

		InverseDefinition(SchemaDefinition* schema, const Data::Inverse* inverse);

		void WriteBody(std::ostream& output, const std::string& indent) final;

	private:
		const Data::Inverse* source;
	};
}