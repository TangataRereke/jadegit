#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Form.h>
#include <jadegit/data/Locale.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ControlMeta.h>
#include <jadegit/data/RootSchema/MenuItemMeta.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	template <typename T>
	class ControlData : public DataNode
	{
	public:
		ControlData(const T* source) : DataNode(source) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const
		{
			auto source = static_cast<const T*>(this->source);

			printer.PushAttribute("name", source->name.c_str());
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Include parent form reference
			if (property == rootSchema.control->form || property == rootSchema.menuItem->form)
				return true;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}

		void PrintCollection(XMLPrinter& printer, const Data::CollClass& type, const Data::Collection& coll) const final
		{
			auto memberType = type.GetMemberType();

			// Passback to base implementation for non-primitive arrays
			if (!memberType->isPrimType())
				return DataNode::PrintCollection(printer, type, coll);
			
			// Handle primitive arrays using control specific format
			printer.PushAttribute("type", type.name.c_str());
			Any entry;
			auto iter = coll.CreateIterator();
			while (iter->Next(entry))
			{
				printer.OpenElement("item");
				PrintValue(printer, entry);
				printer.CloseElement();
			}
		}
	};

	class FormData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		FormData(SchemaDefinition* schema, const Form* form) : DataNode(schema->data, form) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const override
		{
			auto source = static_cast<const Form*>(this->source);

			printer.PushAttribute("locale", source->locale->name.c_str());
			printer.PushAttribute("name", source->name.c_str());
		}

		void PrintData(XMLPrinter& printer) const override
		{
			DataNode::PrintData(printer);

			auto source = static_cast<const Form*>(this->source);

			printer.OpenElement("controlList");
			for (auto& cntrl : source->controlList)
				ControlData(cntrl).Print(printer);
			printer.CloseElement();

			printer.OpenElement("menuList");
			for (auto& menu : source->menuList)
				ControlData(menu).Print(printer);
			printer.CloseElement();
		}
	};
	static NodeRegistration<FormData, Form> registrar;	
}