#pragma once
#include "SchemaDefinitionNode.h"
#include <jadegit/data/Type.h>

namespace JadeGit::Build::Classic
{
	class TypeSource : public SchemaDefinitionNode<Data::Type>
	{
	public:
		using base_node = TypeSource;
		using parent_node = SchemaDefinition;

		TypeSource(SchemaDefinition* schema, const Data::Type* type);

		DefinitionNodes jadeMethodSources;
		DefinitionNodes webServicesMethodSources;
		DefinitionNodes externalMethodSources;

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteBody(std::ostream& output, const std::string& indent) override;
	};
}