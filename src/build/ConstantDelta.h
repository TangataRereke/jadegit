#pragma once
#include "FeatureDelta.h"

namespace JadeGit::Build
{
	template<class TComponent = Constant>
	class ConstantDelta : public FeatureDelta<TComponent>
	{
	public:
		using FeatureDelta<TComponent>::FeatureDelta;
		ConstantDelta(TaskGraph& graph) : ConstantDelta(graph, "Constant") {}

		using FeatureDelta<TComponent>::previous;
		using FeatureDelta<TComponent>::latest;

	protected:
		using FeatureDelta<TComponent>::graph;
		using FeatureDelta<TComponent>::GetDefinition;
		using FeatureDelta<TComponent>::GetDeletion;

		bool AnalyzeEnter() override
		{
			/* Basic analysis */
			if (!FeatureDelta<TComponent>::AnalyzeEnter())
			{
				/* Handle deleting or updating dependent constants before deletion */
				for (Constant* constant : previous->constantRefs)
				{
					if (const IFeatureDelta* constDelta = graph.Analyze<IFeatureDelta>(constant))
					{
						graph.AddEdge(constDelta->GetDeletion(), GetDeletion());
						graph.AddEdge(constDelta->GetDefinition(), GetDeletion());
					}
				}

				return false;
			}

			/* Constant definition is dependent on constants used being defined first */
			for (Constant* constant : latest->constantUsages)
			{
				if (const IFeatureDelta* constDelta = graph.Analyze<IFeatureDelta>(constant))
					graph.AddEdge(constDelta->GetDefinition(), GetDefinition());
			}

			return true;
		}
	};
}