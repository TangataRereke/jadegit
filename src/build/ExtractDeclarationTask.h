#pragma once
#include "ExtractTask.h"

namespace JadeGit::Build
{
	template<class TComponent>
	class ExtractDeclarationTask : public ExtractTask
	{
	public:
		ExtractDeclarationTask(TaskGraph &graph, const TComponent* component) : ExtractTask(graph), component(component) {};

		bool Execute(ExtractStrategy &strategy) const override
		{
			return strategy.Declare(component);
		}

		void Print(std::ostream& output) const override
		{
			output << "Declare " << *component;
		}

	private:
		const TComponent* component;
	};
}