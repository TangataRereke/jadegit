#pragma once
#include "TypeDelta.h"
#include "CommandMoveClassTask.h"
#include "ExtractDeclarationTask.h"
#include <map>
#include <stack>

using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class IClassDelta : public ITypeDelta
	{
	public:
		using ITypeDelta::ITypeDelta;

		virtual CommandMoveClassTask* GetMove() const = 0;
		virtual Task* GetInterfaceMappingDefinition(const JadeInterface* interface) const = 0;
	};

	template<class TComponent, class TInterface = IClassDelta>
	class ClassDelta : public TypeDelta<TComponent, TInterface>
	{
	public:
		ClassDelta(TaskGraph &graph) : TypeDelta<TComponent, TInterface>(graph, "Class") {}

		using TypeDelta<TComponent, TInterface>::previous;
		using TypeDelta<TComponent, TInterface>::latest;

	protected:
		using TypeDelta<TComponent, TInterface>::graph;
		using TypeDelta<TComponent, TInterface>::GetDefinition;
		using TypeDelta<TComponent, TInterface>::GetRename;

		Task* GetCreation() const override
		{
			// Declaration task establishes the new class
			if (!previous)
				return declaration;

			return TypeDelta<TComponent, TInterface>::GetCreation();
		}

		Task* GetDeclaration() const override final
		{
			return declaration;
		}

		CommandMoveClassTask* GetMove() const override final
		{
			return move;
		}

		bool AnalyzeEnter() override
		{
			// Build list of deltas for interface implementations
			std::map<EntityKey, std::pair<JadeInterface*, JadeInterface*>> interfaceDeltas;

			if (previous)
				for (auto& interface : previous->implementedInterfaces)
					interfaceDeltas[interface->getRootType().getKey()].first = interface;

			if (latest)
				for (auto& interface : latest->implementedInterfaces)
					interfaceDeltas[interface->getRootType().getKey()].second = interface;

			// Determine if interface is being de-implemented
			if (latest && previous)
			{
				for (auto& delta : interfaceDeltas)
				{
					// Ignore deltas where previous interface isn't being removed from class
					if (!delta.second.first || delta.second.second)
						continue;

					// Interface is being de-implemented, for which we need to load a complete definition
					interfaceDeimplemented = true;
					break;
				}
			}

			// Basic analysis
			if (!TypeDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				// Handle deleting after subclass moves (or deletions), provided class isn't being deleted implicitly (which'll include subclasses)
				if (!this->implicitDeletion)
					handleMovingSubclassesBefore(this->GetDeletion());

				return false;
			}

			// Analyze latest superclass & base class (closest superschema type or ancestor that's being updated)
			const IClassDelta* latestSuperClass = nullptr;
			{
				const Class* baseClass = latest->superclass ? latest->superclass : static_cast<const Class*>(static_cast<const Type*>(latest->superschemaType));
				while (baseClass && !baseClass->isStatic())
				{
					if (latestBaseClass = graph.Analyze<IClassDelta>(baseClass))
					{
						if (baseClass == latest->superclass)
							latestSuperClass = latestBaseClass;
						break;
					}

					baseClass = baseClass->superclass ? baseClass->superclass : static_cast<const Class*>(static_cast<const Type*>(baseClass->superschemaType));
				}
			}

			// Handle move
			if (previous && previous->superclass && latest->superclass && !this->match<Class>(latestSuperClass, previous->superclass, latest->superclass))
			{
				move = new CommandMoveClassTask(graph, this->QualifiedName(), latest->superclass->GetName());
				graph.AddEdge(GetRename(), move);			

				// Handle move after waypoint associated with previous superclass
				graph.AddEdge(TaskWaypoint::make(graph, static_cast<Class*>(previous->superclass), TaskWaypoint::MoveSubclass), move);

				// Handle move after latest superclass has been created, and subsequently renamed and/or moved
				if (latestSuperClass)
				{
					graph.AddEdge(latestSuperClass->GetCreation(), move);
					graph.AddEdge(latestSuperClass->GetMove(), move);
				}

				// Handle moving after subclass moves (or deletions)
				handleMovingSubclassesBefore(move);
			}
			else if (latestSuperClass && previous)
				move = latestSuperClass->GetMove();

			// Handle forward declaration
			if (RequiresDeclaration())
			{
				declaration = new ExtractDeclarationTask<TComponent>(graph, latest);
				graph.AddEdge(GetRename(), declaration);
				graph.AddEdge(move, declaration);
				graph.AddEdge(declaration, GetDefinition());

				if (latestSuperClass)
				{
					graph.AddEdge(latestSuperClass->GetCreation(), declaration);
					graph.AddEdge(latestSuperClass->GetDefinition(), GetDefinition());
				}
			}

			// Handle interface mappings
			for (auto& delta : interfaceDeltas)
			{
				JadeInterface* previous = delta.second.first;
				JadeInterface* latest = delta.second.second;

				if (latest)
				{
					// Setup definition task for interface mappings
					graph.AddEdge(GetDefinition(), interfaceMappingDefinitions[&latest->getRootType()] = new ExtractDefinitionTask<JadeInterfaceMapping>(graph, &this->latest->GetInterfaceMapping(latest), true));
					
					// Definition is dependent on defining interface
					if (ITypeDelta* interface = graph.Analyze<ITypeDelta>(latest))
						graph.AddEdge(interface->GetDefinition(), GetDefinition());
				}
				// Deleting previous interface is dependent on de-implementation
				else if (previous)
				{
					if (ITypeDelta* interface = graph.Analyze<ITypeDelta>(previous))
						graph.AddEdge(GetDefinition(), interface->GetDeletion());
				}
			}

			// Handle super-interface mapping dependencies
			for (auto& it : interfaceMappingDefinitions)
			{
				for (auto& superInterface : it.first->superinterfaces)
					graph.AddEdge(GetInterfaceMappingDefinition(&superInterface->getRootType()),it.second);
			}

			return true;
		}
		
		// Determines if forward declaration is required
		virtual bool RequiresDeclaration() const
		{
			// Not applicable to subschema copies
			if (latest->isSubschemaCopy())
				return false;

			// Required on creation
			if (!previous)
				return true;

			// Required if class header details are changing
			// TODO: Handle using more generically with method to determine if/when entities need to be included in build (due to changes made)
			return previous->access != latest->access ||
				previous->abstract != latest->abstract ||
				previous->subAccess != latest->subAccess ||
				previous->final != latest->final ||
				previous->subschemaFinal != latest->subschemaFinal ||
				previous->persistentAllowed != latest->persistentAllowed ||
				previous->sharedTransientAllowed != latest->sharedTransientAllowed ||
				previous->transientAllowed != latest->transientAllowed ||
				previous->subclassPersistentAllowed != latest->subclassPersistentAllowed ||
				previous->subclassSharedTransientAllowed != latest->subclassTransientAllowed ||
				previous->subclassTransientAllowed != latest->subclassTransientAllowed;
		}
	
		Task* HandleDefinition(bool complete) override
		{
			// Create complete definition task if requested or interface de-implemented
			return TypeDelta<TComponent, TInterface>::HandleDefinition(complete || interfaceDeimplemented);
		}

	private:
		ExtractDeclarationTask<TComponent>* declaration = nullptr;
		CommandMoveClassTask* move = nullptr;
		bool interfaceDeimplemented = false;
		std::map<const JadeInterface*, Task*> interfaceMappingDefinitions;
		const IClassDelta* latestBaseClass = nullptr;

		virtual Task* GetInterfaceMappingDefinition(const JadeInterface* interface) const override final
		{
			// Check locally
			auto iter = interfaceMappingDefinitions.find(interface);
			if (iter != interfaceMappingDefinitions.end())
				return iter->second;

			// Check base class
			return latestBaseClass ? latestBaseClass->GetInterfaceMappingDefinition(interface) : nullptr;
		}

		void handleMovingSubclassesBefore(Task* dependent)
		{
			std::stack<const Class*> stack;
			stack.push(previous);

			while (!stack.empty())
			{
				auto copy = stack.top();
				
				for (auto subclass : copy->subclasses)
				{
					if (const IClassDelta* delta = graph.Analyze<IClassDelta>(subclass))
					{
						graph.AddEdge(delta->GetDeletion(), dependent);
						graph.AddEdge(delta->GetMove(), dependent);
					}
				}

				stack.pop();
				for (auto type : copy->subschemaTypes)
					stack.push(static_cast<Class*>(type));
			}
		}
	};
}