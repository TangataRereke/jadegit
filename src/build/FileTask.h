#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class FileTask : public Task
	{
	public:
		enum LoadStyle : unsigned char
		{
			Current = 1,
			Latest = 2,
			Any = 3
		};

		const LoadStyle loadStyle;

		FileTask(TaskGraph& graph, LoadStyle loadStyle = LoadStyle::Latest, bool complete = false) : Task(graph, complete), loadStyle(loadStyle) {}

		int Priority() const override
		{
			return loadStyle == LoadStyle::Current ? -1 : 1;
		}

		inline bool isLatest() const { return loadStyle & LoadStyle::Latest; }
		inline bool isLatest(bool latest) const { return loadStyle & (latest ? LoadStyle::Latest : LoadStyle::Current); }
	};
}