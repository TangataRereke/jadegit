#include <jadegit/data/MenuItem.h>
#include <jadegit/data/Form.h>
#include <jadegit/data/Locale.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/MenuItemMeta.h>
#include "ObjectRegistration.h"

namespace JadeGit::Data
{
	static ObjectRegistration<MenuItem, Form> menuItem("MenuItem");

	MenuItem::MenuItem(Form* parent, const Class* dataClass) : MenuItemData(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::menuItem)),
		form(parent)
	{
	}

	MenuItemDataMeta::MenuItemDataMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "MenuItemData", superclass) {}

	MenuItemMeta::MenuItemMeta(RootSchema& parent, const MenuItemDataMeta& superclass) : RootClass(parent, "MenuItem", superclass),
		backColor(NewInteger("backColor")),
		caption(NewString("caption", 101)),
		checked(NewBoolean("checked")),
		commandId(NewInteger("commandId")),
		description(NewString("description")),
		disableReason(NewString("disableReason")),
		enabled(NewBoolean("enabled")),
		foreColor(NewInteger("foreColor")),
		form(NewReference<ExplicitInverseRef>("form", NewType<GUIClass>("Form"))),
		hasSubMenu(NewBoolean("hasSubMenu")),
		helpContextId(NewInteger("helpContextId")),
		helpKeyword(NewString("helpKeyword")),
		helpList(NewBoolean("helpList")),
		level(NewInteger("level")),
		name(NewString("name", 101)),
		picture(NewBinary("picture")),
		securityLevelEnabled(NewInteger("securityLevelEnabled")),
		securityLevelVisible(NewInteger("securityLevelVisible")),
		shortCutFlags(NewInteger("shortCutFlags")),
		shortCutKey(NewCharacter("shortCutKey")),
		visible(NewBoolean("visible")),
		webFileName(NewString("webFileName")),
		windowList(NewBoolean("windowList"))
	{
		form->manual().parent().bind(&MenuItem::form);
		name->bind(&MenuItem::name);

		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "select");
	}
}