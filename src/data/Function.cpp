#include <jadegit/data/Function.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/FunctionMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	static EntityRegistration<Function, Schema> registrar("Function", &Schema::functions);

	template ObjectValue<Schema* const, &FunctionMeta::schema>;

	Function::Function(Schema* parent, const Class* dataClass, const char* name) : Routine(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::function), name),
		schema(parent)
	{
	}

	void Function::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	FunctionMeta::FunctionMeta(RootSchema& parent, const RoutineMeta& superclass) : RootClass(parent, "Function", superclass),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema")))
	{
		schema->manual().parent().bind(&Function::schema);
	}
}