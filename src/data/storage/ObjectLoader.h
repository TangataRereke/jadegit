#pragma once
#include <jadegit/Progress.h>
#include <jadegit/vfs/File.h>
#include <functional>
#include <future>
#include <map>
#include <queue>

namespace JadeGit::Data
{
	class Entity;
	class ObjectFileStorage;

	class ObjectLoader
	{
	public:
		ObjectLoader(const ObjectFileStorage& storage);

		// Load entity file immediately
		Entity* load(std::filesystem::path path, bool shallow);

		// Load queued entities
		bool load(IProgress* progress = nullptr);

		// Retrieve queue size
		inline size_t pending() const { return deferred.size(); }

		// Queue entity to be loaded in deferred manner
		void queue(const File& file, std::function<void(Entity* entity)> callback = nullptr);

	private:
		const ObjectFileStorage& storage;
		std::map<std::filesystem::path, std::shared_future<Entity*>> deferred;
		std::queue<std::future<void>> tasks;
		int depth = 0;

		Entity* load(const File& file, bool shallow, const std::function<void(Entity* entity)>& callback);
	};
}