#pragma once
#include <jadegit/vfs/FileSystem.h>
#include <memory>

namespace JadeGit::Data
{
	class Assembly;
	class Entity;
	class ObjectFileFormat;
	class ObjectLoader;

	class ObjectFileStorage
	{
	public:
		ObjectFileStorage(Assembly& assembly, const FileSystem& fs);
		~ObjectFileStorage();
		
		void dele(const Entity& entity) const;
		void flush() const;
		ObjectLoader& getLoader() const;
		Entity* load(std::filesystem::path path, bool shallow) const;
		void rename(const Entity& entity, const std::string& new_name) const;
		void write(const Entity& entity, bool adding) const;

	protected:
		friend ObjectLoader;

		Assembly& assembly;
		const ObjectFileFormat* format;
		const FileSystem& fs;
		std::unique_ptr<ObjectLoader> loader;
	};
}
