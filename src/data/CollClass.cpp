#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/Key.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalCollClassMeta.h>
#include <jadegit/data/RootSchema/JadeUserCollClassMeta.h>
#include "TypeRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(CollClass)

	static TypeRegistration<CollClass, Schema> registrar("CollClass", &Schema::classes);

	template ObjectValue<Type*, &CollClassMeta::memberType>;

	CollClass::CollClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::collClass, "CollClass"), name, superclass)
	{
	}

	void CollClass::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	Type* CollClass::GetMemberType() const
	{
		// Resolve root type (subschema copies don't define the member type)
		auto& rootType = static_cast<const CollClass&>(getRootType());

		// Ensure root type has been loaded to resolve member type
		rootType.Load();

		if (!rootType.memberType)
			throw jadegit_exception(GetQualifiedName() + " has no member type");

		return rootType.memberType;
	}

	void CollClass::LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, std::queue<std::future<void>>& tasks) const
	{
		/* Load collection */
		Collection* collection = property.GetValue(object, true).Get<Collection*>();
		collection->LoadFor(object, *source, tasks);
	}

	void CollClass::WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent, const Object* value) const
	{
		const Collection* collection = dynamic_cast<const Collection*>(value);

		/* Ignore if collection is empty */
		if (collection && collection->Empty())
			return;

		/* Setup property element */
		tinyxml2::XMLElement* element = parent.GetDocument()->NewElement(property.name.c_str());
		parent.InsertEndChild(element);

		/* Write collection */
		collection->WriteFor(object, *element);
	}

	CollClassMeta::CollClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "CollClass", superclass),
		blockSize(NewInteger("blockSize")),
		duplicatesAllowed(NewBoolean("duplicatesAllowed")),
		expectedPopulation(NewInteger("expectedPopulation")),
		loadFactor(NewInteger("loadFactor")),
		memberType(NewReference<ExplicitInverseRef>("memberType", NewType<Class>("Type"))),
		memberTypePrecision(NewCharacter("memberTypePrecision")),
		memberTypeSFactor(NewCharacter("memberTypeSFactor")),			// TODO: Support mapping to friendlier name (memberTypeScaleFactor)
		memberTypeSize(NewInteger("memberTypeSize"))
	{
		blockSize->bind(&CollClass::blockSize);
		duplicatesAllowed->bind(&CollClass::duplicatesAllowed);
		expectedPopulation->bind(&CollClass::expectedPopulation);
		loadFactor->bind(&CollClass::loadFactor);
		memberType->bind(&CollClass::memberType);
		memberTypePrecision->bind(&CollClass::memberTypePrecision);
		memberTypeSFactor->bind(&CollClass::memberTypeScaleFactor);
		memberTypeSize->bind(&CollClass::memberTypeSize);
	}

	ExternalCollClassMeta::ExternalCollClassMeta(RootSchema& parent, const CollClassMeta& superclass) : RootClass(parent, "ExternalCollClass", superclass) {}

	JadeUserCollClassMeta::JadeUserCollClassMeta(RootSchema& parent, const CollClassMeta& superclass) : RootClass(parent, "JadeUserCollClass", superclass) {}
}