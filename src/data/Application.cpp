#include <jadegit/data/Application.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/JadeWebServiceManager.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceManagerMeta.h>
#include "SchemaEntityRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path Application::subFolder("applications");

	DEFINE_OBJECT_CAST(Application)

	static SchemaEntityRegistration<Application> application("Application", &Schema::applications);
	static ObjectRegistration<JadeWebServiceManager, Application> jadeWebServiceManager("JadeWebServiceManager");
	
	template ObjectValue<Set<JadeExportedPackage*>, &ApplicationMeta::exportedPackages>;
	template ObjectValue<Schema* const, &ApplicationMeta::schema>;

	Application::Application(Schema* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::application), name),
		schema(parent)
	{
	}

	void Application::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	JadeWebServiceManager::JadeWebServiceManager(Application* parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeWebServiceManager)),
		application(parent)
	{
		if (parent->jadeWebServiceManager)
			throw jadegit_exception(parent->GetQualifiedName() + " has duplicate web service managers");

		parent->jadeWebServiceManager = this;
	}

	ApplicationMeta::ApplicationMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Application", superclass),
		aboutFormName(NewString("aboutFormName", 101)),
		appVersion(NewString("appVersion", 31)),
		applicationType(NewCharacter("applicationType")),
		controlSpacing(NewInteger("controlSpacing")),
		defaultLocaleId(NewInteger("defaultLocaleId")),
		defaultMdi(NewBoolean("defaultMdi")),
		exportedPackages(NewReference<ExplicitInverseRef>("exportedPackages", NewType<CollClass>("JadeExportedPackageNDict"))),
		finalizeMethod(NewReference<ImplicitInverseRef>("finalizeMethod", NewType<Class>("Method"))),
		fontBold(NewBoolean("fontBold")),
		fontName(NewString("fontName", 101)),
		fontSize(NewReal("fontSize")),
		formMargin(NewInteger("formMargin")),
		heightSingleLineControl(NewInteger("heightSingleLineControl")),
		helpFile(NewString("helpFile")),
		icon(NewBinary("icon")),
		initializeMethod(NewReference<ImplicitInverseRef>("initializeMethod", NewType<Class>("Method"))),
		internetPipeName(NewString("internetPipeName")),
		mdiStyle(NewInteger(Version(20, 0, 1), "mdiStyle")),
		mdiWindowListOrder(NewInteger(Version(20, 0, 1), "mdiWindowListOrder")),
		name(NewString("name", 101)),
		numberOfPipes(NewInteger("numberOfPipes")),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		startupFormName(NewString("startupFormName", 101)),
		threeDControls(NewReference<ImplicitInverseRef>("threeDControls", NewType<CollClass>("ClassColl"))),
		useBorderStyleOnly(NewBoolean("useBorderStyleOnly")),
		webAppDirectory(NewString("webAppDirectory")),
		webBaseURL(NewString("webBaseURL")),
		webDefaultTimeZone(NewString(Version(22, 0, 1), "webDefaultTimeZone")),
		webDisplayMessages(NewBoolean("webDisplayMessages")),
		webDisplayPreference(NewCharacter("webDisplayPreference")),
		webHomePage(NewString("webHomePage", 101)),
		webMachineName(NewString("webMachineName")),
		webMaxHTMLSize(NewInteger("webMaxHTMLSize")),
		webMinimumResponseTime(NewInteger("webMinimumResponseTime")),
		webServiceClasses(NewString("webServiceClasses")),
		webSessionTimeout(NewInteger("webSessionTimeout")),
		webShowModal(NewBoolean("webShowModal")),
		webStatusLineDisplay(NewString("webStatusLineDisplay")),
		webUseHTML32(NewBoolean("webUseHTML32")),
		webVirtualDirectory(NewString("webVirtualDirectory"))
	{
		aboutFormName->SetAlias("_aboutFormName");
		applicationType->bind(&Application::applicationType);
		defaultLocaleId->SetAlias("_defaultLocaleId");
		exportedPackages->automatic().bind(&Application::exportedPackages);
		internetPipeName->SetAlias("_internetPipeName");
		name->MakeUnwritten()->bind(&Application::name);
		numberOfPipes->SetAlias("_numberOfPipes");
		schema->manual().parent().bind(&Application::schema);
		startupFormName->SetAlias("_startupFormName");
		webAppDirectory->SetAlias("_webAppDirectory");
		webBaseURL->SetAlias("_webBaseURL");
		webDefaultTimeZone->SetAlias("_webDefaultTimeZone");
		webDisplayMessages->SetAlias("_webDisplayMessages");
		webDisplayPreference->SetAlias("_webDisplayPreference");
		webHomePage->SetAlias("_webHomePage");
		webMachineName->SetAlias("_webMachineName");
		webMaxHTMLSize->SetAlias("_webMaxHTMLSize");
		webServiceClasses->SetAlias("_webServiceClasses");
		webSessionTimeout->SetAlias("_webSessionTimeout");
		webShowModal->SetAlias("_webShowModal");
		webStatusLineDisplay->SetAlias("_webStatusLineDisplay");
		webUseHTML32->SetAlias("_webUseHTML32");
		webVirtualDirectory->SetAlias("_webVirtualDirectory");
	}

	JadeWebServiceManagerMeta::JadeWebServiceManagerMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "JadeWebServiceManager", superclass),
		allowedSchemes(NewString("allowedSchemes", 11)),
		application(NewReference<ExplicitInverseRef>("application", NewType<Class>("Application"))),
		provider(NewBoolean("provider")),
		supportLibrary(NewString("supportLibrary", 61))
	{
		allowedSchemes->SetAlias("_allowedSchemes");
		application->manual().parent().bind(&JadeWebServiceManager::application);
	}
}