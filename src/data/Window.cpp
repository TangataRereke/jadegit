#include <jadegit/data/Window.h>
#include <jadegit/data/Form.h>
#include <jadegit/data/Locale.h>
#include <jadegit/data/RootSchema/WindowMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	class WindowRegistration : public EntityRegistration<Window, Window>
	{
	public:
		WindowRegistration() : EntityRegistration("Window", nullptr) {}

	protected:
		std::function<Object* ()> resolver(const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const override
		{
			// Return associated form for control if name matches
			if (const Control* control = dynamic_cast<const Control*>(origin))
				if (source->Attribute("name", control->form->GetName().c_str()))
					return [=]() { return static_cast<Form*>(control->form); };

			// Assume we're looking for a parent control otherwise
			return ObjectFactory::Get().resolver<Control>(origin, source, expected, shallow, inherit);
		}
	};

	static WindowRegistration registrar;

	Window::Window(Object* parent, const Class* dataClass, const char* name) : Entity(parent, dataClass, name) {}

	WindowMeta::WindowMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Window", superclass),
		backBrushStyle(NewInteger("backBrushStyle")),
		backColor(NewInteger("backColor")),
		borderStyle(NewInteger("borderStyle")),
		bubbleHelp(NewString("bubbleHelp")),
		description(NewString("description")),
		disableReason(NewString("disableReason")),
		dragCursor(NewBinary("dragCursor")),
		enabled(NewBoolean("enabled")),
		height(NewReal("height")),
		helpContextId(NewInteger("helpContextId")),
		helpKeyword(NewString("helpKeyword")),
		ignoreSkin(NewBoolean("ignoreSkin")),
		left(NewReal("left")),
		mouseCursor(NewBinary("mouseCursor")),
		mousePointer(NewInteger("mousePointer")),
		name(NewString("name", 101)),
		securityLevelEnabled(NewInteger("securityLevelEnabled")),
		securityLevelVisible(NewInteger("securityLevelVisible")),
		skinCategoryName(NewString("skinCategoryName", 101)),
		tag(NewString("tag")),
		top(NewReal("top")),
		visible(NewBoolean("visible")),
		width(NewReal("width"))
	{
		name->MakeUnwritten()->bind(&Window::name);
	}
}