#include <jadegit/data/JadeExposedList.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeExposedListMeta.h>
#include <jadegit/data/RootSchema/JadeExposedClassMeta.h>
#include <jadegit/data/RootSchema/JadeExposedFeatureMeta.h>
#include <jadegit/data/CollClass.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path JadeExposedList::subFolder("exposures");

	static EntityRegistration<JadeExposedList, Schema> jadeExposedList("JadeExposedList", "exposed list", &Schema::exposedLists);
	static EntityRegistration<JadeExposedClass, JadeExposedList> jadeExposedClass("JadeExposedClass", "exposed class", &JadeExposedList::exposedClasses);
	static EntityRegistration<JadeExposedFeature, JadeExposedClass> jadeExposedFeature("JadeExposedFeature", "exposed feature", &JadeExposedClass::exposedFeatures);

	template EntityDict<JadeExposedClass, &JadeExposedListMeta::exposedClasses>;
	template ObjectValue<Schema* const, &JadeExposedListMeta::schema>;

	JadeExposedList::JadeExposedList(Schema* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExposedList), name),
		schema(parent)
	{
	}

	void JadeExposedList::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template ObjectValue<Array<Constant*>, &JadeExposedClassMeta::consts>;
	template EntityDict<JadeExposedFeature, &JadeExposedClassMeta::exposedFeatures>;
	template ObjectValue<JadeExposedList* const, &JadeExposedClassMeta::exposedList>;
	template ObjectValue<Array<Method*>, &JadeExposedClassMeta::methods>;
	template ObjectValue<Array<Property*>, &JadeExposedClassMeta::properties>;
	template ObjectValue<Class*, &JadeExposedClassMeta::relatedClass>;

	JadeExposedClass::JadeExposedClass(JadeExposedList* parent, const Class* dataClass, const char* name) : Entity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExposedClass), name),
		exposedList(parent)
	{
	}

	void JadeExposedClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template ObjectValue<JadeExposedClass* const, &JadeExposedFeatureMeta::exposedClass>;
	template ObjectValue<Feature*, &JadeExposedFeatureMeta::relatedFeature>;

	JadeExposedFeature::JadeExposedFeature(JadeExposedClass* parent, const Class* dataClass, const char* name) : Entity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExposedFeature), name),
		exposedClass(parent)
	{
	}

	void JadeExposedFeature::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeExposedListMeta::JadeExposedListMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "JadeExposedList", superclass),
		exposedClasses(NewReference<ExplicitInverseRef>("exposedClasses", NewType<CollClass>("JadeExposedClassNDict"))),
		name(NewString("name", 101)),
		priorVersion(NewInteger("priorVersion")),
		registryId(NewString("registryId")),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		secureService(NewBoolean("secureService")),
		sessionHandling(NewBoolean("sessionHandling")),
		useBareFormat(NewBoolean("useBareFormat")),
		useEncodedFormat(NewBoolean("useEncodedFormat")),
		useHttpGet(NewBoolean("useHttpGet")),
		useHttpPost(NewBoolean("useHttpPost")),
		useRPC(NewBoolean("useRPC")),
		useSOAP12(NewBoolean("useSOAP12")),
		version(NewInteger("version")),
		versionControl(NewBoolean("versionControl"))
	{
		exposedClasses->automatic().bind(&JadeExposedList::exposedClasses);
		name->MakeUnwritten()->bind(&JadeExposedList::name);
		priorVersion->bind(&JadeExposedList::priorVersion);
		registryId->bind(&JadeExposedList::registryId);
		schema->manual().parent().bind(&JadeExposedList::schema);
		secureService->bind(&JadeExposedList::secureService);
		sessionHandling->bind(&JadeExposedList::sessionHandling);
		useBareFormat->bind(&JadeExposedList::useBareFormat);
		useEncodedFormat->bind(&JadeExposedList::useEncodedFormat);
		useHttpGet->bind(&JadeExposedList::useHttpGet);
		useHttpPost->bind(&JadeExposedList::useHttpPost);
		useRPC->bind(&JadeExposedList::useRPC);
		useSOAP12->bind(&JadeExposedList::useSOAP12);
		version->bind(&JadeExposedList::version);
		versionControl->bind(&JadeExposedList::versionControl);
	}

	JadeExposedClassMeta::JadeExposedClassMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "JadeExposedClass", superclass),
		classAutoAdded(NewBoolean("classAutoAdded")),
		consts(NewReference<ExplicitInverseRef>("consts", NewType<CollClass>("ConstantNDict"))),
		defaultStyle(NewByte("defaultStyle")),
		exposedFeatures(NewReference<ExplicitInverseRef>("exposedFeatures", NewType<CollClass>("JadeExposedFeatureNDict"))),
		exposedList(NewReference<ExplicitInverseRef>("exposedList", NewType<Class>("JadeExposedList"))),
		exposedName(NewString("exposedName")),
		methods(NewReference<ExplicitInverseRef>("methods", NewType<CollClass>("MethodNDict"))),
		name(NewString("name", 101)),
		properties(NewReference<ExplicitInverseRef>("properties", NewType<CollClass>("PropertyNDict"))),
		relatedClass(NewReference<ExplicitInverseRef>("relatedClass", NewType<Class>("Class")))
	{
		consts->bind(&JadeExposedClass::consts);
		defaultStyle->bind(&JadeExposedClass::defaultStyle);
		exposedFeatures->automatic().bind(&JadeExposedClass::exposedFeatures);
		exposedList->manual().parent().bind(&JadeExposedClass::exposedList);
		exposedName->bind(&JadeExposedClass::exposedName);
		methods->bind(&JadeExposedClass::methods);
		name->MakeUnwritten()->bind(&JadeExposedClass::name);
		properties->bind(&JadeExposedClass::properties);
		relatedClass->MakeUnwritten()->bind(&JadeExposedClass::relatedClass);
	}

	JadeExposedFeatureMeta::JadeExposedFeatureMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "JadeExposedFeature", superclass),
		dbField(NewBoolean("dbField")),
		exposedClass(NewReference<ExplicitInverseRef>("exposedClass", NewType<Class>("JadeExposedClass"))),
		exposedName(NewString("exposedName")),
		exposedType(NewString("exposedType")),
		lazyRead(NewBoolean("lazyRead")),
		name(NewString("name", 101)),
		relatedFeature(NewReference<ExplicitInverseRef>("relatedFeature", NewType<Class>("Feature")))
	{
		exposedClass->manual().parent().bind(&JadeExposedFeature::exposedClass);
		exposedName->bind(&JadeExposedFeature::exposedName);
		exposedType->bind(&JadeExposedFeature::exposedType);
		name->MakeUnwritten()->bind(&JadeExposedFeature::name);
		relatedFeature->MakeUnwritten()->bind(&JadeExposedFeature::relatedFeature);
	}
}