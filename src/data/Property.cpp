#include <jadegit/data/Property.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema/PropertyMeta.h>
#include <Log.h>
#include "FeatureRegistration.h"

namespace JadeGit::Data
{
	static PropertyRegistration<Property> property("Property");

	Property::Property(Class* parent, const Class* dataClass, const char* name, Type* type) : Feature(parent, dataClass, name)
	{
		schemaType = parent;
		this->type = type;
	}

	AnyValue& Property::getActualValue(const Object& object) const
	{
		return *getActualValue(object, true);
	}

	AnyValue* Property::getActualValue(const Object& object, bool instantiate) const
	{
		// Return value stored explicitly
		if (member)
			return const_cast<AnyValue*>(&member->value(object));

		// Return value stored dynamically
		auto iter = object.values.find(this);
		if (iter != object.values.end())
			return iter->second;

		if (!instantiate)
			return nullptr;

		// Instantiate value
		AnyValue* value = InstantiateValue(const_cast<Object&>(object));
		const_cast<Object&>(object).values[this] = value;
		return value;
	}

	int Property::GetLength() const
	{
		return type->GetDefaultLength();
	}

	Any Property::GetValue(const Object &object, bool instantiate) const
	{
		// Return copy of actual value, instantiating if required
		if (auto value = getActualValue(object, instantiate))
			return *value;

		// Return default value
		return *GetDefaultValue();
	}

	const AnyValue* Property::GetDefaultValue() const
	{
		// Setup default value if required
		if (!default_)
			const_cast<Property*>(this)->default_.reset(type->CreateValue());

		return default_.get();
	}

	AnyValue* Property::InstantiateValue() const
	{
		return type->CreateValue();
	}

	void Property::resetValue(Object& object) const
	{
		if (auto value = getActualValue(object, false))
		{
			value->reset();

			// Flag object as being modified
			object.Modified();
		}
	}

	void Property::LoadFor(Object &object, const tinyxml2::XMLElement* source, bool initial, bool shallow, std::queue<std::future<void>>& tasks) const
	{
		bool structural = isStructural();

		// Don't reload structural properties after initial load
		if (structural && !initial)
			return;

		// Don't load non-structural properties during shallow load
		if (!structural && shallow)
			return;

		LOG_DEBUG("Loading property [" << name << "]");
		type->LoadFor(object, *this, source, tasks);
	}

	void Property::WriteFor(const Object &object, tinyxml2::XMLNode& parent) const
	{
		if(WriteFilter())
			type->WriteFor(object, *this, parent);
	}

	PropertyMeta::PropertyMeta(RootSchema& parent, const FeatureMeta& superclass) : RootClass(parent, "Property", superclass),
		embedded(NewBoolean("embedded")),
		exportedPropertyRefs(NewReference<ExplicitInverseRef>("exportedPropertyRefs", NewType<CollClass>("JadeExportedPropertySet"))),
		isHTMLProperty(NewBoolean("isHTMLProperty")),
		keyPathRefs(NewReference<ExplicitInverseRef>("keyPathRefs", NewType<CollClass>("MemberKeySet"))),
		required(NewBoolean("required")),
		virtual_(NewBoolean("virtual")),
		webService(NewBoolean("webService"))
	{
		embedded->bind(&Property::embedded);
		exportedPropertyRefs->automatic();
		isHTMLProperty->bind(&Property::isHTMLProperty);
		keyPathRefs->automatic();
		required->bind(&Property::required);
		virtual_->bind(&Property::virtual_);
		webService->bind(&Property::webService);
	}
}