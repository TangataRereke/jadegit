#include <jadegit/data/JadeWebServicesClass.h>
#include <jadegit/data/JadeWebServicesMethod.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeWebServicesMethodMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceConsumerClassMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceProviderClassMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceSoapHeaderClassMeta.h>
#include "TypeRegistration.h"
#include "FeatureRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(JadeWebServicesMethod)
	DEFINE_OBJECT_CAST(JadeWebServiceSoapHeaderClass)

	static MethodRegistration<JadeWebServicesMethod> jadeWebServicesMethod("JadeWebServicesMethod");
	static TypeRegistration<JadeWebServicesClass, Schema> jadeWebServicesClass("JadeWebServicesClass", &Schema::classes);
	static TypeRegistration<JadeWebServiceConsumerClass, Schema> jadeWebServiceConsumerClass("JadeWebServiceConsumerClass", &Schema::classes);
	static TypeRegistration<JadeWebServiceProviderClass, Schema> jadeWebServiceProviderClass("JadeWebServiceProviderClass", &Schema::classes);
	static TypeRegistration<JadeWebServiceSoapHeaderClass, Schema> jadeWebServiceSoapHeaderClass("JadeWebServiceSoapHeaderClass", &Schema::classes);

	std::map<JadeWebServicesMethod::RPC, const char*> EnumStrings<JadeWebServicesMethod::RPC>::data =
	{
		{ JadeWebServicesMethod::RPC::None, "null" },
		{ JadeWebServicesMethod::RPC::Default, "default" },
		{ JadeWebServicesMethod::RPC::Yes, "yes" },
		{ JadeWebServicesMethod::RPC::No, "no" }
	};

	std::map<JadeWebServiceProviderClass::SecureService, const char*> EnumStrings<JadeWebServiceProviderClass::SecureService>::data =
	{
		{ JadeWebServiceProviderClass::SecureService::Secure, "secure" },
		{ JadeWebServiceProviderClass::SecureService::NotSecure, "notSecure" },
		{ JadeWebServiceProviderClass::SecureService::Default, "default" }
	};

	std::map<JadeWebServiceSoapHeaderClass::Direction, const char*> EnumStrings<JadeWebServiceSoapHeaderClass::Direction>::data =
	{
		{ JadeWebServiceSoapHeaderClass::Direction::Input, "input" },
		{ JadeWebServiceSoapHeaderClass::Direction::Output, "output" },
		{ JadeWebServiceSoapHeaderClass::Direction::IO, "io" }
	};

	JadeWebServicesMethod::JadeWebServicesMethod(Type* parent, const Class* dataClass, const char* name) : JadeMethod(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeWebServicesMethod), name)
	{
	};

	void JadeWebServicesMethod::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeWebServicesClass::JadeWebServicesClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : Class(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeWebServicesClass), name, superclass) {}

	void JadeWebServicesClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeWebServiceConsumerClass::JadeWebServiceConsumerClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : JadeWebServicesClass(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeWebServiceConsumerClass), name, superclass) {}

	void JadeWebServiceConsumerClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeWebServiceProviderClass::JadeWebServiceProviderClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : JadeWebServicesClass(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeWebServiceProviderClass), name, superclass) {}

	void JadeWebServiceProviderClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeWebServiceSoapHeaderClass::JadeWebServiceSoapHeaderClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass) : JadeWebServicesClass(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeWebServiceSoapHeaderClass), name, superclass)
	{
	}

	void JadeWebServiceSoapHeaderClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeWebServicesMethodMeta::JadeWebServicesMethodMeta(RootSchema& parent, const JadeMethodMeta& superclass) : RootClass(parent, "JadeWebServicesMethod", superclass),
		inputEncodingStyle(NewString("inputEncodingStyle")),
		inputNamespace(NewString("inputNamespace")),
		inputUsesEncodedFormat(NewBoolean("inputUsesEncodedFormat")),
		outputEncodingStyle(NewString("outputEncodingStyle")),
		outputNamespace(NewString("outputNamespace")),
		outputUsesEncodedFormat(NewBoolean("outputUsesEncodedFormat")),
		soapAction(NewString("soapAction")),
		soapHeaders(NewReference<ExplicitInverseRef>("soapHeaders", NewType<CollClass>("ObjectArray"))),
		useBareStyle(NewBoolean("useBareStyle")),
		useSoap12(NewBoolean("useSoap12")),
		usesRPC(NewCharacter("usesRPC"))
	{
		inputEncodingStyle->bind(&JadeWebServicesMethod::inputEncodingStyle);
		inputNamespace->bind(&JadeWebServicesMethod::inputNamespace);
		inputUsesEncodedFormat->bind(&JadeWebServicesMethod::inputUsesEncodedFormat);
		outputEncodingStyle->bind(&JadeWebServicesMethod::outputEncodingStyle);
		outputNamespace->bind(&JadeWebServicesMethod::outputNamespace);
		outputUsesEncodedFormat->bind(&JadeWebServicesMethod::outputUsesEncodedFormat);
		soapAction->bind(&JadeWebServicesMethod::soapAction);
		soapHeaders->bind(&JadeWebServicesMethod::soapHeaders);
		useBareStyle->bind(&JadeWebServicesMethod::useBareStyle);
		useSoap12->bind(&JadeWebServicesMethod::useSoap12);
		usesRPC->bind(&JadeWebServicesMethod::usesRPC);
	}

	JadeWebServicesClassMeta::JadeWebServicesClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "JadeWebServicesClass", superclass),
		additionalInfo(NewString("additionalInfo")),
		wsdl(NewString("wsdl"))
	{
		additionalInfo->bind(&JadeWebServicesClass::additionalInfo);
		wsdl->bind(&JadeWebServicesClass::wsdl);
	}

	JadeWebServiceConsumerClassMeta::JadeWebServiceConsumerClassMeta(RootSchema& parent, const JadeWebServicesClassMeta& superclass) : RootClass(parent, "JadeWebServiceConsumerClass", superclass),
		endPointURL(NewString("endPointURL")),
		useAsyncCalls(NewBoolean("useAsyncCalls")),
		useNewPrimTypes(NewBoolean("useNewPrimTypes")),
		useSOAP12(NewBoolean("useSOAP12"))
	{
		endPointURL->bind(&JadeWebServiceConsumerClass::endPointURL);
		useAsyncCalls->bind(&JadeWebServiceConsumerClass::useAsyncCalls);
		useNewPrimTypes->bind(&JadeWebServiceConsumerClass::useNewPrimTypes);
		useSOAP12->bind(&JadeWebServiceConsumerClass::useSOAP12);
	}

	JadeWebServiceProviderClassMeta::JadeWebServiceProviderClassMeta(RootSchema& parent, const JadeWebServicesClassMeta& superclass) : RootClass(parent, "JadeWebServiceProviderClass", superclass),
		secureService(NewByte("secureService"))
	{
		secureService->SetDefault(Value(JadeWebServiceProviderClass::SecureService::Default))->bind(&JadeWebServiceProviderClass::secureService);
	}

	JadeWebServiceSoapHeaderClassMeta::JadeWebServiceSoapHeaderClassMeta(RootSchema& parent, const JadeWebServicesClassMeta& superclass) : RootClass(parent, "JadeWebServiceSoapHeaderClass", superclass),
		_usedInMethods(NewReference<ExplicitInverseRef>("_usedInMethods", NewType<CollClass>("MethodSet"))),
		actor(NewString("actor")),
		direction(NewCharacter("direction")),
		mustUnderstand(NewBoolean("mustUnderstand"))
	{
		_usedInMethods->automatic().bind(&JadeWebServiceSoapHeaderClass::webServicesMethodRefs);
		actor->bind(&JadeWebServiceSoapHeaderClass::actor);
		direction->SetDefault(Value(JadeWebServiceSoapHeaderClass::Direction::Input))->bind(&JadeWebServiceSoapHeaderClass::direction);
		mustUnderstand->bind(&JadeWebServiceSoapHeaderClass::mustUnderstand);
	}
}