#include <jadegit/data/Value.h>
#include <iomanip>

namespace JadeGit::Data
{
	template<>
	bool Value<char>::empty() const
	{
		return !value;
	}

	// Partial specializations for converting strings
	template<>
	void Value<bool>::Set(const std::string& v)
	{
		operator=(v == "true");
	}

	template<>
	void Value<Byte>::Set(const std::string& v)
	{
		operator=(v.empty() ? 0 : static_cast<Byte>(std::stoi(v)));
	}

	template<>
	void Value<char>::Set(const std::string& v)
	{
		operator=(v.empty() ? 0 : *v.c_str());
	}

	template<>
	void Value<int>::Set(const std::string& v)
	{
		operator=(v.empty() ? 0 : std::stoi(v));
	}

	template<>
	void Value<double>::Set(const std::string& v)
	{
		operator=(v.empty() ? 0 : std::stod(v));
	}

	// Partial specializations for formatting strings
	template<>
	std::string Value<bool>::ToString() const
	{
		return value ? "true" : "false";
	}

	template<>
	std::string Value<Byte>::ToString() const
	{
		if (!value)
			return "0";

		std::ostringstream os;
		os << static_cast<int>(value);
		return os.str();
	}

	template<>
	std::string Value<char>::ToString() const
	{
		// Return empty string for null characters
		if (!value)
			return "";

		std::ostringstream os;
		os << value;
		return os.str();
	}

	template<>
	std::string Value<double>::ToString() const
	{
		std::ostringstream os;
		os << std::setprecision(std::numeric_limits<double>::digits10 + 1) << value;
		return os.str();
	}
	
	template Value<Binary>;
	template Value<bool>;
	template Value<Byte>;
	template Value<char>;
	template Value<double>;
	template Value<int>;
	template Value<std::string>;

	// Partial specializations for casting primitive values
	template <>
	static int AnyCast<int>::Get(const AnyValue* v)
	{
		// Attempt direct cast
		if (const TypeValue<int>* value = dynamic_cast<const TypeValue<int>*>(v))
			return *value;

		// Attempt string cast
		if (const TypeValue<std::string>* string = dynamic_cast<const TypeValue<std::string>*>(v))
		{
			Value<int> value;
			value.Set(*string);
			return value;
		}

		// Attempt casting from other numeric types
		if (const TypeValue<char>* value = dynamic_cast<const TypeValue<char>*>(v))
			return *value;

		throw jadegit_cast_exception(typeid(*v), typeid(int));
	};
}