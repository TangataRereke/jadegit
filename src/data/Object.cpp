#include <jadegit/data/Assembly.h>
#include <jadegit/data/File.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include "ObjectRegistration.h"
#include "State.h"
#include <assert.h>
#include <algorithm>

namespace JadeGit::Data
{
	static ObjectRegistration<Object, Object> registrar("Object");

	const Class* Object::GetDataClass(const RootSchema& rootSchema, const char* dataClassName)
	{
		Class* dataClass = rootSchema->classes.Get(dataClassName);
		return dataClass ? dataClass : new Class(rootSchema, nullptr, dataClassName);
	}

	const Class* Object::GetDataClass(Component* parent, const char* dataClassName)
	{
		if (!parent)
			return nullptr;

		return GetDataClass(parent->getAssembly().GetRootSchema(), dataClassName);
	}

	std::function<Object* ()> Object::resolver(const char* klass, const Component* origin, const tinyxml2::XMLElement* source)
	{
		return ObjectFactory::Get().resolver(klass, origin, source);
	}

	std::ostream& operator<<(std::ostream& os, const Object* object)
	{
		if (!object)
			os << "nullptr";
		else
		{
			if (!object->dataClass)
				os << typeid(*object).name();
			else
			{
				auto& name = object->dataClass->name;
				os << std::string(name.begin(), name.end());
			}

			os << " #" << static_cast<const void*>(object);
		}

		return os;
	}

	Object::Object(Object* parent, const Class* dataClass) : Object(parent, dataClass, parent&& parent->isStatic() ? StaticState::Instance() : nullptr) {}

	Object::Object(Assembly& parent, const Class* dataClass, const State* state) : Component(&parent),
		state(state ? state : InitialState::Instance())
	{
		if (dataClass)
			SetDataClass(*dataClass);
	}

	Object::Object(Object* parent, const Class* dataClass, const State* state) : Component(parent),
		state(state ? state : InitialState::Instance())
	{
		if (dataClass)
			SetDataClass(*dataClass);

		// Add to children collection
		if (parent)
			parent->children.push_back(this);
	}

	Object::~Object()
	{
		purgeValues();

		thread_local const Object* origin = nullptr;

		// Remove from parent if it's not caused the delete (caters for scenarios where object creation may have failed due to exception being thrown)
		auto parent = getParentObject();
		if (parent && parent != origin)
		{
			auto it = std::find(std::begin(parent->children), std::end(parent->children), this);
			if (it != std::end(parent->children))
				parent->children.erase(it);
		}

		// Clean-up children
		for (auto& child : children)
		{
			origin = this;	// suppress collection update above
			dele(child);
			origin = nullptr;
		}
	}

	void Object::dele(Object* object)
	{
		// Values need to be purged prior to derived destructors, otherwise inverse maintenance
		// for dynamically created exclusive references fail because dynamic_cast checks fail  
		object->purgeValues();
		delete object;
	}

	void Object::purgeValues()
	{
		for (auto& pair : values)
			delete pair.second;

		values.clear();
	}

	Object* Object::getParentObject() const
	{
		if (!parent || !parent->getParent())
			return nullptr;

		return static_cast<Object*>(parent);
	}

	const Property* Object::getProperty(const std::string& name) const
	{
		return dataClass->GetProperty(name, true);
	}

	const RootSchema& Object::GetRootSchema() const
	{
		return getAssembly().GetRootSchema();
	}

	void Object::SetDataClass(const Class& dataClass)
	{
		assert(!this->dataClass);
		this->dataClass = &dataClass.getRootType();
	}

	bool Object::IsDescendent(const Object* ancestor) const
	{
		if (!ancestor)
			return false;

		if (this == ancestor)
			return true;

		auto parent = getParentObject();
		return parent ? parent->IsDescendent(ancestor) : false;
	}

	bool Object::isImplied() const
	{
		return state == ImpliedState::Instance();
	}

	bool Object::isInferred() const
	{
		return state == InferredState::Instance();
	}

	bool Object::isKindOf(const Type* type) const
	{
		return dataClass && dataClass->InheritsFrom(type);
	}

	bool Object::isLoading() const
	{
		return state->isLoading();
	}

	bool Object::isModified() const
	{
		return state->isModified();
	}

	bool Object::isShallow() const
	{
		return state->isShallow();
	}

	bool Object::isStatic() const
	{
		return state == StaticState::Instance();
	}

	void Object::Clean()
	{
		state->clean(this);
	}

	void Object::Dirty(bool deep)
	{
		state->dirty(this, deep);
	}

	void Object::loading(bool shallow)
	{
		state->loading(this, shallow);
	}

	void Object::loaded(bool shallow)
	{
		state->loaded(this, shallow, true);
	}

	void Object::loaded(bool shallow, std::queue<std::future<void>>& tasks)
	{
		// Handle post-load tasks first time
		if (shallow || !state->isShallow())
			loaded(tasks);

		state->loaded(this, shallow, false);
	}

	void Object::Modified()
	{
		state->modified(this);
	}

	Any Object::GetValue(const Property* property, bool instantiate) const
	{
		return property->GetValue(*this, instantiate);
	}

	Any Object::GetValue(const std::string& name, bool instantiate) const
	{
		return GetValue(dataClass->GetProperty(name, true), instantiate);
	}

	void Object::resetValue(const Property* property)
	{
		property->resetValue(*this);
	}

	void Object::SetValue(const Property* property, const Any& value)
	{
		property->SetValue(*this, value);
	}

	void Object::SetValue(const Property* property, const Binary& value)
	{
		SetValue(property, Any(Value(value)));
	}

	void Object::SetValue(const Property* property, bool value)
	{
		SetValue(property, Any(Value(value)));
	}

	void Object::SetValue(const Property* property, Byte value)
	{
		SetValue(property, Any(Value(value)));
	}

	void Object::SetValue(const Property* property, char value)
	{
		SetValue(property, Any(Value(value)));
	}

	void Object::SetValue(const Property* property, double value)
	{
		SetValue(property, Any(Value(value)));
	}

	void Object::SetValue(const Property* property, int value)
	{
		SetValue(property, Any(Value(value)));
	}

	void Object::SetValue(const Property* property, const char* value)
	{
		SetValue(property, value ? std::string(value) : std::string());
	}

	void Object::SetValue(const Property* property, const std::string& value)
	{
		property->SetValue(*this, value);
	}

	void Object::SetValue(const Property* property, Object* value)
	{
		SetValue(property, Any(Value(value)));
	}

	void Object::Write(tinyxml2::XMLNode &parent, tinyxml2::XMLElement* &element, const Object* origin, bool reference) const
	{
		// Setup element if required
		if (!element)
		{
			element = parent.GetDocument()->NewElement(dataClass->name.c_str());
			parent.InsertEndChild(element);
		}
		
		// Write key details
		WriteHeader(element, origin, reference);

		// Write body if not a reference
		if (!reference)
			WriteBody(element);
	}

	void Object::WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const
	{
		// Must be re-implemented to write relevant keys
		assert(!reference);
	}

	void Object::WriteBody(tinyxml2::XMLElement* element) const
	{
		// Write properties using data-class hierarchy bottom up (most specific first)
		auto dataClass = this->dataClass;
		while (dataClass)
		{
			dataClass = &dataClass->getRootType();

			for (auto prop : dataClass->properties)
				prop.second->WriteFor(*this, *element);

			// Get next super class
			dataClass = dataClass->getSuperClass();
		}

		// Copy/sort children into brackets
		std::pmr::vector<Object*> children = this->children;
		std::stable_sort(children.begin(), children.end(), [](const Object* a, const Object* b) { return a->GetBracket() < b->GetBracket(); });

		// Write children
		for (Object* object : children)
		{
			// Skip inferred objects
			if (object->isInferred())
				continue;

			tinyxml2::XMLElement* child = nullptr;
			object->Write(*element, child, this, false);
		}
	}

	Object& Object::Mutate(Object& into)
	{
		// Resolve common base class
		auto baseClass = this->dataClass;
		while (!into.dataClass->InheritsFrom(baseClass))
			baseClass = baseClass->getSuperClass();

		// TODO - Copy common properties, including references to preserve relationships
		// Not immediately urgent, but this'll likely be needed for scenario where an ExplicitInverseRef/ImplicitInverseRef
		// is being converted which is also used as a key by a collection class that is being updated simultaneously
		assert(baseClass);

		// Transfer current state & flag as modified
		into.state = state;

		// Delete self (now converted)
		delete this;

		// Flag replacement as modified and return
		into.Modified();
		return into;
	}

	ObjectMeta::ObjectMeta(RootSchema& parent) : RootClass(parent, "Object", nullptr) {}
}