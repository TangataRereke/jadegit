#include <jadegit/data/Schema.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ActiveXClassMeta.h>
#include <jadegit/data/RootSchema/ActiveXAttributeMeta.h>
#include <jadegit/data/RootSchema/ActiveXConstantMeta.h>
#include <jadegit/data/RootSchema/ActiveXMethodMeta.h>
#include <jadegit/data/RootSchema/ActiveXInterfaceMeta.h>
#include <jadegit/data/RootSchema/ActiveXLibraryMeta.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/data/RootSchema/CollectionMeta.h>
#include <jadegit/data/RootSchema/DbClassMapMeta.h>
#include <jadegit/data/RootSchema/DevControlTypesMeta.h>
#include <jadegit/data/RootSchema/DevControlPropertiesMeta.h>
#include <jadegit/data/RootSchema/InverseMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicPropertyClusterMeta.h>
#include <jadegit/data/RootSchema/JadeExposedClassMeta.h>
#include <jadegit/data/RootSchema/JadeExposedFeatureMeta.h>
#include <jadegit/data/RootSchema/JadeExposedListMeta.h>
#include <jadegit/data/RootSchema/JadeHTMLDocumentMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceManagerMeta.h>
#include <jadegit/data/RootSchema/ExternalKeyMeta.h>
#include <jadegit/data/RootSchema/MemberKeyMeta.h>
#include <jadegit/data/RootSchema/MenuItemMeta.h>
#include <jadegit/data/RootSchema/RACollectionValueMeta.h>
#include <jadegit/data/RootSchema/RAMethodMeta.h>
#include <jadegit/data/RootSchema/RAOidMeta.h>
#include <jadegit/data/RootSchema/RAParameterMeta.h>
#include <jadegit/data/RootSchema/RAPropertyMeta.h>
#include <jadegit/data/RootSchema/RARelationIndexMeta.h>
#include <jadegit/data/RootSchema/RARelationKeyMeta.h>
#include <jadegit/data/RootSchema/RARelationOidMeta.h>
#include <jadegit/data/RootSchema/RARpsPropertyMeta.h>
#include <jadegit/data/RootSchema/RelationalTableClassMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMethMeta.h>
#include <jadegit/data/RootSchema/RelationalTableRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalTableXRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalViewMeta.h>
#include <jadegit/data/RootSchema/SchemaMeta.h>
#include <jadegit/data/RootSchema/ConstantCategoryMeta.h>
#include <jadegit/data/RootSchema/DatabaseMeta.h>
#include <jadegit/data/RootSchema/DbFileMeta.h>
#include <jadegit/data/RootSchema/JadeImportedConstantMeta.h>
#include <jadegit/data/RootSchema/JadeImportedMethodMeta.h>
#include <jadegit/data/RootSchema/JadeImportedPropertyMeta.h>
#include <jadegit/data/RootSchema/DateFormatMeta.h>
#include <jadegit/data/RootSchema/CurrencyFormatMeta.h>
#include <jadegit/data/RootSchema/TimeFormatMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicCompAttributeMeta.h>
#include <jadegit/data/RootSchema/ExternalPrimAttributeMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicPrimAttributeMeta.h>
#include <jadegit/data/RootSchema/ExternalReferenceMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicExplicitInverseRefMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicImplicitInverseRefMeta.h>
#include <jadegit/data/RootSchema/GlobalConstantMeta.h>
#include <jadegit/data/RootSchema/TranslatableStringMeta.h>
#include <jadegit/data/RootSchema/FunctionMeta.h>
#include <jadegit/data/RootSchema/JadeAnnotationTagMeta.h>
#include <jadegit/data/RootSchema/LockPolicyMeta.h>
#include <jadegit/data/RootSchema/ExternalMethodMeta.h>
#include <jadegit/data/RootSchema/JadeAnnotationContractMeta.h>
#include <jadegit/data/RootSchema/JadeInterfaceMethodMeta.h>
#include <jadegit/data/RootSchema/JadeWebServicesMethodMeta.h>
#include <jadegit/data/RootSchema/PathExpressionMeta.h>
#include <jadegit/data/RootSchema/JadeExportedConstantMeta.h>
#include <jadegit/data/RootSchema/JadeExportedMethodMeta.h>
#include <jadegit/data/RootSchema/JadeExportedPropertyMeta.h>
#include <jadegit/data/RootSchema/JadeExportedClassMeta.h>
#include <jadegit/data/RootSchema/JadeExportedInterfaceMeta.h>
#include <jadegit/data/RootSchema/JadeExportedPackageMeta.h>
#include <jadegit/data/RootSchema/JadeImportedPackageMeta.h>
#include <jadegit/data/RootSchema/LibraryMeta.h>
#include <jadegit/data/RootSchema/LocaleMeta.h>
#include <jadegit/data/RootSchema/JadeUserCollClassMeta.h>
#include <jadegit/data/RootSchema/ExceptionClassMeta.h>
#include <jadegit/data/RootSchema/ActiveXGuiClassMeta.h>
#include <jadegit/data/RootSchema/HTMLClassMeta.h>
#include <jadegit/data/RootSchema/JadeAnnotationClassMeta.h>
#include <jadegit/data/RootSchema/JadeImportedClassMeta.h>
#include <jadegit/data/RootSchema/JadeUserClassMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceConsumerClassMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceProviderClassMeta.h>
#include <jadegit/data/RootSchema/JadeWebServiceSoapHeaderClassMeta.h>
#include <jadegit/data/RootSchema/JadeGenericTypeMeta.h>
#include <jadegit/data/RootSchema/JadeImportedInterfaceMeta.h>
#include <jadegit/data/RootSchema/PrimTypeMeta.h>
#include <jadegit/data/RootSchema/PseudoTypeMeta.h>
#include <jadegit/data/RootSchema/FeatureUsageMeta.h>
#include <jadegit/data/RootSchema/KeyUsageMeta.h>
#include <jadegit/data/RootSchema/InlineTypeUsageMeta.h>
#include <jadegit/data/RootSchema/JadeLocalVarMeta.h>
#include <jadegit/data/RootSchema/ParameterMeta.h>
#include <jadegit/data/RootSchema/ReturnTypeMeta.h>
#include <jadegit/data/RootSchema/ActiveXControlMeta.h>
#include <jadegit/data/RootSchema/BaseControlMeta.h>
#include <jadegit/data/RootSchema/BrowseButtonsMeta.h>
#include <jadegit/data/RootSchema/ButtonMeta.h>
#include <jadegit/data/RootSchema/CheckBoxMeta.h>
#include <jadegit/data/RootSchema/ComboBoxMeta.h>
#include <jadegit/data/RootSchema/FolderMeta.h>
#include <jadegit/data/RootSchema/FrameMeta.h>
#include <jadegit/data/RootSchema/SheetMeta.h>
#include <jadegit/data/RootSchema/JadeDockBarMeta.h>
#include <jadegit/data/RootSchema/JadeDockContainerMeta.h>
#include <jadegit/data/RootSchema/JadeDotNetVisualComponentMeta.h>
#include <jadegit/data/RootSchema/JadeEditMaskMeta.h>
#include <jadegit/data/RootSchema/JadeRichTextMeta.h>
#include <jadegit/data/RootSchema/JadeEditorMeta.h>
#include <jadegit/data/RootSchema/JadeXamlControlMeta.h>
#include <jadegit/data/RootSchema/ProgressBarMeta.h>
#include <jadegit/data/RootSchema/WebHotSpotMeta.h>
#include <jadegit/data/RootSchema/WebInsertMeta.h>
#include <jadegit/data/RootSchema/WebJavaAppletMeta.h>
#include <jadegit/data/RootSchema/ListBoxMeta.h>
#include <jadegit/data/RootSchema/MultiMediaMeta.h>
#include <jadegit/data/RootSchema/OcxMeta.h>
#include <jadegit/data/RootSchema/OleControlMeta.h>
#include <jadegit/data/RootSchema/OptionButtonMeta.h>
#include <jadegit/data/RootSchema/JadeMaskMeta.h>
#include <jadegit/data/RootSchema/HScrollMeta.h>
#include <jadegit/data/RootSchema/VScrollMeta.h>
#include <jadegit/data/RootSchema/StatusLineMeta.h>
#include <jadegit/data/RootSchema/TableMeta.h>
#include <jadegit/data/RootSchema/WebHTMLMeta.h>
#include <jadegit/data/RootSchema/FormMeta.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/HTMLClass.h>
#include <jadegit/data/JadeWebServicesClass.h>

namespace JadeGit::Data
{
	template<class TType>
	RootClass<TType>::RootClass(RootSchema& parent, const char* name, Class* superclass) : MetaClass<TType>(parent, name, superclass) {}
	
	// Explicit instantiations
	template class RootClass<Class>;
	template class RootClass<CollClass>;
	template class RootClass<GUIClass>;
	
	template <class TType = Class>
	TType* NewClass(RootSchema& schema, const char* name, Class* super)
	{
		if (TType* type = schema->classes.Get<TType>(name))
		{
			type->superclass = super;
			return type;
		}
		return new TType(schema, nullptr, name, super);
	}
	
	template <class TType = Class>
	TType* NewClass(const Version& minVersion, RootSchema& schema, const char* name, Class* super)
	{
		if (!schema.version.empty() && minVersion > schema.version)
			return nullptr;
		
		return NewClass<TType>(schema, name, super);
	}
	
	JadeAnnotationTagMeta::JadeAnnotationTagMeta(RootSchema& parent, const RoutineMeta& superclass) : RootClass(parent, "JadeAnnotationTag", superclass),
		annotationClass(NewReference<ImplicitInverseRef>("annotationClass", NewType<Class>("JadeAnnotationClass"))),
		annotationValue(NewReference<ImplicitInverseRef>("annotationValue", NewType<Class>("Object"))),
		target(NewReference<ExplicitInverseRef>("target", NewType<Class>("Object"))) {}
	
	JadeAnnotationContractMeta::JadeAnnotationContractMeta(RootSchema& parent, const MethodMeta& superclass) : RootClass(parent, "JadeAnnotationContract", superclass),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))) {}
	
	JadeAnnotationClassMeta::JadeAnnotationClassMeta(RootSchema& parent, const ClassMeta& superclass) : RootClass(parent, "JadeAnnotationClass", superclass),
		_aliasName(NewString("_aliasName", 101)),
		_contract(NewReference<ImplicitInverseRef>("_contract", NewType<Class>("JadeAnnotationContract"))) {}
	
	JadeGenericTypeMeta::JadeGenericTypeMeta(RootSchema& parent, const TypeMeta& superclass) : RootClass(parent, "JadeGenericType", superclass),
		templateInterface(NewReference<ExplicitInverseRef>("templateInterface", NewType<Class>("JadeInterface"))),
		variance(NewInteger("variance")) {}
	
	const ObjectMeta& ObjectMeta::get(const Object& object) { return *object.GetRootSchema().object; }
	const ActiveXClassMeta& ActiveXClassMeta::get(const Object& object) { return *object.GetRootSchema().activeXClass; }
	const ActiveXFeatureMeta& ActiveXFeatureMeta::get(const Object& object) { return *object.GetRootSchema().activeXFeature; }
	const ActiveXAttributeMeta& ActiveXAttributeMeta::get(const Object& object) { return *object.GetRootSchema().activeXAttribute; }
	const ActiveXConstantMeta& ActiveXConstantMeta::get(const Object& object) { return *object.GetRootSchema().activeXConstant; }
	const ActiveXMethodMeta& ActiveXMethodMeta::get(const Object& object) { return *object.GetRootSchema().activeXMethod; }
	const ActiveXInterfaceMeta& ActiveXInterfaceMeta::get(const Object& object) { return *object.GetRootSchema().activeXInterface; }
	const ActiveXLibraryMeta& ActiveXLibraryMeta::get(const Object& object) { return *object.GetRootSchema().activeXLibrary; }
	const ApplicationMeta& ApplicationMeta::get(const Object& object) { return *object.GetRootSchema().application; }
	const CollectionMeta& CollectionMeta::get(const Object& object) { return *object.GetRootSchema().collection; }
	const DbClassMapMeta& DbClassMapMeta::get(const Object& object) { return *object.GetRootSchema().dbClassMap; }
	const DevelopmentMeta& DevelopmentMeta::get(const Object& object) { return *object.GetRootSchema().development; }
	const DevControlClassMeta& DevControlClassMeta::get(const Object& object) { return *object.GetRootSchema().devControlClass; }
	const DevControlTypesMeta& DevControlTypesMeta::get(const Object& object) { return *object.GetRootSchema().devControlTypes; }
	const DevControlPropertiesMeta& DevControlPropertiesMeta::get(const Object& object) { return *object.GetRootSchema().devControlProperties; }
	const InverseMeta& InverseMeta::get(const Object& object) { return *object.GetRootSchema().inverse; }
	const JadeDynamicPropertyClusterMeta& JadeDynamicPropertyClusterMeta::get(const Object& object) { return *object.GetRootSchema().jadeDynamicPropertyCluster; }
	const JadeExposedClassMeta& JadeExposedClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeExposedClass; }
	const JadeExposedFeatureMeta& JadeExposedFeatureMeta::get(const Object& object) { return *object.GetRootSchema().jadeExposedFeature; }
	const JadeExposedListMeta& JadeExposedListMeta::get(const Object& object) { return *object.GetRootSchema().jadeExposedList; }
	const JadeHTMLDocumentMeta& JadeHTMLDocumentMeta::get(const Object& object) { return *object.GetRootSchema().jadeHTMLDocument; }
	const JadeWebServiceManagerMeta& JadeWebServiceManagerMeta::get(const Object& object) { return *object.GetRootSchema().jadeWebServiceManager; }
	const KeyMeta& KeyMeta::get(const Object& object) { return *object.GetRootSchema().key; }
	const ExternalKeyMeta& ExternalKeyMeta::get(const Object& object) { return *object.GetRootSchema().externalKey; }
	const MemberKeyMeta& MemberKeyMeta::get(const Object& object) { return *object.GetRootSchema().memberKey; }
	const MenuItemDataMeta& MenuItemDataMeta::get(const Object& object) { return *object.GetRootSchema().menuItemData; }
	const MenuItemMeta& MenuItemMeta::get(const Object& object) { return *object.GetRootSchema().menuItem; }
	const RelationalAttributeMeta& RelationalAttributeMeta::get(const Object& object) { return *object.GetRootSchema().relationalAttribute; }
	const RACollectionValueMeta& RACollectionValueMeta::get(const Object& object) { return *object.GetRootSchema().raCollectionValue; }
	const RAMethodMeta& RAMethodMeta::get(const Object& object) { return *object.GetRootSchema().raMethod; }
	const RAOidMeta& RAOidMeta::get(const Object& object) { return *object.GetRootSchema().raOid; }
	const RAParameterMeta& RAParameterMeta::get(const Object& object) { return *object.GetRootSchema().raParameter; }
	const RAPropertyMeta& RAPropertyMeta::get(const Object& object) { return *object.GetRootSchema().raProperty; }
	const RARelationIndexMeta& RARelationIndexMeta::get(const Object& object) { return *object.GetRootSchema().raRelationIndex; }
	const RARelationKeyMeta& RARelationKeyMeta::get(const Object& object) { return *object.GetRootSchema().raRelationKey; }
	const RARelationOidMeta& RARelationOidMeta::get(const Object& object) { return *object.GetRootSchema().raRelationOid; }
	const RARpsPropertyMeta& RARpsPropertyMeta::get(const Object& object) { return *object.GetRootSchema().raRpsProperty; }
	const RelationalTableMeta& RelationalTableMeta::get(const Object& object) { return *object.GetRootSchema().relationalTable; }
	const RelationalTableClassMeta& RelationalTableClassMeta::get(const Object& object) { return *object.GetRootSchema().relationalTableClass; }
	const RelationalTableCollectionMeta& RelationalTableCollectionMeta::get(const Object& object) { return *object.GetRootSchema().relationalTableCollection; }
	const RelationalTableCollectionMethMeta& RelationalTableCollectionMethMeta::get(const Object& object) { return *object.GetRootSchema().relationalTableCollectionMeth; }
	const RelationalTableRelationshipMeta& RelationalTableRelationshipMeta::get(const Object& object) { return *object.GetRootSchema().relationalTableRelationship; }
	const RelationalTableXRelationshipMeta& RelationalTableXRelationshipMeta::get(const Object& object) { return *object.GetRootSchema().relationalTableXRelationship; }
	const RelationalViewMeta& RelationalViewMeta::get(const Object& object) { return *object.GetRootSchema().relationalView; }
	const SchemaMeta& SchemaMeta::get(const Object& object) { return *object.GetRootSchema().schema; }
	const SchemaEntityMeta& SchemaEntityMeta::get(const Object& object) { return *object.GetRootSchema().schemaEntity; }
	const ConstantCategoryMeta& ConstantCategoryMeta::get(const Object& object) { return *object.GetRootSchema().constantCategory; }
	const DatabaseMeta& DatabaseMeta::get(const Object& object) { return *object.GetRootSchema().database; }
	const DbFileMeta& DbFileMeta::get(const Object& object) { return *object.GetRootSchema().dbFile; }
	const FeatureMeta& FeatureMeta::get(const Object& object) { return *object.GetRootSchema().feature; }
	const JadeImportedFeatureMeta& JadeImportedFeatureMeta::get(const Object& object) { return *object.GetRootSchema().jadeImportedFeature; }
	const JadeImportedConstantMeta& JadeImportedConstantMeta::get(const Object& object) { return *object.GetRootSchema().jadeImportedConstant; }
	const JadeImportedMethodMeta& JadeImportedMethodMeta::get(const Object& object) { return *object.GetRootSchema().jadeImportedMethod; }
	const JadeImportedPropertyMeta& JadeImportedPropertyMeta::get(const Object& object) { return *object.GetRootSchema().jadeImportedProperty; }
	const LocaleFormatMeta& LocaleFormatMeta::get(const Object& object) { return *object.GetRootSchema().localeFormat; }
	const DateFormatMeta& DateFormatMeta::get(const Object& object) { return *object.GetRootSchema().dateFormat; }
	const NumberFormatMeta& NumberFormatMeta::get(const Object& object) { return *object.GetRootSchema().numberFormat; }
	const CurrencyFormatMeta& CurrencyFormatMeta::get(const Object& object) { return *object.GetRootSchema().currencyFormat; }
	const TimeFormatMeta& TimeFormatMeta::get(const Object& object) { return *object.GetRootSchema().timeFormat; }
	const PropertyMeta& PropertyMeta::get(const Object& object) { return *object.GetRootSchema().property; }
	const AttributeMeta& AttributeMeta::get(const Object& object) { return *object.GetRootSchema().attribute; }
	const CompAttributeMeta& CompAttributeMeta::get(const Object& object) { return *object.GetRootSchema().compAttribute; }
	const JadeDynamicCompAttributeMeta& JadeDynamicCompAttributeMeta::get(const Object& object) { return *object.GetRootSchema().jadeDynamicCompAttribute; }
	const PrimAttributeMeta& PrimAttributeMeta::get(const Object& object) { return *object.GetRootSchema().primAttribute; }
	const ExternalPrimAttributeMeta& ExternalPrimAttributeMeta::get(const Object& object) { return *object.GetRootSchema().externalPrimAttribute; }
	const JadeDynamicPrimAttributeMeta& JadeDynamicPrimAttributeMeta::get(const Object& object) { return *object.GetRootSchema().jadeDynamicPrimAttribute; }
	const ReferenceMeta& ReferenceMeta::get(const Object& object) { return *object.GetRootSchema().reference; }
	const ExplicitInverseRefMeta& ExplicitInverseRefMeta::get(const Object& object) { return *object.GetRootSchema().explicitInverseRef; }
	const ExternalReferenceMeta& ExternalReferenceMeta::get(const Object& object) { return *object.GetRootSchema().externalReference; }
	const JadeDynamicExplicitInverseRefMeta& JadeDynamicExplicitInverseRefMeta::get(const Object& object) { return *object.GetRootSchema().jadeDynamicExplicitInverseRef; }
	const ImplicitInverseRefMeta& ImplicitInverseRefMeta::get(const Object& object) { return *object.GetRootSchema().implicitInverseRef; }
	const JadeDynamicImplicitInverseRefMeta& JadeDynamicImplicitInverseRefMeta::get(const Object& object) { return *object.GetRootSchema().jadeDynamicImplicitInverseRef; }
	const ScriptMeta& ScriptMeta::get(const Object& object) { return *object.GetRootSchema().script; }
	const ConstantMeta& ConstantMeta::get(const Object& object) { return *object.GetRootSchema().constant; }
	const GlobalConstantMeta& GlobalConstantMeta::get(const Object& object) { return *object.GetRootSchema().globalConstant; }
	const TranslatableStringMeta& TranslatableStringMeta::get(const Object& object) { return *object.GetRootSchema().translatableString; }
	const RoutineMeta& RoutineMeta::get(const Object& object) { return *object.GetRootSchema().routine; }
	const FunctionMeta& FunctionMeta::get(const Object& object) { return *object.GetRootSchema().function; }
	const JadeAnnotationTagMeta& JadeAnnotationTagMeta::get(const Object& object) { return *object.GetRootSchema().jadeAnnotationTag; }
	const LockPolicyMeta& LockPolicyMeta::get(const Object& object) { return *object.GetRootSchema().lockPolicy; }
	const MethodMeta& MethodMeta::get(const Object& object) { return *object.GetRootSchema().method; }
	const ExternalMethodMeta& ExternalMethodMeta::get(const Object& object) { return *object.GetRootSchema().externalMethod; }
	const JadeAnnotationContractMeta& JadeAnnotationContractMeta::get(const Object& object) { return *object.GetRootSchema().jadeAnnotationContract; }
	const JadeInterfaceMethodMeta& JadeInterfaceMethodMeta::get(const Object& object) { return *object.GetRootSchema().jadeInterfaceMethod; }
	const JadeMethodMeta& JadeMethodMeta::get(const Object& object) { return *object.GetRootSchema().jadeMethod; }
	const JadeWebServicesMethodMeta& JadeWebServicesMethodMeta::get(const Object& object) { return *object.GetRootSchema().jadeWebServicesMethod; }
	const PathExpressionMeta& PathExpressionMeta::get(const Object& object) { return *object.GetRootSchema().pathExpression; }
	const JadeExportedEntityMeta& JadeExportedEntityMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedEntity; }
	const JadeExportedFeatureMeta& JadeExportedFeatureMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedFeature; }
	const JadeExportedConstantMeta& JadeExportedConstantMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedConstant; }
	const JadeExportedMethodMeta& JadeExportedMethodMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedMethod; }
	const JadeExportedPropertyMeta& JadeExportedPropertyMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedProperty; }
	const JadeExportedTypeMeta& JadeExportedTypeMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedType; }
	const JadeExportedClassMeta& JadeExportedClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedClass; }
	const JadeExportedInterfaceMeta& JadeExportedInterfaceMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedInterface; }
	const JadePackageMeta& JadePackageMeta::get(const Object& object) { return *object.GetRootSchema().jadePackage; }
	const JadeExportedPackageMeta& JadeExportedPackageMeta::get(const Object& object) { return *object.GetRootSchema().jadeExportedPackage; }
	const JadeImportedPackageMeta& JadeImportedPackageMeta::get(const Object& object) { return *object.GetRootSchema().jadeImportedPackage; }
	const LibraryMeta& LibraryMeta::get(const Object& object) { return *object.GetRootSchema().library; }
	const LocaleMeta& LocaleMeta::get(const Object& object) { return *object.GetRootSchema().locale; }
	const TypeMeta& TypeMeta::get(const Object& object) { return *object.GetRootSchema().type; }
	const ClassMeta& ClassMeta::get(const Object& object) { return *object.GetRootSchema().class_; }
	const CollClassMeta& CollClassMeta::get(const Object& object) { return *object.GetRootSchema().collClass; }
	const JadeUserCollClassMeta& JadeUserCollClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeUserCollClass; }
	const ExceptionClassMeta& ExceptionClassMeta::get(const Object& object) { return *object.GetRootSchema().exceptionClass; }
	const GUIClassMeta& GUIClassMeta::get(const Object& object) { return *object.GetRootSchema().guiClass; }
	const ActiveXGuiClassMeta& ActiveXGuiClassMeta::get(const Object& object) { return *object.GetRootSchema().activeXGuiClass; }
	const HTMLClassMeta& HTMLClassMeta::get(const Object& object) { return *object.GetRootSchema().htmlClass; }
	const JadeAnnotationClassMeta& JadeAnnotationClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeAnnotationClass; }
	const JadeImportedClassMeta& JadeImportedClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeImportedClass; }
	const JadeUserClassMeta& JadeUserClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeUserClass; }
	const JadeWebServicesClassMeta& JadeWebServicesClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeWebServicesClass; }
	const JadeWebServiceConsumerClassMeta& JadeWebServiceConsumerClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeWebServiceConsumerClass; }
	const JadeWebServiceProviderClassMeta& JadeWebServiceProviderClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeWebServiceProviderClass; }
	const JadeWebServiceSoapHeaderClassMeta& JadeWebServiceSoapHeaderClassMeta::get(const Object& object) { return *object.GetRootSchema().jadeWebServiceSoapHeaderClass; }
	const JadeGenericTypeMeta& JadeGenericTypeMeta::get(const Object& object) { return *object.GetRootSchema().jadeGenericType; }
	const JadeInterfaceMeta& JadeInterfaceMeta::get(const Object& object) { return *object.GetRootSchema().jadeInterface; }
	const JadeImportedInterfaceMeta& JadeImportedInterfaceMeta::get(const Object& object) { return *object.GetRootSchema().jadeImportedInterface; }
	const PrimTypeMeta& PrimTypeMeta::get(const Object& object) { return *object.GetRootSchema().primType; }
	const PseudoTypeMeta& PseudoTypeMeta::get(const Object& object) { return *object.GetRootSchema().pseudoType; }
	const ScriptElementMeta& ScriptElementMeta::get(const Object& object) { return *object.GetRootSchema().scriptElement; }
	const FeatureUsageMeta& FeatureUsageMeta::get(const Object& object) { return *object.GetRootSchema().featureUsage; }
	const KeyUsageMeta& KeyUsageMeta::get(const Object& object) { return *object.GetRootSchema().keyUsage; }
	const TypeUsageMeta& TypeUsageMeta::get(const Object& object) { return *object.GetRootSchema().typeUsage; }
	const InlineTypeUsageMeta& InlineTypeUsageMeta::get(const Object& object) { return *object.GetRootSchema().inlineTypeUsage; }
	const JadeLocalVarMeta& JadeLocalVarMeta::get(const Object& object) { return *object.GetRootSchema().jadeLocalVar; }
	const ParameterMeta& ParameterMeta::get(const Object& object) { return *object.GetRootSchema().parameter; }
	const ReturnTypeMeta& ReturnTypeMeta::get(const Object& object) { return *object.GetRootSchema().returnType; }
	const WindowMeta& WindowMeta::get(const Object& object) { return *object.GetRootSchema().window; }
	const ControlMeta& ControlMeta::get(const Object& object) { return *object.GetRootSchema().control; }
	const ActiveXControlMeta& ActiveXControlMeta::get(const Object& object) { return *object.GetRootSchema().activeXControl; }
	const BaseControlMeta& BaseControlMeta::get(const Object& object) { return *object.GetRootSchema().baseControl; }
	const BrowseButtonsMeta& BrowseButtonsMeta::get(const Object& object) { return *object.GetRootSchema().browseButtons; }
	const ButtonMeta& ButtonMeta::get(const Object& object) { return *object.GetRootSchema().button; }
	const CheckBoxMeta& CheckBoxMeta::get(const Object& object) { return *object.GetRootSchema().checkBox; }
	const ComboBoxMeta& ComboBoxMeta::get(const Object& object) { return *object.GetRootSchema().comboBox; }
	const FolderMeta& FolderMeta::get(const Object& object) { return *object.GetRootSchema().folder; }
	const FrameMeta& FrameMeta::get(const Object& object) { return *object.GetRootSchema().frame; }
	const GroupBoxMeta& GroupBoxMeta::get(const Object& object) { return *object.GetRootSchema().groupBox; }
	const SheetMeta& SheetMeta::get(const Object& object) { return *object.GetRootSchema().sheet; }
	const JadeDockBaseMeta& JadeDockBaseMeta::get(const Object& object) { return *object.GetRootSchema().jadeDockBase; }
	const JadeDockBarMeta& JadeDockBarMeta::get(const Object& object) { return *object.GetRootSchema().jadeDockBar; }
	const JadeDockContainerMeta& JadeDockContainerMeta::get(const Object& object) { return *object.GetRootSchema().jadeDockContainer; }
	const JadeDotNetVisualComponentMeta& JadeDotNetVisualComponentMeta::get(const Object& object) { return *object.GetRootSchema().jadeDotNetVisualComponent; }
	const JadeEditMaskMeta& JadeEditMaskMeta::get(const Object& object) { return *object.GetRootSchema().jadeEditMask; }
	const JadeRichTextMeta& JadeRichTextMeta::get(const Object& object) { return *object.GetRootSchema().jadeRichText; }
	const JadeTextEditMeta& JadeTextEditMeta::get(const Object& object) { return *object.GetRootSchema().jadeTextEdit; }
	const JadeEditorMeta& JadeEditorMeta::get(const Object& object) { return *object.GetRootSchema().jadeEditor; }
	const JadeXamlControlMeta& JadeXamlControlMeta::get(const Object& object) { return *object.GetRootSchema().jadeXamlControl; }
	const LabelMeta& LabelMeta::get(const Object& object) { return *object.GetRootSchema().label; }
	const ProgressBarMeta& ProgressBarMeta::get(const Object& object) { return *object.GetRootSchema().progressBar; }
	const WebHotSpotMeta& WebHotSpotMeta::get(const Object& object) { return *object.GetRootSchema().webHotSpot; }
	const WebInsertMeta& WebInsertMeta::get(const Object& object) { return *object.GetRootSchema().webInsert; }
	const WebJavaAppletMeta& WebJavaAppletMeta::get(const Object& object) { return *object.GetRootSchema().webJavaApplet; }
	const ListBoxMeta& ListBoxMeta::get(const Object& object) { return *object.GetRootSchema().listBox; }
	const MultiMediaMeta& MultiMediaMeta::get(const Object& object) { return *object.GetRootSchema().multiMedia; }
	const OcxMeta& OcxMeta::get(const Object& object) { return *object.GetRootSchema().ocx; }
	const OleControlMeta& OleControlMeta::get(const Object& object) { return *object.GetRootSchema().oleControl; }
	const OptionButtonMeta& OptionButtonMeta::get(const Object& object) { return *object.GetRootSchema().optionButton; }
	const PictureMeta& PictureMeta::get(const Object& object) { return *object.GetRootSchema().picture; }
	const JadeMaskMeta& JadeMaskMeta::get(const Object& object) { return *object.GetRootSchema().jadeMask; }
	const ScrollBarMeta& ScrollBarMeta::get(const Object& object) { return *object.GetRootSchema().scrollBar; }
	const HScrollMeta& HScrollMeta::get(const Object& object) { return *object.GetRootSchema().hScroll; }
	const VScrollMeta& VScrollMeta::get(const Object& object) { return *object.GetRootSchema().vScroll; }
	const StatusLineMeta& StatusLineMeta::get(const Object& object) { return *object.GetRootSchema().statusLine; }
	const TableMeta& TableMeta::get(const Object& object) { return *object.GetRootSchema().table; }
	const TextBoxMeta& TextBoxMeta::get(const Object& object) { return *object.GetRootSchema().textBox; }
	const WebHTMLMeta& WebHTMLMeta::get(const Object& object) { return *object.GetRootSchema().webHTML; }
	const FormMeta& FormMeta::get(const Object& object) { return *object.GetRootSchema().form; }
	
	RootSchema::RootSchema(Assembly& parent, Version version) : MetaSchema(parent, "RootSchema", version, *this),
		// Primitives
		any(new PrimType(*this, nullptr, "Any")),
		binary(new PrimType(*this, nullptr, "Binary")),
		boolean(new PrimType(*this, nullptr, "Boolean")),
		byte(new PrimType(*this, nullptr, "Byte")),
		character(new PrimType(*this, nullptr, "Character")),
		date(new PrimType(*this, nullptr, "Date")),
		decimal(new PrimType(*this, nullptr, "Decimal")),
		integer(new PrimType(*this, nullptr, "Integer")),
		integer64(new PrimType(*this, nullptr, "Integer64")),
		memoryAddress(new PrimType(*this, nullptr, "MemoryAddress")),
		point(new PrimType(*this, nullptr, "Point")),
		real(new PrimType(*this, nullptr, "Real")),
		string(new PrimType(*this, nullptr, "String")),
		stringUtf8(new PrimType(*this, nullptr, "StringUtf8")),
		time(new PrimType(*this, nullptr, "Time")),
		timeStamp(new PrimType(*this, nullptr, "TimeStamp")),
		timeStampInterval(new PrimType(*this, nullptr, "TimeStampInterval")),
		timeStampOffset(new PrimType(*this, nullptr, "TimeStampOffset")),
		
		// Classes
		object(newMeta<ObjectMeta>(*this)),
		activeXClass(newMeta<ActiveXClassMeta>(*this, *object)),
		activeXFeature(newMeta<ActiveXFeatureMeta>(*this, *object)),
		activeXAttribute(newMeta<ActiveXAttributeMeta>(*this, *activeXFeature)),
		activeXConstant(newMeta<ActiveXConstantMeta>(*this, *activeXFeature)),
		activeXMethod(newMeta<ActiveXMethodMeta>(*this, *activeXFeature)),
		activeXInterface(newMeta<ActiveXInterfaceMeta>(*this, *object)),
		activeXLibrary(newMeta<ActiveXLibraryMeta>(*this, *object)),
		application(newMeta<ApplicationMeta>(*this, *object)),
		collection(newMeta<CollectionMeta>(*this, *object)),
		dbClassMap(newMeta<DbClassMapMeta>(*this, *object)),
		development(newMeta<DevelopmentMeta>(*this, *object)),
		devControlClass(newMeta<DevControlClassMeta>(*this, *development)),
		devControlTypes(newMeta<DevControlTypesMeta>(*this, *devControlClass)),
		devControlProperties(newMeta<DevControlPropertiesMeta>(*this, *development)),
		inverse(newMeta<InverseMeta>(*this, *object)),
		jadeDynamicPropertyCluster(newMeta<JadeDynamicPropertyClusterMeta>(*this, *object)),
		jadeExposedClass(newMeta<JadeExposedClassMeta>(*this, *object)),
		jadeExposedFeature(newMeta<JadeExposedFeatureMeta>(*this, *object)),
		jadeExposedList(newMeta<JadeExposedListMeta>(*this, *object)),
		jadeHTMLDocument(newMeta<JadeHTMLDocumentMeta>(*this, *object)),
		jadeWebServiceManager(newMeta<JadeWebServiceManagerMeta>(*this, *object)),
		key(newMeta<KeyMeta>(*this, *object)),
		externalKey(newMeta<ExternalKeyMeta>(*this, *key)),
		memberKey(newMeta<MemberKeyMeta>(*this, *key)),
		menuItemData(newMeta<MenuItemDataMeta>(*this, *object)),
		menuItem(newMeta<MenuItemMeta>(*this, *menuItemData)),
		relationalAttribute(newMeta<RelationalAttributeMeta>(*this, *object)),
		raCollectionValue(newMeta<RACollectionValueMeta>(*this, *relationalAttribute)),
		raMethod(newMeta<RAMethodMeta>(*this, *relationalAttribute)),
		raOid(newMeta<RAOidMeta>(*this, *relationalAttribute)),
		raParameter(newMeta<RAParameterMeta>(*this, *relationalAttribute)),
		raProperty(newMeta<RAPropertyMeta>(*this, *relationalAttribute)),
		raRelationIndex(newMeta<RARelationIndexMeta>(*this, *relationalAttribute)),
		raRelationKey(newMeta<RARelationKeyMeta>(*this, *relationalAttribute)),
		raRelationOid(newMeta<RARelationOidMeta>(*this, *relationalAttribute)),
		raRpsProperty(newMeta<RARpsPropertyMeta>(*this, *relationalAttribute)),
		relationalTable(newMeta<RelationalTableMeta>(*this, *object)),
		relationalTableClass(newMeta<RelationalTableClassMeta>(*this, *relationalTable)),
		relationalTableCollection(newMeta<RelationalTableCollectionMeta>(*this, *relationalTable)),
		relationalTableCollectionMeth(newMeta<RelationalTableCollectionMethMeta>(*this, *relationalTable)),
		relationalTableRelationship(newMeta<RelationalTableRelationshipMeta>(*this, *relationalTable)),
		relationalTableXRelationship(newMeta<RelationalTableXRelationshipMeta>(*this, *relationalTable)),
		relationalView(newMeta<RelationalViewMeta>(*this, *object)),
		schema(newMeta<SchemaMeta>(*this, *object)),
		schemaEntity(newMeta<SchemaEntityMeta>(*this, *object)),
		constantCategory(newMeta<ConstantCategoryMeta>(*this, *schemaEntity)),
		database(newMeta<DatabaseMeta>(*this, *schemaEntity)),
		dbFile(newMeta<DbFileMeta>(*this, *schemaEntity)),
		feature(newMeta<FeatureMeta>(*this, *schemaEntity)),
		jadeImportedFeature(newMeta<JadeImportedFeatureMeta>(*this, *feature)),
		jadeImportedConstant(newMeta<JadeImportedConstantMeta>(*this, *jadeImportedFeature)),
		jadeImportedMethod(newMeta<JadeImportedMethodMeta>(*this, *jadeImportedFeature)),
		jadeImportedProperty(newMeta<JadeImportedPropertyMeta>(*this, *jadeImportedFeature)),
		localeFormat(newMeta<LocaleFormatMeta>(*this, *feature)),
		dateFormat(newMeta<DateFormatMeta>(*this, *localeFormat)),
		numberFormat(newMeta<NumberFormatMeta>(*this, *localeFormat)),
		currencyFormat(newMeta<CurrencyFormatMeta>(*this, *numberFormat)),
		timeFormat(newMeta<TimeFormatMeta>(*this, *localeFormat)),
		property(newMeta<PropertyMeta>(*this, *feature)),
		attribute(newMeta<AttributeMeta>(*this, *property)),
		compAttribute(newMeta<CompAttributeMeta>(*this, *attribute)),
		jadeDynamicCompAttribute(newMeta<JadeDynamicCompAttributeMeta>(*this, *compAttribute)),
		primAttribute(newMeta<PrimAttributeMeta>(*this, *attribute)),
		externalPrimAttribute(newMeta<ExternalPrimAttributeMeta>(*this, *primAttribute)),
		jadeDynamicPrimAttribute(newMeta<JadeDynamicPrimAttributeMeta>(*this, *primAttribute)),
		reference(newMeta<ReferenceMeta>(*this, *property)),
		explicitInverseRef(newMeta<ExplicitInverseRefMeta>(*this, *reference)),
		externalReference(newMeta<ExternalReferenceMeta>(*this, *explicitInverseRef)),
		jadeDynamicExplicitInverseRef(newMeta<JadeDynamicExplicitInverseRefMeta>(*this, *explicitInverseRef)),
		implicitInverseRef(newMeta<ImplicitInverseRefMeta>(*this, *reference)),
		jadeDynamicImplicitInverseRef(newMeta<JadeDynamicImplicitInverseRefMeta>(*this, *implicitInverseRef)),
		script(newMeta<ScriptMeta>(*this, *feature)),
		constant(newMeta<ConstantMeta>(*this, *script)),
		globalConstant(newMeta<GlobalConstantMeta>(*this, *constant)),
		translatableString(newMeta<TranslatableStringMeta>(*this, *constant)),
		routine(newMeta<RoutineMeta>(*this, *script)),
		function(newMeta<FunctionMeta>(*this, *routine)),
		jadeAnnotationTag(newMeta<JadeAnnotationTagMeta>(Version(20, 0, 1), *this, *routine)),
		lockPolicy(newMeta<LockPolicyMeta>(*this, *routine)),
		method(newMeta<MethodMeta>(*this, *routine)),
		externalMethod(newMeta<ExternalMethodMeta>(*this, *method)),
		jadeAnnotationContract(newMeta<JadeAnnotationContractMeta>(Version(20, 0, 1), *this, *method)),
		jadeInterfaceMethod(newMeta<JadeInterfaceMethodMeta>(*this, *method)),
		jadeMethod(newMeta<JadeMethodMeta>(*this, *method)),
		jadeWebServicesMethod(newMeta<JadeWebServicesMethodMeta>(*this, *jadeMethod)),
		pathExpression(newMeta<PathExpressionMeta>(*this, *routine)),
		jadeExportedEntity(newMeta<JadeExportedEntityMeta>(*this, *schemaEntity)),
		jadeExportedFeature(newMeta<JadeExportedFeatureMeta>(*this, *jadeExportedEntity)),
		jadeExportedConstant(newMeta<JadeExportedConstantMeta>(*this, *jadeExportedFeature)),
		jadeExportedMethod(newMeta<JadeExportedMethodMeta>(*this, *jadeExportedFeature)),
		jadeExportedProperty(newMeta<JadeExportedPropertyMeta>(*this, *jadeExportedFeature)),
		jadeExportedType(newMeta<JadeExportedTypeMeta>(*this, *jadeExportedEntity)),
		jadeExportedClass(newMeta<JadeExportedClassMeta>(*this, *jadeExportedType)),
		jadeExportedInterface(newMeta<JadeExportedInterfaceMeta>(*this, *jadeExportedType)),
		jadePackage(newMeta<JadePackageMeta>(*this, *schemaEntity)),
		jadeExportedPackage(newMeta<JadeExportedPackageMeta>(*this, *jadePackage)),
		jadeImportedPackage(newMeta<JadeImportedPackageMeta>(*this, *jadePackage)),
		library(newMeta<LibraryMeta>(*this, *schemaEntity)),
		locale(newMeta<LocaleMeta>(*this, *schemaEntity)),
		type(newMeta<TypeMeta>(*this, *schemaEntity)),
		class_(newMeta<ClassMeta>(*this, *type)),
		collClass(newMeta<CollClassMeta>(*this, *class_)),
		jadeUserCollClass(newMeta<JadeUserCollClassMeta>(*this, *collClass)),
		exceptionClass(newMeta<ExceptionClassMeta>(*this, *class_)),
		guiClass(newMeta<GUIClassMeta>(*this, *class_)),
		activeXGuiClass(newMeta<ActiveXGuiClassMeta>(*this, *guiClass)),
		htmlClass(newMeta<HTMLClassMeta>(*this, *class_)),
		jadeAnnotationClass(newMeta<JadeAnnotationClassMeta>(Version(20, 0, 1), *this, *class_)),
		jadeImportedClass(newMeta<JadeImportedClassMeta>(*this, *class_)),
		jadeUserClass(newMeta<JadeUserClassMeta>(*this, *class_)),
		jadeWebServicesClass(newMeta<JadeWebServicesClassMeta>(*this, *class_)),
		jadeWebServiceConsumerClass(newMeta<JadeWebServiceConsumerClassMeta>(*this, *jadeWebServicesClass)),
		jadeWebServiceProviderClass(newMeta<JadeWebServiceProviderClassMeta>(*this, *jadeWebServicesClass)),
		jadeWebServiceSoapHeaderClass(newMeta<JadeWebServiceSoapHeaderClassMeta>(*this, *jadeWebServicesClass)),
		jadeGenericType(newMeta<JadeGenericTypeMeta>(Version(20, 0, 1), *this, *type)),
		jadeInterface(newMeta<JadeInterfaceMeta>(*this, *type)),
		jadeImportedInterface(newMeta<JadeImportedInterfaceMeta>(*this, *jadeInterface)),
		primType(newMeta<PrimTypeMeta>(*this, *type)),
		pseudoType(newMeta<PseudoTypeMeta>(*this, *type)),
		scriptElement(newMeta<ScriptElementMeta>(*this, *object)),
		featureUsage(newMeta<FeatureUsageMeta>(*this, *scriptElement)),
		keyUsage(newMeta<KeyUsageMeta>(*this, *scriptElement)),
		typeUsage(newMeta<TypeUsageMeta>(*this, *scriptElement)),
		inlineTypeUsage(newMeta<InlineTypeUsageMeta>(*this, *typeUsage)),
		jadeLocalVar(newMeta<JadeLocalVarMeta>(*this, *typeUsage)),
		parameter(newMeta<ParameterMeta>(*this, *typeUsage)),
		returnType(newMeta<ReturnTypeMeta>(*this, *typeUsage)),
		window(newMeta<WindowMeta>(*this, *object)),
		control(newMeta<ControlMeta>(*this, *window)),
		activeXControl(newMeta<ActiveXControlMeta>(*this, *control)),
		baseControl(newMeta<BaseControlMeta>(*this, *control)),
		browseButtons(newMeta<BrowseButtonsMeta>(*this, *control)),
		button(newMeta<ButtonMeta>(*this, *control)),
		checkBox(newMeta<CheckBoxMeta>(*this, *control)),
		comboBox(newMeta<ComboBoxMeta>(*this, *control)),
		folder(newMeta<FolderMeta>(*this, *control)),
		frame(newMeta<FrameMeta>(*this, *control)),
		groupBox(newMeta<GroupBoxMeta>(*this, *control)),
		sheet(newMeta<SheetMeta>(*this, *groupBox)),
		jadeDockBase(newMeta<JadeDockBaseMeta>(*this, *control)),
		jadeDockBar(newMeta<JadeDockBarMeta>(*this, *jadeDockBase)),
		jadeDockContainer(newMeta<JadeDockContainerMeta>(*this, *jadeDockBase)),
		jadeDotNetVisualComponent(newMeta<JadeDotNetVisualComponentMeta>(*this, *control)),
		jadeEditMask(newMeta<JadeEditMaskMeta>(*this, *control)),
		jadeRichText(newMeta<JadeRichTextMeta>(*this, *control)),
		jadeTextEdit(newMeta<JadeTextEditMeta>(*this, *control)),
		jadeEditor(newMeta<JadeEditorMeta>(*this, *jadeTextEdit)),
		jadeXamlControl(newMeta<JadeXamlControlMeta>(*this, *control)),
		label(newMeta<LabelMeta>(*this, *control)),
		progressBar(newMeta<ProgressBarMeta>(*this, *label)),
		webHotSpot(newMeta<WebHotSpotMeta>(*this, *label)),
		webInsert(newMeta<WebInsertMeta>(*this, *label)),
		webJavaApplet(newMeta<WebJavaAppletMeta>(*this, *label)),
		listBox(newMeta<ListBoxMeta>(*this, *control)),
		multiMedia(newMeta<MultiMediaMeta>(*this, *control)),
		ocx(newMeta<OcxMeta>(*this, *control)),
		oleControl(newMeta<OleControlMeta>(*this, *control)),
		optionButton(newMeta<OptionButtonMeta>(*this, *control)),
		picture(newMeta<PictureMeta>(*this, *control)),
		jadeMask(newMeta<JadeMaskMeta>(*this, *picture)),
		scrollBar(newMeta<ScrollBarMeta>(*this, *control)),
		hScroll(newMeta<HScrollMeta>(*this, *scrollBar)),
		vScroll(newMeta<VScrollMeta>(*this, *scrollBar)),
		statusLine(newMeta<StatusLineMeta>(*this, *control)),
		table(newMeta<TableMeta>(*this, *control)),
		textBox(newMeta<TextBoxMeta>(*this, *control)),
		webHTML(newMeta<WebHTMLMeta>(*this, *textBox)),
		form(newMeta<FormMeta>(*this, *window))
	{
		initialized = true;
		const RootSchema& rootSchema = *this;
		
		// Inverses
		new Inverse(activeXClass->baseClass, class_->activeXClass);
		new Inverse(activeXFeature->feature, feature->activeXFeature);
		new Inverse(activeXLibrary->_schema, schema->activeXLibraries);
		new Inverse(activeXLibrary->coClasses, activeXClass->activeXLibrary);
		new Inverse(application->exportedPackages, jadeExportedPackage->application);
		new Inverse(application->schema, schema->_applications);
		new Inverse(dbClassMap->database, database->classMaps);
		new Inverse(dbClassMap->diskClass, class_->classMapRefs);
		new Inverse(dbClassMap->diskFile, dbFile->classMapRefs);
		new Inverse(jadeDynamicPropertyCluster->schemaType, class_->dynamicPropertyClusters);
		new Inverse(jadeExposedClass->consts, feature->exposedClassRefs);
		new Inverse(jadeExposedClass->exposedList, jadeExposedList->exposedClasses);
		new Inverse(jadeExposedClass->methods, feature->exposedClassRefs);
		new Inverse(jadeExposedClass->properties, feature->exposedClassRefs);
		new Inverse(jadeExposedClass->relatedClass, class_->exposedClassRefs);
		new Inverse(jadeExposedFeature->exposedClass, jadeExposedClass->exposedFeatures);
		new Inverse(jadeExposedFeature->relatedFeature, feature->exposedFeatureRefs);
		new Inverse(jadeExposedList->schema, schema->_exposedLists);
		new Inverse(jadeHTMLDocument->htmlClass, htmlClass->_jadeHTMLDocument);
		new Inverse(jadeHTMLDocument->schema, schema->_jadeHTMLDocuments);
		new Inverse(memberKey->keyPath, property->keyPathRefs);
		new Inverse(menuItem->form, form->menuList);
		new Inverse(relationalAttribute->table, relationalTable->attributes);
		new Inverse(relationalTable->rView, relationalView->tables);
		new Inverse(relationalView->schema, schema->relationalViews);
		new Inverse(schema->superschema, schema->subschemas);
		new Inverse(constantCategory->schema, schema->constantCategories);
		new Inverse(database->schema, schema->databases);
		new Inverse(dbFile->database, database->dbFiles);
		new Inverse(feature->schemaType, class_->properties);
		new Inverse(feature->schemaType, type->consts);
		new Inverse(feature->schemaType, type->methods);
		new Inverse(feature->type, type->propertyRefs);
		new Inverse(feature->type, type->constantRefs);
		new Inverse(jadeImportedFeature->importedClass, jadeImportedClass->importedConstants);
		new Inverse(jadeImportedFeature->importedClass, jadeImportedClass->importedMethods);
		new Inverse(jadeImportedFeature->importedClass, jadeImportedClass->importedProperties);
		new Inverse(jadeImportedFeature->importedInterface, jadeImportedInterface->importedConstants);
		new Inverse(jadeImportedFeature->importedInterface, jadeImportedInterface->importedMethods);
		new Inverse(jadeImportedFeature->importedInterface, jadeImportedInterface->importedProperties);
		new Inverse(jadeImportedConstant->exportedConstant, jadeExportedConstant->importedConstantRefs);
		new Inverse(jadeImportedMethod->exportedMethod, jadeExportedMethod->importedMethodRefs);
		new Inverse(jadeImportedProperty->exportedProperty, jadeExportedProperty->importedPropertyRefs);
		new Inverse(localeFormat->schema, schema->userFormats);
		new Inverse(jadeDynamicCompAttribute->dynamicPropertyCluster, jadeDynamicPropertyCluster->properties);
		new Inverse(jadeDynamicPrimAttribute->dynamicPropertyCluster, jadeDynamicPropertyCluster->properties);
		new Inverse(reference->constraint, jadeMethod->referenceRefs);
		new Inverse(jadeDynamicExplicitInverseRef->dynamicPropertyCluster, jadeDynamicPropertyCluster->properties);
		new Inverse(jadeDynamicImplicitInverseRef->dynamicPropertyCluster, jadeDynamicPropertyCluster->properties);
		new Inverse(constant->constantRefs, constant->constantUsages);
		new Inverse(globalConstant->category, constantCategory->consts);
		new Inverse(globalConstant->schema, schema->consts);
		new Inverse(translatableString->locale, locale->translatableStrings);
		new Inverse(routine->library, library->entrypoints);
		new Inverse(function->schema, schema->functions);
		new Inverse(method->controlMethod, method->controlMethodRefs);
		new Inverse(method->interfaceImplements, jadeInterfaceMethod->interfaceImplementors);
		new Inverse(jadeWebServicesMethod->soapHeaders, jadeWebServiceSoapHeaderClass->_usedInMethods);
		new Inverse(jadeExportedFeature->exportedType, jadeExportedType->exportedConstants);
		new Inverse(jadeExportedFeature->exportedType, jadeExportedType->exportedMethods);
		new Inverse(jadeExportedFeature->exportedType, jadeExportedType->exportedProperties);
		new Inverse(jadeExportedConstant->constant, constant->exportedConstantRefs);
		new Inverse(jadeExportedMethod->method, method->exportedMethodRefs);
		new Inverse(jadeExportedProperty->property, property->exportedPropertyRefs);
		new Inverse(jadeExportedType->package, jadeExportedPackage->classes);
		new Inverse(jadeExportedType->package, jadeExportedPackage->interfaces);
		new Inverse(jadeExportedClass->originalClass, class_->exportedClassRefs);
		new Inverse(jadeExportedInterface->originalInterface, jadeInterface->exportedInterfaceRefs);
		new Inverse(jadePackage->schema, schema->exportedPackages);
		new Inverse(jadePackage->schema, schema->importedPackages);
		new Inverse(jadeImportedPackage->exportedPackage, jadeExportedPackage->importedPackageRefs);
		new Inverse(library->schema, schema->libraries);
		new Inverse(locale->schema, schema->locales);
		new Inverse(type->schema, schema->classes);
		new Inverse(type->schema, schema->primitives);
		new Inverse(type->schema, schema->pseudoTypes);
		new Inverse(type->schema, schema->interfaces);
		new Inverse(type->superschemaType, type->subschemaTypes);
		new Inverse(class_->superclass, class_->subclasses);
		new Inverse(collClass->memberType, type->collClassRefs);
		new Inverse(jadeImportedClass->exportedClass, jadeExportedClass->importedClassRefs);
		new Inverse(jadeImportedClass->package, jadeImportedPackage->classes);
		new Inverse(jadeInterface->implementorClasses, class_->implementedInterfaces);
		new Inverse(jadeInterface->subinterfaces, jadeInterface->superinterfaces);
		new Inverse(jadeImportedInterface->exportedInterface, jadeExportedInterface->importedInterfaceRefs);
		new Inverse(jadeImportedInterface->package, jadeImportedPackage->interfaces);
		new Inverse(control->form, form->controlList);
		new Inverse(form->locale, locale->forms);
		
		// Primitives
		boolean->SetDefaultLength(1);
		byte->SetDefaultLength(1);
		character->SetDefaultLength(1);
		date->SetDefaultLength(4);
		decimal->SetDefaultLength(16);
		integer->SetDefaultLength(4);
		integer64->SetDefaultLength(8);
		memoryAddress->SetDefaultLength(12);
		point->SetDefaultLength(8);
		real->SetDefaultLength(8);
		time->SetDefaultLength(4);
		timeStamp->SetDefaultLength(8);
		timeStampInterval->SetDefaultLength(8);
		timeStampOffset->SetDefaultLength(12);
		
		// Database Files
		Database* database = new Database(*this, *rootSchema.database, "RootSchemaDb");
		new DbFile(database, *rootSchema.dbFile, "_environ");
		new DbFile(database, *rootSchema.dbFile, "_usergui");
		
		// Libraries
		new Library(*this, *rootSchema.library, "gdi32");
		new Library(*this, *rootSchema.library, "jadax");
		new Library(*this, *rootSchema.library, "jadeactivex");
		new Library(*this, *rootSchema.library, "jadeaqe");
		new Library(*this, *rootSchema.library, "jadedit");
		new Library(*this, *rootSchema.library, "jadedotnet");
		new Library(*this, *rootSchema.library, "jadegm");
		new Library(*this, *rootSchema.library, "jadesoap");
		new Library(*this, *rootSchema.library, "jadesql");
		new Library(*this, *rootSchema.library, "jadessl");
		new Library(*this, *rootSchema.library, "jadexpat");
		new Library(*this, *rootSchema.library, "jadinet");
		new Library(*this, *rootSchema.library, "jadinfo");
		new Library(*this, *rootSchema.library, "jadnet");
		new Library(*this, *rootSchema.library, "jadodbc");
		new Library(*this, *rootSchema.library, "jadodbc_s");
		new Library(*this, *rootSchema.library, "jadpmap");
		new Library(*this, *rootSchema.library, "jadprnt");
		new Library(*this, *rootSchema.library, "javajom");
		new Library(*this, *rootSchema.library, "jlncomp");
		new Library(*this, *rootSchema.library, "jlnintp");
		new Library(*this, *rootSchema.library, "jom");
		new Library(*this, *rootSchema.library, "jomjdi");
		new Library(*this, *rootSchema.library, "jomos");
		new Library(*this, *rootSchema.library, "jomreog");
		new Library(*this, *rootSchema.library, "jomsupp");
		new Library(*this, *rootSchema.library, "jomsys");
		new Library(*this, *rootSchema.library, "jomtools");
		new Library(*this, *rootSchema.library, "jomutil");
		new Library(*this, *rootSchema.library, "kernel32");
		new Library(*this, *rootSchema.library, "shell32");
		new Library(*this, *rootSchema.library, "user32");
		new Library(*this, *rootSchema.library, "wininet");
		
		// Pseudo Types
		new PseudoType(*this, *rootSchema.pseudoType, "InstanceType");
		new PseudoType(*this, *rootSchema.pseudoType, "KeyType");
		new PseudoType(*this, *rootSchema.pseudoType, "MemberType");
		new PseudoType(*this, *rootSchema.pseudoType, "ParamListType");
		new PseudoType(*this, *rootSchema.pseudoType, "PropertyListType");
		new PseudoType(*this, *rootSchema.pseudoType, "PseudoMethodCallType");
		new PseudoType(*this, *rootSchema.pseudoType, "SelfType");
		
		// Classes
		Class* activeXAutomation = NewClass<>(*this, "ActiveXAutomation", *object);
		new PrimAttribute(activeXAutomation, *rootSchema.primAttribute, "remoteServerName");
		new PrimAttribute(activeXAutomation, *rootSchema.primAttribute, "usePresentationClient");
		
		Class* olE_Automation = NewClass<>(*this, "OLE_Automation", activeXAutomation);
		Class* jadeAutoFont = NewClass<>(*this, "JadeAutoFont", olE_Automation);
		Class* jadeAutoPicture = NewClass<>(*this, "JadeAutoPicture", olE_Automation);
		Class* iUnknown = NewClass<>(*this, "IUnknown", *activeXInterface);
		Class* iDispatch = NewClass<>(*this, "IDispatch", iUnknown);
		Class* iJadeAutoFont = NewClass<>(*this, "IJadeAutoFont", iDispatch);
		Class* iJadeAutoFontEvents = NewClass<>(*this, "IJadeAutoFontEvents", iDispatch);
		Class* iJadeAutoPicture = NewClass<>(*this, "IJadeAutoPicture", iDispatch);
		Class* rootSchemaApp = NewClass<>(*this, "RootSchemaApp", *application);
		Class* applicationContext = NewClass<>(*this, "ApplicationContext", *object);
		Class* cmDialog = NewClass<>(*this, "CMDialog", *object);
		new PrimAttribute(cmDialog, *rootSchema.primAttribute, "helpContextId");
		
		Class* cmdColor = NewClass<>(*this, "CMDColor", cmDialog);
		new PrimAttribute(cmdColor, *rootSchema.primAttribute, "color");
		new PrimAttribute(cmdColor, *rootSchema.primAttribute, "fullOpen");
		new PrimAttribute(cmdColor, *rootSchema.primAttribute, "preventFullOpen");
		
		Class* cmdFileOpen = NewClass<>(*this, "CMDFileOpen", cmDialog);
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "allowMultiSelect");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "createPrompt");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "extensionDifferent");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "fileMustExist");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "filterIndex");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "hideReadOnly");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "noReadOnlyReturn");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "pathMustExist");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "readOnly");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "resetCurrentPath");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "shareAware");
		new PrimAttribute(cmdFileOpen, *rootSchema.primAttribute, "validate");
		
		Class* cmdFileSave = NewClass<>(*this, "CMDFileSave", cmDialog);
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "allowMultiSelect");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "createPrompt");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "extensionDifferent");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "fileMustExist");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "filterIndex");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "hideReadOnly");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "noReadOnlyReturn");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "overwritePrompt");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "pathMustExist");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "readOnly");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "resetCurrentPath");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "shareAware");
		new PrimAttribute(cmdFileSave, *rootSchema.primAttribute, "validate");
		
		Class* cmdFont = NewClass<>(*this, "CMDFont", cmDialog);
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "ansiOnly");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "fixedPitchOnly");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "fontBold");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "fontItalic");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "fontName");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "fontSize");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "fontStrikethru");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "fontUnderline");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "forceFontExist");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "maxSize");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "minSize");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "noNameSelection");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "noSizeSelection");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "noStyleSelection");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "printerDC");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "printerDC64");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "printerFonts");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "scalableOnly");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "screenFonts");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "showEffects");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "simulations");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "textColor");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "trueTypeOnly");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "vectorFonts");
		new PrimAttribute(cmdFont, *rootSchema.primAttribute, "wysiwyg");
		
		Class* cmdPrint = NewClass<>(*this, "CMDPrint", cmDialog);
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "allPagesStatus");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "collateStatus");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "copies");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "disablePageNumbers");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "disablePrintToFile");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "disableSelection");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "duplex");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "fromPage");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "hidePrintToFile");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "initializeWith");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "maxPage");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "minPage");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "orientation");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "pageNumbersStatus");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "paperSource");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "printSetup");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "printToFileStatus");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "printerDC");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "printerDC64");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "returnDC");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "selectionStatus");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "toPage");
		new PrimAttribute(cmdPrint, *rootSchema.primAttribute, "warnIfNoDefault");
		
		CollClass* btree = NewClass<CollClass>(*this, "Btree", *collection);
		CollClass* dictionary = NewClass<CollClass>(*this, "Dictionary", btree);
		CollClass* dynaDictionary = NewClass<CollClass>(*this, "DynaDictionary", dictionary);
		CollClass* extKeyDictionary = NewClass<CollClass>(*this, "ExtKeyDictionary", dictionary);
		CollClass* jadeAnyValueDict = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeAnyValueDict", extKeyDictionary);
		CollClass* jadeIdentifierMapper = NewClass<CollClass>(Version(22, 0, 1), *this, "JadeIdentifierMapper", extKeyDictionary);
		CollClass* objectByObjectDict = NewClass<CollClass>(*this, "ObjectByObjectDict", extKeyDictionary);
		CollClass* objectLongNameDict = NewClass<CollClass>(*this, "ObjectLongNameDict", extKeyDictionary);
		CollClass* memberKeyDictionary = NewClass<CollClass>(*this, "MemberKeyDictionary", dictionary);
		CollClass* activeXClassGuidDict = NewClass<CollClass>(*this, "ActiveXClassGuidDict", memberKeyDictionary);
		CollClass* activeXLibraryGuidDict = NewClass<CollClass>(*this, "ActiveXLibraryGuidDict", memberKeyDictionary);
		CollClass* appNameDict = NewClass<CollClass>(*this, "AppNameDict", memberKeyDictionary);
		CollClass* classNDict = NewClass<CollClass>(*this, "ClassNDict", memberKeyDictionary);
		CollClass* classNumberDict = NewClass<CollClass>(*this, "ClassNumberDict", memberKeyDictionary);
		CollClass* constCategoryNDict = NewClass<CollClass>(*this, "ConstCategoryNDict", memberKeyDictionary);
		CollClass* constantNDict = NewClass<CollClass>(*this, "ConstantNDict", memberKeyDictionary);
		CollClass* controlPropNameDict = NewClass<CollClass>(*this, "ControlPropNameDict", memberKeyDictionary);
		CollClass* databaseNDict = NewClass<CollClass>(*this, "DatabaseNDict", memberKeyDictionary);
		CollClass* dbClassMapDict = NewClass<CollClass>(*this, "DbClassMapDict", memberKeyDictionary);
		CollClass* dbFileNDict = NewClass<CollClass>(*this, "DbFileNDict", memberKeyDictionary);
		CollClass* entryPointDict = NewClass<CollClass>(*this, "EntryPointDict", memberKeyDictionary);
		CollClass* externalColumnByNameDict = NewClass<CollClass>(*this, "ExternalColumnByNameDict", memberKeyDictionary);
		CollClass* externalDatabaseByNameDict = NewClass<CollClass>(*this, "ExternalDatabaseByNameDict", memberKeyDictionary);
		CollClass* externalForeignKeyByNameDict = NewClass<CollClass>(*this, "ExternalForeignKeyByNameDict", memberKeyDictionary);
		CollClass* externalIndexByNameDict = NewClass<CollClass>(*this, "ExternalIndexByNameDict", memberKeyDictionary);
		CollClass* externalStoredProcByNameDict = NewClass<CollClass>(*this, "ExternalStoredProcByNameDict", memberKeyDictionary);
		CollClass* externalTableByNameDict = NewClass<CollClass>(*this, "ExternalTableByNameDict", memberKeyDictionary);
		CollClass* formNameDict = NewClass<CollClass>(*this, "FormNameDict", memberKeyDictionary);
		CollClass* functionNDict = NewClass<CollClass>(*this, "FunctionNDict", memberKeyDictionary);
		CollClass* jadeAnnotationAliasDict = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeAnnotationAliasDict", memberKeyDictionary);
		CollClass* jadeDynamicPropertyClusterDict = NewClass<CollClass>(*this, "JadeDynamicPropertyClusterDict", memberKeyDictionary);
		CollClass* jadeEventMethodUsageDict = NewClass<CollClass>(*this, "JadeEventMethodUsageDict", memberKeyDictionary);
		CollClass* jadeExportedClassNDict = NewClass<CollClass>(*this, "JadeExportedClassNDict", memberKeyDictionary);
		CollClass* jadeExportedConstantNDict = NewClass<CollClass>(*this, "JadeExportedConstantNDict", memberKeyDictionary);
		CollClass* jadeExportedInterfaceNDict = NewClass<CollClass>(*this, "JadeExportedInterfaceNDict", memberKeyDictionary);
		CollClass* jadeExportedMethodNDict = NewClass<CollClass>(*this, "JadeExportedMethodNDict", memberKeyDictionary);
		CollClass* jadeExportedPackageNDict = NewClass<CollClass>(*this, "JadeExportedPackageNDict", memberKeyDictionary);
		CollClass* jadeExportedPropertyNDict = NewClass<CollClass>(*this, "JadeExportedPropertyNDict", memberKeyDictionary);
		CollClass* jadeExposedClassNDict = NewClass<CollClass>(*this, "JadeExposedClassNDict", memberKeyDictionary);
		CollClass* jadeExposedFeatureNDict = NewClass<CollClass>(*this, "JadeExposedFeatureNDict", memberKeyDictionary);
		CollClass* jadeExposedListNDict = NewClass<CollClass>(*this, "JadeExposedListNDict", memberKeyDictionary);
		CollClass* jadeHTMLAttributeDict = NewClass<CollClass>(*this, "JadeHTMLAttributeDict", memberKeyDictionary);
		CollClass* jadeHTMLAttributeOffsetDict = NewClass<CollClass>(*this, "JadeHTMLAttributeOffsetDict", memberKeyDictionary);
		CollClass* jadeHTMLDocumentDict = NewClass<CollClass>(*this, "JadeHTMLDocumentDict", memberKeyDictionary);
		CollClass* jadeHTMLEntityNameDict = NewClass<CollClass>(*this, "JadeHTMLEntityNameDict", memberKeyDictionary);
		CollClass* jadeImportedClassNDict = NewClass<CollClass>(*this, "JadeImportedClassNDict", memberKeyDictionary);
		CollClass* jadeImportedClassNumberDict = NewClass<CollClass>(*this, "JadeImportedClassNumberDict", memberKeyDictionary);
		CollClass* jadeImportedConstantNDict = NewClass<CollClass>(*this, "JadeImportedConstantNDict", memberKeyDictionary);
		CollClass* jadeImportedInterfaceNDict = NewClass<CollClass>(*this, "JadeImportedInterfaceNDict", memberKeyDictionary);
		CollClass* jadeImportedMethodNDict = NewClass<CollClass>(*this, "JadeImportedMethodNDict", memberKeyDictionary);
		CollClass* jadeImportedPackageNDict = NewClass<CollClass>(*this, "JadeImportedPackageNDict", memberKeyDictionary);
		CollClass* jadeImportedPropertyNDict = NewClass<CollClass>(*this, "JadeImportedPropertyNDict", memberKeyDictionary);
		CollClass* jadeInterfaceEncodingDict = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeInterfaceEncodingDict", memberKeyDictionary);
		CollClass* jadeInterfaceNDict = NewClass<CollClass>(*this, "JadeInterfaceNDict", memberKeyDictionary);
		CollClass* jadePatchControlDict = NewClass<CollClass>(*this, "JadePatchControlDict", memberKeyDictionary);
		CollClass* jadePatchVersionDetailDict = NewClass<CollClass>(*this, "JadePatchVersionDetailDict", memberKeyDictionary);
		CollClass* jadePatchVersionDict = NewClass<CollClass>(*this, "JadePatchVersionDict", memberKeyDictionary);
		CollClass* jadeSkinApplicationNameDict = NewClass<CollClass>(*this, "JadeSkinApplicationNameDict", memberKeyDictionary);
		CollClass* jadeSkinCategoryNameDict = NewClass<CollClass>(*this, "JadeSkinCategoryNameDict", memberKeyDictionary);
		CollClass* jadeSkinControlNameDict = NewClass<CollClass>(*this, "JadeSkinControlNameDict", memberKeyDictionary);
		CollClass* jadeSkinEntityNameDict = NewClass<CollClass>(*this, "JadeSkinEntityNameDict", memberKeyDictionary);
		CollClass* jadeSkinFormNameDict = NewClass<CollClass>(*this, "JadeSkinFormNameDict", memberKeyDictionary);
		CollClass* jadeSkinMenuNameDict = NewClass<CollClass>(*this, "JadeSkinMenuNameDict", memberKeyDictionary);
		CollClass* jadeSkinSimpleButtonNameDict = NewClass<CollClass>(*this, "JadeSkinSimpleButtonNameDict", memberKeyDictionary);
		CollClass* jadeSkinWindowStateNameDict = NewClass<CollClass>(*this, "JadeSkinWindowStateNameDict", memberKeyDictionary);
		CollClass* jadeWebSocketDictionary = NewClass<CollClass>(*this, "JadeWebSocketDictionary", memberKeyDictionary);
		CollClass* libraryNDict = NewClass<CollClass>(*this, "LibraryNDict", memberKeyDictionary);
		CollClass* localeNDict = NewClass<CollClass>(*this, "LocaleNDict", memberKeyDictionary);
		CollClass* lockPolicyNDict = NewClass<CollClass>(*this, "LockPolicyNDict", memberKeyDictionary);
		CollClass* methodNDict = NewClass<CollClass>(*this, "MethodNDict", memberKeyDictionary);
		CollClass* nodeDict = NewClass<CollClass>(*this, "NodeDict", memberKeyDictionary);
		CollClass* pathExpressionNDict = NewClass<CollClass>(*this, "PathExpressionNDict", memberKeyDictionary);
		CollClass* primTypeNDict = NewClass<CollClass>(*this, "PrimTypeNDict", memberKeyDictionary);
		CollClass* primTypeNumberDict = NewClass<CollClass>(*this, "PrimTypeNumberDict", memberKeyDictionary);
		CollClass* processDict = NewClass<CollClass>(*this, "ProcessDict", memberKeyDictionary);
		CollClass* propertyNDict = NewClass<CollClass>(*this, "PropertyNDict", memberKeyDictionary);
		CollClass* propertyODict = NewClass<CollClass>(*this, "PropertyODict", memberKeyDictionary);
		CollClass* pseudoTypeNDict = NewClass<CollClass>(*this, "PseudoTypeNDict", memberKeyDictionary);
		CollClass* pseudoTypeNumberDict = NewClass<CollClass>(*this, "PseudoTypeNumberDict", memberKeyDictionary);
		CollClass* relationalAttributeDict = NewClass<CollClass>(*this, "RelationalAttributeDict", memberKeyDictionary);
		CollClass* relationalTableDict = NewClass<CollClass>(*this, "RelationalTableDict", memberKeyDictionary);
		CollClass* relationalViewDict = NewClass<CollClass>(*this, "RelationalViewDict", memberKeyDictionary);
		CollClass* relationshipNDict = NewClass<CollClass>(*this, "RelationshipNDict", memberKeyDictionary);
		CollClass* schemaEntityNDict = NewClass<CollClass>(*this, "SchemaEntityNDict", memberKeyDictionary);
		CollClass* schemaEntityNumberDict = NewClass<CollClass>(*this, "SchemaEntityNumberDict", memberKeyDictionary);
		CollClass* schemaNDict = NewClass<CollClass>(*this, "SchemaNDict", memberKeyDictionary);
		CollClass* schemaNumberDict = NewClass<CollClass>(*this, "SchemaNumberDict", memberKeyDictionary);
		CollClass* schemaViewNDict = NewClass<CollClass>(*this, "SchemaViewNDict", memberKeyDictionary);
		CollClass* sysEntryPointDict = NewClass<CollClass>(*this, "SysEntryPointDict", memberKeyDictionary);
		CollClass* sysSchemaNDict = NewClass<CollClass>(*this, "SysSchemaNDict", memberKeyDictionary);
		CollClass* sysSchemaNumberDict = NewClass<CollClass>(*this, "SysSchemaNumberDict", memberKeyDictionary);
		CollClass* typeNDict = NewClass<CollClass>(*this, "TypeNDict", memberKeyDictionary);
		CollClass* userProfileNDict = NewClass<CollClass>(*this, "UserProfileNDict", memberKeyDictionary);
		CollClass* webSessionsDict = NewClass<CollClass>(*this, "WebSessionsDict", memberKeyDictionary);
		CollClass* referenceSet = NewClass<CollClass>(*this, "ReferenceSet", btree);
		CollClass* set = NewClass<CollClass>(*this, "Set", btree);
		CollClass* objectSet = NewClass<CollClass>(*this, "ObjectSet", set);
		CollClass* breakpointsSet = NewClass<CollClass>(*this, "BreakpointsSet", objectSet);
		CollClass* classSet = NewClass<CollClass>(*this, "ClassSet", objectSet);
		CollClass* collClassSet = NewClass<CollClass>(*this, "CollClassSet", objectSet);
		CollClass* constantSet = NewClass<CollClass>(*this, "ConstantSet", objectSet);
		CollClass* controlLogicSet = NewClass<CollClass>(*this, "ControlLogicSet", objectSet);
		CollClass* dbClassMapSet = NewClass<CollClass>(*this, "DbClassMapSet", objectSet);
		CollClass* externalAttributeMapSet = NewClass<CollClass>(*this, "ExternalAttributeMapSet", objectSet);
		CollClass* externalClassMapSet = NewClass<CollClass>(*this, "ExternalClassMapSet", objectSet);
		CollClass* externalCollClassMapSet = NewClass<CollClass>(*this, "ExternalCollClassMapSet", objectSet);
		CollClass* externalForeignKeySet = NewClass<CollClass>(*this, "ExternalForeignKeySet", objectSet);
		CollClass* externalKeySet = NewClass<CollClass>(*this, "ExternalKeySet", objectSet);
		CollClass* externalRPSClassMapSet = NewClass<CollClass>(*this, "ExternalRPSClassMapSet", objectSet);
		CollClass* externalReferenceMapSet = NewClass<CollClass>(*this, "ExternalReferenceMapSet", objectSet);
		CollClass* externalTableSet = NewClass<CollClass>(*this, "ExternalTableSet", objectSet);
		CollClass* featureSet = NewClass<CollClass>(*this, "FeatureSet", objectSet);
		CollClass* featureUsageSet = NewClass<CollClass>(*this, "FeatureUsageSet", objectSet);
		CollClass* formSet = NewClass<CollClass>(*this, "FormSet", objectSet);
		CollClass* inverseSet = NewClass<CollClass>(*this, "InverseSet", objectSet);
		CollClass* jadeConnectionSet = NewClass<CollClass>(*this, "JadeConnectionSet", objectSet);
		CollClass* jadeEditorAccelKeysSet = NewClass<CollClass>(*this, "JadeEditorAccelKeysSet", objectSet);
		CollClass* jadeExportedClassSet = NewClass<CollClass>(*this, "JadeExportedClassSet", objectSet);
		CollClass* jadeExportedConstantSet = NewClass<CollClass>(*this, "JadeExportedConstantSet", objectSet);
		CollClass* jadeExportedInterfaceSet = NewClass<CollClass>(*this, "JadeExportedInterfaceSet", objectSet);
		CollClass* jadeExportedMethodSet = NewClass<CollClass>(*this, "JadeExportedMethodSet", objectSet);
		CollClass* jadeExportedPropertySet = NewClass<CollClass>(*this, "JadeExportedPropertySet", objectSet);
		CollClass* jadeExposedClassSet = NewClass<CollClass>(*this, "JadeExposedClassSet", objectSet);
		CollClass* jadeExposedFeatureSet = NewClass<CollClass>(*this, "JadeExposedFeatureSet", objectSet);
		CollClass* jadeGenericTypeSet = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeGenericTypeSet", objectSet);
		CollClass* jadeImportedClassSet = NewClass<CollClass>(*this, "JadeImportedClassSet", objectSet);
		CollClass* jadeImportedConstantSet = NewClass<CollClass>(*this, "JadeImportedConstantSet", objectSet);
		CollClass* jadeImportedInterfaceSet = NewClass<CollClass>(*this, "JadeImportedInterfaceSet", objectSet);
		CollClass* jadeImportedMethodSet = NewClass<CollClass>(*this, "JadeImportedMethodSet", objectSet);
		CollClass* jadeImportedPackageSet = NewClass<CollClass>(*this, "JadeImportedPackageSet", objectSet);
		CollClass* jadeImportedPropertySet = NewClass<CollClass>(*this, "JadeImportedPropertySet", objectSet);
		CollClass* jadeInterfaceSet = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeInterfaceSet", objectSet);
		CollClass* jadeSchemaEntitySet = NewClass<CollClass>(*this, "JadeSchemaEntitySet", objectSet);
		CollClass* jadeSchemaEntityViewSet = NewClass<CollClass>(*this, "JadeSchemaEntityViewSet", objectSet);
		CollClass* jadeWebServiceMethodSet = NewClass<CollClass>(*this, "JadeWebServiceMethodSet", objectSet);
		CollClass* jadeXMLNodeSet = NewClass<CollClass>(*this, "JadeXMLNodeSet", objectSet);
		CollClass* keyUsageSet = NewClass<CollClass>(*this, "KeyUsageSet", objectSet);
		CollClass* memberKeySet = NewClass<CollClass>(*this, "MemberKeySet", objectSet);
		CollClass* methodSet = NewClass<CollClass>(*this, "MethodSet", objectSet);
		CollClass* propertySet = NewClass<CollClass>(*this, "PropertySet", objectSet);
		CollClass* referencePropertySet = NewClass<CollClass>(*this, "ReferencePropertySet", objectSet);
		CollClass* relationalAttributeSet = NewClass<CollClass>(*this, "RelationalAttributeSet", objectSet);
		CollClass* relationalTableSet = NewClass<CollClass>(*this, "RelationalTableSet", objectSet);
		CollClass* routineSet = NewClass<CollClass>(*this, "RoutineSet", objectSet);
		CollClass* schemaViewSet = NewClass<CollClass>(*this, "SchemaViewSet", objectSet);
		CollClass* scriptSet = NewClass<CollClass>(*this, "ScriptSet", objectSet);
		CollClass* sysCollClassSet = NewClass<CollClass>(*this, "SysCollClassSet", objectSet);
		CollClass* sysConstantSet = NewClass<CollClass>(*this, "SysConstantSet", objectSet);
		CollClass* sysDbClassMapSet = NewClass<CollClass>(*this, "SysDbClassMapSet", objectSet);
		CollClass* sysExternalKeySet = NewClass<CollClass>(*this, "SysExternalKeySet", objectSet);
		CollClass* sysFeatureUsageSet = NewClass<CollClass>(*this, "SysFeatureUsageSet", objectSet);
		CollClass* sysJadeExposedClassSet = NewClass<CollClass>(*this, "SysJadeExposedClassSet", objectSet);
		CollClass* sysJadeExposedFeatureSet = NewClass<CollClass>(*this, "SysJadeExposedFeatureSet", objectSet);
		CollClass* sysJadeInterfaceSet = NewClass<CollClass>(Version(20, 0, 1), *this, "SysJadeInterfaceSet", objectSet);
		CollClass* sysKeyUsageSet = NewClass<CollClass>(*this, "SysKeyUsageSet", objectSet);
		CollClass* sysMemberKeySet = NewClass<CollClass>(*this, "SysMemberKeySet", objectSet);
		CollClass* sysMethodSet = NewClass<CollClass>(*this, "SysMethodSet", objectSet);
		CollClass* sysPropertySet = NewClass<CollClass>(*this, "SysPropertySet", objectSet);
		CollClass* sysTypeSet = NewClass<CollClass>(*this, "SysTypeSet", objectSet);
		CollClass* sysTypeUsageSet = NewClass<CollClass>(*this, "SysTypeUsageSet", objectSet);
		CollClass* typeSet = NewClass<CollClass>(*this, "TypeSet", objectSet);
		CollClass* typeUsageSet = NewClass<CollClass>(*this, "TypeUsageSet", objectSet);
		CollClass* userDbClassMapSet = NewClass<CollClass>(*this, "UserDbClassMapSet", objectSet);
		CollClass* userPropertySet = NewClass<CollClass>(*this, "UserPropertySet", objectSet);
		CollClass* userTypeSet = NewClass<CollClass>(*this, "UserTypeSet", objectSet);
		CollClass* jadeBytes = NewClass<CollClass>(*this, "JadeBytes", *collection);
		CollClass* jadeRTree = NewClass<CollClass>(*this, "JadeRTree", *collection);
		CollClass* jadeExternalKeyRTree = NewClass<CollClass>(*this, "JadeExternalKeyRTree", jadeRTree);
		CollClass* jadeSpatialRTree = NewClass<CollClass>(*this, "JadeSpatialRTree", jadeExternalKeyRTree);
		CollClass* jadeMemberKeyRTree = NewClass<CollClass>(*this, "JadeMemberKeyRTree", jadeRTree);
		CollClass* list = NewClass<CollClass>(*this, "List", *collection);
		CollClass* array = NewClass<CollClass>(*this, "Array", list);
		CollClass* binaryArray = NewClass<CollClass>(*this, "BinaryArray", array)->SetMemberType(binary);
		CollClass* booleanArray = NewClass<CollClass>(*this, "BooleanArray", array)->SetMemberType(boolean);
		CollClass* byteArray = NewClass<CollClass>(*this, "ByteArray", array)->SetMemberType(byte);
		CollClass* characterArray = NewClass<CollClass>(*this, "CharacterArray", array)->SetMemberType(character);
		CollClass* dateArray = NewClass<CollClass>(*this, "DateArray", array)->SetMemberType(date);
		CollClass* decimalArray = NewClass<CollClass>(*this, "DecimalArray", array)->SetMemberType(decimal);
		CollClass* hugeStringArray = NewClass<CollClass>(*this, "HugeStringArray", array)->SetMemberType(string);
		CollClass* integer64Array = NewClass<CollClass>(*this, "Integer64Array", array)->SetMemberType(integer64);
		CollClass* integerArray = NewClass<CollClass>(*this, "IntegerArray", array)->SetMemberType(integer);
		CollClass* internalPseudoArray = NewClass<CollClass>(*this, "InternalPseudoArray", array)->SetMemberType(any);
		CollClass* internalPseudoArrayBoolean = NewClass<CollClass>(*this, "InternalPseudoArrayBoolean", internalPseudoArray)->SetMemberType(boolean);
		CollClass* internalPseudoArrayControl = NewClass<CollClass>(*this, "InternalPseudoArrayControl", internalPseudoArray);
		CollClass* internalPseudoArrayInteger = NewClass<CollClass>(*this, "InternalPseudoArrayInteger", internalPseudoArray)->SetMemberType(integer);
		CollClass* internalPseudoArrayMenuItem = NewClass<CollClass>(*this, "InternalPseudoArrayMenuItem", internalPseudoArray);
		CollClass* internalPseudoArrayObject = NewClass<CollClass>(*this, "InternalPseudoArrayObject", internalPseudoArray)->SetMemberType(*object);
		CollClass* internalPseudoArrayPicture = NewClass<CollClass>(*this, "InternalPseudoArrayPicture", internalPseudoArray)->SetMemberType(binary);
		CollClass* internalPseudoArrayString = NewClass<CollClass>(*this, "InternalPseudoArrayString", internalPseudoArray)->SetMemberType(string);
		CollClass* objectArray = NewClass<CollClass>(*this, "ObjectArray", array)->SetMemberType(*object);
		CollClass* applicationArray = NewClass<CollClass>(*this, "ApplicationArray", objectArray);
		CollClass* classColl = NewClass<CollClass>(*this, "ClassColl", objectArray)->SetMemberType(*class_);
		CollClass* constantColl = NewClass<CollClass>(*this, "ConstantColl", objectArray)->SetMemberType(*constant);
		CollClass* controlOrdList = NewClass<CollClass>(*this, "ControlOrdList", objectArray);
		CollClass* dbFileArray = NewClass<CollClass>(*this, "DbFileArray", objectArray)->SetMemberType(*dbFile);
		CollClass* featureArray = NewClass<CollClass>(*this, "FeatureArray", objectArray)->SetMemberType(*feature);
		CollClass* featureUsageColl = NewClass<CollClass>(*this, "FeatureUsageColl", objectArray);
		CollClass* fileNodeArray = NewClass<CollClass>(*this, "FileNodeArray", objectArray);
		CollClass* inlineUsageColl = NewClass<CollClass>(*this, "InlineUsageColl", objectArray);
		CollClass* jadeAnnotationTagArray = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeAnnotationTagArray", objectArray)->SetMemberType(*jadeAnnotationTag);
		CollClass* jadeDbFilePartitionArray = NewClass<CollClass>(*this, "JadeDbFilePartitionArray", objectArray);
		CollClass* jadeDynamicObjectArray = NewClass<CollClass>(*this, "JadeDynamicObjectArray", objectArray);
		CollClass* jadeGenericTypeArray = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeGenericTypeArray", objectArray)->SetMemberType(*jadeGenericType);
		CollClass* jadeInterfaceColl = NewClass<CollClass>(*this, "JadeInterfaceColl", objectArray)->SetMemberType(*jadeInterface);
		CollClass* jadeJWTClaimArray = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeJWTClaimArray", objectArray);
		CollClass* jadeLocalVarColl = NewClass<CollClass>(*this, "JadeLocalVarColl", objectArray);
		CollClass* jadePrintDataArray = NewClass<CollClass>(*this, "JadePrintDataArray", objectArray);
		CollClass* jadeRequiredClaimArray = NewClass<CollClass>(Version(20, 0, 1), *this, "JadeRequiredClaimArray", objectArray);
		CollClass* jadeSkinsColl = NewClass<CollClass>(*this, "JadeSkinsColl", objectArray);
		CollClass* jadeWebServiceUnknownHdrArray = NewClass<CollClass>(*this, "JadeWebServiceUnknownHdrArray", objectArray);
		CollClass* jadeXMLAttributeArray = NewClass<CollClass>(*this, "JadeXMLAttributeArray", objectArray);
		CollClass* jadeXMLElementArray = NewClass<CollClass>(*this, "JadeXMLElementArray", objectArray);
		CollClass* jadeXMLNodeArray = NewClass<CollClass>(*this, "JadeXMLNodeArray", objectArray);
		CollClass* keyColl = NewClass<CollClass>(*this, "KeyColl", objectArray);
		CollClass* keyUsageColl = NewClass<CollClass>(*this, "KeyUsageColl", objectArray);
		CollClass* lockArray = NewClass<CollClass>(*this, "LockArray", objectArray);
		CollClass* menuOrdList = NewClass<CollClass>(*this, "MenuOrdList", objectArray);
		CollClass* methodColl = NewClass<CollClass>(*this, "MethodColl", objectArray)->SetMemberType(*method);
		CollClass* notificationArray = NewClass<CollClass>(*this, "NotificationArray", objectArray);
		CollClass* oleArray = NewClass<CollClass>(*this, "OleArray", objectArray);
		CollClass* parameterColl = NewClass<CollClass>(*this, "ParameterColl", objectArray);
		CollClass* patchVersionDetailColl = NewClass<CollClass>(*this, "PatchVersionDetailColl", objectArray);
		CollClass* processStackArray = NewClass<CollClass>(*this, "ProcessStackArray", objectArray);
		CollClass* propertyColl = NewClass<CollClass>(*this, "PropertyColl", objectArray)->SetMemberType(*property);
		CollClass* rectangleArray = NewClass<CollClass>(*this, "RectangleArray", objectArray);
		CollClass* schemaColl = NewClass<CollClass>(*this, "SchemaColl", objectArray)->SetMemberType(*schema);
		CollClass* sortActorArray = NewClass<CollClass>(*this, "SortActorArray", objectArray);
		new PrimAttribute(sortActorArray, *rootSchema.primAttribute, "kway");
		new PrimAttribute(sortActorArray, *rootSchema.primAttribute, "lcid");
		new PrimAttribute(sortActorArray, *rootSchema.primAttribute, "maxMem");
		
		CollClass* typeColl = NewClass<CollClass>(*this, "TypeColl", objectArray)->SetMemberType(*type);
		CollClass* pointArray = NewClass<CollClass>(*this, "PointArray", array)->SetMemberType(point);
		CollClass* realArray = NewClass<CollClass>(*this, "RealArray", array)->SetMemberType(real);
		CollClass* stringArray = NewClass<CollClass>(*this, "StringArray", array)->SetMemberType(string);
		CollClass* jadeIdentifierArray = NewClass<CollClass>(*this, "JadeIdentifierArray", stringArray)->SetMemberType(string);
		CollClass* jadeLongStringArray = NewClass<CollClass>(*this, "JadeLongStringArray", stringArray)->SetMemberType(string);
		CollClass* stringUtf8Array = NewClass<CollClass>(*this, "StringUtf8Array", array)->SetMemberType(stringUtf8);
		CollClass* timeArray = NewClass<CollClass>(*this, "TimeArray", array)->SetMemberType(time);
		CollClass* timeStampArray = NewClass<CollClass>(*this, "TimeStampArray", array)->SetMemberType(timeStamp);
		CollClass* timeStampIntervalArray = NewClass<CollClass>(*this, "TimeStampIntervalArray", array)->SetMemberType(timeStampInterval);
		Class* connection = NewClass<>(*this, "Connection", *object);
		Class* jadeSerialPort = NewClass<>(*this, "JadeSerialPort", connection);
		Class* namedPipe = NewClass<>(*this, "NamedPipe", connection);
		new PrimAttribute(namedPipe, *rootSchema.primAttribute, "serverName");
		
		Class* internetPipe = NewClass<>(*this, "InternetPipe", namedPipe);
		Class* tcpIpConnection = NewClass<>(*this, "TcpIpConnection", connection);
		new ImplicitInverseRef(tcpIpConnection, *rootSchema.implicitInverseRef, "userObject");
		
		Class* jadeInternetTCPIPConnection = NewClass<>(*this, "JadeInternetTCPIPConnection", tcpIpConnection);
		ExceptionClass* exception = NewClass<ExceptionClass>(*this, "Exception", *object);
		new PrimAttribute(exception, *rootSchema.primAttribute, "category");
		new PrimAttribute(exception, *rootSchema.primAttribute, "continuable");
		new ImplicitInverseRef(exception, *rootSchema.implicitInverseRef, "currentMethodDesc");
		new PrimAttribute(exception, *rootSchema.primAttribute, "errorCode");
		new PrimAttribute(exception, *rootSchema.primAttribute, "errorItem");
		new PrimAttribute(exception, *rootSchema.primAttribute, "helpBook");
		new PrimAttribute(exception, *rootSchema.primAttribute, "kind");
		new PrimAttribute(exception, *rootSchema.primAttribute, "level");
		new PrimAttribute(exception, *rootSchema.primAttribute, "remoteErrorCode");
		new ImplicitInverseRef(exception, *rootSchema.implicitInverseRef, "reportingMethodDesc");
		new PrimAttribute(exception, *rootSchema.primAttribute, "resumable");
		
		ExceptionClass* fatalError = NewClass<ExceptionClass>(*this, "FatalError", exception);
		ExceptionClass* normalException = NewClass<ExceptionClass>(*this, "NormalException", exception);
		ExceptionClass* connectionException = NewClass<ExceptionClass>(*this, "ConnectionException", normalException);
		ExceptionClass* fileException = NewClass<ExceptionClass>(*this, "FileException", normalException);
		ExceptionClass* jadeMessagingException = NewClass<ExceptionClass>(*this, "JadeMessagingException", normalException);
		ExceptionClass* jadeRegexException = NewClass<ExceptionClass>(Version(20, 0, 1), *this, "JadeRegexException", normalException);
		new PrimAttribute(jadeRegexException, *rootSchema.primAttribute, "compileErrorOffset");
		new PrimAttribute(jadeRegexException, *rootSchema.primAttribute, "nativeErrorCode");
		new PrimAttribute(jadeRegexException, *rootSchema.primAttribute, "nativeErrorMessage");
		
		ExceptionClass* jadeSOAPException = NewClass<ExceptionClass>(*this, "JadeSOAPException", normalException);
		ExceptionClass* jadeXMLException = NewClass<ExceptionClass>(*this, "JadeXMLException", normalException);
		new PrimAttribute(jadeXMLException, *rootSchema.primAttribute, "columnNumber");
		new PrimAttribute(jadeXMLException, *rootSchema.primAttribute, "lineNumber");
		
		ExceptionClass* odbcException = NewClass<ExceptionClass>(*this, "ODBCException", normalException);
		ExceptionClass* systemException = NewClass<ExceptionClass>(*this, "SystemException", normalException);
		ExceptionClass* deadlockException = NewClass<ExceptionClass>(*this, "DeadlockException", systemException);
		ExceptionClass* integrityViolation = NewClass<ExceptionClass>(*this, "IntegrityViolation", systemException);
		ExceptionClass* lockException = NewClass<ExceptionClass>(*this, "LockException", systemException);
		ExceptionClass* notificationException = NewClass<ExceptionClass>(*this, "NotificationException", systemException);
		ExceptionClass* userInterfaceException = NewClass<ExceptionClass>(*this, "UserInterfaceException", normalException);
		ExceptionClass* activeXInvokeException = NewClass<ExceptionClass>(*this, "ActiveXInvokeException", userInterfaceException);
		new PrimAttribute(activeXInvokeException, *rootSchema.primAttribute, "activeXErrorCode");
		new PrimAttribute(activeXInvokeException, *rootSchema.primAttribute, "helpContext");
		new PrimAttribute(activeXInvokeException, *rootSchema.primAttribute, "helpFile");
		new PrimAttribute(activeXInvokeException, *rootSchema.primAttribute, "source");
		
		ExceptionClass* jadeDotNetInvokeException = NewClass<ExceptionClass>(*this, "JadeDotNetInvokeException", userInterfaceException);
		new ImplicitInverseRef(jadeDotNetInvokeException, *rootSchema.implicitInverseRef, "dotNetExceptionObject");
		
		ExceptionClass* webSocketException = NewClass<ExceptionClass>(*this, "WebSocketException", normalException);
		Class* exceptionHandlerDesc = NewClass<>(*this, "ExceptionHandlerDesc", *object);
		new ImplicitInverseRef(exceptionHandlerDesc, *rootSchema.implicitInverseRef, "armingApplication");
		new ImplicitInverseRef(exceptionHandlerDesc, *rootSchema.implicitInverseRef, "armingMethod");
		new ImplicitInverseRef(exceptionHandlerDesc, *rootSchema.implicitInverseRef, "exceptionClass");
		new ImplicitInverseRef(exceptionHandlerDesc, *rootSchema.implicitInverseRef, "exceptionHandlerMethod");
		new ImplicitInverseRef(exceptionHandlerDesc, *rootSchema.implicitInverseRef, "exceptionHandlerReceiver");
		new PrimAttribute(exceptionHandlerDesc, *rootSchema.primAttribute, "invocationCount");
		new PrimAttribute(exceptionHandlerDesc, *rootSchema.primAttribute, "isGlobal");
		
		Class* externalDatabase = NewClass<>(*this, "ExternalDatabase", *object);
		new PrimAttribute(externalDatabase, *rootSchema.primAttribute, "name");
		new PrimAttribute(externalDatabase, *rootSchema.primAttribute, "password");
		new PrimAttribute(externalDatabase, *rootSchema.primAttribute, "userName");
		
		Class* fileNode = NewClass<>(*this, "FileNode", *object);
		Class* file = NewClass<>(*this, "File", fileNode);
		new PrimAttribute(file, *rootSchema.primAttribute, "allowReplace");
		new PrimAttribute(file, *rootSchema.primAttribute, "endOfField");
		new PrimAttribute(file, *rootSchema.primAttribute, "kind");
		new PrimAttribute(file, *rootSchema.primAttribute, "maxIOSize");
		new PrimAttribute(file, *rootSchema.primAttribute, "maxRecordSize");
		new PrimAttribute(file, *rootSchema.primAttribute, "mode");
		new PrimAttribute(file, *rootSchema.primAttribute, "recordSize");
		new PrimAttribute(file, *rootSchema.primAttribute, "shareMode");
		new PrimAttribute(file, *rootSchema.primAttribute, "unicodeBOM");
		
		Class* fileFolder = NewClass<>(*this, "FileFolder", fileNode);
		Class* global = NewClass<>(*this, "Global", *object);
		Class* rootSchemaGlobal = NewClass<>(*this, "RootSchemaGlobal", global);
		Class* iterator = NewClass<>(*this, "Iterator", *object);
		Class* arrayIterator = NewClass<>(*this, "ArrayIterator", iterator);
		Class* jadeVariableArrayIterator = NewClass<>(Version(20, 0, 1), *this, "JadeVariableArrayIterator", arrayIterator);
		Class* dictIterator = NewClass<>(*this, "DictIterator", iterator);
		Class* externalIterator = NewClass<>(*this, "ExternalIterator", iterator);
		Class* mergeIterator = NewClass<>(*this, "MergeIterator", iterator);
		new PrimAttribute(mergeIterator, *rootSchema.primAttribute, "ignoreDuplicates");
		
		Class* setIterator = NewClass<>(*this, "SetIterator", iterator);
		Class* setMergeIterator = NewClass<>(*this, "SetMergeIterator", iterator);
		new PrimAttribute(setMergeIterator, *rootSchema.primAttribute, "ignoreDuplicates");
		
		Class* jadeAnnotation = NewClass<>(Version(20, 0, 1), *this, "JadeAnnotation", *object);
		Class* jadeSystemAnnotation = NewClass<>(Version(20, 0, 1), *this, "JadeSystemAnnotation", jadeAnnotation);
		Class* jadeRequiredClaimAnnotation = NewClass<>(Version(20, 0, 1), *this, "JadeRequiredClaimAnnotation", jadeSystemAnnotation);
		new ImplicitInverseRef(jadeRequiredClaimAnnotation, *rootSchema.implicitInverseRef, "target");
		
		Class* jadeRequiredDelegateClaimAnnotation = NewClass<>(Version(20, 0, 1), *this, "JadeRequiredDelegateClaimAnnotation", jadeRequiredClaimAnnotation);
		Class* jadeRequiredOneOfValueClaimAnnotation = NewClass<>(Version(20, 0, 1), *this, "JadeRequiredOneOfValueClaimAnnotation", jadeRequiredClaimAnnotation);
		Class* jadeRequiredSingleValueClaimAnnotation = NewClass<>(Version(20, 0, 1), *this, "JadeRequiredSingleValueClaimAnnotation", jadeRequiredClaimAnnotation);
		Class* jadeAuditAccess = NewClass<>(*this, "JadeAuditAccess", *object);
		Class* jadeCompilerResult = NewClass<>(Version(22, 0, 1), *this, "JadeCompilerResult", *object);
		Class* jadeDataTransferObject = NewClass<>(Version(22, 0, 1), *this, "JadeDataTransferObject", *object);
		Class* jadeSchemaModelDTO = NewClass<>(Version(22, 0, 1), *this, "JadeSchemaModelDTO", jadeDataTransferObject);
		Class* jadeDatabaseAdmin = NewClass<>(*this, "JadeDatabaseAdmin", *object);
		Class* jadeDbFilePartition = NewClass<>(*this, "JadeDbFilePartition", *object);
		new ImplicitInverseRef(jadeDbFilePartition, *rootSchema.implicitInverseRef, "dbFile");
		new PrimAttribute(jadeDbFilePartition, *rootSchema.primAttribute, "partitionID");
		
		Class* jadeDotNetType = NewClass<>(*this, "JadeDotNetType", *object);
		new PrimAttribute(jadeDotNetType, *rootSchema.primAttribute, "usePresentationClient");
		
		Class* jadeDynamicObject = NewClass<>(*this, "JadeDynamicObject", *object);
		new ExplicitInverseRef(jadeDynamicObject, *rootSchema.explicitInverseRef, "children");
		new PrimAttribute(jadeDynamicObject, *rootSchema.primAttribute, "name");
		new ExplicitInverseRef(jadeDynamicObject, *rootSchema.explicitInverseRef, "parent");
		new PrimAttribute(jadeDynamicObject, *rootSchema.primAttribute, "type");
		
		Class* jadeGenericMessage = NewClass<>(*this, "JadeGenericMessage", *object);
		Class* jadeGenericQueue = NewClass<>(*this, "JadeGenericQueue", *object);
		new PrimAttribute(jadeGenericQueue, *rootSchema.primAttribute, "fullName");
		new PrimAttribute(jadeGenericQueue, *rootSchema.primAttribute, "name");
		
		Class* jadeGenericQueueManager = NewClass<>(*this, "JadeGenericQueueManager", *object);
		new PrimAttribute(jadeGenericQueueManager, *rootSchema.primAttribute, "name");
		
		HTMLClass* jadeHTMLClass = NewClass<HTMLClass>(*this, "JadeHTMLClass", *object);
		new PrimAttribute(jadeHTMLClass, *rootSchema.primAttribute, "executeCodeAllowed");
		new PrimAttribute(jadeHTMLClass, *rootSchema.primAttribute, "executeMethodAllowed");
		new PrimAttribute(jadeHTMLClass, *rootSchema.primAttribute, "generateHTMLForJadeTagsOnly");
		new ImplicitInverseRef(jadeHTMLClass, *rootSchema.implicitInverseRef, "gotoPage");
		new PrimAttribute(jadeHTMLClass, *rootSchema.primAttribute, "securePage");
		
		Class* jadeHTTPConnection = NewClass<>(*this, "JadeHTTPConnection", *object);
		new PrimAttribute(jadeHTTPConnection, *rootSchema.primAttribute, "connectTimeout");
		new PrimAttribute(jadeHTTPConnection, *rootSchema.primAttribute, "protocolFamily");
		new PrimAttribute(jadeHTTPConnection, *rootSchema.primAttribute, "proxyConfig");
		new PrimAttribute(jadeHTTPConnection, *rootSchema.primAttribute, "receiveTimeout");
		new PrimAttribute(jadeHTTPConnection, *rootSchema.primAttribute, "sendTimeout");
		new PrimAttribute(jadeHTTPConnection, *rootSchema.primAttribute, "state");
		new PrimAttribute(jadeHTTPConnection, *rootSchema.primAttribute, "usePresentationClient");
		
		Class* jadeJWTModel = NewClass<>(Version(20, 0, 1), *this, "JadeJWTModel", *object);
		Class* jadeJWKSAuthProviderResponse = NewClass<>(Version(20, 0, 1), *this, "JadeJWKSAuthProviderResponse", jadeJWTModel);
		Class* jadeJWTClaim = NewClass<>(Version(20, 0, 1), *this, "JadeJWTClaim", jadeJWTModel);
		Class* jadeJWTParser = NewClass<>(Version(20, 0, 1), *this, "JadeJWTParser", jadeJWTModel);
		Class* jadeJWTValidator = NewClass<>(Version(20, 0, 1), *this, "JadeJWTValidator", jadeJWTModel);
		Class* jadeJsonWebKeySetReader = NewClass<>(Version(20, 0, 1), *this, "JadeJsonWebKeySetReader", jadeJWTModel);
		Class* jadeJsonWebToken = NewClass<>(Version(20, 0, 1), *this, "JadeJsonWebToken", jadeJWTModel);
		new ImplicitInverseRef(jadeJsonWebToken, *rootSchema.implicitInverseRef, "claims");
		new PrimAttribute(jadeJsonWebToken, *rootSchema.primAttribute, "id");
		new PrimAttribute(jadeJsonWebToken, *rootSchema.primAttribute, "issuedAt");
		new PrimAttribute(jadeJsonWebToken, *rootSchema.primAttribute, "issuer");
		new PrimAttribute(jadeJsonWebToken, *rootSchema.primAttribute, "subject");
		new PrimAttribute(jadeJsonWebToken, *rootSchema.primAttribute, "validFrom");
		new PrimAttribute(jadeJsonWebToken, *rootSchema.primAttribute, "validTo");
		
		Class* jadeJson = NewClass<>(*this, "JadeJson", *object);
		new PrimAttribute(jadeJson, *rootSchema.primAttribute, "offsetForTimeStamps");
		new PrimAttribute(jadeJson, *rootSchema.primAttribute, "useISO8601");
		
		Class* jadeLicenceInfo = NewClass<>(*this, "JadeLicenceInfo", *object);
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "developmentLicences");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "expiryDate");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "jadeStandardMin");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "jadeThinMin");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "jadeVersion");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "licenceName");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "licenceRestriction");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "licenceType");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "maxDBSize");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "nCpuSockets");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "nHtmlThinSessions");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "nJadeDevProcesses");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "nJadeThinNonJadeDevProcesses");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "nNonJadeDevProcesses");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "nProcessesLeft");
		new PrimAttribute(jadeLicenceInfo, *rootSchema.primAttribute, "processLicences");
		
		Class* jadeLog = NewClass<>(*this, "JadeLog", *object);
		Class* jadeMessagingFactory = NewClass<>(*this, "JadeMessagingFactory", *object);
		Class* jadeMetadataAnalyzer = NewClass<>(*this, "JadeMetadataAnalyzer", *object);
		Class* jadeMethodContext = NewClass<>(*this, "JadeMethodContext", *object);
		new PrimAttribute(jadeMethodContext, *rootSchema.primAttribute, "tag");
		
		Class* jadeMultiDimensionalObject = NewClass<>(*this, "JadeMultiDimensionalObject", *object);
		new PrimAttribute(jadeMultiDimensionalObject, *rootSchema.primAttribute, "dimension");
		new ImplicitInverseRef(jadeMultiDimensionalObject, *rootSchema.implicitInverseRef, "envelope");
		
		Class* jadeGeometry = NewClass<>(*this, "JadeGeometry", jadeMultiDimensionalObject);
		new PrimAttribute(jadeGeometry, *rootSchema.primAttribute, "srid");
		
		Class* jadeRegion = NewClass<>(*this, "JadeRegion", jadeGeometry);
		Class* jadeMultiWorkerTcpConnection = NewClass<>(*this, "JadeMultiWorkerTcpConnection", *object);
		Class* jadeMultiWorkerTcpTransport = NewClass<>(*this, "JadeMultiWorkerTcpTransport", *object);
		Class* jadeOpenAPI = NewClass<>(Version(20, 0, 2), *this, "JadeOpenAPI", *object);
		Class* jadeOpenAPIGenerator = NewClass<>(Version(20, 0, 2), *this, "JadeOpenAPIGenerator", jadeOpenAPI);
		Class* jadePaginationEnvelope = NewClass<>(Version(22, 0, 1), *this, "JadePaginationEnvelope", *object);
		new ImplicitInverseRef(jadePaginationEnvelope, *rootSchema.implicitInverseRef, "data");
		
		Class* jadePatchControlInterface = NewClass<>(*this, "JadePatchControlInterface", *object);
		Class* jadePatchDiffer = NewClass<>(Version(20, 0, 1), *this, "JadePatchDiffer", *object);
		Class* jadePatchVersion = NewClass<>(*this, "JadePatchVersion", *object);
		Class* jadePatchVersionDetail = NewClass<>(*this, "JadePatchVersionDetail", *object);
		Class* jadePrintData = NewClass<>(*this, "JadePrintData", *object);
		Class* jadePrintDirect = NewClass<>(*this, "JadePrintDirect", jadePrintData);
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "fontBold");
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "fontItalic");
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "fontName");
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "fontSize");
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "fontStrikethru");
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "fontUnderline");
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "left");
		new PrimAttribute(jadePrintDirect, *rootSchema.primAttribute, "top");
		
		Class* jadePrintPage = NewClass<>(*this, "JadePrintPage", jadePrintData);
		new PrimAttribute(jadePrintPage, *rootSchema.primAttribute, "customPaperHeight");
		new PrimAttribute(jadePrintPage, *rootSchema.primAttribute, "customPaperWidth");
		new PrimAttribute(jadePrintPage, *rootSchema.primAttribute, "documentType");
		new PrimAttribute(jadePrintPage, *rootSchema.primAttribute, "orientation");
		new PrimAttribute(jadePrintPage, *rootSchema.primAttribute, "pageNumber");
		new PrimAttribute(jadePrintPage, *rootSchema.primAttribute, "paperSource");
		
		Class* jadeProfiler = NewClass<>(*this, "JadeProfiler", *object);
		Class* jadeRegexLibrary = NewClass<>(Version(20, 0, 1), *this, "JadeRegexLibrary", *object);
		Class* jadeRegex = NewClass<>(Version(20, 0, 1), *this, "JadeRegex", jadeRegexLibrary);
		Class* jadeRegexCapture = NewClass<>(Version(20, 0, 1), *this, "JadeRegexCapture", jadeRegexLibrary);
		Class* jadeRegexMatch = NewClass<>(Version(20, 0, 1), *this, "JadeRegexMatch", jadeRegexLibrary);
		Class* jadeRegexPattern = NewClass<>(Version(20, 0, 1), *this, "JadeRegexPattern", jadeRegexLibrary);
		Class* jadeRegexResult = NewClass<>(Version(20, 0, 1), *this, "JadeRegexResult", jadeRegexLibrary);
		Class* jadeReport = NewClass<>(*this, "JadeReport", *object);
		new PrimAttribute(jadeReport, *rootSchema.primAttribute, "collate");
		new PrimAttribute(jadeReport, *rootSchema.primAttribute, "copies");
		new PrimAttribute(jadeReport, *rootSchema.primAttribute, "description");
		new PrimAttribute(jadeReport, *rootSchema.primAttribute, "duplex");
		new ImplicitInverseRef(jadeReport, *rootSchema.implicitInverseRef, "printArray");
		new PrimAttribute(jadeReport, *rootSchema.primAttribute, "timeStamp");
		
		Class* jadeReportWriterManager = NewClass<>(*this, "JadeReportWriterManager", *object);
		new PrimAttribute(jadeReportWriterManager, *rootSchema.primAttribute, "configAppName");
		new PrimAttribute(jadeReportWriterManager, *rootSchema.primAttribute, "designAppName");
		new ImplicitInverseRef(jadeReportWriterManager, *rootSchema.implicitInverseRef, "schema");
		new ImplicitInverseRef(jadeReportWriterManager, *rootSchema.implicitInverseRef, "secObject");
		new ImplicitInverseRef(jadeReportWriterManager, *rootSchema.implicitInverseRef, "security");
		new PrimAttribute(jadeReportWriterManager, *rootSchema.primAttribute, "userName");
		
		Class* jadeReportWriterReport = NewClass<>(*this, "JadeReportWriterReport", *object);
		new ImplicitInverseRef(jadeReportWriterReport, *rootSchema.implicitInverseRef, "myManager");
		new ImplicitInverseRef(jadeReportWriterReport, *rootSchema.implicitInverseRef, "myReport");
		new PrimAttribute(jadeReportWriterReport, *rootSchema.primAttribute, "name");
		new ImplicitInverseRef(jadeReportWriterReport, *rootSchema.implicitInverseRef, "persistentReport");
		new ImplicitInverseRef(jadeReportWriterReport, *rootSchema.implicitInverseRef, "schema");
		
		Class* jadeReportWriterSecurity = NewClass<>(*this, "JadeReportWriterSecurity", *object);
		Class* jadeRestClient = NewClass<>(Version(20, 0, 1), *this, "JadeRestClient", *object);
		Class* jadeRestProxy = NewClass<>(Version(20, 0, 1), *this, "JadeRestProxy", *object);
		Class* jadeRestDataModelProxy = NewClass<>(Version(20, 0, 1), *this, "JadeRestDataModelProxy", jadeRestProxy);
		Class* jadeRestResourceProxy = NewClass<>(Version(20, 0, 1), *this, "JadeRestResourceProxy", jadeRestProxy);
		new ImplicitInverseRef(jadeRestResourceProxy, *rootSchema.implicitInverseRef, "hook");
		
		Class* jadeRestProxyHook = NewClass<>(Version(20, 0, 1), *this, "JadeRestProxyHook", *object);
		Class* jadeRestRequest = NewClass<>(Version(20, 0, 1), *this, "JadeRestRequest", *object);
		Class* jadeRestResponse = NewClass<>(Version(20, 0, 1), *this, "JadeRestResponse", *object);
		Class* jadeRestService = NewClass<>(*this, "JadeRestService", *object);
		new PrimAttribute(jadeRestService, *rootSchema.primAttribute, "exceptionFormat");
		new PrimAttribute(jadeRestService, *rootSchema.primAttribute, "httpStatusCode");
		new ImplicitInverseRef(jadeRestService, *rootSchema.implicitInverseRef, "objectsToBeDeleted");
		
		Class* jadeSSLContext = NewClass<>(*this, "JadeSSLContext", *object);
		new PrimAttribute(jadeSSLContext, *rootSchema.primAttribute, "methodType");
		new PrimAttribute(jadeSSLContext, *rootSchema.primAttribute, "verifyDepth");
		new PrimAttribute(jadeSSLContext, *rootSchema.primAttribute, "verifyRemoteCertificate");
		new ImplicitInverseRef(jadeSSLContext, *rootSchema.implicitInverseRef, "x509");
		
		Class* jadeScript = NewClass<>(*this, "JadeScript", *object);
		Class* jadeSkin = NewClass<>(*this, "JadeSkin", *object);
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "backColor");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionActiveColor");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionFontBold");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionFontItalic");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionFontName");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionFontSize");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionInactiveColor");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionLeft");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "captionTop");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "centerCaption");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "menuFontBold");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "menuFontItalic");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "menuFontName");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "menuFontSize");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "menuLeftPosition");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "menuTopPosition");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "name");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "showMenuLineAlways");
		new PrimAttribute(jadeSkin, *rootSchema.primAttribute, "transparentColor");
		
		Class* jadeSkinEntity = NewClass<>(*this, "JadeSkinEntity", *object);
		new ExplicitInverseRef(jadeSkinEntity, *rootSchema.explicitInverseRef, "myOwners");
		new ExplicitInverseRef(jadeSkinEntity, *rootSchema.explicitInverseRef, "mySkinRoot");
		new PrimAttribute(jadeSkinEntity, *rootSchema.primAttribute, "name");
		
		Class* jadeSkinApplication = NewClass<>(*this, "JadeSkinApplication", jadeSkinEntity);
		new ExplicitInverseRef(jadeSkinApplication, *rootSchema.explicitInverseRef, "myControlSkins");
		new ExplicitInverseRef(jadeSkinApplication, *rootSchema.explicitInverseRef, "myFormSkins");
		
		Class* jadeSkinArea = NewClass<>(*this, "JadeSkinArea", jadeSkinEntity);
		new PrimAttribute(jadeSkinArea, *rootSchema.primAttribute, "backColor");
		new PrimAttribute(jadeSkinArea, *rootSchema.primAttribute, "innerIsBrush");
		
		Class* jadeSkinMenu = NewClass<>(*this, "JadeSkinMenu", jadeSkinArea);
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "backColorSelected");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "borderStyle");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "drawMenuSelectionFlat");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "fontBold");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "fontItalic");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "fontName");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "fontSize");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "foreColor");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "foreColorDisabled");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "foreColorSelected");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "lineHeight");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "pixelsAfterCheckMark");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "pixelsAfterPicture");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "pixelsBeforeAccelerator");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "pixelsBeforeCheckMark");
		new PrimAttribute(jadeSkinMenu, *rootSchema.primAttribute, "pixelsBeforeRightArrow");
		
		Class* jadeSkinWindow = NewClass<>(*this, "JadeSkinWindow", jadeSkinArea);
		new ExplicitInverseRef(jadeSkinWindow, *rootSchema.explicitInverseRef, "myHorizontalScrollBarSkin");
		new ExplicitInverseRef(jadeSkinWindow, *rootSchema.explicitInverseRef, "myImageMask");
		new ExplicitInverseRef(jadeSkinWindow, *rootSchema.explicitInverseRef, "mySkinCategory");
		new ExplicitInverseRef(jadeSkinWindow, *rootSchema.explicitInverseRef, "myVerticalScrollBarSkin");
		
		Class* jadeSkinControl = NewClass<>(*this, "JadeSkinControl", jadeSkinWindow);
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "applyCondition");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "borderColorSingle");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "borderStyle");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "focusBackColor");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "focusForeColor");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "fontBold");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "fontItalic");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "fontName");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "fontSize");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "fontStrikethru");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "fontUnderline");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "foreColor");
		new PrimAttribute(jadeSkinControl, *rootSchema.primAttribute, "foreColorDisabled");
		
		Class* jadeSkinBaseControl = NewClass<>(*this, "JadeSkinBaseControl", jadeSkinControl);
		Class* jadeSkinBrowseButtons = NewClass<>(*this, "JadeSkinBrowseButtons", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinBrowseButtons, *rootSchema.explicitInverseRef, "myFirstButton");
		new ExplicitInverseRef(jadeSkinBrowseButtons, *rootSchema.explicitInverseRef, "myLastButton");
		new ExplicitInverseRef(jadeSkinBrowseButtons, *rootSchema.explicitInverseRef, "myNextButton");
		new ExplicitInverseRef(jadeSkinBrowseButtons, *rootSchema.explicitInverseRef, "myPriorButton");
		
		Class* jadeSkinButton = NewClass<>(*this, "JadeSkinButton", jadeSkinControl);
		new PrimAttribute(jadeSkinButton, *rootSchema.primAttribute, "createRegionFromMask");
		new ExplicitInverseRef(jadeSkinButton, *rootSchema.explicitInverseRef, "myButtonDisabled");
		new ExplicitInverseRef(jadeSkinButton, *rootSchema.explicitInverseRef, "myButtonDown");
		new ExplicitInverseRef(jadeSkinButton, *rootSchema.explicitInverseRef, "myButtonFocus");
		new ExplicitInverseRef(jadeSkinButton, *rootSchema.explicitInverseRef, "myButtonFocusDown");
		new ExplicitInverseRef(jadeSkinButton, *rootSchema.explicitInverseRef, "myButtonRollOver");
		new ExplicitInverseRef(jadeSkinButton, *rootSchema.explicitInverseRef, "myButtonRollUnder");
		new ExplicitInverseRef(jadeSkinButton, *rootSchema.explicitInverseRef, "myButtonUp");
		
		Class* jadeSkinCheckBox = NewClass<>(*this, "JadeSkinCheckBox", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinCheckBox, *rootSchema.explicitInverseRef, "myFalseImage");
		new ExplicitInverseRef(jadeSkinCheckBox, *rootSchema.explicitInverseRef, "myTrueImage");
		
		Class* jadeSkinComboBox = NewClass<>(*this, "JadeSkinComboBox", jadeSkinControl);
		new PrimAttribute(jadeSkinComboBox, *rootSchema.primAttribute, "buttonRightOffset");
		new ExplicitInverseRef(jadeSkinComboBox, *rootSchema.explicitInverseRef, "myComboButton");
		new ExplicitInverseRef(jadeSkinComboBox, *rootSchema.explicitInverseRef, "myListBoxSkin");
		new ExplicitInverseRef(jadeSkinComboBox, *rootSchema.explicitInverseRef, "mySimpleComboTextBoxSkin");
		
		Class* jadeSkinFolder = NewClass<>(*this, "JadeSkinFolder", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinFolder, *rootSchema.explicitInverseRef, "myTabScrollLeftButton");
		new ExplicitInverseRef(jadeSkinFolder, *rootSchema.explicitInverseRef, "myTabScrollRightButton");
		new ExplicitInverseRef(jadeSkinFolder, *rootSchema.explicitInverseRef, "myTabsButton");
		new PrimAttribute(jadeSkinFolder, *rootSchema.primAttribute, "tabActiveColor");
		new PrimAttribute(jadeSkinFolder, *rootSchema.primAttribute, "tabHeight");
		new PrimAttribute(jadeSkinFolder, *rootSchema.primAttribute, "tabInactiveColor");
		new PrimAttribute(jadeSkinFolder, *rootSchema.primAttribute, "tabScrollButtonBackColor");
		
		Class* jadeSkinFrame = NewClass<>(*this, "JadeSkinFrame", jadeSkinControl);
		Class* jadeSkinGroupBox = NewClass<>(*this, "JadeSkinGroupBox", jadeSkinControl);
		new PrimAttribute(jadeSkinGroupBox, *rootSchema.primAttribute, "captionPosition");
		new PrimAttribute(jadeSkinGroupBox, *rootSchema.primAttribute, "captionPositionLeftOffset");
		new PrimAttribute(jadeSkinGroupBox, *rootSchema.primAttribute, "captionPositionTopOffset");
		new ExplicitInverseRef(jadeSkinGroupBox, *rootSchema.explicitInverseRef, "myLabelSkin");
		
		Class* jadeSkinJadeDockBase = NewClass<>(*this, "JadeSkinJadeDockBase", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinJadeDockBase, *rootSchema.explicitInverseRef, "myHorizontalGripBar");
		new ExplicitInverseRef(jadeSkinJadeDockBase, *rootSchema.explicitInverseRef, "myHorizontalResizeBar");
		new ExplicitInverseRef(jadeSkinJadeDockBase, *rootSchema.explicitInverseRef, "myVerticalGripBar");
		new ExplicitInverseRef(jadeSkinJadeDockBase, *rootSchema.explicitInverseRef, "myVerticalResizeBar");
		
		Class* jadeSkinJadeDockBar = NewClass<>(*this, "JadeSkinJadeDockBar", jadeSkinJadeDockBase);
		Class* jadeSkinJadeDockContainer = NewClass<>(*this, "JadeSkinJadeDockContainer", jadeSkinJadeDockBase);
		Class* jadeSkinJadeEditMask = NewClass<>(*this, "JadeSkinJadeEditMask", jadeSkinControl);
		Class* jadeSkinJadeMask = NewClass<>(*this, "JadeSkinJadeMask", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinJadeMask, *rootSchema.explicitInverseRef, "myButtonSkin");
		
		Class* jadeSkinJadeRichText = NewClass<>(*this, "JadeSkinJadeRichText", jadeSkinControl);
		Class* jadeSkinLabel = NewClass<>(*this, "JadeSkinLabel", jadeSkinControl);
		Class* jadeSkinListBox = NewClass<>(*this, "JadeSkinListBox", jadeSkinControl);
		new PrimAttribute(jadeSkinListBox, *rootSchema.primAttribute, "alternatingRowBackColor");
		new PrimAttribute(jadeSkinListBox, *rootSchema.primAttribute, "alternatingRowBackColorCount");
		new PrimAttribute(jadeSkinListBox, *rootSchema.primAttribute, "selectionColor");
		new PrimAttribute(jadeSkinListBox, *rootSchema.primAttribute, "selectionColorText");
		
		Class* jadeSkinOleControl = NewClass<>(*this, "JadeSkinOleControl", jadeSkinControl);
		Class* jadeSkinOptionButton = NewClass<>(*this, "JadeSkinOptionButton", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinOptionButton, *rootSchema.explicitInverseRef, "myFalseImage");
		new ExplicitInverseRef(jadeSkinOptionButton, *rootSchema.explicitInverseRef, "myTrueImage");
		
		Class* jadeSkinPicture = NewClass<>(*this, "JadeSkinPicture", jadeSkinControl);
		Class* jadeSkinProgressBar = NewClass<>(*this, "JadeSkinProgressBar", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinProgressBar, *rootSchema.explicitInverseRef, "myProgressImage");
		
		Class* jadeSkinScrollBar = NewClass<>(*this, "JadeSkinScrollBar", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinScrollBar, *rootSchema.explicitInverseRef, "myThumbTrack");
		new ExplicitInverseRef(jadeSkinScrollBar, *rootSchema.explicitInverseRef, "myThumbTrackDisabled");
		new ExplicitInverseRef(jadeSkinScrollBar, *rootSchema.explicitInverseRef, "myThumbTrackDown");
		new ExplicitInverseRef(jadeSkinScrollBar, *rootSchema.explicitInverseRef, "myThumbTrackRollOver");
		
		Class* jadeSkinHScroll = NewClass<>(*this, "JadeSkinHScroll", jadeSkinScrollBar);
		new ExplicitInverseRef(jadeSkinHScroll, *rootSchema.explicitInverseRef, "myLeftButton");
		new ExplicitInverseRef(jadeSkinHScroll, *rootSchema.explicitInverseRef, "myRightButton");
		
		Class* jadeSkinVScroll = NewClass<>(*this, "JadeSkinVScroll", jadeSkinScrollBar);
		new ExplicitInverseRef(jadeSkinVScroll, *rootSchema.explicitInverseRef, "myBottomButton");
		new ExplicitInverseRef(jadeSkinVScroll, *rootSchema.explicitInverseRef, "myTopButton");
		
		Class* jadeSkinSheet = NewClass<>(*this, "JadeSkinSheet", jadeSkinControl);
		new ExplicitInverseRef(jadeSkinSheet, *rootSchema.explicitInverseRef, "myTabButton");
		
		Class* jadeSkinStatusLine = NewClass<>(*this, "JadeSkinStatusLine", jadeSkinControl);
		Class* jadeSkinTable = NewClass<>(*this, "JadeSkinTable", jadeSkinControl);
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "alternatingRowBackColor");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "alternatingRowBackColorCount");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "fixed3D");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "fixedColumnsBackColor");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "fixedColumnsForeColor");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "fixedRowColorHasPrecedence");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "fixedRowsBackColor");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "fixedRowsForeColor");
		new ExplicitInverseRef(jadeSkinTable, *rootSchema.explicitInverseRef, "myCheckBoxSkin");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "selectionColor");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "selectionColorText");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "tabActiveColor");
		new PrimAttribute(jadeSkinTable, *rootSchema.primAttribute, "tabInactiveColor");
		
		Class* jadeSkinTextBox = NewClass<>(*this, "JadeSkinTextBox", jadeSkinControl);
		new PrimAttribute(jadeSkinTextBox, *rootSchema.primAttribute, "hintBackColor");
		new PrimAttribute(jadeSkinTextBox, *rootSchema.primAttribute, "hintForeColor");
		
		Class* jadeSkinForm = NewClass<>(*this, "JadeSkinForm", jadeSkinWindow);
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionActiveForeColor");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionFontBold");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionFontItalic");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionFontName");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionFontSize");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionInactiveForeColor");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionLeft");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "captionTop");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "centerCaption");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "drawMenuSelectionFlat");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuBackColor");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuBackColorSelected");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuFontBold");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuFontItalic");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuFontName");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuFontSize");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuForeColor");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuForeColorDisabled");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuForeColorSelected");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuLeftPosition");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "menuTopPosition");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myChildMinimizeBtn");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myChildRestoreBtn");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myChildTerminateBtn");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myMaximizeBtn");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myMaximizedBtn");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myMenuSkin");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myMinimizeBtn");
		new ExplicitInverseRef(jadeSkinForm, *rootSchema.explicitInverseRef, "myTerminateBtn");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "showMenuLineAlways");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "transparentColorForButtons");
		new PrimAttribute(jadeSkinForm, *rootSchema.primAttribute, "useMenuLineSkinForMenus");
		
		Class* jadeSkinWindowStateImage = NewClass<>(*this, "JadeSkinWindowStateImage", jadeSkinArea);
		new PrimAttribute(jadeSkinWindowStateImage, *rootSchema.primAttribute, "foreColor");
		new PrimAttribute(jadeSkinWindowStateImage, *rootSchema.primAttribute, "isImageMask");
		
		Class* jadeSkinCategory = NewClass<>(*this, "JadeSkinCategory", jadeSkinEntity);
		Class* jadeSkinSimpleButton = NewClass<>(*this, "JadeSkinSimpleButton", jadeSkinEntity);
		Class* jadeSkinRoot = NewClass<>(*this, "JadeSkinRoot", *object);
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allApplicationSkins");
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allControlSkins");
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allFormSkins");
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allMenuSkins");
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allSimpleButtonSkins");
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allSkinCategories");
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allSkinEntities");
		new ExplicitInverseRef(jadeSkinRoot, *rootSchema.explicitInverseRef, "allWindowStateImages");
		
		Class* jadeTableElement = NewClass<>(*this, "JadeTableElement", *object);
		Class* jadeTableCell = NewClass<>(*this, "JadeTableCell", jadeTableElement);
		new PrimAttribute(jadeTableCell, *rootSchema.primAttribute, "column");
		new PrimAttribute(jadeTableCell, *rootSchema.primAttribute, "row");
		new PrimAttribute(jadeTableCell, *rootSchema.primAttribute, "sheet");
		
		Class* jadeTableColumn = NewClass<>(*this, "JadeTableColumn", jadeTableElement);
		new PrimAttribute(jadeTableColumn, *rootSchema.primAttribute, "column");
		new PrimAttribute(jadeTableColumn, *rootSchema.primAttribute, "sheet");
		
		Class* jadeTableRow = NewClass<>(*this, "JadeTableRow", jadeTableElement);
		new PrimAttribute(jadeTableRow, *rootSchema.primAttribute, "row");
		new PrimAttribute(jadeTableRow, *rootSchema.primAttribute, "sheet");
		
		Class* jadeTableSheet = NewClass<>(*this, "JadeTableSheet", jadeTableElement);
		new ImplicitInverseRef(jadeTableSheet, *rootSchema.implicitInverseRef, "myTable");
		new PrimAttribute(jadeTableSheet, *rootSchema.primAttribute, "sheet");
		
		Class* jadeTcpIpProxy = NewClass<>(*this, "JadeTcpIpProxy", *object);
		new PrimAttribute(jadeTcpIpProxy, *rootSchema.primAttribute, "browserType");
		new PrimAttribute(jadeTcpIpProxy, *rootSchema.primAttribute, "port");
		new PrimAttribute(jadeTcpIpProxy, *rootSchema.primAttribute, "proxyType");
		
		Class* jadeTestCase = NewClass<>(*this, "JadeTestCase", *object);
		new PrimAttribute(jadeTestCase, *rootSchema.primAttribute, "codeCoverage");
		new PrimAttribute(jadeTestCase, *rootSchema.primAttribute, "logCallStack");
		new PrimAttribute(jadeTestCase, *rootSchema.primAttribute, "profile");
		
		Class* jadeTestRunner = NewClass<>(*this, "JadeTestRunner", *object);
		Class* jadeTimeZone = NewClass<>(Version(20, 0, 1), *this, "JadeTimeZone", *object);
		Class* jadeTransactionTrace = NewClass<>(*this, "JadeTransactionTrace", *object);
		new ImplicitInverseRef(jadeTransactionTrace, *rootSchema.implicitInverseRef, "myProcess");
		new PrimAttribute(jadeTransactionTrace, *rootSchema.primAttribute, "startTime");
		new PrimAttribute(jadeTransactionTrace, *rootSchema.primAttribute, "status");
		new PrimAttribute(jadeTransactionTrace, *rootSchema.primAttribute, "stopTime");
		new PrimAttribute(jadeTransactionTrace, *rootSchema.primAttribute, "tranId");
		
		Class* jadeUserSlobContainer = NewClass<>(*this, "JadeUserSlobContainer", *object);
		JadeWebServicesClass* jadeWebService = NewClass<JadeWebServicesClass>(*this, "JadeWebService", *object);
		JadeWebServiceConsumerClass* jadeWebServiceConsumer = NewClass<JadeWebServiceConsumerClass>(*this, "JadeWebServiceConsumer", jadeWebService);
		new PrimAttribute(jadeWebServiceConsumer, *rootSchema.primAttribute, "characterConversionException");
		new PrimAttribute(jadeWebServiceConsumer, *rootSchema.primAttribute, "handleCharConversionException");
		new PrimAttribute(jadeWebServiceConsumer, *rootSchema.primAttribute, "logStatistics");
		new PrimAttribute(jadeWebServiceConsumer, *rootSchema.primAttribute, "proxyConfig");
		new ImplicitInverseRef(jadeWebServiceConsumer, *rootSchema.implicitInverseRef, "soapHeaders");
		new ExplicitInverseRef(jadeWebServiceConsumer, *rootSchema.explicitInverseRef, "unknownHeaders");
		new PrimAttribute(jadeWebServiceConsumer, *rootSchema.primAttribute, "workerApp");
		
		JadeWebServiceProviderClass* jadeWebServiceProvider = NewClass<JadeWebServiceProviderClass>(*this, "JadeWebServiceProvider", jadeWebService);
		new PrimAttribute(jadeWebServiceProvider, *rootSchema.primAttribute, "deleteTransientReturnType");
		new PrimAttribute(jadeWebServiceProvider, *rootSchema.primAttribute, "rawXML");
		new ExplicitInverseRef(jadeWebServiceProvider, *rootSchema.explicitInverseRef, "unknownHeaders");
		
		JadeWebServiceSoapHeaderClass* jadeWebServiceSoapHeader = NewClass<JadeWebServiceSoapHeaderClass>(*this, "JadeWebServiceSoapHeader", jadeWebService);
		new PrimAttribute(jadeWebServiceSoapHeader, *rootSchema.primAttribute, "didUnderstand");
		new PrimAttribute(jadeWebServiceSoapHeader, *rootSchema.primAttribute, "mustUnderstand");
		
		JadeWebServiceSoapHeaderClass* jadeSessionHeader = NewClass<JadeWebServiceSoapHeaderClass>(*this, "JadeSessionHeader", jadeWebServiceSoapHeader);
		new PrimAttribute(jadeSessionHeader, *rootSchema.primAttribute, "sessionId");
		
		JadeWebServicesClass* jadeWebServiceUnknownHeader = NewClass<JadeWebServicesClass>(*this, "JadeWebServiceUnknownHeader", jadeWebService);
		new ExplicitInverseRef(jadeWebServiceUnknownHeader, *rootSchema.explicitInverseRef, "webService");
		
		Class* jadeWebSocket = NewClass<>(*this, "JadeWebSocket", *object);
		new PrimAttribute(jadeWebSocket, *rootSchema.primAttribute, "id");
		
		Class* jadeWebSocketServer = NewClass<>(*this, "JadeWebSocketServer", *object);
		Class* jadeX509Certificate = NewClass<>(*this, "JadeX509Certificate", *object);
		Class* jadeXMLNode = NewClass<>(*this, "JadeXMLNode", *object);
		new ExplicitInverseRef(jadeXMLNode, *rootSchema.explicitInverseRef, "childNodes");
		new ExplicitInverseRef(jadeXMLNode, *rootSchema.explicitInverseRef, "parentNode");
		
		Class* jadeXMLAttribute = NewClass<>(*this, "JadeXMLAttribute", jadeXMLNode);
		new ExplicitInverseRef(jadeXMLAttribute, *rootSchema.explicitInverseRef, "element");
		
		Class* jadeXMLCharacterData = NewClass<>(*this, "JadeXMLCharacterData", jadeXMLNode);
		Class* jadeXMLCDATA = NewClass<>(*this, "JadeXMLCDATA", jadeXMLCharacterData);
		Class* jadeXMLComment = NewClass<>(*this, "JadeXMLComment", jadeXMLCharacterData);
		Class* jadeXMLText = NewClass<>(*this, "JadeXMLText", jadeXMLCharacterData);
		Class* jadeXMLDocument = NewClass<>(*this, "JadeXMLDocument", jadeXMLNode);
		new ImplicitInverseRef(jadeXMLDocument, *rootSchema.implicitInverseRef, "docType");
		new PrimAttribute(jadeXMLDocument, *rootSchema.primAttribute, "endOfLine");
		new PrimAttribute(jadeXMLDocument, *rootSchema.primAttribute, "indentString");
		new PrimAttribute(jadeXMLDocument, *rootSchema.primAttribute, "keepWhitespace");
		new PrimAttribute(jadeXMLDocument, *rootSchema.primAttribute, "outputDeclaration");
		new ImplicitInverseRef(jadeXMLDocument, *rootSchema.implicitInverseRef, "rootElement");
		
		Class* jadeXMLDocumentType = NewClass<>(*this, "JadeXMLDocumentType", jadeXMLNode);
		Class* jadeXMLElement = NewClass<>(*this, "JadeXMLElement", jadeXMLNode);
		new ExplicitInverseRef(jadeXMLElement, *rootSchema.explicitInverseRef, "attributes");
		
		Class* jadeXMLProcessingInstruction = NewClass<>(*this, "JadeXMLProcessingInstruction", jadeXMLNode);
		Class* jadeXMLParser = NewClass<>(*this, "JadeXMLParser", *object);
		Class* jadeXMLDocumentParser = NewClass<>(*this, "JadeXMLDocumentParser", jadeXMLParser);
		Class* localeNameInfo = NewClass<>(*this, "LocaleNameInfo", *object);
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "abbreviatedCountryName");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "abbreviatedLangName");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "countryCode");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "englishCountryName");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "englishLangName");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "languageId");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "localeId");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "localizedCountryName");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "localizedLangName");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "nativeCountryName");
		new PrimAttribute(localeNameInfo, *rootSchema.primAttribute, "nativeLangName");
		
		Class* localeFullInfo = NewClass<>(*this, "LocaleFullInfo", localeNameInfo);
		Class* lock = NewClass<>(*this, "Lock", *object);
		Class* lockContentionInfo = NewClass<>(*this, "LockContentionInfo", *object);
		new PrimAttribute(lockContentionInfo, *rootSchema.primAttribute, "maxWaitTime");
		new PrimAttribute(lockContentionInfo, *rootSchema.primAttribute, "totalContentions");
		new PrimAttribute(lockContentionInfo, *rootSchema.primAttribute, "totalWaitTime");
		
		Class* methodCallDesc = NewClass<>(*this, "MethodCallDesc", *object);
		new PrimAttribute(methodCallDesc, *rootSchema.primAttribute, "executionLocation");
		new PrimAttribute(methodCallDesc, *rootSchema.primAttribute, "invocationMode");
		new ImplicitInverseRef(methodCallDesc, *rootSchema.implicitInverseRef, "method");
		new PrimAttribute(methodCallDesc, *rootSchema.primAttribute, "position");
		
		Class* objMethodCallDesc = NewClass<>(*this, "ObjMethodCallDesc", methodCallDesc);
		Class* primMethodCallDesc = NewClass<>(*this, "PrimMethodCallDesc", methodCallDesc);
		Class* multiMediaType = NewClass<>(*this, "MultiMediaType", *object);
		new PrimAttribute(multiMediaType, *rootSchema.primAttribute, "usePresentationFileSystem");
		
		Class* sound = NewClass<>(*this, "Sound", multiMediaType);
		new PrimAttribute(sound, *rootSchema.primAttribute, "name");
		
		Class* node = NewClass<>(*this, "Node", *object);
		Class* notification = NewClass<>(*this, "Notification", *object);
		Class* oleObject = NewClass<>(*this, "OleObject", *object);
		new PrimAttribute(oleObject, *rootSchema.primAttribute, "compressed");
		new ImplicitInverseRef(oleObject, *rootSchema.implicitInverseRef, "oleData");
		new PrimAttribute(oleObject, *rootSchema.primAttribute, "shortName");
		
		Class* printer = NewClass<>(*this, "Printer", *object);
		new PrimAttribute(printer, *rootSchema.primAttribute, "autoPaging");
		new PrimAttribute(printer, *rootSchema.primAttribute, "drawFontBold");
		new PrimAttribute(printer, *rootSchema.primAttribute, "drawFontItalic");
		new PrimAttribute(printer, *rootSchema.primAttribute, "drawFontSize");
		new PrimAttribute(printer, *rootSchema.primAttribute, "drawFontStrikethru");
		new PrimAttribute(printer, *rootSchema.primAttribute, "drawFontUnderline");
		new ImplicitInverseRef(printer, *rootSchema.implicitInverseRef, "footerFrame");
		new ImplicitInverseRef(printer, *rootSchema.implicitInverseRef, "headerFrame");
		new PrimAttribute(printer, *rootSchema.primAttribute, "pageBorderWidth");
		new PrimAttribute(printer, *rootSchema.primAttribute, "pageNumber");
		new PrimAttribute(printer, *rootSchema.primAttribute, "printPreviewAllowPrint");
		new PrimAttribute(printer, *rootSchema.primAttribute, "printPreviewAllowSelect");
		new PrimAttribute(printer, *rootSchema.primAttribute, "printPreviewReduce");
		new PrimAttribute(printer, *rootSchema.primAttribute, "retainCMDValues");
		
		Class* process = NewClass<>(*this, "Process", *object);
		Class* rectangle = NewClass<>(*this, "Rectangle", *object);
		new PrimAttribute(rectangle, *rootSchema.primAttribute, "bottom");
		new PrimAttribute(rectangle, *rootSchema.primAttribute, "left");
		new PrimAttribute(rectangle, *rootSchema.primAttribute, "right");
		new PrimAttribute(rectangle, *rootSchema.primAttribute, "top");
		
		Class* dbServer = NewClass<>(*this, "DbServer", *schemaEntity);
		Class* schemaView = NewClass<>(*this, "SchemaView", *schemaEntity);
		Class* sortActor = NewClass<>(*this, "SortActor", *object);
		new PrimAttribute(sortActor, *rootSchema.primAttribute, "ascending");
		new PrimAttribute(sortActor, *rootSchema.primAttribute, "fieldNo");
		new PrimAttribute(sortActor, *rootSchema.primAttribute, "length");
		new PrimAttribute(sortActor, *rootSchema.primAttribute, "numeric");
		new PrimAttribute(sortActor, *rootSchema.primAttribute, "random");
		new PrimAttribute(sortActor, *rootSchema.primAttribute, "sortType");
		new PrimAttribute(sortActor, *rootSchema.primAttribute, "startPosition");
		
		Class* system = NewClass<>(*this, "System", *object);
		Class* userProfile = NewClass<>(*this, "UserProfile", *object);
		Class* defaultJadeUserProfile = NewClass<>(*this, "DefaultJadeUserProfile", userProfile);
		new ExplicitInverseRef(defaultJadeUserProfile, *rootSchema.explicitInverseRef, "allMyShortCutMappings");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "alwaysUpdatePatchHistory");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "applicationForSchema");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "attributeDefaultAccess");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "baseLocale");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "beepOnError");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "browseOption");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "checkOutNewMethod");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "checkOutOption");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "classDefaultAbstract");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "classDefaultAccess");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "classDefaultTransient");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "classFileSuffix");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "colorGrid");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "confirmOnExit");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "createAccessMethods");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "darkTheme");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "debugOptAnimateSpeed");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "debugOptSaveSettings");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "debugOptions");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "defaultTransientOnly");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "deltaIdMustBeSet");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "dictKeysAreUppercase");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "disablePictures");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "displayHierarchy");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "displayInherited");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "editorAccelKeys");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "editorFontBold");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "editorFontItalic");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "editorFontName");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "editorFontSize");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "editorFontStrikeout");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "editorFontUnderline");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "editorProfile");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "enableDragDrop");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "generateInterfaceStubMethods");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "generateMappingMethod");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "generateMethods");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "graphFontBold");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "graphFontItalic");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "graphFontName");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "graphFontSize");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "graphFontStrikeout");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "graphFontUnderline");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "heightGrid");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "hideAlignmentPalette");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "hideBubbleHelp");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "hideControlPalette");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "hideGrid");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "hideStatusLine");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "hideToolsPalette");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "ignoreCase");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "ignoreWhiteSpace");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "integratedEditor");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "jadeSchemaVersionSkin");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "jadeSkin");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "jadeThemeTemplateCatalog");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "keyMapProfile");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "lightTheme");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "listClasses");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "listMethods");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "listVariables");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "lockRetry");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "methShowProtected");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "methShowPublic");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "methodFileSuffix");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "openSchemas");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "paramFileSuffix");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "propertiesOnTop");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "referenceDefaultAccess");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "retryCount");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "saveBreakpoints");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "saveOpenWindows");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "saveWindowSize");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "schemaEntityViews");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "schemaFileSuffix");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "schemaViewVersion");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showBackdrop");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showBubbleHelp");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showMdiStatusLine");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showProtected");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showPublic");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showReadOnly");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showStartUpScreen");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showStatusLine");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showTipDialog");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showToolbar");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showWarningOnEntityVersion");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "showWarningOnSchemaVersion");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "singleColourEditor");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "snapToGrid");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "special");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "statusListCompiled");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "statusListInError");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "statusListUncompiled");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "suppressionFlags");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "tabStop");
		new ImplicitInverseRef(defaultJadeUserProfile, *rootSchema.implicitInverseRef, "textTemplates");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "versionViewOption");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "viewContext");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "visualView");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "widthGrid");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "windowFontBold");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "windowFontItalic");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "windowFontName");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "windowFontSize");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "windowFontStrikeout");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "windowFontUnderline");
		new PrimAttribute(defaultJadeUserProfile, *rootSchema.primAttribute, "workFileSuffix");
		
		Class* jadeUserProfile = NewClass<>(*this, "JadeUserProfile", defaultJadeUserProfile);
		Class* webSession = NewClass<>(*this, "WebSession", *object);
		new ImplicitInverseRef(webSession, *rootSchema.implicitInverseRef, "_creatingNode");
		new PrimAttribute(webSession, *rootSchema.primAttribute, "lastAccessTime");
		new PrimAttribute(webSession, *rootSchema.primAttribute, "sessionId");
		new PrimAttribute(webSession, *rootSchema.primAttribute, "startTime");
		new PrimAttribute(webSession, *rootSchema.primAttribute, "usePageSequencing");
		
		Class* rootSchemaSession = NewClass<>(*this, "RootSchemaSession", webSession);
		new PrimAttribute(rootSchemaSession, *rootSchema.primAttribute, "allowHiddenControlEvents");
		new PrimAttribute(rootSchemaSession, *rootSchema.primAttribute, "userSecurityLevel");
		
		GUIClass* jade_Microsoft_Internet_Ctrl = NewClass<GUIClass>(*this, "Jade_Microsoft_Internet_Ctrl", *activeXControl);
		GUIClass* jade_WebBrowser = NewClass<GUIClass>(*this, "Jade_WebBrowser", jade_Microsoft_Internet_Ctrl);
		GUIClass* rootFiller = NewClass<GUIClass>(*this, "RootFiller", *label);
		GUIClass* toolbarPicture = NewClass<GUIClass>(*this, "ToolbarPicture", *picture);
		GUIClass* controlAboutBox = NewClass<GUIClass>(*this, "ControlAboutBox", *form);
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "btnOK");
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "control");
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "lblControlClass");
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "lblControlClassP");
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "lblControlType");
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "lblSchema");
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "lblSchemaP");
		new ImplicitInverseRef(controlAboutBox, *rootSchema.implicitInverseRef, "picControlIcon");
		
		GUIClass* jadeBackupDatabaseDialog = NewClass<GUIClass>(*this, "JadeBackupDatabaseDialog", *form);
		GUIClass* jadeIFrameForm = NewClass<GUIClass>(*this, "JadeIFrameForm", *form);
		new ImplicitInverseRef(jadeIFrameForm, *rootSchema.implicitInverseRef, "originalForm");
		new ImplicitInverseRef(jadeIFrameForm, *rootSchema.implicitInverseRef, "pictureControl");
		
		GUIClass* jadeLogicalCertifierSetTranIDDialog = NewClass<GUIClass>(Version(20, 0, 1), *this, "JadeLogicalCertifierSetTranIDDialog", *form);
		new ImplicitInverseRef(jadeLogicalCertifierSetTranIDDialog, *rootSchema.implicitInverseRef, "btnCancel");
		new ImplicitInverseRef(jadeLogicalCertifierSetTranIDDialog, *rootSchema.implicitInverseRef, "btnOK");
		new ImplicitInverseRef(jadeLogicalCertifierSetTranIDDialog, *rootSchema.implicitInverseRef, "grpDivider");
		new ImplicitInverseRef(jadeLogicalCertifierSetTranIDDialog, *rootSchema.implicitInverseRef, "grpTranID");
		new PrimAttribute(jadeLogicalCertifierSetTranIDDialog, *rootSchema.primAttribute, "initialCertifyTranID");
		new ImplicitInverseRef(jadeLogicalCertifierSetTranIDDialog, *rootSchema.implicitInverseRef, "lblCertifylTranID");
		new ImplicitInverseRef(jadeLogicalCertifierSetTranIDDialog, *rootSchema.implicitInverseRef, "picFiller");
		new ImplicitInverseRef(jadeLogicalCertifierSetTranIDDialog, *rootSchema.implicitInverseRef, "txtCertifyTranID");
		
		GUIClass* jadeMsgBoxSuppressible = NewClass<GUIClass>(Version(20, 0, 1), *this, "JadeMsgBoxSuppressible", *form);
		GUIClass* jadePrintPreviewFind = NewClass<GUIClass>(*this, "JadePrintPreviewFind", *form);
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "btnCancel");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "btnFind");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "chkCased");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "chkWholeWords");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "lblFindText");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "lblPageError");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "lblPageNum");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "tbFindText");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "tbPageNum");
		new ImplicitInverseRef(jadePrintPreviewFind, *rootSchema.implicitInverseRef, "v_printPreview");
		
		GUIClass* jadeSkinMaint = NewClass<GUIClass>(*this, "JadeSkinMaint", *form);
		GUIClass* jadeSkinMaintenance = NewClass<GUIClass>(*this, "JadeSkinMaintenance", *form);
		GUIClass* jadeSkinSelect = NewClass<GUIClass>(*this, "JadeSkinSelect", *form);
		GUIClass* jadeSkinSelection = NewClass<GUIClass>(*this, "JadeSkinSelection", *form);
		GUIClass* jadeTestDialog = NewClass<GUIClass>(*this, "JadeTestDialog", *form);
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "btnClose");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "btnRun");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "currentMethodCount");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "currentMethodName");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "drawXMax");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "drawXMin");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "drawY1");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "drawY2");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "exceptionsCount");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "failedAssertsCount");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "failureCount");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lblActivity");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lblResults");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lblSelectTests");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lineActivity");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lineResizeV_UP");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lineResults");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lineSelect");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "lstClasses");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "lstDefaultColor");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "lstFailedColor");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "lstPassedColor");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "methodCount");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuAssertDebug");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuCodeCoverage");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuCodeCoverageReport");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuCodeCoverageView");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuCopyToClipboard");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuDebugException");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuDebugUnexpectedException");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuExit");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuFile");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuFontBigger");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuFontSmaller");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuIncludePassed");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuPopupResults");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuPopupTests");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuProfile");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuProfileReport");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuRefresh");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuRunSeletedTests");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSelectFail");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSelectNotRun");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSelectPassed");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSelectSkipped");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSeparator1");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSeparator2");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSeparator3");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSeparator4");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSeparator5");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuSeperator6");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "mnuView");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "mouseMileage");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "passedAssertsCount");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "passedCount");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "picBackColor");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picCollapseAll");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picCollapseAll_dark");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picCollapseAll_light");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picExpandAll");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picExpandAll_dark");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picExpandAll_light");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picFailed");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picNotRun");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picPassed");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "picSkipped");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "plinthLeft");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "plinthRight");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "prgResult");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "refreshTime");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "resizeOption");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "resizerColor");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "runCount");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "skipCount");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "startTime");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "tblProgress");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "tblResults");
		new ImplicitInverseRef(jadeTestDialog, *rootSchema.implicitInverseRef, "testEntity");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "testsAborted");
		new PrimAttribute(jadeTestDialog, *rootSchema.primAttribute, "uiFontSize");
		
		GUIClass* jadeWebServiceShowStats = NewClass<GUIClass>(*this, "JadeWebServiceShowStats", *form);
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "currentStatsObject");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "doc");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "fldWSStats");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "jrtRequest");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "jrtResponse");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "lblRequest");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "lblResponse");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "shtMsgs");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "shtStats");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "slobs");
		new ImplicitInverseRef(jadeWebServiceShowStats, *rootSchema.implicitInverseRef, "tblStats");
		
		
		// Interfaces
		JadeInterface* jadeDbAdminNotificationIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeDbAdminNotificationIF");
		new JadeInterfaceMethod(jadeDbAdminNotificationIF, *rootSchema.jadeInterfaceMethod, "userNotification");
		
		JadeInterface* jadeGenericMessagingIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeGenericMessagingIF");
		new JadeInterfaceMethod(jadeGenericMessagingIF, *rootSchema.jadeInterfaceMethod, "managementEvent");
		new JadeInterfaceMethod(jadeGenericMessagingIF, *rootSchema.jadeInterfaceMethod, "messageArrivedEvent");
		
		JadeInterface* jadeIterableIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeIterableIF");
		new JadeInterfaceMethod(jadeIterableIF, *rootSchema.jadeInterfaceMethod, "createIterator");
		
		JadeInterface* jadeIteratorIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeIteratorIF");
		new JadeInterfaceMethod(jadeIteratorIF, *rootSchema.jadeInterfaceMethod, "current");
		new JadeInterfaceMethod(jadeIteratorIF, *rootSchema.jadeInterfaceMethod, "next");
		
		JadeInterface* jadeMultiWorkerTcpTransportIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeMultiWorkerTcpTransportIF");
		new JadeInterfaceMethod(jadeMultiWorkerTcpTransportIF, *rootSchema.jadeInterfaceMethod, "closedEvent");
		new JadeInterfaceMethod(jadeMultiWorkerTcpTransportIF, *rootSchema.jadeInterfaceMethod, "connectionEvent");
		new JadeInterfaceMethod(jadeMultiWorkerTcpTransportIF, *rootSchema.jadeInterfaceMethod, "managementEvent");
		new JadeInterfaceMethod(jadeMultiWorkerTcpTransportIF, *rootSchema.jadeInterfaceMethod, "openedEvent");
		new JadeInterfaceMethod(jadeMultiWorkerTcpTransportIF, *rootSchema.jadeInterfaceMethod, "readReadyEvent");
		new JadeInterfaceMethod(jadeMultiWorkerTcpTransportIF, *rootSchema.jadeInterfaceMethod, "userEvent");
		
		JadeInterface* jadePaginationEnvelopeIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadePaginationEnvelopeIF");
		new JadeInterfaceMethod(jadePaginationEnvelopeIF, *rootSchema.jadeInterfaceMethod, "getNextPage");
		
		JadeInterface* jadeRelationalAttributeIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeRelationalAttributeIF");
		new JadeInterfaceMethod(jadeRelationalAttributeIF, *rootSchema.jadeInterfaceMethod, "getJadeType");
		new JadeInterfaceMethod(jadeRelationalAttributeIF, *rootSchema.jadeInterfaceMethod, "getLength");
		new JadeInterfaceMethod(jadeRelationalAttributeIF, *rootSchema.jadeInterfaceMethod, "getSQLName");
		new JadeInterfaceMethod(jadeRelationalAttributeIF, *rootSchema.jadeInterfaceMethod, "getScaleFactor");
		
		JadeInterface* jadeRelationalEntityIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeRelationalEntityIF");
		new JadeInterfaceMethod(jadeRelationalEntityIF, *rootSchema.jadeInterfaceMethod, "allInstances");
		new JadeInterfaceMethod(jadeRelationalEntityIF, *rootSchema.jadeInterfaceMethod, "callIFAllInstances");
		new JadeInterfaceMethod(jadeRelationalEntityIF, *rootSchema.jadeInterfaceMethod, "getJadeClass");
		new JadeInterfaceMethod(jadeRelationalEntityIF, *rootSchema.jadeInterfaceMethod, "getPropertyValue");
		new JadeInterfaceMethod(jadeRelationalEntityIF, *rootSchema.jadeInterfaceMethod, "getQueryProvider");
		new JadeInterfaceMethod(jadeRelationalEntityIF, *rootSchema.jadeInterfaceMethod, "getSQLName");
		new JadeInterfaceMethod(jadeRelationalEntityIF, *rootSchema.jadeInterfaceMethod, "isAttributeValid");
		
		JadeInterface* jadeRelationalQueryProviderIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeRelationalQueryProviderIF");
		new JadeInterfaceMethod(jadeRelationalQueryProviderIF, *rootSchema.jadeInterfaceMethod, "binaryExpression");
		new JadeInterfaceMethod(jadeRelationalQueryProviderIF, *rootSchema.jadeInterfaceMethod, "executeQuery");
		new JadeInterfaceMethod(jadeRelationalQueryProviderIF, *rootSchema.jadeInterfaceMethod, "finalizeQuery");
		new JadeInterfaceMethod(jadeRelationalQueryProviderIF, *rootSchema.jadeInterfaceMethod, "getResultSet");
		new JadeInterfaceMethod(jadeRelationalQueryProviderIF, *rootSchema.jadeInterfaceMethod, "unaryExpression");
		
		JadeInterface* jadeReverseIterableIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeReverseIterableIF");
		new JadeInterfaceMethod(jadeReverseIterableIF, *rootSchema.jadeInterfaceMethod, "createReversibleIterator");
		
		JadeInterface* jadeReversibleIteratorIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeReversibleIteratorIF");
		new JadeInterfaceMethod(jadeReversibleIteratorIF, *rootSchema.jadeInterfaceMethod, "back");
		
		JadeInterface* jadeRpsDataPumpIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeRpsDataPumpIF");
		new JadeInterfaceMethod(jadeRpsDataPumpIF, *rootSchema.jadeInterfaceMethod, "updateCallback");
		
		JadeInterface* jadeRpsNotificationIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeRpsNotificationIF");
		new JadeInterfaceMethod(jadeRpsNotificationIF, *rootSchema.jadeInterfaceMethod, "userNotification");
		
		JadeInterface* jadeTestListenerIF = new JadeInterface(*this, *rootSchema.jadeInterface, "JadeTestListenerIF");
		new JadeInterfaceMethod(jadeTestListenerIF, *rootSchema.jadeInterfaceMethod, "finish");
		new JadeInterfaceMethod(jadeTestListenerIF, *rootSchema.jadeInterfaceMethod, "message");
		new JadeInterfaceMethod(jadeTestListenerIF, *rootSchema.jadeInterfaceMethod, "methodSuccess");
		new JadeInterfaceMethod(jadeTestListenerIF, *rootSchema.jadeInterfaceMethod, "start");
		new JadeInterfaceMethod(jadeTestListenerIF, *rootSchema.jadeInterfaceMethod, "testFailure");
		new JadeInterfaceMethod(jadeTestListenerIF, *rootSchema.jadeInterfaceMethod, "testSkipped");
		new JadeInterfaceMethod(jadeTestListenerIF, *rootSchema.jadeInterfaceMethod, "testSuccess");
		
		// Global Constants
		new GlobalConstant(*this, *rootSchema.globalConstant, "Any_System_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Any_User_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Azure");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Black");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Blue");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Busy");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Compiler_Config_Updated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Cr");
		new GlobalConstant(*this, *rootSchema.globalConstant, "CrLf");
		new GlobalConstant(*this, *rootSchema.globalConstant, "CurrentLocation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "DarkBlue");
		new GlobalConstant(*this, *rootSchema.globalConstant, "DarkGray");
		new GlobalConstant(*this, *rootSchema.globalConstant, "DatabaseServer");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Db_File_Not_Found");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Db_User_Aborted_Operation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "EnableNewLoadControl");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Ex_Abort_Action");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Ex_Continue");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Ex_Pass_Back");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Ex_Resume_Method_Epilog");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Ex_Resume_Next");
		new GlobalConstant(*this, *rootSchema.globalConstant, "ExcludeFromTransientLeakReport");
		new GlobalConstant(*this, *rootSchema.globalConstant, "ExcludeFromUnusedParameterReport");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Exclusive_Lock");
		new GlobalConstant(*this, *rootSchema.globalConstant, "FileVolatility_Frozen");
		new GlobalConstant(*this, *rootSchema.globalConstant, "FileVolatility_Stable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "FileVolatility_Transparent");
		new GlobalConstant(*this, *rootSchema.globalConstant, "FileVolatility_Volatile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Get_Lock");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Gray");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Green");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Idle");
		new GlobalConstant(*this, *rootSchema.globalConstant, "IsUnicodeSystem");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JAA_MemberKeyDictionaryEntryName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JAA_MemberKeyDictionaryEntryType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_CREATE_WS_APP_FAILED");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_DECIMAL_OVERFLOW");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_ENUM_FAULT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_INTEGER_OVERFLOW");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_INVALID_REQUEST");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_INVALID_RESPONSE");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_LICENCES_EXCEEDED");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_NO_WEBSERVICE_CLASS");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_NO_WEBSERVICE_METHOD");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_RESPONSE_TIME_EXCEEDED");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_SERVICE_FAULT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_SERVICE_UNAVAILABLE");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_SESSION_ENDED");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_SESSION_TIMED_OUT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_STRING_TOO_LONG");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_VERSION_MISMATCH");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JADEWS_WSDL_GENERATION_FAILED");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Connection_Max");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Connection_Min");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_DTNC_Max");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_DTNC_Min");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Development_Max");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Development_Min");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_File_Max");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_File_Min");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Interp_Max");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Interp_Min");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_JadeEditMask_Max");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_JadeEditMask_Min");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Runtime_Max");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_Runtime_Min");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_TextBox_Max_Decimals");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_TextBox_Max_Numeric");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_TextBox_Min_Decimals");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErrRange_TextBox_Min_Numeric");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_AccessModeViolation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_AlreadyInTransTranState");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_AlreadyInTransactionState");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ApplictnLicensesExceeded");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_AssocBufLeaderNotLocked");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_AttemptToInvokeAbstractMethod");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Attribute_Name_Conflict");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_AutomaticLockIgnored");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Base64DecodeFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_BatchExtractInvalidParameterFileReference");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_BinaryTooLong");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_BinaryWriteOnTextFile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ByteOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotChangeName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotEstablishInverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotInheritMethod");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotModifySystemObject");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotPartitionAllInstancesCollection");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotPartitionCollclass");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotPartitionDefaultMapFile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotSetSubobjectRef");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotSetSubobjectReference");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CannotUpdateLatestVersion");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassAccessFrequenciesDis");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassCannotPersist");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassDefinitionIncomplete");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassInUse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassIsStillReferenced");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassNeedsReorg");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ClassStillHasSubclass");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CollBlockStructureError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CollEntryNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CollInvalidIndex");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CollInvalidType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CollNotAttached");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CollectionNotEmpty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ColumnName_Cannot_Change");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Column_Not_Found");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CompInvalidKeyProperty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CompVersioningError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CompressFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConnectionBindFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConnectionClosed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConnectionNotTransient");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConnectionTimedOut");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstantNeedsCompile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstructorAlreadyCalled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstructorDoesNotHaveParameters");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstructorHasParameters");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstructorNotCalled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstructorNotExported");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstructorParametersNotAllowed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ConstructorTargetUsage");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ContentionStatsActive");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_CreateNotAllowed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DataPumpAlreadyRunning");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DatabaseIsUnavailable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbCertifyError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbDiskFull");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbEditionOutOfDate");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbFileExists");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbFileNotCreated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbFileNotDefined");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbFileNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbFileOffline");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbIllegalSysFileUpdate");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbInvalidMethod");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbLockedForArchive");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbLockedForReorg");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbNoFileControlRec");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbOperationInvalidMode");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbPartitionModulusRangeError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbPartitionRangeError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbQuietPointTimeOut");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbSizeMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbUserAbort");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DbuOperationFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Deadlock");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DecimalConversionError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DecimalOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DecimalOverflowCont");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DeleteChildrenNotComplete");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DevAssertFailure");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DevCallToObsoleteMethod");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DevPreconditionViolation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DevUndefConsoleCallback");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DevUnhandledCodePath");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DevUserAbort");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DictInvalidKeyOrdinal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DictKeysTooLong");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DirectoryNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DivideByZero");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicateTimer");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicatedArrayInverseVal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicatedBtreeValue");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicatedIdentifier");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicatedKey");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicatedObject");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicatedRelationshipReference");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DuplicatedUserId");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictDefinitionClosed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictDefnIncomplete");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictKeyChanged");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictKeyLengthRequired");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictKeysRequired");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictMemberTypeUnknown");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictNotEmpty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictReqsExternalKeys");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaDictReqsMemberKeys");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaObjInvalidState");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaObjPropNameConflict");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynaObjPropertyOutOfRange");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynamicPropertyConversion");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynamicPropertyConversionError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_DynamicPropertyConversionInvalid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_EnvObjOperationOutOfScope");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_EstablishedInverseBroken");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ExceptionHandlerInvalidReturnCode");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ExceptionHandlersStackOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ExpectingTStringName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ExternalDatabaseOpenFail");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FeatureNotAvailable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Feature_Deprecated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileCreateDirectoryFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileDirectoryInvalid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileInstantiated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileInvalidFilename");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileOpenFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileOutOfDisk");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FilePathNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileReadTooBig");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileRenameFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileRenameToDiffrntDevice");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_FileWriteIncomplete");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_GetInstallDirFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_HardcodedScmClassUpdate");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_HtmlEscDecodeFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IllegalConversion");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IllegalDecimalConversion");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IllegalOperation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IncompatibleDeferredUpdate");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IncompatibleParameterType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IncompatibleRetrievedValueType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InstancesExist");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Integer64Overflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IntegerOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InternalDataPacketInconsi");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InterpDebuggerAttached");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InterpInvalidExtParam");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InterpMethodNailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InterpOutOfResource");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InterpStringBoundsError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InterpUserInterrupt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IntlAnsiToUnicodeFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IntlIllegalUTF8ByteSeqnce");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IntlUnicodeCodePoint");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IntlUnicodeToAnsiFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidApplication");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidArgument");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidClassForProperty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidClassNumber");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidDate");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidExceptionClass");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidExceptionCode");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidExceptionInstance");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidMessage");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidParameterType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidParameterValue");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidProperty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidPropertyType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidSchema");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidTimeArg");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidTimeZone");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidTransientClassProperty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_InvalidUpdateVersion");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Invalid_For_RpsMapping");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Invalid_Option");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_IsCurrentTransactionTraceObject");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeActxCreateB4Access");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeActxInvokeException");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeAutoInvalidEventClass");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeAutoInvalidParameter");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeCaseVersusNumeric");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeConvertingPicture");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeEditMaskEmptyField");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeFeatureNotAvailable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeFileOpenFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeFormWasUnloaded");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeGit_Certificate_Error");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeInvalidPropertyValue");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeListTooMany");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeMethodNeedsWindow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeMethodWrongSignature");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeNoSuchEvent");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeNoSuchMethod");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeNotPictureFile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeOleCallFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadePropertyNeedsWindow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JadeTableInvalidRow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JomDecimalOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JomIntegerOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JomInternalError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_JomMethodAborted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_LegalOnRpsOnly");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_LoadFormBuildData");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Load_FormsFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Load_FormsFailedWithInfo");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Load_MethodsInError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Load_NoAppOrGlobal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Load_NoDefaultMapFile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Load_NotAvailable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Load_ReorgRequired");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_LocalExceptionHandlersStackOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_LockCannotBeContinued");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_LogicalCertifyErrors");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MemoryAddressNodeInvalid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodCheckedOutToAnother");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodMockDuplicated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodMockMultipleSchemas");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodMockNotDeleted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodMockNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodMockingNotEnabled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodMockingNotSystemOnly");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodNotCompiled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MethodsInError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_DataSizeTooBig");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_InvalidPriorityTy");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_InvalidRequest");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_InvalidRequestGro");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_InvalidResponseTy");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_ServerNodeOnly");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_TargetNodeNoBgThr");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtMsg_TargetNodeNotFoun");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MgmtReplyTruncated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ModifiedCurrentVersionObj");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MultiLineCommentNotTerminated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_MustCallSuperclassConstructor");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NestedExceptionsLimitExceeded");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NoMemoryForBuffers");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_No_Jade_Type");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NonStaticTString");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NotDataPumpApp");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NotInTransactionState");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NotInTransientTransactionState");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NotValidInConstructorOrDestructor");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Not_Soft_Table");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NoteDoesNotExist");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NotifSubscriberNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NullDate");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_NullObjectReference");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_OK");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectDeleted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectIsDirty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectLocked");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectLockedLocally");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectNotAvailable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectNotPresent");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ObjectUnlocked");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_OffLineFileAccess");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_OperationProhibited");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_OperatorIncompatibleWithMethodType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_OwnerObjSubobjectLocked");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ParamNumberMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ParameterNumberMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_PartitionModulusRangeErr");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_PersistentReference");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RealOverflow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReferenceInverseMissing");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RemoteExecutionAborted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RemoteMgmtRequestFailure");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReorgInProgress");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReorgRestartDataExists");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReorgRpsPrimaryExtract");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReorgSchemaCompileInProgr");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReorgUnversionFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReorgUnversionFailed2");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ReorgUserCancelled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsAdminHalt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsConnectionError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsDuplicatedKey");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsExtractRequestError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsJournalIDMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsMappingMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsMultiRowAffected");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsRelationalDbError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsTableNameNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_RpsZeroRowsAffected");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SQL_Type_Not_Mapped");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SameNameAsFormat");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsIllegalOnPrimary");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsIllegalOnSecondary");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsIncompleteJournal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsInvalidCommand");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsMaxSecondariesExceeded");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsMissingName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsNotInitialized");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsResponseTimeout");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsSecondaryNotAttached");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsSecondaryNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SdsTrackerBusy");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SecurityInvalidId");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ServerLicensesExceeded");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SharedTransientReference");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SlobOrBlobZeroSubId");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_StartAppLockError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_StartTimerFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_StringBoundsError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_StringDelimiterMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_StringTooLong");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_StringUtf8TooLong");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SubobjectOwnerObjLocked");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SuperclassConstructorDoesNotHaveParameters");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SuperclassConstructorHasParameters");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SuperclassCreateOnlyInConstructor");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SystemClassDeleteNotAllowed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_SystemFileDelete");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Table_Name_Conflict");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Table_Not_Found");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TimeZoneNotFound");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TooManyClassMaps");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TooManyParameters");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TooManySubobjects");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TransactionIntegrityViolation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TransactionMustBeAborted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TransactionTraceAlreadyStarted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TransactionTraceNotStarted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TransientClassCannotBeChanged");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TransientClassNonSharedTransientOnly");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TypeGuardFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_TypeMethodCannotUseSelf");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UncompressFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UnitTest_UnableToRun");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UnitTest_UnitTestFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UnknownPropertyOrMethod");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UnknownSystemFile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UnknownTransStrOrFormat");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UnversionLockError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpdateOutsideTransaction");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpdateOutsideTransientTrx");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpdateToAutomaticVar");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpgradeCheckedOutMethods");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpgradeGeneral");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpgradeMethodsInError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpgradeMethodsNoSource");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UpgradeValidateFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_UserCancelled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_ValidOnRpsMappingOnly");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_VersionMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_WbemQueryError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_WebSocketProtocolError");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JErr_Web_Invalid_Sequence");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JInfo_DeprecatedFeature");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_ArrayName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_ArrayType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_DictionaryName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_DictionaryType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_JadeBytesName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_JadeBytesType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_SetName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JStats_SetType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_ExpressionResultUnused");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_NotAllPathsReturnAValue");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_RedundantTypeGuard");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_ReferencingDeprecatedFeature");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_ReferencingUnpublishedFeature");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_ReferencingUnpublishedType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_SameNameAsClassProperty");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_SameNameAsType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_SameNameAsTypeConstant");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_SameNameGlobalConstant");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_UnreachableCodeAfterReturn");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_UnreferencedConstant");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_UnreferencedParameter");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JWarning_UnreferencedVariable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_0");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_1");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_2");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_3");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_4");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_5");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_6");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_7");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_8");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_9");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_A");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Ampersand");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Asterisk");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_AtSign");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_B");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Back");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_BackSlash");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Bar");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_C");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Carat");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Colon");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Comma");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Ctrl");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_CurlyLeft");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_CurlyRight");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_D");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Delete");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Dollar");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_DoubleQuote");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_DownArrow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_E");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_End");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Enter");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Equal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Escape");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Exclamation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F1");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F10");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F11");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F12");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F2");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F3");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F4");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F5");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F6");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F7");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F8");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_F9");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_G");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_GreaterThan");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_H");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Hash");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Home");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Hyphen");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_I");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Insert");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_J");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_K");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_L");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_LeftArrow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_LeftBracket");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_LeftParenthesis");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_LeftQuote");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_LessThan");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Linefeed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_M");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_N");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_NumPadDivide");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_NumPadMinus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_NumPadMultiply");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_NumPadPlus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_O");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_P");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_PageDown");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_PageUp");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Percent");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Plus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Q");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Question");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_R");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Return");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_RightArrow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_RightBracket");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_RightParenthesis");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_S");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_SemiColon");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Shift");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_SingleQuote");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Slash");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Space");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Stop");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_T");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Tab");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Tilde");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_U");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_UnderScore");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_UpArrow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_V");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_W");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_X");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Y");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_Z");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_a");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_b");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_c");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_d");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_e");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_f");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_g");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_h");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_i");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_j");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_k");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_l");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_m");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_n");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_o");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_p");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_q");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_r");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_s");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_t");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_u");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_v");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_w");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_x");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_y");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_key_z");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_with_Alt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_with_Ctrl");
		new GlobalConstant(*this, *rootSchema.globalConstant, "J_with_Shift");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeBatchExtractAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeBatchUnitTestExtractName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeEnableIncrementalLogicalCertify");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeInsertSchemaAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeLoaderNonGuiAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeLogicalCertifierNonGuiAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeMonitorAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeMonitorSchemaName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeReorgAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeSchemaLoaderName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeSchemaName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeTestJadeMonitorSchemaName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeTestJadeSchemaName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeTestJadeToolsSchemaName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeTestRootSchemaName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeUnitTestAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeUnitTestRunAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeUnitTestRunGuiNoFormsAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeWorkspaceAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "JadeWorkspaceNonGuiAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Jerr_DeleteChildrenNotComplete");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Jerr_TableInvalidRows");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Arabic_Bahrain");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Arabic_Egypt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Arabic_Kuwait");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Arabic_SaudiArabia");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Arabic_UAE");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Assamese_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Bengali_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Chinese_SimplfdSingapore");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Chinese_SimplifiedPRC");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Chinese_TraditionalMacao");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Chinese_TraditionalTaiwan");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Chinese_TraditnalHongKong");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Dutch_Belgium");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Dutch_Netherlands");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_Australia");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_Canada");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_Ireland");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_Jamaica");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_Malaysia");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_NewZealand");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_Singapore");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_SouthAfrica");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_UnitedKingdom");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_English_UnitedStates");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_French_Belgium");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_French_Canada");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_French_France");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_French_Switzerland");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_German_Austria");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_German_Germany");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_German_Switzerland");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Greek_Greece");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Gujarati_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Hindi_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Indonesian_Indonesia");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Invariant");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Irish_Ireland");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Italian_Italy");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Japanese_Japan");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Kannada_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Konkani_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Korean_Korea");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Malay_Malaysia");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Malayalam_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Maori_NewZealand");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Marathi_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Oriya_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Polish_Poland");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Portuguese_Brazil");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Portuguese_Portugal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Punjabi_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Russian_Russia");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Sanskrit_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_SessionWithOverrides");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_Argentina");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_Chile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_Mexico");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_Nicaragua");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_PuertoRico");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_Spain_InternatSrt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_Spain_TradSort");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Spanish_UnitedStates");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Tamil_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Telugu_India");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Thai_Thailand");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Vietnamese_Vietnam");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LCID_Welsh_UnitedKingdom");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Lf");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LightGreen");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LightYellow");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LockTimeout_Immediate");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LockTimeout_Infinite");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LockTimeout_Process_Defined");
		new GlobalConstant(*this, *rootSchema.globalConstant, "LockTimeout_Server_Defined");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Mauve");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MaxUserRemapClassNum");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Byte");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Decimal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Identifier_Length");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Integer");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Integer64");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Method_Parameters");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Partition_Id");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Partition_Modulus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_Real");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Max_UnboundedLength");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MaximumCollectionBlockSize");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MaximumCollectionDisplay");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MinUserRemapClassNum");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Min_Byte");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Min_Decimal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Min_Integer");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Min_Integer64");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Min_Partition_Id");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Min_Partition_Modulus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Min_Real");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Cancel_Five");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Cancel_Four");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Cancel_None");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Cancel_One");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Cancel_Three");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Cancel_Two");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Default_Fifth");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Default_First");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Default_Fourth");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Default_Second");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Default_Third");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Icon_Exclamation_Mark");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Icon_Information");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Icon_Question_Mark");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Icon_Stop");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Return_Five");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Return_Four");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Return_One");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Return_Three");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBoxCustom_Return_Two");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Abort_Retry_Ignore");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_App_Modal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Default_First");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Default_Second");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Default_Third");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Exclamation_Mark_Icon");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Information_Icon");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_OK_Cancel");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_OK_Only");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Question_Mark_Icon");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Retry_Cancel");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Return_Abort");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Return_Cancel");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Return_Ignore");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Return_No");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Return_OK");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Return_Retry");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Return_Yes");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Stop_Icon");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_System_Modal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Yes_No");
		new GlobalConstant(*this, *rootSchema.globalConstant, "MsgBox_Yes_No_Cancel");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Object_Create_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Object_Delete_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Object_Update_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Patch_History_Load_Dup_Patch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Patch_History_Load_No_Schema");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Persistent_Duration");
		new GlobalConstant(*this, *rootSchema.globalConstant, "PresentationClient");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_10X11");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_10X14");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_11X17");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_15X11");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_9X11");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A2");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A3");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A3_Extra");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A3_Extra_Transverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A3_Transverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A4");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A4Small");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A4_Extra");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A4_Plus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A4_Transverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A5");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A5_Extra");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A5_Transverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_A_Plus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_B4");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_B5");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_B5_Extra");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_B5_Transverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_B_Plus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_CSheet");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Cancelled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Collate_Ignored");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Copies_Ignored");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Currently_Open");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Custom_Paper");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_DSheet");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_DocumentType_Invalid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Duplex_Ignored");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Duplex_Invalid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_ESheet");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_10");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_11");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_12");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_14");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_9");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_B4");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_B5");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_B6");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_C3");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_C4");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_C5");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_C6");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_C65");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_DL");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_Invite");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_Italy");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_Monarch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Env_Personal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Executive");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Failed_To_Obtain_Printer");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Fanfold_Lgl_German");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Fanfold_Std_German");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Fanfold_US");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Folio");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Frame_Too_Large");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Header_Footer_Too_Large");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_ISO_B4");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_In_Use_By_Preview");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Invalid_Control");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Invalid_Position");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Japanese_PostCard");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Landscape");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Ledger");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Legal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Legal_Extra");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Letter");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_LetterSmall");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Letter_Extra");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Letter_Extra_Transverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Letter_Plus");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Letter_Transverse");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_NewPage_Failed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_NoDefault_Printer");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Not_Available");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Note");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Orientation_Invalid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_PaperSource_Invalid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Portrait");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Preview_Ignored");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_PrintReport_Ignored");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Printer_Ignored");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Printer_Not_Open");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Printer_Open_Failed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Quarto");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Restricted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Statement");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Stopped");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Successful");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Tabloid");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Tabloid_Extra");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_TextOut_Error");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Print_Unformatted_failed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_Call_Stack_Info_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_Local_Stats_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_Method_Cache_Stats_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_Remote_Stats_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_TDB_Analysis_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_TDB_Info_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_Translate_ClsNo_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Process_Web_Stats_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "ProfileAllKeys");
		new GlobalConstant(*this, *rootSchema.globalConstant, "ProfileAllSections");
		new GlobalConstant(*this, *rootSchema.globalConstant, "ProfileRemoveKey");
		new GlobalConstant(*this, *rootSchema.globalConstant, "ProfileRemoveSection");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Purple");
		new GlobalConstant(*this, *rootSchema.globalConstant, "RPS_HaltAutoScript");
		new GlobalConstant(*this, *rootSchema.globalConstant, "RPS_HaltManualScript");
		new GlobalConstant(*this, *rootSchema.globalConstant, "RPS_HaltMappingDeleted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "RPS_HaltNoScript");
		new GlobalConstant(*this, *rootSchema.globalConstant, "RPS_SchemaTransition");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Red");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Reserve_Lock");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Response_Cancel");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Response_Continuous");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Response_Suspend");
		new GlobalConstant(*this, *rootSchema.globalConstant, "RootSchemaName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_AuditStopTrackingAll");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_AuditStopTrackingNative");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_AuditStopTrackingRdb");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_BlockWrite");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_Connected");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_Connecting");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ConnectionFailed");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ConnectionStateChange");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ConnectionStateUndefined");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_Disconnected");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_HostileTakeoverInitiated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_JournalSwitch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_JournalTransferStopped");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_PrimaryName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_PrimaryRoleActive");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_PrimaryRoleRelinquished");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_PrimaryType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonAdminAudited");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonAdminDirect");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonAutoUpgradeMismatch");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonDeltaModeEntered");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonEnablingDbCrypt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonErrorHalt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonRestart");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonRpsAdminHalt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonRpsReorgHalt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonRpsRestart");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonRpsSnapshot");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonTakeover");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReasonTransition");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_RelinquishPrimaryRole");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_RelinquishSecondaryRole");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReorgStateNotReorging");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReorgStateOfflinePhase");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReorgStateReorgingFiles");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReorgStateRestarting");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReorgStateSeekingApproval");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_ReorgStateStarting");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_RoleChangeEvent");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_RoleChangeProgress");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_RolePrimary");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_RoleSecondary");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_RoleUndefined");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SecondaryName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SecondaryProxyName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SecondaryProxyType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SecondaryRoleActive");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SecondaryRoleRelinquished");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SecondaryType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_StateCatchingUp");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_StateDisconnected");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_StateReorging");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_StateSynchronized");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_StateTrackingHalted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_StateTransferHalted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SubroleNative");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_SubroleRelational");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TakeoverAbandoned");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TakeoverConditional");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TakeoverFailure");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TakeoverForced");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TakeoverInitiated");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TrackingDisabled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TrackingEnabled");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TrackingHalted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TrackingStarted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TrackingStopped");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranDeferred");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranInDoubt");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranInterrupted");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranNormal");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranPrepareToCommit");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranReadyToAbort");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranReadyToCommit");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TranWaitingAuditCommit");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TransactionName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_TransactionType");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_WaitForQuietPoint");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SDS_WaitForTakeoverDisposition");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_COLLECTION");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_COLLECTION_METHOD");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_EXPLICIT_INVERSE");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_IMPLICIT_INVERSE");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_INVALID");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_METHOD");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_PROPERTY");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_BIGINT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_BINARY");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_BIT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_CHAR");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_DATE");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_DATE_VERSION3");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_DECIMAL");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_DOUBLE");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_FLOAT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_INTEGER");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_INTERVAL_DAY_TO_SEC");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_LONGVARBINARY");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_LONGVARCHAR");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_NULL");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_NUMERIC");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_OID");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_REAL");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_SMALLINT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_TIME");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_TIMESTAMP");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_TIMESTAMP_VERSION3");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_TIME_VERSION3");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_TINYINT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_VARBINARY");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_VARCHAR");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_WCHAR");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_WLONGVARCHAR");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_TYPE_WVARCHAR");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SQL_XKEYDICT");
		new GlobalConstant(*this, *rootSchema.globalConstant, "SchemaInspectorAppName");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Session_Duration");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Share_Lock");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Snd_Asterisk");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Snd_Beep");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Snd_Default");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Snd_Exclamation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Snd_Hand");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Snd_Question");
		new GlobalConstant(*this, *rootSchema.globalConstant, "System_Base_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Tab");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Timer_Continuous");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Timer_OneShot");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Transaction_Duration");
		new GlobalConstant(*this, *rootSchema.globalConstant, "URL_JadePlatform");
		new GlobalConstant(*this, *rootSchema.globalConstant, "URL_JadeWorld");
		new GlobalConstant(*this, *rootSchema.globalConstant, "URL_NewsGroups");
		new GlobalConstant(*this, *rootSchema.globalConstant, "URL_OnlineDocumentation");
		new GlobalConstant(*this, *rootSchema.globalConstant, "URL_ResourceLibrary");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Update_Lock");
		new GlobalConstant(*this, *rootSchema.globalConstant, "UserRemapOffset");
		new GlobalConstant(*this, *rootSchema.globalConstant, "User_Base_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "User_Max_Event");
		new GlobalConstant(*this, *rootSchema.globalConstant, "VariantDce");
		new GlobalConstant(*this, *rootSchema.globalConstant, "VariantMicrosoft");
		new GlobalConstant(*this, *rootSchema.globalConstant, "VariantNcs");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Volatility_Frozen");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Volatility_Stable");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Volatility_Volatile");
		new GlobalConstant(*this, *rootSchema.globalConstant, "White");
		new GlobalConstant(*this, *rootSchema.globalConstant, "Yellow");
	}
}
