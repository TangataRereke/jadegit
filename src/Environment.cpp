#include "Environment.h"
#include <Windows.h>

using namespace std;

namespace JadeGit
{
	filesystem::path getExecutablePath() 
	{
		basic_string<TCHAR> result(MAX_PATH, 0);
		while (true)
		{
			auto len = GetModuleFileName(nullptr, &result[0], result.length());

			if (len == 0)
				return basic_string<TCHAR>();

			if (len < result.length() - 1)
			{
				result.resize(len);
				result.shrink_to_fit();
				return result;
			}

			result.resize(result.length() * 2);
		}
	}

	filesystem::path getHomeDirectory()
	{
		// Get parent path of exe
		auto bin = getExecutablePath().parent_path();

		// Treat parent folder as home (assumes binaries are located in a bin folder)
		if (bin.has_parent_path())
			return bin.parent_path();

		// Fallback to using same folder as exe
		return bin;
	}

	filesystem::path makeTempDirectory()
	{
		// Define temp folder relative to home directory
		auto path = getHomeDirectory() / "temp" / ".jadegit";

		// Ensure directory exists
		if (filesystem::create_directories(path))
		{
			// Hide .jadegit on creation
			SetFileAttributesW(path.wstring().c_str(), FILE_ATTRIBUTE_HIDDEN);
		}

		return path;
	}
}