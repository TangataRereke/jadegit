#pragma once
#include "Session.h"

namespace JadeGit::Development
{
	class Command : protected ICommand
	{
	public:
		Command(Session& session, NoteEventType eventType) : session(session), eventType(eventType) {}

		bool Execute();

	private:
		Session& session;
		const NoteEventType eventType;
	};

	class SetupSchemaCommand : public Command
	{
	public:
		SetupSchemaCommand(Session& session, const std::string& schemaName);

	protected:
		void GetInfo(DskParam& userInfo) const override;
		bool ProcessReply(DskParam& params) override;

		const Jade::String schemaName;
	};
}