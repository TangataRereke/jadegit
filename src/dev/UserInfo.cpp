#include "Development.h"
#include "MsgBox.h"
#include "Session.h"
#include <schema/install/Install.h>
#include <jade/AppContext.h>
#include <jadegit/Exception.h>
#include <Log.h>

using namespace Jade;
using namespace JadeGit;

namespace JadeGit::Development
{
	class TempUserProfile
	{
	public:
		TempUserProfile(const Character* userName) : app(AppContext::GetApp())
		{
			// Ignore if profile has been setup already
			jade_throw(app.getProperty(TEXT("userProfile"), profile));
			if (!profile.isNull())
				return;

			// Lookup user profile
			DskSchema rootSchema(&RootSchemaOid);
			DskMemberKeyDictionary users;
			jade_throw(rootSchema.getProperty(PRP_Schema_users, users));
			jade_throw(users.getAtKey(userName, profile));

			// Make default profile if required
			if (profile.isNull())
			{
				profile.setCls(DSKJADEUSERPROFILE);
				jade_throw(profile.createTransientObject());
				jade_throw(profile.sendMsg(TEXT("_setDefaultValues")));
			}

			// Set profile
			jade_throw(app.setProperty(TEXT("userProfile"), &profile));

			// Set defaulted flag so destructor will reset
			defaulted = true;
		}

		~TempUserProfile()
		{
			if (!defaulted)
				return;

			// Reset profile set above
			jade_throw(app.setProperty(TEXT("userProfile"), &NullDskObjectId));

			// Clean-up transient profile created above
			if (profile.isNonSharedTransient())
				jade_throw(profile.deleteObject());
		}

	private:
		DskObject app;
		DskObject profile;
		bool defaulted = false;
	};

	int jadeUserInfo(const Character* pUserName, const Character* pPassword)
	{
		try
		{
			// Ignore any monitor schema apps
			if (AppContext::currentSchema() == JadeMonitorSchemaOid)
				return JADEGIT_ALLOW;

			auto userName = narrow(pUserName);

			LOG_INFO("UserInfo: userName=" << userName);

			// Check library is not being called in server-only execution
			if (AppContext::GetNodeType() == Node_Type::DATABASE_SERVER)
				throw jadegit_exception("The jadegit development library cannot be used in conjunction with the JADE Database Server (jadrap).\n\n\
The JADE IDE needs to be run in single-user mode directly or using an application server in single-user mode.");

			// Check patch control hooks are enabled
			if (!AppContext::GetIniSettingBoolean(TEXT("JadeSecurity"), TEXT("JadePatchControlSecurity"), false))
				throw jadegit_exception("The jadegit development library depends on the JADE IDE patch control security being enabled to track changes.\n\n\
The JadeSecurity.JadePatchControlSecurity INI setting needs to be set to true.");

			// Setup user profile temporarily to avoid exceptions when re-org transition dialog is being shown & retrieves profile settings
			// Work-around for PAR #67927
			TempUserProfile profile(pUserName);

			// Check schema is up to date
			Schema::install();

			// Setup session and sign-on
			return Session::SignOn(userName) ? JADEGIT_ALLOW : JADEGIT_CANCEL;
		}
		catch (std::runtime_error& e)
		{
			MsgBox::display(e);
		}

		return JADEGIT_ERROR;
	};
}