#pragma once
#include "PatchControl.h"
#include <jomoid.hpp>
#include <set>

namespace JadeGit::Extract
{
	class Entity;
}

namespace JadeGit::Schema
{
	class GitChange;
	class GitLocalBranch;
}

namespace JadeGit::Development
{
	class EntityRenameStrategy;

	class EntityControl : public PatchControl
	{
	public:
		EntityControl();

		bool prelude(Session& session, const ClassNumber& entityType, const QualifiedName& entityName, bool removal) override;
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) override;
	
	protected:
		EntityControl(std::unique_ptr<EntityRenameStrategy> renameStrategy);

		std::unique_ptr<EntityRenameStrategy> renameStrategy;

		Schema::GitChange change(const std::unique_ptr<Extract::Entity>& actual, const std::string& operation);
		Schema::GitChange rename(const Extract::Entity& actual, const std::string& previousName) const;
		std::unique_ptr<Extract::Entity> resolve(const std::string& entityType, const QualifiedName& entityName) const;

	private:
		Integer64 tranid = 0;
		DskObjectId parent = NullDskObjectId;
	};
}