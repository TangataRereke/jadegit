#pragma once
#include "EntityControl.h"
#include <schema/data/Change.h>
#include <schema/data/Entity.h>
#include <vector>

namespace JadeGit::Development
{
	class HTMLDocumentControl : public EntityControl
	{
	public:
		HTMLDocumentControl();

	protected:
		bool prelude(Session& session, const ClassNumber& entityType, const QualifiedName& entityName, bool removal) override;
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) override;

	private:
		bool importing = false;
		bool removing = false;
		Integer64 tranid = 0;
		std::vector<Schema::GitChange> changes;
		std::vector<std::unique_ptr<Extract::Entity>> renames;
	};
}