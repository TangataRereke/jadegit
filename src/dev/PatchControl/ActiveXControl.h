#pragma once
#include "EntityControl.h"
#include <schema/data/Change.h>

namespace JadeGit::Development
{
	class ActiveXControl : public EntityControl
	{
	public:
		ActiveXControl();

	protected:
		bool prelude(Session& session, const ClassNumber& entityType, const QualifiedName& entityName, bool removal) override;
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) override;

	private:
		bool importing = false;
		bool removing = false;
		std::unique_ptr<Schema::GitChange> last;
		std::unique_ptr<Schema::GitChange> library;
		DskObjectId baseClass = NullDskObjectId;
	};
}