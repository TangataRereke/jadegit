#include "EntityRenameStrategy.h"
#include <jade/AppContext.h>
#include <jade/Transaction.h>
#include <schema/Workers.h>
#include <schema/data/Change.h>
#include <schema/data/Entity.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Schema;
using namespace JadeGit::Extract;

namespace JadeGit::Development
{
	bool EntityRenameStrategy::rename(const Extract::Entity& actual, GitChange& change)
	{
		// Reset targets already renamed when transaction has been restarted
		if (tranid != Transaction::GetTransactionId())
		{
			tranid = Transaction::GetTransactionId();
			renamed.clear();
		}
		// Suppress rename if handled already
		else if (renamed.find(actual.oid) != renamed.end())
			return false;

		// Attempt to retrieve previous name for which rename should be handled
		string name;
		if (!previousName(actual, name) || name == actual.getName())
			return false;

		// Remember what's been renamed
		renamed.insert(actual.oid);

		// Handle rename operation
		change = rename(actual, name);
		return true;
	}

	GitChange EntityRenameStrategy::rename(const Entity& actual, const string& previousName) const
	{
		// Resolve latest entity just updated
		unique_ptr<GitEntity> latest = GitEntity::resolve(actual, false);
		if (!latest)
			return GitChange();

		// Resolve previous entity being renamed
		// TODO: Resolve sibling from latest parent, rather than doing it from scratch
		unique_ptr<GitEntity> previous = GitEntity::resolve(actual, previousName.c_str(), false);
		if (!previous)
			return GitChange();

		// Handle rename
		return GitChange::rename(*previous, *latest, actual);
	}

	void EntityRenameStrategy::reset()
	{
		tranid = 0;
		renamed.clear();
	}

	void EntityRenamePreludeStrategy::reset()
	{
		EntityRenameStrategy::reset();

		name = string();
		targets.clear();
	}

	bool EntityRenamePreludeStrategy::previousName(const Entity& actual, string& name) const
	{
		if (targets.find(actual.oid) == targets.end())
			return false;

		name = this->name;
		return true;
	}

	void EntityRenamePreludeStrategy::save(const Entity& actual)
	{
		EntityRenameStrategy::save(actual);

		// Store current name and all namesakes which may be renamed during subsequent update operations
		name = actual.getName();
		actual.GetNamesakes(targets);
	}

	bool EntityRenameBackgroundStrategy::previousName(const Entity& actual, string& name) const
	{
		// Return name stored before update if available
		if (EntityRenamePreludeStrategy::previousName(actual, name))
			return true;

		if (actual.isNew())
			return false;

		// Retrieve current name via background thread which can read unmodified version
		name = Backend::workers.push(bind(&Entity::getName, &actual)).get();
		return true;
	}
}