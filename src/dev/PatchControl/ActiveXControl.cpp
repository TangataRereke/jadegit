#include "ActiveXControl.h"
#include "EntityRenameStrategy.h"
#include <extract/EntityFactory.h>
#include <schema/data/Entity.h>

using namespace std;
using namespace JadeGit::Schema;

namespace JadeGit::Development
{
	ActiveXControl::ActiveXControl() : EntityControl(make_unique<EntityRenameBackgroundStrategy>()) {}

	bool ActiveXControl::prelude(Session& session, const ClassNumber& entityType, const QualifiedName& entityName, bool removal)
	{
		// Reset
		importing = false;
		removing = false;
		last = make_unique<GitChange>();
		library.reset();
		baseClass = NullDskObjectId;

		// Pass onto next in chain if library isn't being modified
		if (entityType != DSKACTIVEXLIBRARY)
			return PatchControl::prelude(session, entityType, entityName, removal);

		importing = !removal;
		removing = removal;

		// Reset base details
		return EntityControl::prelude(session, 0, entityName, removal);
	}

	bool ActiveXControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		// Handle changes during import or removal
		if (importing || removing)
		{
			// Handle explicit library update
			if (entityType == "ActiveXLibrary")
			{
				library = make_unique<GitChange>(change(resolve(entityType, entityName), operation).peerOf(*last));
			}
			else
			{
				// Resolve entity being modified
				auto entity = resolve(entityType, entityName);
				
				// Handle nested update, suppressing subsequent processing if ignored
				auto nested = change(entity, operation);
				if (nested.isNull())
					return true;

				// Make nested update a peer of the library update or the last change if that hasn't been resolved yet
				last = make_unique<GitChange>(nested.peerOf(library ? *library : *last));

				// Handle resolving library if required (not always invoked explicitly during import)
				if (importing && !library)
				{
					if (entity->isKindOf(DSKCLASS))
					{
						if (baseClass.isNull())
						{
							baseClass = entity->oid;
						}
						else
						{
							Extract::Object activeXClass = Extract::Object(baseClass).getProperty<Extract::Object>(PRP_Class_activeXClass);
							if (activeXClass.isNull())
								throw jadegit_exception("Failed to resolve ActiveXClass during import");

							DskObjectId oid = activeXClass.getProperty<DskObjectId>(PRP_ActiveXClass_activeXLibrary);
							if (oid.isNull())
								throw jadegit_exception("Failed to resolve ActiveXLibrary during import");

							auto activeXLibrary = Extract::EntityFactory::Get().Create(oid);
							library = make_unique<GitChange>(change(activeXLibrary, activeXLibrary->isNew() ? "A" : "U").peerOf(*last));
						}
					}
				}
			}

			return true;
		}

		// Pass onto next in chain
		return PatchControl::execute(session, entityType, entityName, operation);
	}
}