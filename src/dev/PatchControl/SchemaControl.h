#pragma once
#include "PatchControl.h"
#include <schema/data/Schema.h>

namespace JadeGit::Development
{
	// SchemaControl handles checking status of the schema being updated , setting up if required when new one is being added
	class SchemaControl : public PatchControl
	{
	protected:
		bool prelude(Session& session, const ClassNumber& entityType, const QualifiedName& entityName, bool removal) override;
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) override;

	private:
		Schema::GitSchema::Status status(Session& session, const std::string& name, bool setup = true, bool adding = false);
	};
}