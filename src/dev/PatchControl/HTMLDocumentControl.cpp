#include "HTMLDocumentControl.h"
#include "EntityRenameStrategy.h"
#include <jade/Transaction.h>
#include <schema/data/Change.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Schema;

namespace JadeGit::Development
{
	HTMLDocumentControl::HTMLDocumentControl() : EntityControl(make_unique<EntityRenameBackgroundStrategy>()) {}

	bool HTMLDocumentControl::prelude(Session& session, const ClassNumber& entityType, const QualifiedName& entityName, bool removal)
	{
		// Reset
		importing = entityType == DSKJADEHTMLDOCUMENT && !removal;
		removing = entityType == DSKJADEHTMLDOCUMENT && removal;
		changes.clear();
		renames.clear();

		// Pass onto next in chain if document isn't being imported or removed
		if (!importing && !removing)
			return PatchControl::prelude(session, entityType, entityName, removal);

		// Reset base details
		return EntityControl::prelude(session, !entityName.parent ? 0 : entityType, entityName, removal);
	}

	bool HTMLDocumentControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		// Handle changes during import or removal
		if (importing || removing)
		{
			// Reset if transaction restarted
			if (tranid != Transaction::GetTransactionId())
			{
				tranid = Transaction::GetTransactionId();
				changes.clear();
				renames.clear();
			}

			// Queue intended renames (no 'U' operation subsequently invoked with new name)
			if (operation == "PRBC" || (entityType == "HTMLClass" && operation == "U"))
			{
				renames.push_back(resolve(entityType, entityName));
			}
			else
			{
				bool flushing = (importing && entityType == "JadeHTMLDocument") || (removing && entityType == "HTMLClass");

				// Handle queued renames
				if (flushing)
				{
					for (auto& rename : renames)
						changes.push_back(EntityControl::change(rename, "U"));
				}

				// Handle change
				GitChange change = EntityControl::change(resolve(entityType, entityName), operation);

				// Make all changes peers
				if (flushing)
				{
					for (auto& peer : changes)
						change.peerOf(peer);
				}
				else
					changes.push_back(change);
			}

			return true;
		}

		// Pass onto next in chain
		return PatchControl::execute(session, entityType, entityName, operation);
	}
}