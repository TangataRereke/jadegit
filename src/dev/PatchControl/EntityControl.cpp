#include "EntityControl.h"
#include "EntityRenameStrategy.h"
#include <extract/EntityFactory.h>
#include <jade/AppContext.h>
#include <jade/Transaction.h>
#include <schema/data/Change.h>
#include <schema/data/Entity.h>
#include <schema/deploy/DeploymentLock.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Schema;
using namespace JadeGit::Extract;

namespace JadeGit::Development
{
	EntityControl::EntityControl() : EntityControl(make_unique<EntityRenamePreludeStrategy>()) {}

	EntityControl::EntityControl(unique_ptr<EntityRenameStrategy> renameStrategy) : PatchControl(), renameStrategy(move(renameStrategy)) {}

	bool EntityControl::prelude(Session& session, const ClassNumber& entityType, const QualifiedName& entityName, bool removal)
	{
		tranid = 0;
		parent = NullDskObjectId;
		renameStrategy->reset();

		if (entityType)
		{

			// Resolve entity about to be updated or removed
			auto entity = EntityFactory::Get().resolve(entityType, entityName);

			// Save associates of entities about to be removed
			if (removal)
			{
				GitEntity::resolve(*entity)->updateAssociates(*entity);

				if (entity->getVersionState() == ObjectVersionState::OBJVERSTATE_VERSIONED_CURRENT)
				{
					DskObject object;
					jade_throw(entity->getNextVersionObject(&object));
					assert(!object.isNull());

					auto latest = EntityFactory::Get().Create(object.oid);
					GitEntity::resolve(*latest)->updateAssociates(*latest);
				}
			}
			else
			{
				// Save any details needed in case of rename
				renameStrategy->save(*entity);
			}
		}

		return PatchControl::prelude(session, entityType, entityName, removal);
	}

	bool EntityControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		// Ignore schema entity RBC & RAC operations (renames before/after commit)
		// These are skipped because entities may have already been renamed when JADE invokes BC operation (classes in particular),
		// and the AC operation when entities have been renamed is invoked after transaction has already been committed.
		// Instead we store previous names during function selected calls just before editing (followed by U operation).
		if (operation == "MRBC" ||
			operation == "PRBC" ||
			operation == "CRBC" ||
			operation == "CLRBC")
			return true;

		// Register change
		change(resolve(entityType, entityName), operation);

		return true;
	}

	GitChange EntityControl::change(const unique_ptr<Extract::Entity>& actual, const string& operation)
	{
		/*
			Attempts to update outside of transaction state are treated as a dryrun where we
			assume Jade simply needs to check if things can be changed (i.e. editing methods).
		*/
		bool dryrun = !Transaction::InTransactionState();

		// Determine if update is being performed
		bool update = (operation == "U" || operation == "L" || operation == "US" || operation == "IS");

		// Ignore updates to parent as a result of child entity being deleted in same transaction
		if (update && !dryrun && tranid)
		{
			if (tranid != Transaction::GetTransactionId())
				tranid = 0;
			else if (parent == actual->oid)
			{
				parent = NullDskObjectId;
				return GitChange();
			}
		}

		// Prevent changes while deploying
		DeploymentLock lock;

		// Handle rename during update
		if (update)
		{
			GitChange change;
			if (renameStrategy->rename(*actual, change))
				return change;
		}

		// Resolve entity just modified
		unique_ptr<GitEntity> entity = GitEntity::resolve(*actual, dryrun);
		if (!entity)
			return GitChange();

		// TODO: Implement a CanUpdate method?  .. or perhaps dryrun parameter on a ChangeOperation::Update method?
		// Currently assume dryrun is allowed
		if (dryrun)
			return GitChange();

		// Handle update
		if (update)
			return GitChange::update(*entity);

		// Handle add
		else if (operation == "A" || operation == "AS")
			return GitChange::add(*entity);

		// Handle delete
		else if (operation == "D" || operation == "DS")
		{
			// Remember current transaction and parent of entity being deleted
			// Used to detect/ignore implied parent update above
			tranid = Transaction::GetTransactionId();
			parent = actual->GetParentId();

			return GitChange::dele(*entity, actual.get());
		}
		else
			throw jadegit_exception("Unhandled operation: " + operation);
	}

	GitChange EntityControl::rename(const Entity& actual, const string& previousName) const
	{
		// Prevent changes while deploying
		DeploymentLock lock;

		// Handle rename
		return renameStrategy->rename(actual, previousName);
	}

	unique_ptr<Entity> EntityControl::resolve(const string& entityType, const QualifiedName& entityName) const
	{
		// Resolve entity
		// NOTE: This will return latest version if it doesn't exist in current schema version
		unique_ptr<Entity> entity = EntityFactory::Get().resolve(entityType, entityName);

		// Get latest version
		if (entity->getVersionState() == ObjectVersionState::OBJVERSTATE_VERSIONED_CURRENT)
		{
			DskObject object;
			jade_throw(entity->getNextVersionObject(&object));
			assert(!object.isNull());

			unique_ptr<Entity> latest = EntityFactory::Get().Create(object.oid);

			// Consider switching to latest if name matches current (resolved based on name supplied to patch control hook)
			// TODO: Compare fully qualified name to account for scenarios where parent has been renamed?
			if (latest->getName() == entity->getName())
			{
				// Switch to latest if locked or current is not locked (implying latest version is being updated)
				if (latest->isLockedByMe() || !entity->isLockedByMe())
					latest.swap(entity);
			}
		}

		return entity;
	}
}