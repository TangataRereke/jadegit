#include "Command.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	bool Command::Execute()
	{
		return session.Send(*this, eventType);
	}

	/* Define specific events used for each command */
	const NoteEventType SETUP_SCHEMA_EVENT = USER_BASE_EVENT;

	SetupSchemaCommand::SetupSchemaCommand(Session& session, const string& schemaName) : Command(session, SETUP_SCHEMA_EVENT), 
		schemaName(widen(schemaName))
	{}

	void SetupSchemaCommand::GetInfo(DskParam& userInfo) const
	{
		jade_throw(paramSetCString(userInfo, schemaName));
	}

	bool SetupSchemaCommand::ProcessReply(DskParam& params)
	{
		bool result = false;
		jade_throw(paramGetBoolean(params, result));
		return result;
	}
}