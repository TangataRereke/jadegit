#pragma once
#include <stdexcept>
#include <string>

namespace JadeGit::Development
{
	class MsgBox
	{
	public:
		enum Style
		{
			Default = 0,
			Error = 16,
			Query = 32,
			Warning = 48,
			Information = 64
		};

		enum Options
		{
			OK_Only = 0,
			OK_Cancel = 1,
			Abort_Retry_Ignore = 2,
			Yes_No_Cancel = 3,
			Yes_No = 4,
			Retry_Cancel = 5
		};

		enum DefaultOption
		{
			First = 0,
			Second = 256,
			Third = 512
		};

		enum Scope
		{
			Application = 0,
			System = 4096
		};

		enum Result
		{
			Abort = 3,
			Cancel = 2,
			Ignore = 5,
			No = 7,
			OK = 1,
			Retry = 4,
			Yes = 6
		};

		static Result display(std::string message, const char* title, Style style = Default, Options options = OK_Only, DefaultOption defaultOption = First, Scope scope = Application);
		static Result display(std::string message, Style style = Default, Options options = OK_Only, DefaultOption defaultOption = First, Scope scope = Application);
		static void display(const std::runtime_error& e);
		static void displayError(std::string message);
		static bool displayWarning(std::string message);
	};
}