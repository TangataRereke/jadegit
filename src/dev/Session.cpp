#include "Session.h"
#include <jadegit/schema/Backend.h>
#include <jadegit/Exception.h>
#include <jade/Any.h>
#include <jade/AppContext.h>
#include <Log.h>
#include <map>
#include <shared_mutex>
#include <sstream>

using namespace std;
using namespace Jade;
using namespace JadeGit::Schema;

namespace JadeGit::Development
{
	shared_mutex session_mutex;
	map<DskObjectId, Session*> sessionsByBrowser;
	
	int JOMAPI sessionTerminated(DskHandle* pHandle, void* pParam, bool nodeTermination)
	{
		Session* session = (Session*)pParam;
		session->SignOff();
		delete session;
		return 0;
	}

	class Shutdown
	{
	public:
		~Shutdown()
		{
			// Deregister termination callbacks for any remaining sessions
			while (!sessionsByBrowser.empty())
			{
				Session* session = sessionsByBrowser.begin()->second;
				jomDeregisterTerminationCallBack(nullptr, nullptr, sessionTerminated, session, ProcessTerminationCallBack, __LINE__);
				delete session;
			}
		}
	};
	static Shutdown shutdown;

	Session* Session::Find(DskObjectId browser)
	{
		shared_lock<shared_mutex> lock(session_mutex);

		map<DskObjectId, Session*>::const_iterator iter(sessionsByBrowser.find(browser));
		return iter != sessionsByBrowser.end() ? iter->second : nullptr;
	}

	Session::Session(const string& userName, DskObjectId browser) : userName(userName), browser(browser)
	{
		unique_lock<shared_mutex> lock(session_mutex);
		sessionsByBrowser[browser] = this;

		jomRegisterTerminationCallBack(nullptr, nullptr, sessionTerminated, this, ProcessTerminationCallBack, __LINE__);

		LOG_INFO(userName << " signed on");
	}

	Session::~Session()
	{
		unique_lock<shared_mutex> lock(session_mutex);
		sessionsByBrowser.erase(browser);
	}

	void Session::SignOff()
	{
		// Shutdown background workers no longer required
		if (state == State::SignedOn)
			Backend::Shutdown();

		LOG_INFO(userName << " signed off");
	}

	Session* Session::SignOn(const string& userName)
	{
		// Get current process (which is the main IDE browser process)
		DskObjectId browser = AppContext::process();

		// Clean-up previous session if one exists
		if (Session* session = Find(browser))
		{
			jomDeregisterTerminationCallBack(nullptr, nullptr, sessionTerminated, session, ProcessTerminationCallBack, __LINE__);
			delete session;
		}

		// Setup session
		Schema::Session::set(nullptr);
		return new Session(userName, browser);
	}

	Session* Session::Switch(bool administration)
	{
		// Resolve session for current process
		auto session = Find(AppContext::process());

		// Resolve session for parent process if required (covers scenarios where user is loading schema files or using painter)
		if (!session)
			session = Find(AppContext::parentProcess());

		if (!session)
			throw jadegit_exception("Failed to resolve session for current process");

		// Establish session mode if required
		if (session->state == Session::Stopped && administration)
			session->state = Session::Administration;

		// Start explorer if required
		if (!session->Start())
			return nullptr;

		Schema::Session::set(session);
		return session;
	}

	bool Session::Start()
	{
		/* No need to start explorer in administration mode */
		if (state == Administration)
			return true;
		
		/* Lock session before checking/updating */
		unique_lock<mutex> lock(mtx);

		/* Check explorer process is still valid */
		if (state != State::Stopped && interop != NullDskObjectId)
		{
			bool valid = false;
			jomIsValidObject(NULL, &(interop), valid, __LINE__);

			/* No need to continue if it's still valid */
			if (valid)
				return true;
		}

		/* Restart session state */
		state = State::Starting;
		stalledError = 0;

		/* Start 'Jade-Git Explorer' for user */
		/* NOTE: This is essentially a work-around, JADE would ideally allow us to implement our own IDE extensions */
		ShortDskParam meth;
		paramSetCString(meth, TEXT("startJadeAppWithParameter"));

		DskParam pSchemaName;
		DskParam pAppName;
		DskParam pInitializeParameter;
		DskParam pIn;
		DskParam pOut;

		paramSetCString(pSchemaName, TEXT("JadeGitSchema"));
		paramSetCString(pAppName, TEXT("JadeGitExplorer"));
		paramSetOid(pInitializeParameter, browser);
		paramSetParamList(pIn, &pSchemaName, &pAppName, &pInitializeParameter);

		paramSetOid(pOut);

		jomSendMsg(nullptr, AppContext::GetApp(), (DskParam*)&meth, &pIn, &pOut, __LINE__);

		/* Store session interop process so we can check if restart is required later above */
		paramGetOid(pOut, (interop));

		/* Wait for interop worker to invoke started method */
		if (!cv.wait_for(lock, chrono::seconds(10), [this]() {return (state != State::Starting); }))
			throw jadegit_exception("Failed to start interop (timeout)");

		/* Wait for interop worker to invoke sign-on method, after user has setup record, hence long delay */
		if (!cv.wait_for(lock, chrono::seconds(600), [this]() {return (state == State::Stopped || state == State::SignedOn); }))
			throw jadegit_exception("Sign-on aborted (timeout)");

		/* Check if interop worker was stopped */
		if (state == State::Stopped)
			if (stalledError)
				throw Jade::Exception(stalledError);
			else
				return false;

		// Startup backend workers and register dependency
		Backend::Startup();

		/* Signed On */
		return true;
	}

	string Session::Initialized()
	{
		/* Lock session before updating */
		unique_lock<mutex> lock(mtx);

		this->state = State::Started;

		/* Unlock and notify waiting thread */
		lock.unlock();
		cv.notify_one();

		return userName;
	}

	void Session::SignOn(DskObjectId* user)
	{
		/* Lock session before updating */
		unique_lock<mutex> lock(mtx);

		this->user_.oid = *user;
		this->state = State::SignedOn;

		/* Unlock and notify waiting thread */
		lock.unlock();
		cv.notify_one();
	}

	void Session::Stalled(int errorCode)
	{
		/* Lock session before updating */
		unique_lock<mutex> lock(mtx);

		this->state = State::Stopped;
		this->stalledError = stalledError;

		/* Unlock and notify waiting thread */
		lock.unlock();
		cv.notify_one();
	}

	void Session::Finalized()
	{
		/* Lock session before updating */
		unique_lock<mutex> lock(mtx);

		this->state = State::Stopped;
		this->stalledError = 0;

		/* Unlock and notify waiting thread (if there is one) */
		lock.unlock();
		cv.notify_one();
	}

	bool Session::Send(ICommand& command, NoteEventType eventType)
	{
		/* Lock session before causing event */
		unique_lock<mutex> lock(mtx);

		/* Set current command ready for callback */
		this->command = &command;

		/* Reset result (set when procesing reply) */
		this->result = false;

		/* Get info to send */
		DskParamAny info;
		command.GetInfo(info);
		
		/* Cause event */
		jade_throw(jomCauseEvent(nullptr, &interop, eventType, TRUE, &info, &browser, __LINE__));

		/* Wait for response (long delay due to user interaction) */
		if (!cv.wait_for(lock, chrono::seconds(600), [this]() {return !this->command; }))
			throw jadegit_exception("Remote command aborted (timeout)");

		return this->result;
	}

	bool Session::ProcessReply(DskParam& params)
	{
		/* Lock session before updating */
		unique_lock<mutex> lock(mtx);

		/* Process reply */
		assert(command);
		result = command->ProcessReply(params);

		/* Reset command */
		command = nullptr;

		/* Unlock and notify waiting thread (above) */
		lock.unlock();
		cv.notify_one();

		return result;
	}

	/*
		Exported Functions
	*/
	int JOMAPI jadegit_explorer_started(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		DskObjectId browser;
		JADE_RETURN(paramGetOid(*pParams, browser));

		Session* session = Session::Find(browser);
		if (!session)
			return OBJECT_NOT_FOUND;

		string userName = session->Initialized();
		return paramSetString(*pReturn, widen(userName));
	}

	int JOMAPI jadegit_explorer_signon(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		DskParam* pBrowser = NULL;
		DskParam* pUser = NULL;
		
		JADE_RETURN(paramGetParameter(*pParams, 1, pBrowser));
		JADE_RETURN(paramGetParameter(*pParams, 2, pUser));
		
		DskObjectId browser;
		JADE_RETURN(paramGetOid(*pBrowser, browser));

		DskObjectId user;
		JADE_RETURN(paramGetOid(*pUser, user));

		Session* session = Session::Find(browser);
		if (!session)
			return OBJECT_NOT_FOUND;

		session->SignOn(&user);
		return 0;
	}

	int JOMAPI jadegit_explorer_stalled(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		DskParam* pBrowser = NULL;
		DskParam* pErrorCode = NULL;

		JADE_RETURN(paramGetParameter(*pParams, 1, pBrowser));
		JADE_RETURN(paramGetParameter(*pParams, 2, pErrorCode));

		DskObjectId browser;
		JADE_RETURN(paramGetOid(*pBrowser, browser));

		int errorCode;
		JADE_RETURN(paramGetInteger(*pErrorCode, errorCode));

		Session* session = Session::Find(browser);
		if (!session)
			return OBJECT_NOT_FOUND;
		
		session->Stalled(errorCode);
		return 0;
	}

	int JOMAPI jadegit_explorer_stopped(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		DskObjectId browser;
		JADE_RETURN(paramGetOid(*pParams, browser));

		Session* session = Session::Find(browser);
		if (!session)
			return OBJECT_NOT_FOUND;

		session->Finalized();
		return 0;
	}

	int JOMAPI jadegit_explorer_reply(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		/* Resolve session based on first parameter */
		DskParam* pBrowser = nullptr;
		JADE_RETURN(paramGetParameter(*pParams, 1, pBrowser));
		
		DskObjectId browser;
		JADE_RETURN(paramGetOid(*pBrowser, browser));

		Session* session = Session::Find(browser);
		if (!session)
			return OBJECT_NOT_FOUND;

		/* Process reply, with first parameter omitted */
		if (pParams->body.parameterList.entries == 2)
		{
			DskParam* pReply = nullptr;
			JADE_RETURN(paramGetParameter(*pParams, 2, pReply));

			/* Process reply with single parameter */
			session->ProcessReply(*pReply);
		}
		else
		{
			/* Put multiple parameters into new parameter list (omitting first) */
			DskParam params;
			for (int i = 1; i < pParams->body.parameterList.entries; i++)
				paramSetParameter(params, i, pParams->body.parameterList.param[i]);

			/* Process reply with multiple parameters */
			session->ProcessReply(params);
		}

		return 0;
	}
}