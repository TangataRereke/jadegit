#include "Function.h"
#include <dev/PatchControl/PatchControl.h>

namespace JadeGit::Development
{
	// Update function (those for which we need to remember what's being changed)
	class UpdateFunction : public Function
	{
	public:
		UpdateFunction(std::initializer_list<const char*> taskNames, ClassNumber entityType) : Function(taskNames), entityType(entityType) {}
		UpdateFunction(const char* taskName, ClassNumber entityType) : UpdateFunction({ taskName }, entityType) {};

	protected:
		bool execute(const std::string& entityName) const override final
		{
			// Notify patch control of entity that may be renamed during update operation
			return PatchControl::get().prelude(entityType, entityName);
		}

	private:
		ClassNumber entityType;
	};

	// Map update functions to associated entity class numbers
	static UpdateFunction changeApplication(Function::ChangeApplication, DSKAPPLICATION);
	static UpdateFunction changeAttribute(Function::ChangeAttribute, DSKATTRIBUTE);
	static UpdateFunction changeClass(Function::ChangeClass, DSKCLASS);
	static UpdateFunction changeConstant(Function::ChangeConstant, DSKCONSTANT);
	static UpdateFunction changeExposureList(Function::ChangeExposureList, DSKJADEEXPOSEDLIST);
	static UpdateFunction changeForm({Function::ChangeForm, Function::CopyForm}, DSKFORM);
	static UpdateFunction changeFormat(Function::ChangeFormat, DSKLOCALEFORMAT);
	static UpdateFunction changeGlbConstant(Function::ChangeGblConstant, DSKGLOBALCONSTANT);
	static UpdateFunction changeConstsCategory(Function::ChangeGblConstsCategory, DSKCONSTANTCATEGORY);
	static UpdateFunction changeConsumer(Function::ChangeConsumer, DSKJADEEXPOSEDLIST);
	static UpdateFunction changeInterface(Function::ChangeInterface, DSKJADEINTERFACE);
	static UpdateFunction changeHTMLDocument({ Function::AddHTMLDocument, Function::ChangeHTMLDocument, Function::ReloadHTMLDocument }, DSKJADEHTMLDOCUMENT);
	static UpdateFunction changeMapFile(Function::ChangeMapFile, DSKDBFILE);
	static UpdateFunction changePackage(Function::ChangePackage, DSKJADEPACKAGE);
	static UpdateFunction changeRelationalView(Function::ChangeRelationalView, DSKRELATIONALVIEW);
	static UpdateFunction importActiveXLibrary({ Function::ImportActiveXControlLib, Function::ImportAutomationLib, Function::ImportDotNetLib }, DSKACTIVEXLIBRARY);
	static UpdateFunction renameMethod(Function::RenameMethod, DSKMETHOD);
}