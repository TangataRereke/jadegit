#include "Function.h"
#include <jadegit/Exception.h>
#include <schema/data/Root.h>
#include <schema/data/Schema.h>

namespace JadeGit::Development
{
	// Re-org functions (those that may conflict with source control deployments)
	class ReorgFunction : public SessionFunction
	{
	public:
		ReorgFunction() : SessionFunction
		({
			Function::RemoveSchema,			// JADE reuses this for versisioning & unversioning as well
			Function::ReorgAbort,
			Function::ReorgInstances
			}) {};

	protected:
		bool execute(Session* session, const std::string& entityName) const override
		{
			// Check deployment isn't in progress
			// TODO: Query repository associated with schema
			if (Schema::Root::get().deploying())
				throw jadegit_exception("Schemas may not be re-organized or removed until pending deployment has completed");

			return true;
		}
	};
	static ReorgFunction reorg;

	class TransitionFunction : public SessionFunction
	{
	public:
		TransitionFunction() : SessionFunction({Function::SetSchema}) {}

	protected:
		bool execute(Session* session, const std::string& entityName) const override
		{
			Schema::GitSchema::selected(entityName);
			return true;
		}
	};
	static TransitionFunction transition;
}