#pragma once
#include <jomtypes.h>
#include <map>
#include <string>

namespace JadeGit::Development
{
	class Function
	{
	public:
		static Function& get(const std::string& taskName);

		virtual bool execute(const std::string& entityName) const = 0;

		// Task name constants added manually
		static constexpr const char* Administration = "administration";
		static constexpr const char* CopyMethod = "CopyMethod";
		static constexpr const char* MoveClass = "MoveClass";
		static constexpr const char* MoveMethod = "MoveMethod";

		// Task name constants generated based on JadeDevSecurityInterface constants
		static constexpr const char* AccessClasses = "accessClasses";	//  Used by HierarchyBrowser?
		static constexpr const char* AccessToAdminFns = "menuAdmin";
		static constexpr const char* AccessToClassFns = "typesMenu";	// Use by none
		static constexpr const char* AccessToConstantFns = "constantsMenu";	//  Used by HierarchyBrowser
		static constexpr const char* AccessToHistoryFns = "menuHistory";	//  used by HierarchyBrowser
		static constexpr const char* AccessToInterfaceFns = "menuInterfaces";
		static constexpr const char* AccessToMethodFns = "methodsMenu";	//  Used by HierarchyBrowser
		static constexpr const char* AccessToPatchFns = "menuPatch";
		static constexpr const char* AccessToPrimTypeFns = "typesMenu";	//  Used by PrimitiveHierarchyBrowser
		static constexpr const char* AccessToPropertyFns = "variablesMenu";	//  Used by HierarchyBrowser
		static constexpr const char* AccessToTypeFns = "typesMenu";	//  Used by HierarchyBrowser
		static constexpr const char* AddAdHocIndex = "mnuAdHocIndexAdd";
		static constexpr const char* AddApplication = "addApplication";	//  Used by ApplicationBrowser
		static constexpr const char* AddClass = "addType";	//  Used by HierarchyBrowser
		static constexpr const char* AddCondition = "addConditions";	//  Used by HierarchyBrowser
		static constexpr const char* AddConstant = "addConstant";	//  Used by HierarchyBrowser
		static constexpr const char* AddConsumer = "mnuAddConsumer";
		static constexpr const char* AddCurrencyFormat = "menuAddCurrency";	//  Used by FormatBrowser
		static constexpr const char* AddDelta = "addDelta";	//  Used by DeltaBrowser
		static constexpr const char* AddExposureList = "addExposedList";	//  Used by ExposedListBrowser
		static constexpr const char* AddExtFunction = "addFunction";	//  Used by ExternalFunctionsBrowser
		static constexpr const char* AddExtFunctionLibrary = "addFunctionLibrary";	//  Used by ExternalFunctionsBrowser
		static constexpr const char* AddExternalDb = "menuAdd";	//  Used by ExposedListBrowser
		static constexpr const char* AddExternalMethod = "externalMethod";	//  Used by HierarchyBrowser
		static constexpr const char* AddForm = "addForm";
		static constexpr const char* AddGblConstsCategory = "addCategory";	//  Used by GlobalConstantsBrowser
		static constexpr const char* AddGlobalConstant = "addGlobalConstant";	//  Used by GlobalConstantsBrowser
		static constexpr const char* AddHTMLDocument = "addHTMLDocument";	//  Used by HTMLBrowser
		static constexpr const char* AddInterface = "addInterface";
		static constexpr const char* AddJadeMethod = "alMethod";	//  Used by HierarchyBrowser
		static constexpr const char* AddLibrary = "addLibrary";	//  Used by LibraryBrowser
		static constexpr const char* AddLongDateFormat = "menuAddLongDate";	//  Used by FormatBrowser
		static constexpr const char* AddMapFile = "addFile";	//  Used by ClassMapsBrowser
		static constexpr const char* AddMethodView = "addMethodView";
		static constexpr const char* AddNumericFormat = "menuAddNumeric";	//  Used by FormatBrowser
		static constexpr const char* AddPackage = "addPackage";	//  Used by PackagesBrowser
		static constexpr const char* AddPatch = "mnuAddPatch";	//  Used by PatchesBrowser
		static constexpr const char* AddPrimType = "addType";
		static constexpr const char* AddProperty = "addVariable";	//  Used by HierarchyBrowser
		static constexpr const char* AddRPSView = "menuRPSAdd";	// Used by RPS
		static constexpr const char* AddRelationalView = "menuODBCAdd";	//  Used by ODBCInterface
		static constexpr const char* AddRemapTable = "addRemap";	//  Used by RemapTableBrowser
		static constexpr const char* AddSchema = "addSchema";	//  Used by SchemaBrowser
		static constexpr const char* AddSchemaView = "addSchemaView";	//  Used by SchemaViewBrowser
		static constexpr const char* AddShortDateFormat = "menuAddShortDate";	//  Used by FormatBrowser
		static constexpr const char* AddSubclass = "subclass";
		static constexpr const char* AddTimeFormat = "menuAddTime";	//  Used by FormatBrowser
		static constexpr const char* BroadcastMessage = "broadcastMessage";	//  Used by JadeWindow
		static constexpr const char* BrowseActiveXLibrary = "mnuBrowseActiveXLib";	//  Used by JadeWindow
		static constexpr const char* BrowseAdHocIndexes = "mnuBrowseAdhocIndexes";
		static constexpr const char* BrowseApplications = "mnuBrowseApplications";
		static constexpr const char* BrowseBreakpoints = "mnuBreakpoints";	//  Used by JadeWindow
		static constexpr const char* BrowseChangedMethods = "mnuChangedMethods";	//  Used by JadeWindow
		static constexpr const char* BrowseChkOutMethods = "mnuCheckedOutMethods";	//  Used by JadeWindow
		static constexpr const char* BrowseClasses = "mnuBrowseClasses";	//  Used by JadeWindow
		static constexpr const char* BrowseClassesInUse = "mnuBrowseClassesInUse";	//  Used by JadeWindow
		static constexpr const char* BrowseConstants = "mnuBrowseConstants";	//  Used by JadeWindow
		static constexpr const char* BrowseConsumer = "mnuBrowseConsumer";
		static constexpr const char* BrowseDeltas = "mnuBrowseDeltas";	//  Used by JadeWindow
		static constexpr const char* BrowseExportPackages = "mnuExportBrowser";	//  Used by JadeWindow
		static constexpr const char* BrowseExposedLists = "mnuBrowseExposedLists";	//  Used by JadeWindow
		static constexpr const char* BrowseExposureList = "mnuBrowseExposureList";
		static constexpr const char* BrowseExtFunctions = "mnuBrowseFunctions";	//  Used by JadeWindow
		static constexpr const char* BrowseExternalDb = "mnuExternalDb";	//  Used by JadeWindow
		static constexpr const char* BrowseFormats = "schemaMenuFormats";	//  Used by SchemaBrowser
		static constexpr const char* BrowseHTML = "mnuBrowseHTML";	//  Used by JadeWindow
		static constexpr const char* BrowseImportPackages = "mnuImportBrowser";	//  Used by JadeWindow
		static constexpr const char* BrowseInterfaces = "mnuBrowseInterfaces";
		static constexpr const char* BrowseLibraries = "mnuBrowseLibraries";	//  Used by JadeWindow
		static constexpr const char* BrowseLocales = "menuLocales";	//  Used by SchemaBrowser
		static constexpr const char* BrowseMapFiles = "mnuBrowseMaps";
		static constexpr const char* BrowseMethodStatusList = "mnuBrowseStatusCurrent";	//  Used by JadeWindow
		static constexpr const char* BrowseMethods = "mnuBrowseMethods";	//  Used by JadeWindow
		static constexpr const char* BrowseMethods1 = "browse";	//  Used by HierarchyBrowser
		static constexpr const char* BrowseMethodsAndProps = "mnuBrowseMethsAndVars";	//  Used by JadeWindow
		static constexpr const char* BrowseMthdStatusList = "mnuMethodStatusList";	//  Used by JadeWindow
		static constexpr const char* BrowsePackageClasses = "browsePackageClasses";
		static constexpr const char* BrowsePackageUsages = "browsePackageUsages";
		static constexpr const char* BrowsePatchSummary = "mnuShowPatchSummary";	//  Used by JadeWindow
		static constexpr const char* BrowsePatches = "mnuBrowsePatches";	//  Used by JadeWindow
		static constexpr const char* BrowsePrimTypes = "mnuBrowseTypes";	//  Used by JadeWindow
		static constexpr const char* BrowseProperties = "mnuBrowseVariables";	//  Used by JadeWindow
		static constexpr const char* BrowseRPS = "mnuBrowseRPS";
		static constexpr const char* BrowseRelViews = "mnuRelationalViews";	//  Used by JadeWindow
		static constexpr const char* BrowseRemapTables = "mnuBrowseRemapTables";	//  Used by JadeWindow
		static constexpr const char* BrowseSchemaViews = "mnuBrowseViews";	//  Used by JadeWindow
		static constexpr const char* BrowseTranslateForms = "menuTranslateForms";	//  Used by SchemaBrowser
		static constexpr const char* BrowseTranslateStrings = "menuStrings";	//  Used by SchemaBrowser
		static constexpr const char* BrowseUnreferMethods = "mnuUnreferencedMethods";	//  Used by JadeWindow
		static constexpr const char* Browse_Java_Lists = "mnuBrowseJavaLists";
		static constexpr const char* BuildAdHocIndex = "mnuAdHocIndexBuild";
		static constexpr const char* CancelAdHocIndex = "mnuAdHocIndexCancel";
		static constexpr const char* ChangeAdHocIndex = "mnuAdHocIndexChange";
		static constexpr const char* ChangeApplication = "applicationOptions";	//  Used by ApplicationBrowser
		static constexpr const char* ChangeAttribute = "changeVariable";	//  Used by HierarchyBrowser
		static constexpr const char* ChangeClass = "changeType";	//  Used by HierarchyBrowser
		static constexpr const char* ChangeClassCluster = "changeClassCluster";
		static constexpr const char* ChangeConstant = "changeConstant";	//  Used by HierarchyBrowser
		static constexpr const char* ChangeConsumer = "mnuChangeConsumer";
		static constexpr const char* ChangeDelta = "mnuChangeDelta";	// Used by Delta Browser
		static constexpr const char* ChangeExposureList = "maintainExposedList";	//  Used by ExposedListBrowser
		static constexpr const char* ChangeExternalDb = "menuChange";	//  Used by ExposedListBrowser
		static constexpr const char* ChangeForm = "saveForm";
		static constexpr const char* ChangeFormat = "menuEditFormat";	//  Used by FormatBrowser
		static constexpr const char* ChangeFunction = "updateFunction";
		static constexpr const char* ChangeGblConstant = "changeGlobalConstant";	//  Used by GlobalConstantsBrowser
		static constexpr const char* ChangeGblConstsCategory = "changeCategory";	//  Used by GlobalConstantsBrowser
		static constexpr const char* ChangeHTMLDocument = "changeHTMLDocument";	//  Used by HTMLBrowser
		static constexpr const char* ChangeInterface = "mnuChangeInterface";
		static constexpr const char* ChangeMapFile = "changeFile";	//  Used by ClassMapsBrowser
		static constexpr const char* ChangeMethod = "updateMethod";
		static constexpr const char* ChangeMethodView = "changeMethodView";
		static constexpr const char* ChangePackage = "maintainPackage";	//  Used by PackagesBrowser
		static constexpr const char* ChangePatch = "mnuChangePatch";	//  Used by PatchesBrowser
		static constexpr const char* ChangeRPSView = "menuRPSChange";	// Used by RPS
		static constexpr const char* ChangeRelationalView = "menuODBCChange";	//  Used by ODBCInterface
		static constexpr const char* ChangeRemapTable = "changeRemap";	//  Used by RemapTableBrowser
		static constexpr const char* ChangeSchemaView = "maintainSchemaView";	//  Used by SchemaViewBrowser
		static constexpr const char* CheckInAllInDelta = "checkInAll";	//  Used by DeltaBrowser
		static constexpr const char* CheckInMethod = "checkInMethod";	//  Used by HierarchyBrowser, SendersBrowser
		static constexpr const char* CheckOutMethod = "checkOutMethod";	//  Used by HierarchyBrowser
		static constexpr const char* ClearHistory = "clearHistory";
		static constexpr const char* CloseDelta = "closeDelta";	//  Used by DeltaBrowser
		static constexpr const char* ClosePatch = "mnuClosePatch";	//  Used by PatchesBrowser
		static constexpr const char* CloseSchemaWindows = "closeSchemaWindows";	//  Used by SchemaBrowser
		static constexpr const char* CompareMethods = "compareMethods";	//  Used by HierarchyBrowser, SendersBrowser
		static constexpr const char* CompileClass = "compileType";	//  Used by HierarchyBrowser
		static constexpr const char* CompileExtFunction = "compile";	//  Used by ExternalFunctionsBrowser
		static constexpr const char* CompileMethod = "compile";	//  Used by ClassReferencesBrowser,HierarchyBrowser,MessagesBrowser,MethodStatusList,SendersBrowser,SourceWindow
		static constexpr const char* CompileType = "compileType";	//  Used by HierarchyBrowser
		static constexpr const char* Component_ActiveX = "mnuComponentActiveX";
		static constexpr const char* Component_DotNet = "menuDotNetComponent";
		static constexpr const char* CopyForm = "copyForm";
		static constexpr const char* CopyMethodView = "copyMethodView";
		static constexpr const char* CopyToClipboard = "copyToClipboard";
		static constexpr const char* CreateCommandFile = "mnuCreateCommand";
		static constexpr const char* CreateMappingMethod = "createMappingMethod";
		static constexpr const char* DebugJadeScript = "debugExecute";	//  Used by HierarchyBrowser,MethodStatusList, SourceWindow
		static constexpr const char* DeleteAdHocIndex = "mnuAdHocIndexDelete";
		static constexpr const char* DisplayVersionInfo = "displayVersionInfo";
		static constexpr const char* DropAdHocIndex = "mnuAdHocIndexDrop";
		static constexpr const char* EditCopy = "copy";	//  Used by JadeWindow
		static constexpr const char* EditCut = "cut";	//  Used by JadeWindow
		static constexpr const char* EditMacroEdit = "mnuMacroEdit";
		static constexpr const char* EditMacroLibrary = "mnuEditMacroLibrary";
		static constexpr const char* EditMacroPlay = "mnuEditMacroPlay";
		static constexpr const char* EditMacroRecord = "mnuEditMacroRecord";
		static constexpr const char* EditPaste = "paste";	//  Used by JadeWindow
		static constexpr const char* EditRedo = "redo";	//  Used by JadeWindow
		static constexpr const char* EditUndo = "undo";	//  Used by JadeWindow
		static constexpr const char* EnableInspectToolbar = "enableInspectToolbar";
		static constexpr const char* ExecuteJadeScript = "executeit";	//  Used by JadeWindow, HierarchyBrowser,PrimitiveHierarchyBrowser, MethodStatusList, SourceWindow
		static constexpr const char* ExpandSchemaHierarchy = "expandSchemaHierarchy";	//  Used by SchemaBrowser
		static constexpr const char* ExtractActiveX = "extractActiveX";	//  Used by ActiveXBrowser
		static constexpr const char* ExtractAll = "saveAll";	//  Used by HierarchyBrowser
		static constexpr const char* ExtractAllMethods = "extractMethods";	//  Used by ClassReferencesBrowser
		static constexpr const char* ExtractConsumer = "mnuExtractConsumer";
		static constexpr const char* ExtractDotNetLib = "extractDotNetLib";
		static constexpr const char* ExtractExposureList = "extractExposureList";	//  Used by ExposedListBrowser
		static constexpr const char* ExtractExternalDb = "menuExtract";	//  Used by ExposedListBrowser
		static constexpr const char* ExtractFunction = "mnuExtractFunction";
		static constexpr const char* ExtractHTMLDocument = "extractHTMLDocument";
		static constexpr const char* ExtractInterface = "extractInterface";
		static constexpr const char* ExtractMethod = "fileOutMethod";	//  Used by HierarchyBrowser
		static constexpr const char* ExtractMethod1 = "extractMethod";	//  Used by SourceWindow
		static constexpr const char* ExtractMethodView = "extractMethodView";
		static constexpr const char* ExtractMethods = "extractMethods";	//  Used by MessagesBrowser,SendersBrowser,MethodStatusList
		static constexpr const char* ExtractPackage = "extractPackage";	//  Used by PackagesBrowser
		static constexpr const char* ExtractPatch = "mnuExtractPatch";	//  Used by PatchesBrowser
		static constexpr const char* ExtractPrimType = "saveType";	//  Used by PrimitiveHierarchyBrowser
		static constexpr const char* ExtractRPSView = "menuRPSExtract";	// Used by RPS
		static constexpr const char* ExtractRelationalView = "menuODBCExtract";	//  Used by ODBCInterface
		static constexpr const char* ExtractRemapTable = "extractRemapTable";	//  Used by RemapTableBrowser
		static constexpr const char* ExtractSchema = "schemaSave";	//  Used by SchemaBrowser
		static constexpr const char* ExtractSchemaView = "extractView";	//  Used by SchemaViewBrowser
		static constexpr const char* ExtractType = "saveType";	//  Used by HierarchyBrowser
		static constexpr const char* FindBookmark = "findBookmark";
		static constexpr const char* FindClass = "findType";	//  Used by HierarchyBrowser
		static constexpr const char* FindCodePosition = "findCodePosition";	//  Used by HierarchyBrowser
		static constexpr const char* FindCollClsRefs = "collectionReferences";	//  Used by ClassReferencesBrowser
		static constexpr const char* FindConstantRefs = "constantUsages";	//  Used by HierarchyBrowser
		static constexpr const char* FindExtFunctionRefs = "references";	//  Used by ExternalFunctionsBrowser
		static constexpr const char* FindFormatRefs = "menuReferences";	//  Used by FormatBrowser
		static constexpr const char* FindGblConstant = "findGlobalConstant";
		static constexpr const char* FindGblConstsRefs = "constantReferences";	//  Used by GlobalConstantsBrowser
		static constexpr const char* FindInterface = "findInterface";
		static constexpr const char* FindMethodImpls = "implementors";	//  Used by ClassReferencesBrowser,HierarchyBrowser,SendersBrowser,MessagesBrowser, MethodStatusList, SourceWindow
		static constexpr const char* FindMethodLocalImpls = "localImplementors";	//  Used by HierarchyBrowser
		static constexpr const char* FindMethodLocalImplsRefs = "localImplementorsRefs";	//  used by HierarchyBrowser
		static constexpr const char* FindMethodLocalRefs = "localSenders";	//  Used by HierarchyBrowser
		static constexpr const char* FindMethodReferences = "references";	//  Used by HierarchyBrowser
		static constexpr const char* FindMethodRefs = "references";	//  Used by ClassReferencesBrowser,,SendersBrowser,MessagesBrowser, MethodStatusList, SourceWindow
		static constexpr const char* FindPrimType = "findType";	//  Used by PrimitiveHierarchyBrowser
		static constexpr const char* FindPrimTypeRefs = "typeReferences";	//  Used by PrimitiveHierarchyBrowser
		static constexpr const char* FindPropertyReadRefs = "readReferences";	//  Used by HierarchyBrowser
		static constexpr const char* FindPropertyRefs = "usages";	//  Used by HierarchyBrowser
		static constexpr const char* FindPropertyUpdateRefs = "updateReferences";	//  Used by HierarchyBrowser
		static constexpr const char* FindSchema = "findSchema";
		static constexpr const char* FindTransientLeaks = "mnuFindTransientLeaks";
		static constexpr const char* FindTypeRefs = "typeReferences";	//  Used by HierarchyBrowser
		static constexpr const char* FindUnusedClassEntities = "mnuFindUnusedClassEntities";
		static constexpr const char* FindUnusedLocalVars = "unusedLocalVars";	//  Used by HierarchyBrowser
		static constexpr const char* GenerateJavaScript = "generateJavaScriptConsumer";	//  Used by RIAGenerateOptions (WebServiceConsumerBrowser, ExposedListBrowser)
		static constexpr const char* GenerateTestCase = "mnuGenerateTestCase";
		static constexpr const char* GitCheckout = "gitCheckout";
		static constexpr const char* GitClone = "gitClone";
		static constexpr const char* GitCommit = "gitCommit";
		static constexpr const char* GitConfig = "gitConfig";
		static constexpr const char* GitPull = "gitPull";
		static constexpr const char* GitPush = "gitPush";
		static constexpr const char* ImportActiveXControlLib = "importControlLib";	//  Used by ActiveXBrowser
		static constexpr const char* ImportAutomationLib = "importAutomationLib";	//  Used by ActiveXBrowser
		static constexpr const char* ImportDotNetLib = "importDotNetLib";
		static constexpr const char* ImportOpenAPI = "importOpenAPI";
		static constexpr const char* InspectAllPersntInsts = "inspectAllInstances";	//  Used by HierarchyBrowser
		static constexpr const char* InspectMethod = "inspectMethod";
		static constexpr const char* InspectPersistentInsts = "inspectInstances";	//  Used by HierarchyBrowser
		static constexpr const char* InspectSchemaTransients = "inspSchemaTransients";	//  Used by SchemaBrowser
		static constexpr const char* InspectTransientInsts = "inspectTransients";	//  Used by HierarchyBrowser
		static constexpr const char* InspectorSearch = "inspectorSearch";
		static constexpr const char* InterfaceMapper = "interfaceMapper";
		static constexpr const char* LoadAdHocIndex = "mnuAdHocIndexLoad";
		static constexpr const char* LoadExternalDb = "menuLoad";	//  Used by ExposedListBrowser
		static constexpr const char* LoadRPSView = "menuRPSLoad";	// Used by RPS
		static constexpr const char* LoadRelationalView = "menuODBCLoad";	//  Used by ODBCInterface
		static constexpr const char* LoadSchema = "installSchema";	//  Used by SchemaBrowser
		static constexpr const char* Messages = "messages";	//  Used by HierarchyBrowser?
		static constexpr const char* MethodsViewMenu = "methodsViewMenu";
		static constexpr const char* MovePatchDetail = "movePatchDetail";	//  Used by PatchVersionSummary
		static constexpr const char* NewWorkspace = "newWorkspace";	//  Used by JadeWindow
		static constexpr const char* OpenSchemaView = "openView";	//  Used by SchemaViewBrowser
		static constexpr const char* OpenWorkspace = "openWorkspace";	//  Used by JadeWindow
		static constexpr const char* PackageAddClass = "packageAddClass";
		static constexpr const char* PackageAddConstant = "packageAddConstant";
		static constexpr const char* PackageAddInterface = "packageAddInterface";
		static constexpr const char* PackageAddMethod = "packageAddMethod";
		static constexpr const char* PackageAddProperty = "packageAddProperty";
		static constexpr const char* PackageChangeProperty = "packageChangeProperty";
		static constexpr const char* PackageChangeType = "packageChangeType";
		static constexpr const char* PackageFindType = "packageFindType";
		static constexpr const char* PackageRemoveConstant = "packageRemoveConstant";
		static constexpr const char* PackageRemoveMethod = "packageRemoveMethod";
		static constexpr const char* PackageRemoveProperty = "packageRemoveProperty";
		static constexpr const char* PackageRemoveType = "packageRemoveType";
		static constexpr const char* PrintExternalDb = "menuPrint";	//  Used by ExposedListBrowser
		static constexpr const char* PrintMethod = "printMethod";	//  Used by HierarchyBrowser
		static constexpr const char* PrintRPSView = "menuRPSPrint";	// Used by RPS
		static constexpr const char* PrintRelationalView = "menuODBCPrint";	//  Used by ODBCInterface
		static constexpr const char* PrintSchema = "printSchema";	//  Used by SchemaBrowser
		static constexpr const char* PrintSelected = "printSelected";	//  Used by JadeWindow,GlobalConstantsBrowser, PatchVersionSummary
		static constexpr const char* PrintSetup = "printSetup";	//  Used by JadeWindow
		static constexpr const char* RecreatePatchVersion = "menuRecreate";	// Used by JadeInstallationPrefDialog
		static constexpr const char* RefreshPatchList = "mnuRefreshPatchList";	//  Used by ?PatchesBrowser
		static constexpr const char* RegisterActiveXServer = "registerServer";	//  Used by ActiveXBrowser
		static constexpr const char* ReloadHTMLDocument = "reloadHTMLDocument";
		static constexpr const char* RemoveActiveX = "removeActiveX";	//  Used by ActiveXBrowser
		static constexpr const char* RemoveAllMethodSources = "mnuAdminRemove";
		static constexpr const char* RemoveApplication = "removeApplication";	//  Used by ApplicationBrowser
		static constexpr const char* RemoveBreakpoint = "removeBreakpoint";	//  Used by DebuggerBreakPoints
		static constexpr const char* RemoveConstant = "removeConstant";	//  Used by HierarchyBrowser
		static constexpr const char* RemoveConsumer = "mnuRemoveConsumer";
		static constexpr const char* RemoveDotNetLib = "removeDotNetLib";
		static constexpr const char* RemoveExposureList = "removeExposedList";	//  Used by ExposedListBrowser
		static constexpr const char* RemoveExtFunction = "removeFunction";	//  Used by ExternalFunctionsBrowser
		static constexpr const char* RemoveExtFunctionLibrary = "removeFunctionLibrary";	//  Used by ExternalFunctionsBrowser
		static constexpr const char* RemoveExternalDb = "menuRemove";	//  Used by ExposedListBrowser
		static constexpr const char* RemoveForm = "deleteForm";
		static constexpr const char* RemoveFormat = "menuRemoveFormat";	//  Used by FormatBrowser
		static constexpr const char* RemoveGblConstant = "removeGlobalConstant";	//  Used by GlobalConstantsBrowser
		static constexpr const char* RemoveGblConstsCategory = "removeCategory";	//  Used by GlobalConstantsBrowser
		static constexpr const char* RemoveHTMLDocument = "removeHTMLDocument";	//  Used by HTMLBrowser
		static constexpr const char* RemoveInterface = "mnuRemoveInterface";
		static constexpr const char* RemoveLibrary = "removeLibrary";	//  Used by LibraryBrowser
		static constexpr const char* RemoveMapFile = "removeFile";	//  Used by ClassMapsBrowser
		static constexpr const char* RemoveMethod = "removeMethod";	//  Used by HierarchyBrowser, SendersBrowser, MethodStatusLis
		static constexpr const char* RemoveMethodFromList = "removeFromList";	//  Used by SendersBrowser
		static constexpr const char* RemoveMethodView = "removeMethodView";
		static constexpr const char* RemoveOpenAPI = "removeOpenAPI";
		static constexpr const char* RemovePackage = "removePackage";	//  Used by PackagesBrowser
		static constexpr const char* RemovePackageConstant = "removeConstant";
		static constexpr const char* RemovePackageMethod = "removeMethod";
		static constexpr const char* RemovePackageProperty = "removeVariable";
		static constexpr const char* RemovePackageType = "removeType";
		static constexpr const char* RemovePrimType = "removeType";	//  Used by PrimitiveHierarchyBrowser
		static constexpr const char* RemoveProperty = "removeVariable";	//  Used by HierarchyBrowser
		static constexpr const char* RemoveRPSView = "menuRPSRemove";	// Used by RPS
		static constexpr const char* RemoveRelationalView = "menuODBCRemove";	//  Used by ODBCInterface
		static constexpr const char* RemoveRemapTable = "removeRemap";	//  Used by RemapTableBrowser
		static constexpr const char* RemoveSchema = "removeSchema";	//  Used by SchemaBrowser
		static constexpr const char* RemoveSchemaView = "removeSchemaView";	//  Used by SchemaViewBrowser
		static constexpr const char* RemoveType = "removeType";	//  Used by HierarchyBrowser
		static constexpr const char* RemoveUnusedLocalVariables = "mnuNRemoveUnusedLocalVars";
		static constexpr const char* RenameMethod = "renameMethod";	//  Used by HierarchyBrowser
		static constexpr const char* ReopenPatch = "mnuReopenPatch";	//  Used by PatchesBrowser
		static constexpr const char* ReorgAbort = "reorgAbort";
		static constexpr const char* ReorgInstances = "reorgOption";	//  Used by SchemaBrowser
		static constexpr const char* ResetUserPreferences = "menuAdminReset";
		static constexpr const char* RestSecurityOptions = "applyRestSecurity";
		static constexpr const char* RpsCreateJcf = "mnuRpsCreateJcf";
		static constexpr const char* RunAdHocIndexController = "mnuAdHocIndexRun";
		static constexpr const char* RunApplication = "runApplication";	//  Used by ApplicationBrowser, JadeWindow
		static constexpr const char* Save = "save";
		static constexpr const char* SaveAdHocIndex = "mnuAdHocIndexSave";
		static constexpr const char* SaveAs = "saveAs";
		static constexpr const char* SaveHTML = "mnuSaveHTML";
		static constexpr const char* SaveMethod = "saveMethod";	//  Used by HierarchyBrowser
		static constexpr const char* SaveMethodAs = "saveMethodAs";	//  Used by HierarchyBrowser
		static constexpr const char* SchemaEntityBrowse = "browseSchemaEntityViews";
		static constexpr const char* SchemaEntityRecent = "schemaEntityRecent";
		static constexpr const char* SchemaEntityView = "mnuMethodsView";
		static constexpr const char* SchemaEntityViewNew = "newSchemaEntityView";
		static constexpr const char* SearchInstances = "searchInstances";
		static constexpr const char* SearchListedMethods = "searchListedMethods";
		static constexpr const char* SetApplication = "setApplication";	//  Used by ApplicationBrowser
		static constexpr const char* SetBookmark = "setBookmark";
		static constexpr const char* SetBreakpoint = "toggleBreakpoint";	//  Used by HierarchyBrowser
		static constexpr const char* SetConstantText = "constantText";	//  Used by HierarchyBrowser
		static constexpr const char* SetConstantsSortOrder = "sortedByValue";	//  Used by HierarchyBrowser
		static constexpr const char* SetDelta = "setDelta";	//  Used by DeltaBrowser
		static constexpr const char* SetExtFunctionText = "functionText";	//  Used by ExternalFunctionsBrowser
		static constexpr const char* SetGblConstsSortOrder = "sortedByValue";	//  Used by GlobalConstantsBrowser
		static constexpr const char* SetGlobalConstantText = "updateComments";
		static constexpr const char* SetHTMLText = "htmlText";
		static constexpr const char* SetMapFileText = "mapFileText";	//  Used by ClassMapsBrowser
		static constexpr const char* SetMethodText = "methodText";	//  Used by HierarchyBrowser
		static constexpr const char* SetPatch = "mnuSetPatch";	//  Used by PatchesBrowser
		static constexpr const char* SetPatchNumber = "mnuSetPatchNumber";
		static constexpr const char* SetPatchVersionEnabled = "menuEnable";
		static constexpr const char* SetPatchVersionNumber = "menuSetNumber";	//  Used by JadeInstallationPrefDialog
		static constexpr const char* SetPropertyText = "variableText";	//  Used by HierarchyBrowser
		static constexpr const char* SetRemapTable = "setRemap";	//  Used by RemapTableBrowser
		static constexpr const char* SetSchema = "selectSchema";
		static constexpr const char* SetSchemaText = "textSchema";	//  Used by SchemaBrowser
		static constexpr const char* SetSchemaView = "setView";	//  Used by SchemaViewBrowser
		static constexpr const char* SetSchemaViewText = "schemaViewText";	//  Used by SchemaViewBrowser
		static constexpr const char* SetText = "setText";
		static constexpr const char* SetTypeText = "typeText";	//  Used by HierarchyBrowser
		static constexpr const char* SetTypesSortOrder = "sortedOrder";	//  Used by HierarchyBrowser
		static constexpr const char* SetUserPreferences = "menuFileSaveAndExit";
		static constexpr const char* ShowAllMethodView = "showAllMethodViews";
		static constexpr const char* ShowAllPatchedMethods = "mnuShowAllMethods";
		static constexpr const char* ShowBackupDb = "backupDatabase";
		static constexpr const char* ShowCompositeVersion = "showCompositeVersion";
		static constexpr const char* ShowCurrentVersion = "showCurrentVersion";
		static constexpr const char* ShowEditForm = "viewForm";	//  Used by HierarchyBrowser
		static constexpr const char* ShowEditForm2 = "editForm";
		static constexpr const char* ShowExternalDb = "menuView";	//  Used by ExposedListBrowser
		static constexpr const char* ShowExtractPatch = "menuExtractPatch";
		static constexpr const char* ShowFormWizard = "formWizard";	//  Used by JadeWindow
		static constexpr const char* ShowGitCheckoutForm = "gitCheckoutForm";
		static constexpr const char* ShowGitCloneForm = "gitCloneForm";
		static constexpr const char* ShowGitCommitForm = "gitCommitForm";
		static constexpr const char* ShowGitConfigForm = "gitConfigForm";
		static constexpr const char* ShowGitPullForm = "gitPullForm";
		static constexpr const char* ShowGitPushForm = "gitPushForm";
		static constexpr const char* ShowGlobalFindReplace = "globalSearch";	//  Used by JadeWindow
		static constexpr const char* ShowImportedClasses = "showImportedClasses";
		static constexpr const char* ShowImportedInterfaces = "showImportedInterfaces";
		static constexpr const char* ShowInterfaceMappings = "showInterfaceMappings";
		static constexpr const char* ShowLatestVersion = "showLatestVersion";
		static constexpr const char* ShowLocalFindReplace = "findReplace";	//  Used by JadeWindow
		static constexpr const char* ShowMethodHistory = "showHistory";	//  Used by HierarchyBrowser
		static constexpr const char* ShowMethodsInDelta = "viewMethods";	//  Used by DeltaBrowser
		static constexpr const char* ShowMonitor = "monitor";	//  Used by JadeWindow
		static constexpr const char* ShowPainter = "painter";	//  Used by JadeWindow
		static constexpr const char* ShowPatchSummary = "mnuDispPatchSummary";	//  Used by ?PatchesBrowser
		static constexpr const char* ShowPatchedMethods = "mnuShowMethods";
		static constexpr const char* ShowPromoteConstant = "promoteConstant";	//  Used by HierarchyBrowser
		static constexpr const char* ShowProtected = "showProtected";
		static constexpr const char* ShowPublic = "showPublic";
		static constexpr const char* ShowQuickNavigation = "quickNavigation";	//  Used by JadeWindow
		static constexpr const char* ShowReadOnly = "showReadOnly";
		static constexpr const char* ShowRelationDiagram = "graphicalReferences";	//  Used by HierarchyBrowser
		static constexpr const char* ShowRelatnDiagramOptions = "graphViewOptions";	//  Used by GraphicalReferences
		static constexpr const char* ShowRemapClass = "remapClass";	//  Used by HierarchyBrowser
		static constexpr const char* ShowRemovePatchHistory = "menuRemoveHistory";
		static constexpr const char* ShowTypeHistory = "showTypeHistory";	//  Used by HierarchyBrowser
		static constexpr const char* ShowUserPreferences = "userPreferences";	//  Used by JadeWindow
		static constexpr const char* ShowVersionInfoObject = "showVersionInfoObject";
		static constexpr const char* ShowWSDLMappings = "mnuShowWSDLMappings";
		static constexpr const char* ShowXMIImport = "mnuXMIImport";
		static constexpr const char* UncheckOutAllInDelta = "uncheckoutAll";	//  Used by DeltaBrowser
		static constexpr const char* UncheckOutMethod = "uncheckoutMethod";	//  Used by HierarchyBrowser,SendersBrowser
		static constexpr const char* Unknown = "Should ever see";
		static constexpr const char* UnregisterActiveXServer = "unregisterServer";	//  Used by ActiveXBrowser
		static constexpr const char* UnsetDelta = "unsetDelta";	//  Used by DeltaBrowser
		static constexpr const char* UnsetPatch = "mnuUnsetPatch";	// Used by Patches Browser
		static constexpr const char* UnsetRemapTable = "unsetRemap";	//  Used by RemapTableBrowser
		static constexpr const char* UserPreferences_Logoff = "menuFileSaveAndLogoff";
		static constexpr const char* ValidateSchema = "validateSchema";	//  Used by SchemaBrowser
		static constexpr const char* ViewAllPatches = "mnuAllPatches";	//  Used by PatchesBrowser
		static constexpr const char* ViewBubbleHelp = "showBubbleHelp";	//  Used by HierarchyBrowser
		static constexpr const char* ViewClass = "selectClass";
		static constexpr const char* ViewClosedPatches = "mnuClosedPatches";	//  Used by PatchesBrowser
		static constexpr const char* ViewCompiledMethods = "compiledMethods";	//  Used by MethodStatusList
		static constexpr const char* ViewCurrentUserPatches = "mnuCurrentUserPatches";	// used by Patches Browser
		static constexpr const char* ViewExpandedHTree = "expandHierarchy";	//  Used by HierarchyBrowser
		static constexpr const char* ViewInherited = "showInherited";	//  Used by HierarchyBrowser
		static constexpr const char* ViewLibrarySuperscms = "librarySuperschemas";	//  Used by LibraryBrowser
		static constexpr const char* ViewMethodSource = "viewMethod";
		static constexpr const char* ViewMethodsInError = "inErrorMethods";	//  Used by MethodStatusList
		static constexpr const char* ViewMethodsPane = "methodsWindow";	//  Used by HierarchyBrowser
		static constexpr const char* ViewOpenPatches = "mnuOpenPatches";	//  Used by PatchesBrowser
		static constexpr const char* ViewPrimType = "selectClass";
		static constexpr const char* ViewPropertiesPane = "propertiesWindow";	//  Used by HierarchyBrowser
		static constexpr const char* ViewResetRoot = "resetRoot";	//  Used by HierarchyBrowser
		static constexpr const char* ViewSetRoot = "setRoot";	//  Used by HierarchyBrowser
		static constexpr const char* ViewSuperschemas = "superschemas";	//  Used by PrimitiveHierarchyBrowser, ExternalFunctionsBrowser
		static constexpr const char* ViewUncompiledMethods = "uncompiledMethods";	//  Used by MethodStatusList
		static constexpr const char* WebServicesMethodOptions = "webServicesMethodOptions";
		static constexpr const char* WebServicesOptions = "mnuWebServicesOptions";
		static constexpr const char* ZoomInRelationDiagram = "zoomIn";	//  Used by GraphicalReferences
		static constexpr const char* ZoomOutRelationDiagram = "zoomOut";	//  Used by GraphicalReferences

	protected:
		Function() {};
		Function(std::initializer_list<const char*> taskNames);
	};

	class Session;

	// Abstract function dependent on session being initialized
	class SessionFunction : public Function
	{
	public:
		using Function::Function;

	protected:
		SessionFunction(std::initializer_list<const char*> taskNames, bool administration) : Function(taskNames), administration(administration) {}

		virtual bool execute(Session* session, const std::string& entityName) const = 0;

	private:
		bool administration = false;

		bool execute(const std::string& entityName) const override final;
	};
}