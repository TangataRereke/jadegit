#include "Function.h"

namespace JadeGit::Development
{
	// Sign-on functions (those that don't require specific handling, except to complete sign-on process)
	class SignOnFunction : public SessionFunction
	{
	public:
		SignOnFunction(std::initializer_list<const char*> taskNames, bool administration) : SessionFunction(taskNames, administration) {}

	protected:
		bool execute(Session* session, const std::string& entityName) const override
		{
			// Sign-on completed
			return true;
		}
	};

	static SignOnFunction signon({ Function::BrowseClasses, Function::ShowPainter }, false);
	static SignOnFunction signonAdmin({ Function::Administration }, true);
}