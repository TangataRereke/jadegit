#pragma once
#include "DeploymentBuilder.h"
#include <jadegit/vfs/FileSystem.h>
#include <xml/XMLPrinter.h>

namespace JadeGit::Deploy
{
	class XMLDeploymentBuilder : public DeploymentBuilder
	{
	public:
		XMLDeploymentBuilder(const FileSystem& fs);

		void start(std::string origin) final;
		void finish() final;

		void startStage(std::string currentCommit, std::string latestCommit) final;
		void finishStage() final;
		
		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) final;

		void addScript(const Build::Script& script) final;

		void Reorg() final;

	private:
		const FileSystem& fs;
		XMLPrinter printer;
		int stage = 0;
		int stages = 0;
		int files = 0;
		const char* loadStyle = nullptr;
		bool needsReorg = false;
		bool reorging = false;

		std::unique_ptr<std::ostream> addFile(const std::string& schema, const char* extension, const char* type);
		void changeLoadStyle(bool latestVersion);

		template <class T>
		void pushAttribute(const char* name, T value) 
		{
			printer.PushAttribute(name, value);
		}

		void pushAttribute(const char* name, const std::string& value)
		{
			printer.PushAttribute(name, value.c_str());
		}

		template <class T>
		void pushAttributeIfNotEmpty(const char* name, T value)
		{
			if (value != T())
				pushAttribute(name, value);
		}
	};
}