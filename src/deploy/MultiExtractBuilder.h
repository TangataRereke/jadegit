#pragma once
#include "DeploymentBuilder.h"
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit::Deploy
{
	class MultiExtractBuilder : public DeploymentBuilder
	{
	public:
		MultiExtractBuilder(const FileSystem& fs, const std::string& name);
		~MultiExtractBuilder();

	protected:
		const FileSystem& fs;
		std::unique_ptr<std::ostream> mul;

		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) override;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) override;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) override;

	private:
		std::string lastschema;
		int lastpos = 0;
		int counter = 0;

		std::unique_ptr<std::ostream> AddFile(const std::string& schema, const char* extension, int pos);

		void addScript(const Build::Script& script) final;
	};
}