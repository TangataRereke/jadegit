#include "Director.h"
#include "DeploymentBuilder.h"
#include <jadegit/build/Director.h>
#include <vfs/GitDiffSource.h>
#include <vfs/GitFileSystem.h>

using namespace std;

namespace JadeGit::Deploy
{
	class Director::Impl
	{
	public:
		Impl(git_repository& repo, DeploymentBuilder& builder, IProgress* progress) : repo(repo), builder(builder), progress(progress)
		{
		}

		void build(string revision) const
		{
			git_revspec revspec;
			git_throw(git_revparse(&revspec, &repo, revision.c_str()));

			unique_ptr<git_commit> from;
			unique_ptr<git_commit> to;

			switch (revspec.flags)
			{
			case git_revspec_t::GIT_REVSPEC_MERGE_BASE:
				throw jadegit_exception("Revision string may be a range, not a merge base");

			case git_revspec_t::GIT_REVSPEC_RANGE:
				git_throw(git_object_peel((git_object**)(static_cast<git_commit**>(git_ptr(from))), revspec.from, git_object_t::GIT_OBJECT_COMMIT));
				git_object_free(revspec.from);
				git_throw(git_object_peel((git_object**)(static_cast<git_commit**>(git_ptr(to))), revspec.to, git_object_t::GIT_OBJECT_COMMIT));
				git_object_free(revspec.to);
				break;

			case git_revspec_t::GIT_REVSPEC_SINGLE:
				git_throw(git_object_peel((git_object**)(static_cast<git_commit**>(git_ptr(to))), revspec.from, git_object_t::GIT_OBJECT_COMMIT));
				git_object_free(revspec.from);
				break;
			}

			build(move(from), move(to));
		}

		void build(string from, string to) const
		{
			build(lookup(from), lookup(to));
		}

		void build(unique_ptr<git_commit> from, unique_ptr<git_commit> to) const
		{
			// lookup origin
			unique_ptr<git_remote> origin;
			git_throw(git_remote_lookup(git_ptr(origin), &repo, "origin"));

			// start building deployment
			this->builder.start(git_remote_url(origin.get()));

			// build stage (currently just one)
			buildStage(move(from), move(to));

			// finish building deployment
			this->builder.finish();
		}

	protected:
		string id(const unique_ptr<git_commit>& commit) const
		{
			return commit ? string(git_oid_tostr_s(git_commit_id(commit.get()))) : string();
		}

		unique_ptr<git_commit> lookup(const string& id) const
		{
			if (id.empty())
				return nullptr;

			git_oid oid = { 0 };
			git_throw(git_oid_fromstrp(&oid, id.c_str()));

			unique_ptr<git_commit> commit;
			git_throw(git_commit_lookup(git_ptr(commit), &repo, &oid));
			return commit;
		}

		void buildStage(unique_ptr<git_commit> from, unique_ptr<git_commit> to) const
		{
			// start building stage
			this->builder.startStage(id(from), id(to));

			// setup current file system
			unique_ptr<GitFileSystem> current;
			if (from)
			{
				current = make_unique<GitFileSystem>(&repo, false);
				current->add(move(from));
			}

			// setup target file system
			unique_ptr<GitFileSystem> target;
			if (to)
			{
				target = make_unique<GitFileSystem>(&repo, false);
				target->add(move(to));
			}

			// setup build source
			GitDiffSource source(&repo, current.get(), target.get());

			// build stage
			Build::Director director(source, this->progress);
			director.Build(this->builder);

			// finish building stage
			this->builder.finishStage();
		}

	private:
		git_repository& repo;
		DeploymentBuilder& builder;
		IProgress* progress = nullptr;
	};

	Director::Director(git_repository& repo, DeploymentBuilder& builder, IProgress* progress)
	{
		impl = make_unique<Impl>(repo, builder, progress);
	}

	Director::~Director() = default;

	void Director::build(string revision) const
	{
		impl->build(revision);
	}

	void Director::build(string from, string to) const
	{
		impl->build(from, to);
	}
}