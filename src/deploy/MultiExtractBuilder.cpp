#include "MultiExtractBuilder.h"
#include <jadegit/vfs/File.h>
#include <jadegit/Exception.h>
#include <sstream>

namespace JadeGit::Deploy
{
	MultiExtractBuilder::MultiExtractBuilder(const FileSystem& fs, const std::string& name) : fs(fs)
	{
		// Setup mul file
		mul = fs.open(name + ".mul").createOutputStream();
		*mul << "#MULTIPLE_SCHEMA_EXTRACT";
	}

	MultiExtractBuilder::~MultiExtractBuilder()
	{
		if(mul)
			*mul << std::endl << std::flush;
	}

	std::unique_ptr<std::ostream> MultiExtractBuilder::AddCommandFile(bool latestVersion)
	{
		throw jadegit_unsupported_feature("Loading command files during MUL deployment");

		// TODO: Enable behind experimental feature flag in case JADE implements support in future?
	//	return AddFile("Commands", "jcf", 1);
	}

	std::unique_ptr<std::ostream> MultiExtractBuilder::AddFile(const std::string& schema, const char* extension, int pos)
	{
		// Determine if current group of files in MUL can be appended or new group needed
		if (!lastschema.empty() && lastschema == schema && lastpos < pos)
			*mul << " ";
		else
		{
			*mul << std::endl;
			counter++;
		}

		// Update last schema/pos
		lastschema = schema;
		lastpos = pos;

		// Derive filename with leading index
		std::stringstream filename;
		filename << std::setfill('0') << std::setw(2) << counter << "-" << schema << "." << extension;

		// Open file
		File file = fs.open(filename.str());

		// Check file doesn't already exist
		if(file.exists())
			throw jadegit_exception("File already exists (" + filename.str() + ")");

		// Append filename to MUL
		*mul << filename.str();

		// Setup output stream
		return file.createOutputStream();
	}

	std::unique_ptr<std::ostream> MultiExtractBuilder::AddSchemaFile(const std::string& schema, bool latestVersion)
	{
		return AddFile(schema, "scm", 1);
	}

	std::unique_ptr<std::ostream> MultiExtractBuilder::AddSchemaDataFile(const std::string& schema, bool latestVersion)
	{
		return AddFile(schema, "ddx", 2);
	}

	void MultiExtractBuilder::addScript(const Build::Script& script)
	{
		throw jadegit_unsupported_feature("Running scripts during MUL deployment");
	}
}