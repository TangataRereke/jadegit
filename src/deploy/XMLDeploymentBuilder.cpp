#include "XMLDeploymentBuilder.h"
#include <jadegit/vfs/File.h>
#include <jadegit/Exception.h>
#include <assert.h>

using namespace std;

namespace JadeGit::Deploy
{
	XMLDeploymentBuilder::XMLDeploymentBuilder(const FileSystem& fs) : fs(fs)
	{
	}

	void XMLDeploymentBuilder::start(string origin)
	{
		stage = 0;
		stages = 0;
		files = 0;

		printer.ClearBuffer();
		printer.OpenElement("Deployment");

		if (!origin.empty())
			printer.PushAttribute("origin", origin.c_str());
	}

	void XMLDeploymentBuilder::finish()
	{
		assert(!stage);

		printer.CloseElement();

		auto file = fs.open("deployment.xml").createOutputStream();
		*file << printer.CStr() << flush;
	}

	void XMLDeploymentBuilder::startStage(string currentCommit, string latestCommit)
	{
		assert(!stage);

		stage = ++stages;
		printer.OpenElement("Stage");

		if (!currentCommit.empty())
			printer.PushAttribute("currentCommit", currentCommit.c_str());

		if (!latestCommit.empty())
			printer.PushAttribute("latestCommit", latestCommit.c_str());
	}

	void XMLDeploymentBuilder::finishStage()
	{
		assert(stage);

		if (needsReorg)
			Reorg();

		printer.CloseElement();
		stage = 0;
	}

	unique_ptr<ostream> XMLDeploymentBuilder::AddCommandFile(bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		return addFile("Commands", "jcf", "CommandFile");
	}

	unique_ptr<ostream> XMLDeploymentBuilder::AddSchemaFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		return addFile(schema, "scm", "SchemaFile");
	}

	unique_ptr<ostream> XMLDeploymentBuilder::AddSchemaDataFile(const string& schema, bool latestVersion)
	{
		// Change load style
		changeLoadStyle(latestVersion);

		return addFile(schema, "ddx", "SchemaDataFile");
	}

	void XMLDeploymentBuilder::addScript(const Build::Script& script)
	{
		if (needsReorg)
			Reorg();

		// Add script to deployment
		printer.OpenElement("Script");
		pushAttributeIfNotEmpty("schema", script.schema);
		pushAttributeIfNotEmpty("app", script.app);
		pushAttributeIfNotEmpty("executeSchema", script.executeSchema);
		pushAttributeIfNotEmpty("executeClass", script.executeClass);
		pushAttributeIfNotEmpty("executeMethod", script.executeMethod);
		pushAttributeIfNotEmpty("executeParam", script.executeParam);
		pushAttributeIfNotEmpty("executeTransient", script.executeTransient);
		pushAttributeIfNotEmpty("executeTypeMethod", script.executeTypeMethod);
		printer.CloseElement();
	}

	unique_ptr<ostream> XMLDeploymentBuilder::addFile(const string& schema, const char* extension, const char* type)
	{
		assert(stage);

		// Increment file counter
		files++;

		// Derive filename
		stringstream filename;
		filename << "stage-" << setfill('0') << setw(2) << stage << "/" << setfill('0') << setw(2) << files << "-" << schema << "." << extension;

		// Open file
		File file = fs.open(filename.str());

		// Check file doesn't already exist
		if (file.exists())
			throw jadegit_exception("File already exists (" + filename.str() + ")");

		// Add file to deployment
		printer.OpenElement(type);
		pushAttribute("path", filename.str());
		pushAttribute("loadStyle", loadStyle);
		printer.CloseElement();

		// Setup output stream
		return file.createOutputStream();
	}

	void XMLDeploymentBuilder::Reorg()
	{
		// Suppress duplicate re-orgs
		if (reorging)
			return;

		// Add re-org to deployment
		printer.OpenElement("Reorg");
		printer.CloseElement();

		// Set flag indicating we've just added re-org
		reorging = true;

		// Reset flag indicating reorg is needed
		needsReorg = false;
	}

	void XMLDeploymentBuilder::changeLoadStyle(bool latestVersion)
	{
		// Update load style
		loadStyle = latestVersion ? "latestSchemaVersion" : "currentSchemaVersion";

		// Reset flag indicating re-org has just been added
		reorging = false;

		// Set flag indicating reorg is needed
		if (latestVersion)
			needsReorg = true;

		// Perform re-org before loading into current schema version
		else if (needsReorg)
			Reorg();
	}
}