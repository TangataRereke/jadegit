#pragma once
#include <build/Builder.h>

namespace JadeGit::Deploy
{
	class DeploymentBuilder : public Build::Builder
	{
	public:
		virtual void start(std::string origin) {};
		virtual void finish() {};

		virtual void startStage(std::string currentCommit, std::string latestCommit) {};
		virtual void finishStage() {};
	};
}