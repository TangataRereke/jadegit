## 0.10.0 (2022-08-23)
### Added
- Support for imported classes with local features.
- Console command for cloning repository.
- Support for .jadegit configuration file using toml++.
- Console deploy manifest commands to store/retrieve origin/commit during deployment.

### Changed
- Prevent redundant subschema class copy extracts.
- Use 'jox' file extension for xml format.
- Use subfolders for schemas & forms, and lowercase all entity subfolder names.
- Use system configuration file.
- Removed option to set target JADE version during build (now defined by config).

### Fixed
- Renamed/split library into jadegitdev/jadegitscm so entry points are found by JADE when commands invoked via console.
- Derive logs directory from executable path (rather than relative to current working directory).

## 0.9.0 (2022-04-09)
### Added
- Support for HTML documents.
- Support for dynamic properties.
- Support for JADE 2020 SP1.

### Changed
- Refactored inverse maintenance within proxy data model using metadata.
- Use 'main' as default branch name.
- Resolve current session using process instead of username.
- Refactored id allocation & storage using stduuid.

### Fixed
- Use integer input type for helpContextId properties during extract.
- Ignore interface references during extract.
- Handle resolving inherited/cloned locale entities.
- Handle deleting subschema subclasses before base class.
- Define static schema properties for subschema member key dictionary usage.
- Ignore duplicated system files during extract.
- Confirm order during initial extract when objects may be created out of order.
- Mark entities as inferred when they may not be extracted.
- Reset data items when ignored during extract.
- Ignore attribute precision unless decimal type during extract.
- Clean up non-static schemas when unloading during save.
- Handle encoding accent characters within documentation text.
- Handle defining custom event methods before any element of dependent classes.
- Handle resolving inherited applications.
- Handle loading circular package references.

### Removed
- Support for JADE 2018.
- Support for JADE 2020.

## 0.8.0 (2021-04-26)
### Added
- Support for JADE 2020.
- Support for unicode environments.
- Ability for console to open JADE database connection.
- Console commands to open bare repository, switch branch, extract schemas & commit.
- CardSchema definition (now excluded from source control by default).

### Changed
- Refactored console to support shell mode for series of commands while connected to JADE database.
- Pushing new branches will now default to only or oldest remote when 'origin' doesn't exist.
- Embedded database definition within schema file to support names which don't match default.

### Fixed
- ActiveXControl properties which cannot be accessed without creating ActiveX object will now be skipped during extract.
- Allow text boxes on remote dialog to scroll horizontally.
- Support for adding/deleting remotes.
- Support for deleting translatable strings.

# 0.7.0 (2020-11-05)
### Added
- Support for showing progress.
- Support for ActiveX libraries.
- Support for .NET libraries.
- Support for type methods.

### Changed
- Evaluate prior/next version when schema selected.
- Install process to allow re-orgs when required due to control changes.

### Fixed
- Handling of schema deregistration and resetting branch state when unloading repositories.
- Handling of form & control renames.
- Handling of form deletions.
- Handling of copying forms.
- Support for moving classes (better handling for new classes being inserted above existing).
- Staging renames with updates to related entities.
- Handling of super-interface mapping dependencies.

## 0.6.0 (2020-06-07)
### Added
- Support for relational views.
- Support for tracking changes to latest schema version.

### Changed
- Updated internal deployments to use JadeSchemaLoader & JadeReorgApp.
- Refactored build process to generate DDX data files.
- Improved support for locales (changing schema default & converting base locale to/from clone).
- Simplified DbClassMap format (now written as single element within class file).
- Refactored authentication using presentation client library to retrieve git credentials (jadegitauth).
- Refactored RootSchema metadata generation to simplify future updates.

### Fixed
- Handling of duplicates allowed option for collection classes.
- Handling of collection class option to map all instances to selected map file.
- Handling of dependency between form control methods and custom control event methods.
- Handling of cross-dependencies between global constants spanning multiple categories.

### Removed
- Support for JADE 2016.

## 0.5.0 (2020-02-05)
### Added
- Support for C# exposures.
- Support for web services.
- Support for schema views (local use only).

### Changed
- Refactored how references are stored to support branch folders.
- Refactored schema file generation to include modified timestamps.
- Refactored console build to load data from git repository.
- Removed use of default 'JADE' root folder.

### Fixed
- Support for cloning repositories from local filesystem.
- Support for cloned locale definitions.
- Support for references to imported types & imported interface mappings.
- Removal of local branches (config backend refactored to support removals).
- Schema deletion now handled via separate command files (avoids problems when internal deployment is being restarted).

## 0.4.0 (2019-11-02)
### Added
- Ability to restart & abort internal deployments (used to load/unload/reset branches & repositories).
- Ability to stage & commit changes selectively.
- Ability to reset index with changes being kept (unstage all).

### Changed
- Refactored internal deployment build process to store files persistently.
- Schema versioning & re-org functions are now allowed, provided there is no internal deployment outstanding.
- Building internal deployments are now prevented when there's an outstanding re-org.
- Major entity file path now used to resolve parent during load, removing need to store qualified name within file.

## 0.3.0 (2019-07-04)
### Added
- Support for adding existing schemas to source control.
- Support for custom control subclasses.
- Support for building schema updates using command line utility with previous/latest source folders.
- Support for imported packages.
- Support for locale formats.
- Implemented branch reset functions to discard changes.
- Implemented repository reset function to unload all branches with complete schema reload.

### Changed
- Deep extract now performed when new entity is added, removing the need to track child changes.
- Consolidated into one shared library and/or command line executable (jadegit.dll & jadegit.exe).
- Suppress extract of shallow/proxy entities which cannot be loaded (i.e. subschema copies of an interface).

### Fixed
- Support for external methods.
- Support for removing class interface mappings.
- Support for libraries & external functions.

## 0.2.0 (2019-04-16)
### Added
- Support for JADE 2018.
- Support for translatable strings.
- Support for schema/repository associations.
- Support for unloading repositories.
- Support for removing schemas.
- Support for renaming schema entities.
- Support for conditional collections.

### Changed
- Improved extract process using data mappings which can be explicitly defined or automatically generated as required.
- Improved support for subschemas.
- Improved support for forms (renames, event methods, menu items).
- Improved support for adding/removing inverses between references.
- Jade-Git Explorer now uses notifications to refresh/show changes (possibly caused by another user).

### Fixed
- Replaced use of paramSetString with paramSetCString to explicitly initialize DskParam format.
- Suppress 'unknown property' errors with warnings being logged instead (identified during automatic data mapping).
- Suppress Jade-Git Explorer when starting IDE in administration mode.

## 0.1.0 (2018-12-15)
### Added
- This CHANGELOG file to document notable changes going forward.
- jadegit UI explorer, which runs alongside the JADE IDE (refer to README for configuration steps), provides:
  - Ability to create/clone repositories.
  - Ability to manage branches, with push & pull support for remote repositories.
  - Ability to track changes to basic schema entities (supporting add/update/delete operations).
  - Ability to commit changes, during which affected entities are re-extracted from JADE and converted to source control friendly format.
- Ability to build schemas from jadegit repository using command line utility (independently of JADE IDE).
